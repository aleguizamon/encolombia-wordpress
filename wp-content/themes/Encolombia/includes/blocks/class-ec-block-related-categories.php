<?php
class EC_Block_Related_Categories {

    use EC_Base_Category;

    private $data = null;

    /**
     * EC_Block_Related_Categories constructor.
     * @param WP_Term $term
     */
    public function __construct(WP_Term $term)
    {
        $this->require_level = false;
        $this->initialize($term);
        $this->load_data();
    }

    public function render() {
        $buffy = '';
        if($this->data){
            $buffy .= '<div class="enc_block_related_pubs">';
            $buffy .= '<h3 class="enc-block-title-1">';
            $buffy .= 'PUBLICACIONES RELACIONADAS';
            $buffy .= '<span class="titledot"></span><span class="titleline"></span>';
            $buffy .= '</h3>';
            $buffy .= '<div class="enc-row">';

            foreach ($this->data as $cat):
                //$category_id = get_cat_ID( 'Category Name' );
                $base_url = get_category_link( $cat );

                $buffy .= '<div class="col-lg-2 col-md-3 col-sm-6">';
                $buffy .= '<div class="pub-item">';
                $buffy .= '<div class="pub-thumb">';
                $buffy .= '<a class="item-image" href="'.$base_url.'" title="'. $cat->name .'">';
                $buffy .= '<img src="'.$this->get_image_url($cat->term_id, '180x225').'" alt="'.$cat->name.'"  title="'.$cat->name.'"  />';
                $buffy .= '</a>';
                $buffy .= '</div>';
                $buffy .= '<div class="pub-content">';
                $buffy .= '<h3 class="pub-title"><a href="'.$base_url.'">'.$cat->name.'</a></h3>';
                $buffy .= '</div>';
                $buffy .= '</div>';
                $buffy .= '</div>';
            endforeach;

            $buffy .= '</div>';
            $buffy .= '</div>';
        }
        return $buffy;
    }

    public function load_data()
    {
        $args = array(
            'type' => 'post',
            'child_of' => 0,
            'parent' => $this->category->parent,
            'orderby' => 'name',
            'order' => 'ASC',
            'hide_empty' => 1,
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'number' => 6,
            'taxonomy' => 'category',
            'pad_counts' => false
        );
        $this->data = get_categories($args);
    }
}