<?php

class EC_Block_Big_Grid extends EC_Base_Block
{

    public function render($atts, $td_column_number = '')
    {

        if (EC()->is_mobile) {
            return '';
        }

        $td_column_number = 5;
        $buffy = '';
        parent::render($atts, $td_column_number);
        $buffy .= '<div class="enc_block_big_grid_1">';
        $buffy .= '<div class="enc_block_inner">';
        $buffy .= $this->inner($this->td_query->posts); //inner content of the block
        $buffy .= '</div>';
        $buffy .= '</div><br> <!-- ./block -->';
        return $buffy;
    }

    public function inner($posts)
    {
        $buffy = '';
        if (!empty($posts)) {
            $buffy .= '<div class="enc-big-grid-wrapper">';
            $post_count = 0;
            foreach ($posts as $post) {
                if ($post_count == 0) {
                    $enc_module_mx1 = new EC_Module_Mx1($post);
                    $buffy .= $enc_module_mx1->render($post_count);
                    $post_count++;
                    continue;
                } else if ($post_count == 1) {
                    $buffy .= '<div class="enc-big-grid-scroll">';
                }
                $enc_module_mx4 = new EC_Module_Mx4($post);
                $buffy .= $enc_module_mx4->render($post_count);
                $post_count++;
            }
            if (count($posts) > 1) {
                $buffy .= '</div>';
            }
            $buffy .= '</div>';
        }
        return $buffy;
    }
}
