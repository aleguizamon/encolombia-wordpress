<?php

class EC_Block_Related_Posts extends EC_Base_Block {

  private static $instance;

  /**
   * EC_Block_Related_Posts constructor.
   */
  private function __construct()
  {}

  static function get_instance() {
      if ( !isset( self::$instance ) ) {
          self::$instance = new EC_Block_Related_Posts();
      }
      return self::$instance;
  }


  function render($atts, $column_number) {
      global $post;

      $atts['limit'] = 8;

      parent::render($atts,$column_number); // sets the live atts, $this->atts, $this->block_uid, $this->td_query (it runs the query)
      extract(shortcode_atts(
              array(
                  'td_column_number' => ''
              ), $atts));

      if ($this->td_query->post_count == 0) {
          return;
      }

      $buffy = '<div class="enc_block_related_posts">';
      $buffy .= '<h3 class="enc-block-title-1">';
      $buffy .= 'ARTÍCULOS SIMILARES';
      $buffy .= '<span class="titledot"></span>';
      $buffy .= '<span class="titleline"></span>';
      $buffy .= '</h3>';
      $buffy .= $this->inner($this->td_query->posts, $column_number);  //inner content of the block
      $buffy .= '</div>';
      return $buffy;
  }

  function inner($posts, $td_column_number = '') {
      $buffy = '';
      if (!empty($posts)) {
          $buffy.="<div class='enc-row'>";
          foreach ($posts as $td_post_count => $post) {
              $enc_module_related_posts = new EC_Module_1($post);
              $buffy .= "<div class='col-lg-3 col-md-4 col-sm-6'>";
              $buffy .= $enc_module_related_posts->render();
              $buffy .= "</div>";
          } //end for each
          $buffy.="</div>";
      }
      return $buffy;
  }

}
