<?php

class EC_Scheme_Recipe implements EC_Scheme {

    private $autormetadata;
    private static $instance;

    const URI_RECETAS = '/vida-estilo/alimentacion/recetas/';

    /**
     * EC_Scheme_Recipe constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new EC_Scheme_Recipe();
        }
        return self::$instance;
    }

    public static function is_recipe(){
        return EC_Util::is_uri_section(self::URI_RECETAS);
    }

    public function configure()
    {
        if(self::is_recipe()){
            add_filter( 'wpseo_json_ld_output', array($this, 'yoastVerifyScheme') );
            add_action('wp_head', array($this, 'installSchemeRecipe'), 99);
        }
    }
    
    public function yoastVerifyScheme(){
        if ( is_single () ) {
            return false;
        }
    }

    public function installSchemeRecipe(){
        global $post;
        
        if(!empty($post) && is_object($post) && is_single($post->ID)){
            $schemesreturn = $this->getAllDataRecipe($post->ID);
    ?>
        <script class="recipe-schema" type="application/ld+json"><?php echo $schemesreturn; ?></script>
    <?php
        } else {echo "";}
    }

    protected function getAllDataRecipe($postid, $format = 'json'){
        
        $postmetadata = get_post_meta($postid);
        $postitle = get_the_title( $postid );
        $featuredimg = wp_get_attachment_image_src( get_post_thumbnail_id($postid), 'thumbnail' );
        $datecreation = get_the_date('Y-m-d',$postid);
        // $this->$autormetadata = get_post_meta("249011");
        //$recipeingredient = $postmetadata;

        $alldata = array (
            "@context"=>"https://schema.org/",
            "@type"=>"Recipe",
            "name"=>"$postitle",
            "image" => array (//imagen destacada
                $featuredimg[0],
            ),
            "author"=> array(
                "@type" => "Organization",
                "name" => "Encolombia"
            ),
            "datePublished" => $datecreation,//fecha de creación del post
            "description" => isset($postmetadata['description']) ? $postmetadata['description'][0] : '',
            "prepTime" => isset($postmetadata['prepTime']) ? $postmetadata['prepTime'][0] : '',
            "cookTime" => isset($postmetadata['cookTime']) ? $postmetadata['cookTime'][0] : '',
            "totalTime" => isset($postmetadata['totalTime']) ? $postmetadata['totalTime'][0] : '',
            "keywords" => isset($postmetadata['_yoast_wpseo_focuskw']) ? $postmetadata['_yoast_wpseo_focuskw'][0] : '',
            "recipeYield" => isset($postmetadata['recipeYield']) ? $postmetadata['recipeYield'][0] : '',
            "recipeCategory" => isset($postmetadata['recipeCategory']) ? $postmetadata['recipeCategory'][0] : '',
            "recipeCuisine" => isset($postmetadata['recipeCuisine']) ? $postmetadata['recipeCuisine'][0] : '',
            "nutrition" => array(
                "@type" => "NutritionInformation",
                "calories" => isset($postmetadata['calories'], $postmetadata['calories'][0]) && !empty($postmetadata['calories'][0]) ? $postmetadata['calories'][0].' calorias' : ''
            ),
            "recipeIngredient" => array(
                //ciclo
            ),
            "recipeInstructions" => array(
                //ciclo
            )
        );

        $videoName        = isset($postmetadata['video_videoName'], $postmetadata['video_videoName'][0]) && !empty($postmetadata['video_videoName'][0]) ? $postmetadata['video_videoName'][0] : '';
        $videoDescription = isset($postmetadata['video_videoDescription'], $postmetadata['video_videoDescription'][0]) && !empty($postmetadata['video_videoDescription'][0]) ? $postmetadata['video_videoDescription'][0] : '';
        $videoContentUrl  = isset($postmetadata['video_contentUrl'], $postmetadata['video_contentUrl'][0]) && !empty($postmetadata['video_contentUrl'][0]) ? $postmetadata['video_contentUrl'][0] : '';
        $videoEmbed       = isset($postmetadata['video_embedUrl'], $postmetadata['video_embedUrl'][0]) && !empty($postmetadata['video_embedUrl'][0]) ? $postmetadata['video_embedUrl'][0] : '';
        $videoDuration    = isset($postmetadata['video_videoDuration'], $postmetadata['video_videoDuration'][0]) && !empty($postmetadata['video_videoDuration'][0]) ? $postmetadata['video_videoDuration'][0] : '';
        $videoUploadDate    = isset($postmetadata['video_uploadDate'], $postmetadata['video_uploadDate'][0]) && !empty($postmetadata['video_uploadDate'][0]) ? $postmetadata['video_uploadDate'][0] : '';
        //$videoThumbnail   = isset($postmetadata['video_videoThumbnail'], $postmetadata['video_videoThumbnail'][0]) && !empty($postmetadata['video_videoThumbnail'][0]) ? $postmetadata['video_videoThumbnail'][0] : '';


        if(1==0 && is_user_logged_in()){
            echo "<pre>";var_dump($postmetadata);exit;
            //var_dump($videoThumbnail);exit;
        }

        if(!empty($videoName) && !empty($videoDuration)){
            $alldata['video'] = array();
            $alldata['video']['@type'] = "VideoObject";
            $alldata['video']['name'] = $videoName;
            if(!empty($videoDescription)) $alldata['video']['description'] = $videoDescription;
            //if(!empty($videoThumbnail))   $alldata['video']['thumbnailUrl'] = $videoThumbnail;
            if(!empty($videoContentUrl))  $alldata['video']['contentUrl'] = $videoContentUrl;
            if(!empty($videoEmbed))       $alldata['video']['embedUrl'] = $videoEmbed;
            $alldata['video']['duration'] = $videoDuration;
            $alldata['video']['uploadDate'] = $videoUploadDate;
        }
        
        //Ingredients
        if(isset($postmetadata['ingredientes'])){
            $count = intval($postmetadata['ingredientes'][0]);
            
            for ($r = 0; $r < $count; $r++ ){
                
                //$ingredientewhile[] = $postmetadata["ingredientes_".$r."_recipeIngredient"];
                $alldata['recipeIngredient'][$r] = $postmetadata["ingredientes_".$r."_recipeIngredient"][0];
            }
            
        }
        
        
        //Intrucciones
        if(isset($postmetadata['intrucciones'])){
            $count = intval($postmetadata['intrucciones'][0]);
            
            for ($r = 0; $r < $count; $r++ ){
                $itemImage = '';
                if(isset($postmetadata["intrucciones_".$r."_image"], $postmetadata["intrucciones_".$r."_image"][0]) && is_numeric($postmetadata["intrucciones_".$r."_image"][0])){
                    $itemImage = wp_get_attachment_image_src( (int)$postmetadata["intrucciones_".$r."_image"][0], 'medium' );
                }
                //$ingredientewhile[] = $postmetadata["ingredientes_".$r."_recipeIngredient"];
                $alldata['recipeInstructions'][$r]['@type'] = 'HowToStep';
                $alldata['recipeInstructions'][$r]['name'] = isset($postmetadata["intrucciones_".$r."_name"]) ? $postmetadata["intrucciones_".$r."_name"][0] : "";
                $alldata['recipeInstructions'][$r]['text'] = isset($postmetadata["intrucciones_".$r."_text"]) ? $postmetadata["intrucciones_".$r."_text"][0] : "";
                $alldata['recipeInstructions'][$r]['url'] = isset($postmetadata["intrucciones_".$r."_url"]) ? $postmetadata["intrucciones_".$r."_url"][0] : "";
                $alldata['recipeInstructions'][$r]['image'] = isset($itemImage[0]) ? $itemImage[0] : '';
            }
            
        }
        
        //Video Thumbnail Url
        if(isset($postmetadata['video_video_imagen'])){
            $count = intval($postmetadata['video_video_imagen'][0]);
            
            for ($r = 0; $r < $count; $r++ ){
                $videoImage = '';
                if(isset($postmetadata["video_video_imagen_".$r."_thumbnailUrl"], $postmetadata["video_video_imagen_".$r."_thumbnailUrl"][0]) && !empty($postmetadata["video_video_imagen_".$r."_thumbnailUrl"][0]) && is_numeric($postmetadata["video_video_imagen_".$r."_thumbnailUrl"][0])){
                    $videoImage = wp_get_attachment_image_src( (int)$postmetadata["video_video_imagen_".$r."_thumbnailUrl"][0], 'medium' );
                }
                //$ingredientewhile[] = $postmetadata["ingredientes_".$r."_recipeIngredient"];
                $alldata['video']['thumbnailUrl'][$r] = isset($videoImage[0]) ? $videoImage[0] : '';
                
            }
            
        }
        return $format == 'json' ? json_encode($alldata, JSON_UNESCAPED_SLASHES) : $alldata;
    }

}
    
    
    


