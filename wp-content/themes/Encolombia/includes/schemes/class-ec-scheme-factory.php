<?php
class EC_Scheme_Factory{

    /**
     *
     * @return EC_Scheme|null
     */
    public function get_scheme(){
        if(EC_Scheme_Recipe::is_recipe()){
            return EC_Scheme_Recipe::get_instance();
        }

        return null;
    }

}