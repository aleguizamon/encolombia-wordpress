<?php
/**
 * EC_Base_Post
 *
 * Clase dedicada a proporcionar informacion basicas de un post
 *
 * @since 7.0.0
 */
trait EC_Base_Post
{
    /**
     * Instancia de WP_Post de la página actual
     *
     * @var WP_Post
     */
    public $post;

    /**
     * Título del post
     *
     * @var string
     */
    public $title;

    /**
     * Título del post usado para atributos de etiquetas HTML
     *
     * @var string
     */
    public $title_attribute;

    /**
     * ID de la imagen destacada del post
     *
     * @var int|null
     */
    public $post_thumb_id = NULL;

    /**
     * URL del post
     *
     * @var string
     */
    public $permalink;

    /**
     * URL de la imagen destacada del post
     *
     * @var string|null
     */
    public $post_thumb_url = null;

    /**
     * Array de objetos de WP_Term, uno por cada categoría asignada al post.
     *
     * @var WP_Term[]
     */
    public $categories = null;

    /**
     * @param $post WP_Post
     */
    public function init_post(WP_Post $post) {
        $this->post = $post;
        $this->categories = get_the_category($post->ID);
        $this->permalink = esc_url(get_permalink($post));
        $this->title = get_the_title($post);
        $this->title_attribute = esc_attr(strip_tags($this->title));

        if (has_post_thumbnail($post)) {
            $this->post_thumb_url = get_the_post_thumbnail_url($post);
            $this->post_thumb_id = get_post_thumbnail_id($post);
        }
    }

    /**
     * Indica si el post tiene imagen destacada
     *
     * @return bool
     */
    public function has_featured_image() {
        return $this->post_thumb_id !== NULL;
    }

    /**
     * get image - v 3.0  23 ian 2015
     * @param $thumbType
     * @return string
     */
    public function get_image($thumbType) {
        $buffy = '';
        $srcset_sizes = '';
        if (!empty($this->post_thumb_id)) {
            $td_temp_image_url = wp_get_attachment_image_src($this->post_thumb_id, $thumbType);

            $attachment_alt = get_post_meta($this->post_thumb_id, '_wp_attachment_image_alt', true );
            if(empty($attachment_alt)) {
                $attachment_alt = $this->title;
            }
            $attachment_alt = 'alt="' . esc_attr(strip_tags($attachment_alt)) . '"';
            $attachment_title = ' title="' . esc_attr(strip_tags($this->title)) . '"';

            if (empty($td_temp_image_url[0])) {
                $td_temp_image_url[0] = '';
            }

            if (empty($td_temp_image_url[1])) {
                $td_temp_image_url[1] = '';
            }

            if (empty($td_temp_image_url[2])) {
                $td_temp_image_url[2] = '';
            }

            $srcset_sizes = EC_Thumbs::get_srcset_sizes($thumbType, $td_temp_image_url[1], $td_temp_image_url[0], $this->post_thumb_id);
            //if(is_user_logged_in()){ var_dump($srcset_sizes);  var_dump($thumbType); var_dump($td_temp_image_url[1]); var_dump($td_temp_image_url[0]);}
        } else {
            $td_temp_image_url = array();
            if (strpos($thumbType, 'td_') === 0) {
                $parts = explode('td_', $thumbType);
                $parts = explode('x', $parts[1]);
                $td_temp_image_url[1] = $parts[0];
                $td_temp_image_url[2] = (int)$parts[1] == 0 ? 'auto' : $parts[1];
                $td_temp_image_url[0] = EC_TEMPLATE_DIRECTORY_URI . '/assets/images/no-thumb/' . $thumbType . '.png';
            } else {
                $td_temp_image_url[1] = '';
                $td_temp_image_url[2] = '';
                $td_temp_image_url[0] = EC_TEMPLATE_DIRECTORY_URI . '/assets/images/no-thumb/thumbnail.png';
            }
            $attachment_alt = 'alt=""';
            $attachment_title = '';
        }


        if(isset($td_temp_image_url[0]) && !empty($td_temp_image_url[0])){
            //$buffy .= '<div class="enc-module-thumb enc-post-featured-image" '. (!is_front_page() && is_single() ? ' style="margin-bottom:21px;'. (wp_is_mobile() ? 'height: 205px;' : '') .'"' : '') .'>';
            $buffy .= '<div class="enc-module-thumb enc-post-featured-image" >';
            if (current_user_can('edit_posts')) {
                $buffy .= '<a class="enc-admin-edit" href="' . get_edit_post_link($this->post->ID) . '">edit</a>';
            }

            $buffy .= '<a href="' . $this->permalink . '"  title="' . $this->title_attribute . '">';
            $buffy .= '<img width="' . $td_temp_image_url[1] . '" height="' . $td_temp_image_url[2] . '" class="entry-thumb" src="' . $td_temp_image_url[0] . '"' . $srcset_sizes . ' ' . $attachment_alt . $attachment_title . '/>';
            $buffy .= '</a>';
            $buffy .= '</div>'; //end wrapper

            //if (ENC_DEV_MODE) {
            $buffy = str_replace(EC_DEV_ROUTE, EC_PROD_ROUTE, $buffy);
            //}
        }

        return $buffy;
    }
}