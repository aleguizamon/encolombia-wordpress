<?php
/**
 * Encolombia setup
 */
defined( 'ABSPATH' ) || exit;

/**
 * Main Encolombia Class.
 *
 * @class Encolombia
 */
final class Encolombia {
    /**
     * The single instance of the class.
     *
     * @var Encolombia
     */
    protected static $_instance = null;

    /**
     * Almacena la url del sitio
     *
     * @var string
     */
    public $site_url;

    /**
     * Indica si la petición actual es por ajax
     *
     * @var bool
     */
    public $doing_ajax;

    /**
     * Indica si la petición actual es desde movil
     *
     * @var bool
     */
    public $is_mobile;

    /**
     * Indica si la página actual es una página de cliente
     *
     * @var bool
     */
    public $is_customer_section = false;

    /**
     * Indica si la página actual es una página de genfar
     * 
     * @var bool
     */
    public $is_genfar_section = false;

    /**
     * El tipo de página actual (posts|category|home)
     *
     * @var string
     */
    public $page_type;

    /**
     * Instancia de la clase de EC_Manager_Post
     *
     * @var EC_Manager_Post
     */
    public $manager_post = null;

    /**
     * Instancia de la clase de EC_Manager_Category
     *
     * @var EC_Manager_Category
     */
    public $manager_category = null;

    /**
     * Instancia de la clase de layout
     *
     * @var EC_Layout
     */
    public $layout;

    /**
     * Instancia de la clase de EC_Sidebars
     *
     * @var EC_Sidebars
     */
    public $sidebars;

    /**
     * Instancia de la clase de EC_External_Scripts
     *
     * @var EC_External_Scripts
     */
    public $scripts;

    /**
     * Instancia de la clase de EC_Shortcodes
     *
     * @var EC_Shortcodes
     */
    public $shortcodes;

    /**
     * Instancia de la clase de EC_Thumbs
     *
     * @var EC_Thumbs
     */
    public $thumbs;

    /**
     * Instancia de la clase de EC_Yoast_Breadcrumb_Extend
     *
     * @var EC_Yoast_Breadcrumb_Extend
     */
    public $yoast_breadcrumb;

    /**
     * Instancia de la clase de EC_Performance
     *
     * @var EC_Performance
     */
    public $performance;

    /**
     * Instancia de la clase de EC_Extend_Comments
     *
     * @var EC_Extend_Comments
     */
    public $comments;

    /**
     * Instancia de la clase de EC_Admin_Config
     *
     * @var EC_Admin_Config
     */
    public $admin;

    /**
     * Instancia de la clase de EC_Ajax
     *
     * @var EC_Ajax
     */
    public $ajax;

    /**
     * Instancia de la clase de EC_Scheme
     *
     * @var EC_Scheme
     */
    public $scheme;

    /**
     * Main Encolombia Instance.
     *
     * Ensures only one instance of Encolombia is loaded or can be loaded.
     *
     * @static
     * @see EC()
     * @return Encolombia - Main instance.
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Cloning is forbidden.
     */
    public function __clone() {
        _doing_it_wrong( __FUNCTION__, __( 'Cloning is forbidden.', 'EC' ), '7.0' );
    }

    /**
     * Unserializing instances of this class is forbidden.
     */
    public function __wakeup() {
        _doing_it_wrong( __FUNCTION__, __( 'Unserializing instances of this class is forbidden.', 'EC' ), '7.0' );
    }

    /**
     * WooCommerce Constructor.
     */
    public function __construct() {}

    public function configure() {
        $this->define_constants();
        $this->includes();

        $this->layout = EC_Layout::get_instance();
        $this->sidebars = EC_Sidebars::get_instance();
        $this->scripts = EC_External_Scripts::get_instance();
        $this->shortcodes = EC_Shortcodes::get_instance();
        $this->thumbs = EC_Thumbs::get_instance();
        $this->yoast_breadcrumb = EC_Yoast_Breadcrumb_Extend::get_instance();
        $this->performance = EC_Performance::get_instance();
        $this->comments = EC_Extend_Comments::get_instance();
        $this->admin = EC_Admin_Config::get_instance();
        $this->ajax = EC_Ajax::get_instance();
        $this->scheme = (new EC_Scheme_Factory())->get_scheme();

        $this->init_hooks();
    }

    /**
     * Hook into actions and filters.
     */
    private function init_hooks() {
        /** Registra los sidebars del tema */
        $this->sidebars->register();

        add_theme_support( 'post-thumbnails' );
        /**
         * Este Hook se ejecuta primero e inicializa las primeras variables que pueden estar disponibles
         */
        add_action( 'init', array($this, 'init_one'), 990);

        /**
         * Este Hook indica que WordPress está listo para cargar la plantilla.
         * Esto permite que en este punto se reconozcan las funciones de WP como is_front_page(), is_category(), etc
         */
        add_action('template_redirect', array($this, 'init_two'), 990);

        add_action('pre_get_posts', array($this, 'set_category_posts_order'));
        add_action( 'wp_enqueue_scripts', array($this, 'theme_enqueue_styles'), 1001);

        /** Inicializo los hooks de los scripts externos */
        $this->scripts->configure();

        /** Inicializo los shortcodes */
        $this->shortcodes->configure();

        /** Inicializo los thumbs */
        $this->thumbs->configure();

        /** Inicializo las personalizaciones al breadcrumb de yoast */
        $this->yoast_breadcrumb->configure();

        /** Inicializo las configuraciones de rendimiento */
        $this->performance->configure();

        /** Inicializo las configuraciones de comentarios */
        $this->comments->configure();

        /** Inicializo las configuraciones de administración */
        $this->admin->configure();

        /** Inicializo las configuraciones de ajax */
        $this->ajax->configure();

        /** Inicializo la configuracion de esquema de recetas */
        if($this->scheme){
            $this->scheme->configure();
        }
    }

    public function init_one(){
        $this->site_url = site_url();
        $this->doing_ajax = wp_doing_ajax();
        $this->is_mobile = wp_is_mobile();
        $this->is_customer_section = EC_Util::is_customer_section();
        $this->is_genfar_section = EC_Util::is_genfar_section();
        if ( $this->is_genfar_section ) {
            add_filter( 'body_class', function( $classes ) {return array_merge( $classes, array( 'page_genfar' ) );} );
        }

        /** Registro los tipo de menu del tema */
        register_nav_menus(
            array(
                'top-menu' => 'Top Header Menu',
                'header-menu' => 'Header Menu (main)',
                'footer-menu' => 'Footer Menu'
            )
        );

        /** Registro los sidebars del tema */
        $this->sidebars->register();

        /** Si esta en modo DEV cargo las imágenes de encolombia.com */
        if (EC_DEV_MODE) {
            add_filter( 'wp_get_attachment_image_src', array($this, 'replace_image_url_for_dev') );
        }
        //echo "initialized1";
    }

    public function init_two() {
        $this->page_type = EC_Util::get_page_type();//X
        if ($this->is_post()) {
            $this->manager_post = new EC_Manager_Post(get_queried_object());
            if($this->manager_post->is_publication) {
                add_filter( 'body_class', function( $classes ) {return array_merge( $classes, array( 'is_pub' ) );} );
            }
        } else if($this->is_category()) {
            $this->manager_category = new EC_Manager_Category(get_queried_object());
            if($this->manager_category->is_publication) {
                $this->manager_category->redirect_if_last();
            }
        }

        //echo "initialized2";
        //echo "<pre>";var_dump($this->manager_post->is_shortcode_ultimate);
    }

    /**
     * Define EC Constants.
     */
    private function define_constants() {
        define('EC_THEME_VERSION',  '7.0');
        define('EC_PROD_ROUTE',  'https://encolombia.com/');
        define('EC_TEMPLATE_DIRECTORY_URI',  get_template_directory_uri());
        define('EC_FOUNDATION_YEAR',  '1998');
        define('EC_SITE_NAME',  'encolombia.com');
        define('EC_LOGO_URL',  'https://encolombia.com/wp-content/uploads/2021/02/logo-encolombia-4.png');
        define('EC_RETINA_LOGO_URL',  'https://encolombia.com/wp-content/uploads/2021/02/logo-encolombia-4-retina.png');
        define('EC_LOGO_URL_M',  'https://encolombia.com/wp-content/uploads/2021/03/logo-encolombia-4m.png');
        define('EC_LOGO_RETINA_URL_M',  'https://encolombia.com/wp-content/uploads/2021/03/logo-encolombia-4m-retina.png');
        define('EC_LOGO_URL_FOOTER',  'https://encolombia.com/wp-content/uploads/2017/06/logo-encolombia-nuevo.png');
        define('EC_LOGO_RETINA_URL_FOOTER',  'https://encolombia.com/wp-content/uploads/2017/06/logo-encolombia-nuevo-retina.png');
        define('EC_TRANSLATE_DOMAIN',  'EC');
        define('EC_FAV_ICON',  'https://encolombia.com/wp-content/uploads/2017/06/logo-encolombia-nuevo-favicon.png');
        define('EC_ICON_IOS_76',  'https://encolombia.com/wp-content/uploads/2018/08/logo-encolombia-ios-76.png');
        define('EC_ICON_IOS_120',  'https://encolombia.com/wp-content/uploads/2018/08/logo-encolombia-ios-120.png');
        define('EC_ICON_IOS_152',  'https://encolombia.com/wp-content/uploads/2018/08/logo-encolombia-ios-152.png');
        define('EC_ICON_IOS_114',  'https://encolombia.com/wp-content/uploads/2018/08/logo-encolombia-ios-114.png');
        define('EC_ICON_IOS_144',  'https://encolombia.com/wp-content/uploads/2018/08/logo-encolombia-ios-144.png');
        define('EC_SOCIAL_FACEBOOK_LINK',  'https://www.facebook.com/people/encolombiacom/100060538192215/');
        define('EC_SOCIAL_PINTEREST_LINK',  'https://co.pinterest.com/encolombiacom/boards/');
        define('EC_SOCIAL_TWITTER_LINK',  'https://twitter.com/encolombia1');
        define('EC_SOCIAL_YOUTUBE_LINK',  'https://www.youtube.com/encolombia1');
        define('EC_EXCERPT_LENGTH',  90);
        define('EC_POSTS_PER_PAGE',  10);
        define('EC_AVATAR',  'https://encolombia.com/wp-content/uploads/2017/06/logo-encolombia-nuevo-favicon.png');
    }

    /**
     * Include required files.
     */
    public function includes() {
        include_once EC_DIR . '/includes/ec-dev-mode.php';
        include_once EC_DIR . '/includes/class-ec-menu.php';
        include_once EC_DIR . '/includes/class-ec-util.php';
        include_once EC_DIR . '/includes/class-ec-util-legacy.php';
        include_once EC_DIR . '/includes/class-ec-layout.php';
        include_once EC_DIR . '/includes/class-ec-sidebars.php';
        include_once EC_DIR . '/includes/class-ec-external-scripts.php';
        include_once EC_DIR . '/includes/class-ec-shortcodes.php';
        include_once EC_DIR . '/includes/class-ec-thumb-registry.php';
        include_once EC_DIR . '/includes/class-ec-thumbs.php';
        include_once EC_DIR . '/includes/class-ec-data-source.php';
        include_once EC_DIR . '/includes/class-ec-base-block.php';
        require_once(EC_DIR . '/includes/class-ec-base-post.php');
        include_once EC_DIR . '/includes/class-ec-base-category.php';
        include_once EC_DIR . '/includes/class-ec-base-author.php';
        include_once EC_DIR . '/includes/class-ec-yoast-breadcrumb-extend.php';
        include_once EC_DIR . '/includes/class-ec-performance.php';
        include_once EC_DIR . '/includes/class-ec-extend-comments.php';

        require_once(EC_DIR . '/includes/class-ec-manager-post.php');
        require_once(EC_DIR . '/includes/class-ec-manager-category.php');
        require_once(EC_DIR . '/includes/modules/class-ec-base-module.php');
        require_once(EC_DIR . '/includes/modules/class-ec-module-mx1.php');
        require_once(EC_DIR . '/includes/modules/class-ec-module-mx4.php');
        require_once(EC_DIR . '/includes/modules/class-ec-module-6.php');
        require_once(EC_DIR . '/includes/modules/class-ec-module-10.php');
        require_once(EC_DIR . '/includes/modules/class-ec-module-4.php');
        require_once(EC_DIR . '/includes/modules/class-ec-module-1.php');
        require_once(EC_DIR . '/includes/modules/class-ec-module-search.php');
        require_once(EC_DIR . '/includes/modules/class-ec-module-post-author.php');
        require_once(EC_DIR . '/includes/modules/class-ec-module-author.php');
        require_once(EC_DIR . '/includes/blocks/class-ec-block-big-grid.php');
        require_once(EC_DIR . '/includes/blocks/class-ec-block-category-sections.php');
        require_once(EC_DIR . '/includes/blocks/class-ec-block-7.php');
        require_once(EC_DIR . '/includes/blocks/class-ec-block-11.php');
        require_once(EC_DIR . '/includes/blocks/class-ec-block-related-categories.php');
        require_once(EC_DIR . '/includes/blocks/class-ec-block-related-posts.php');
        require_once(EC_DIR . '/includes/class-ec-ajax.php');

        require_once(EC_DIR . '/includes/shortcodes/su/class-ec-su-button.php');
        require_once(EC_DIR . '/includes/shortcodes/su/class-ec-su-column.php');
        require_once(EC_DIR . '/includes/shortcodes/su/class-ec-su-note.php');
        require_once(EC_DIR . '/includes/shortcodes/su/class-ec-su-row.php');
        require_once(EC_DIR . '/includes/shortcodes/su/class-ec-su-spacer.php');
        require_once(EC_DIR . '/includes/shortcodes/su/class-ec-su-spoiler.php');
        require_once(EC_DIR . '/includes/shortcodes/class-ec-wpbakery.php');
        require_once(EC_DIR . '/includes/shortcodes/class-ec-is-mobile.php');
        require_once(EC_DIR . '/includes/shortcodes/class-ec-is-desktop.php');
        require_once(EC_DIR . '/includes/shortcodes/class-ec-is-url.php');
        require_once(EC_DIR . '/includes/shortcodes/class-ec-can-render-admanager.php');

        require_once(EC_DIR . '/includes/class-ec-admin-config.php');
        require_once(EC_DIR . '/includes/class-ec-cf-category.php');
        require_once(EC_DIR . '/includes/class-ec-options-page.php');

        require_once(EC_DIR . '/includes/schemes/class-ec-scheme-interface.php');
        require_once(EC_DIR . '/includes/schemes/class-ec-scheme-factory.php');
        require_once(EC_DIR . '/includes/schemes/class-ec-scheme-recipe.php');
    }

    public function replace_image_url_for_dev($image){
        $image[0] = str_replace(EC_DEV_ROUTE, EC_PROD_ROUTE, $image[0]);
        return $image;
    }

    public function set_category_posts_order($query) {
        if ($query->is_category() && $query->is_main_query()) {
            $query->set('orderby', 'date'); // ordena por título de manera ascendente
            $query->set('order', 'DESC');
        }
    }

    public function theme_enqueue_styles() {
        $is_mobile = wp_is_mobile();
        $is_front_page = is_front_page();
        wp_register_script('enc-scripts', EC_TEMPLATE_DIRECTORY_URI . '/assets/js/enc-scripts.js', array('jquery'));
        wp_enqueue_script('enc-scripts');
        wp_dequeue_script('otw_grid_manager');
        
        wp_enqueue_style('enc-fonts', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/fonts.css', '', EC_THEME_VERSION, 'all' );
        wp_enqueue_style('enc-layout', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/layout.css', '', EC_THEME_VERSION, 'all' );
        wp_enqueue_style('enc-general', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/general.css', '', EC_THEME_VERSION, 'all' );

        if(!$is_front_page){
            wp_enqueue_style('enc-breadcrumb', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/breadcrumbs.css', '', EC_THEME_VERSION, 'all' );
        }

        if($is_front_page){
            if(!$is_mobile){
                wp_enqueue_style('enc-big-grid', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/big-grid.css', '', EC_THEME_VERSION, 'all' );
            }
            wp_enqueue_style('enc-module10', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/module10.css', '', EC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-secciones-home', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/secciones-home.css', '', EC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-block7', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/block7.css', '', EC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-module6', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/module6.css', '', EC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-search-home', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/search-sidebar-home.css', '', EC_THEME_VERSION, 'all' );
        } else if(is_404()){
            wp_enqueue_style('enc-404', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/404-page.css', '', EC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-module4', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/module4.css', '', EC_THEME_VERSION, 'all' );
        } else if(is_search()){
            wp_enqueue_style('enc-search', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/search-page.css', '', EC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-module4', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/module4.css', '', EC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-pagination', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/pagination.css', '', EC_THEME_VERSION, 'all' );
            if(!$is_mobile){
                wp_enqueue_style('enc-block7', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/block7.css', '', EC_THEME_VERSION, 'all' );
                wp_enqueue_style('enc-module6', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/module6.css', '', EC_THEME_VERSION, 'all' );
            }
        } else if(is_single()){
            //$cats = get_the_category( get_the_ID() );
            wp_register_script('enc-posts-scripts', EC_TEMPLATE_DIRECTORY_URI . '/assets/js/enc-post-scripts.js', array('jquery'));
            wp_enqueue_script('enc-posts-scripts');
            wp_localize_script( 'enc-posts-scripts', 'enc_data', array(
                'ajax_url' => admin_url( 'admin-ajax.php' ),
                'post_scroll_limit' => 100,
                'current_post' => get_the_ID(),
                'is_sponsored' => EC()->is_customer_section ? 'Y' : 'N',
                //'current_cat' => $cats[0]->term_id
            ));

            wp_enqueue_style('enc-post-detail-v3', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/post-detail-v3.css', '', EC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-module-1', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/module1.css', '', EC_THEME_VERSION, 'all' );

            if(!$is_mobile) {
                wp_enqueue_style('enc-block7', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/block7.css', '', EC_THEME_VERSION, 'all');
                wp_enqueue_style('enc-module6', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/module6.css', '', EC_THEME_VERSION, 'all');
            }
            /*wp_enqueue_style('enc-more-than', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/more-than.css', '', ENC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-social', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/social-networks.css', '', ENC_THEME_VERSION, 'all' );
            if(!wp_is_mobile()){
                wp_enqueue_style('enc-block7', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/block7.css', '', ENC_THEME_VERSION, 'all' );
                wp_enqueue_style('enc-module6', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/module6.css', '', ENC_THEME_VERSION, 'all' );
                wp_enqueue_style('enc-related-posts', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/related-posts.css', '', ENC_THEME_VERSION, 'all' );
            }*/
            wp_enqueue_style('enc-comments', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/comments.css', '', EC_THEME_VERSION, 'all' );
            //if(EC()->is_page_builder_content){
                wp_enqueue_style('enc-visual-composer', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/visual-composer.css', '', EC_THEME_VERSION, 'all' );
            //}
            //if(EC()->is_shortcode_ultimate){
                wp_enqueue_style('enc-su', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/shortcode-ultimate.css', '', EC_THEME_VERSION, 'all' );
            //}
            if ( EC()->is_genfar_section ) {
                wp_enqueue_style('enc-genfar', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/genfar-pages.css', '', EC_THEME_VERSION, 'all' );
                wp_enqueue_style('enc-genfar-tabs', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/tabs-genfar.css', '', EC_THEME_VERSION, 'all' );
            }
            //if (EC()->is_publication) {
                wp_enqueue_style('enc-list-pub', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/publications-list.css', '', EC_THEME_VERSION, 'all' );
            //}
        } else if(is_category()){
            wp_register_script('enc-cats-scripts', EC_TEMPLATE_DIRECTORY_URI . '/assets/js/enc-cats-scripts.js', array('jquery'));
            wp_enqueue_script('enc-cats-scripts');
            wp_localize_script( 'enc-cats-scripts', 'enc_data', array(
                'ajax_url' => admin_url( 'admin-ajax.php' ),
                'cat_slug' => get_query_var('category_name'),
            ));
            wp_enqueue_style('enc-category', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/category.css', '', EC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-module4', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/module4.css', '', EC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-module-1', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/module1.css', '', EC_THEME_VERSION, 'all' );
            //wp_enqueue_style('enc-breadcrumb', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/breadcrumbs.css', '', ENC_THEME_VERSION, 'all' );
            //if(EC()->is_publication){
                wp_enqueue_style('enc-list-pub', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/publications-list.css', '', EC_THEME_VERSION, 'all' );
            //} else {
                wp_enqueue_style('enc-list-subcats', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/subcategories-list.css', '', EC_THEME_VERSION, 'all' );
            //}
            //wp_enqueue_style('enc-more-than', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/more-than.css', '', ENC_THEME_VERSION, 'all' );
            //wp_enqueue_style('enc-new-pub', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/new-publications.css', '', ENC_THEME_VERSION, 'all' );
            //wp_enqueue_style('enc-pagination', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/pagination.css', '', ENC_THEME_VERSION, 'all' );
            //wp_enqueue_style('enc-social', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/social-networks.css', '', ENC_THEME_VERSION, 'all' );
            //wp_enqueue_style('enc-comments', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/comments.css', '', ENC_THEME_VERSION, 'all' );
            //if(!wp_is_mobile()){
            //    wp_enqueue_style('enc-block7', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/block7.css', '', ENC_THEME_VERSION, 'all' );
            //    wp_enqueue_style('enc-module6', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/module6.css', '', ENC_THEME_VERSION, 'all' );
            //}
        } else if(is_author()){
            wp_enqueue_style('enc-pagination', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/pagination.css', '', EC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-author', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/author.css', '', EC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-module4', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/module4.css', '', EC_THEME_VERSION, 'all' );
        }
        //else if ( EC()->is_genfar_section ) {
            //wp_enqueue_style('enc-genfar', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/genfar-pages.css', '', ENC_THEME_VERSION, 'all' );
        //}
        else if(is_page()){
            //wp_enqueue_style('enc-social', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/social-networks.css', '', ENC_THEME_VERSION, 'all' );
            if(!$is_mobile){
                wp_enqueue_style('enc-block7', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/block7.css', '', EC_THEME_VERSION, 'all' );
                wp_enqueue_style('enc-module6', EC_TEMPLATE_DIRECTORY_URI . '/assets/css/module6.css', '', EC_THEME_VERSION, 'all' );
            }
        }

    }

    public function is_home() {
        return $this->page_type === 'home';
    }

    public function is_category() {
        return $this->page_type === 'category';
    }

    public function is_tag() {
        return $this->page_type === 'tag';
    }

    public function is_tax() {
        return $this->page_type === 'tax';
    }

    public function is_post() {
        return $this->page_type === 'post';
    }

    public function is_author() {
        return $this->page_type === 'author';
    }

    public function is_page() {
        return $this->page_type === 'page';
    }

    public function is_search() {
        return $this->page_type === 'search';
    }

}