<?php

class EC_Api_Thumb{

    private static $components_list = array();

    public static function add($block_id, $params_array = ''){
      if (!isset(self::$components_list[$block_id])) {
        self::$components_list[$block_id] = $params_array;
      }
    }

    public static function get_all(){
      return self::$components_list;
    }

    public static function save(){
      foreach (self::get_all() as $thumb_array) {
        if ($thumb_array['name'] != '') {
          add_image_size($thumb_array['name'], $thumb_array['width'], $thumb_array['height'], $thumb_array['crop']);
        }
      }
    }
}
