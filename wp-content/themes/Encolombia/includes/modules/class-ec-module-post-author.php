<?php

class EC_Module_Post_Author extends EC_Base_Author {

    function __construct($post) {
        parent::__construct($post);
    }

    function render() {
        ob_start();
        ?>
        <div id="autor" class="post-author" itemprop="author" itemscope itemtype="https://schema.org/Person">
            <div class="author-img">
                <?php echo $this->author_color; ?>
                <a href="<?php echo $this->author_posts_url; ?>" rel="author" class="post-author-url">
                    <span itemprop="name"><?php echo $this->author_name; ?></span>
                </a>
            </div>
            <div class="author-info">
                <p class="post-author-description" itemprop="description"><?php echo $this->author_bio; ?></p>
                <ul class="author-social">
                    <?php if($this->author_url != '') { ?>
                        <li><a target="_blank" itemprop="url" title="Página Web" href="<?php echo $this->author_url; ?>"><i class="author-social-icon icon-web"></i></a></li>
                    <?php } ?>
                    <?php if($this->facebook != '') { ?>
                        <li><a target="_blank" title="Facebook" href="<?php echo $this->facebook; ?>"><i class="author-social-icon icon-facebook"></i></a></li>
                    <?php } ?>
                    <?php if($this->instagram != '') { ?>
                        <li><a target="_blank" title="Instagram" href="<?php echo $this->instagram; ?>"><i class="author-social-icon icon-instagram"></i></a></li>
                    <?php } ?>
                    <?php if($this->linkedin != '') { ?>
                        <li><a target="_blank" title="Linkedin" href="<?php echo $this->linkedin; ?>"><i class="author-social-icon icon-linkedin"></i></a></li>
                    <?php } ?>
                    <?php if($this->twitter != '') { ?>
                        <li><a target="_blank" title="Twitter" href="http://www.twitter.com/<?php echo $this->twitter; ?>"><i class="author-social-icon icon-twitter"></i></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }


}
