<?php

class EC_Module_Search extends EC_Base_Module
{
    function __construct($post)
    {
        //run the parrent constructor
        parent::__construct($post);
    }

    function render()
    {
        ob_start();
        ?>
        <div class="enc_module_4 enc_module_wrap">
            <?php echo $this->get_title(); ?>
            <small class="enc-link-post"><a
                    href="<?php echo get_permalink(); ?>"><?php echo get_permalink(); ?></a></small>
            <div class="enc-excerpt">
                <?php echo $this->get_excerpt(EC_EXCERPT_LENGTH * 2); ?>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }
}
