<?php
/**
 * EC_Base_Module
 *
 * Clase base para los módulos
 *
 * @since 7.0.0
 */
class EC_Base_Module
{
    use EC_Base_Post;

    /**
     * @param $post WP_Post
     */
    public function __construct(WP_Post $post) {
        $this->init_post($post);
    }

    /**
     * This function returns the title with the appropriate markup.
     * @return string
     */

    public function get_title() {
        $buffy = '';
        $buffy .= '<h4 class="entry-title enc-module-title">';
        $buffy .='<a href="' . $this->permalink . '"  title="' . $this->title_attribute . '">';
        $buffy .= $this->title;
        $buffy .='</a>';
        $buffy .= '</h4>';
        return $buffy;
    }

    /**
     * This method is used by modules to get content that has to be excerpted (cut)
     * IT RETURNS THE EXCERPT FROM THE POST IF IT'S ENTERED IN THE EXCERPT CUSTOM POST FIELD BY THE USER
     * @param string $cut_at - if provided the method will just cat at that point
     * @return string
     */
    public function get_excerpt($cut_at = '') {
        if ($this->post->post_excerpt != '') {
            return $this->post->post_excerpt;
        }

        $buffy = '';
        if ($cut_at != '') {
            // simple, $cut_at and return
            $buffy .= EC_Util::excerpt($this->post->post_content, $cut_at);
        } else {
            $buffy .= $this->post->post_content;
        }
        return $buffy;
    }
}