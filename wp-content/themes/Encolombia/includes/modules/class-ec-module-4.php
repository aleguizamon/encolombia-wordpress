<?php

class EC_Module_4 extends EC_Base_Module {
    function __construct($post) {
        //run the parrent constructor
        parent::__construct($post);
    }

    function render($cols = 2) {
        $thumbsize = 'enc_337x204';

        ob_start();
        echo '<div class="col-lg-3 col-md-4 col-sm-6">';
        echo '<div class="enc_module_4 enc_module_wrap">'
        ?> 
        
            <?php
                echo $this->get_image($thumbsize);
                echo $this->get_title();
            ?>
            <div class="enc-excerpt">
                <?php echo $this->get_excerpt(130); ?>
            </div>
        </div>
        <?php
        echo '</div>';
        return ob_get_clean();
    }
}
