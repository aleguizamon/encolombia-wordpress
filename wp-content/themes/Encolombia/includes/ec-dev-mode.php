<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-28
 * Time: 18:43
 */

if (!defined('EC_DEV_MODE')) {
    define("EC_DEV_MODE", true);
}

if (!defined('EC_DEV_ROUTE')) {
    define("EC_DEV_ROUTE", 'https://encolombia.local');
}
