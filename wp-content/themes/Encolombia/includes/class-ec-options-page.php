<?php
class EC_Options_Page{

    private static $instance;

    /**
     * EC_Options_Page constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new EC_Options_Page();
        }
        return self::$instance;
    }

    public function configure(){
        
        if( function_exists('acf_add_options_page') ) {
            acf_add_options_page(array(
                'page_title' => __('Opciones Encolombia', 'EC'),
                'menu_title' => __('Opciones Encolombia', 'EC'),
                'menu_slug' => 'opciones-encolombia',
            ));
	
        }
        
    }
}
