<?php

trait EC_Base_Category{

    /**
     * Objeto de WP_Term de la categoría actual del post.
     *
     * @var WP_Term
     */
    public $category;

    public $title_attribute;

    public $require_level;

    public $category_level;

    /**
     * Inicializa las propiedades de la categoría actual.
     *
     * @param WP_Term $category
     * @return void
     */
    public function initialize(WP_Term $category){
        $this->category = $category;
        $this->title_attribute = esc_attr(strip_tags($category->name));
        if($this->require_level){
            $this->category_level = EC()->is_post() ? 'P' : $this->get_category_level();
        }
        //$this->current_cat = is_single() ? get_the_category() : array(get_category(get_query_var('cat')));
        /*if($this->require_parent && isset($this->current_cat[0], $this->current_cat[0]->parent)){
            $this->parentCat = get_category($this->current_cat[0]->parent);
        }
        */
    }

    public function generate_title_attribute($title){
        return esc_attr(strip_tags($title));
    }

    /*public function update_exist_subcats($data){
        global $exist_subcats;
        $exist_subcats = !empty($data);
    }*/



    /**/
    private function get_category_level(){
        $parent_separator = ' --- ';
        $parents = get_category_parents($this->category->term_id,false, $parent_separator);
        if(is_wp_error($parents)){
            $parents = '';
        }
        return count(explode($parent_separator, trim($parents)))-1;
    }

    public function get_image_url($term_id = NULL, $size = 'full') {
        if (empty($term_id)) {
            $term_id = $this->category->term_id;
        }

        $taxonomy_image_url = get_option('z_taxonomy_image'.$term_id);

        if(!empty($taxonomy_image_url)){
            if(!empty($size)){
                $parts = explode('.', $taxonomy_image_url);
                $taxonomy_image_url = str_replace('.'.$parts[count($parts)-1], '-' . $size . '.' . $parts[count($parts)-1], $taxonomy_image_url);
            }
            //if(ENC_DEV_MODE)
            $taxonomy_image_url = str_replace(EC_DEV_ROUTE, EC_PROD_ROUTE, $taxonomy_image_url);
        }

        return $taxonomy_image_url;
    }

    public function get_title($title = '', $size = 40){
        $title = empty($title) ? $this->category->name : $title;
        return strlen($title) > $size ? substr($title, 0, $size).'...' : $title;
    }

}