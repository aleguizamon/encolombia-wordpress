<?php
class EC_Cf_Category{
    private static $instance;

    /**
     * EC_Cf_Category constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new EC_Cf_Category();
        }

        return self::$instance;
    }

    public function configure(){
        add_action('admin_init', array($this, 'category_config_init'));
        add_action( 'admin_head', array($this, 'add_style') );
        add_action('edit_term', array($this, 'save_category_pub'));
        add_action('create_term', array($this, 'save_category_pub'));

    }

    public function category_config_init() {
        add_action('category_add_form_fields', array($this, 'add_category_field'));
        add_action('category_edit_form_fields', array($this, 'edit_category_field'));
        add_filter( 'manage_edit-category_columns', array($this, 'category_columns'), 99, 3 );
        add_filter( 'manage_category_custom_column', array($this, 'category_column'), 99, 3 );
    }

    public function add_style(){
        echo '<style type="text/css" media="screen">
            th.column-publication {width:50px;}            
        </style>';
    }

    public function add_category_field(){
        echo '<div class="form-field">
            <label for="enc_is_pub"><input type="checkbox" name="enc_is_pub" id="enc_is_ṕub" value="1" />&nbsp;' . __('Seleccione si desea que la categoría tenga el formato de publicación', 'enc') . '</label>
                                   
        </div>';
    }

    public function edit_category_field($taxonomy) {
        $value = get_option('enc_is_pub-' . $taxonomy->term_id, '0');
        echo '<tr class="form-field">
                <th scope="row" valign="top">¿Es publicación?</th>
                <td>
                    <label for="enc_is_pub"><input type="checkbox" name="enc_is_pub" id="enc_is_ṕub" value="1" '.($value == '1' ? 'checked' : '').' />&nbsp;' . __('Seleccione si desea que la categoría tenga el formato de publicación', 'enc') . '</label>
                </td>
              </tr>';
    }

    public function category_columns( $columns ) {
        $new_columns = array();
        $new_columns['cb'] = $columns['cb'];
        $new_columns['publication'] = __('Es Pub?', 'enc');

        unset( $columns['cb'] );

        return array_merge( $new_columns, $columns );
    }

    public function category_column( $columns, $column, $id ) {
        if ( $column == 'publication' ){
            $value = get_option('enc_is_pub-' . $id, '0');
            $columns = '<div style="text-align:center;">'.($value == '1' ? 'Si' : 'No').'</div>';
        }
        return $columns;
    }

    public function save_category_pub($term_id) {
        if(isset($_POST['taxonomy']) && $_POST['taxonomy'] == 'category'){
            update_option('enc_is_pub-'.$term_id, isset($_POST['enc_is_pub']) ? '1' : '0', NULL);
        }
    }


}