<?php
/**
 * EC_Util_Legacy
 *
 * Clase con funciones de utilidad para soportar algunas funciones del tema anterior con pagebuilder
 *
 * @since 7.0.0
 */
class EC_Util_Legacy {

    /**
     * Checks a page content and tries to determin if a page was build with a pagebuilder (tdc or vc)
     * @param $post WP_Post
     * @return bool
     */
    public static function is_pagebuilder_content($post) {

        if (empty($post->post_content)) {
            return false;
        }

        /**
         * detect the page builder
         * check for the vc_row, evey pagebuilder page must have vc_row in it
         */
        $matches = array();
        //$preg_match_ret = preg_match('/\[.*vc_row.*\]/s', $post->post_content, $matches);
        $preg_match_ret = preg_match('/.*class="vc_row wpb_row vc_row-fluid".*/s', $post->post_content, $matches);
        if ($preg_match_ret !== 0 && $preg_match_ret !== false ) {
            return true;
        }

        return false;
    }

    static function is_shortcode_ultimate($post) {
        if (empty($post->post_content)) {
            return false;
        }

        /**
         * detect the page builder
         * check for the vc_row, evey pagebuilder page must have vc_row in it
         */
        $matches = array();
        $preg_match_ret = preg_match('/\[.*su_.*\]/s', $post->post_content, $matches);
        if ($preg_match_ret !== 0 && $preg_match_ret !== false ) {
            return true;
        }

        return false;
    }



    /**
     * Helper function that adjusts lightness of a given HEX color value.
     *
     * Examples of use:
     * `su_adjust_lightness( '#fc0', 50 )` - increase color lightness by 50%
     * `su_adjust_lightness( 'ffcc00', -50 )` - decrease color lightness by 50%
     *
     * @since  5.6.0
     * @param  string $color A valid HEX color
     * @param  int $percent  The percent to adjust lightness to.
     * @return string        Adjusted HEX color value.
     */
    public static function su_adjust_lightness( $color, $percent ) {

        if (
            ! self::su_is_valid_hex( $color ) ||
            ! is_numeric( $percent )
        ) {
            return $color;
        }

        $percent   = max( -100, min( 100, $percent ) );
        $color     = ltrim( $color, '#' );
        $new_color = '#';

        if ( 3 === strlen( $color ) ) {
            $color = self::su_expand_short_color( $color );
        }

        $color = array_map( 'hexdec', str_split( $color, 2 ) );

        foreach ( $color as $part ) {

            $limit  = $percent < 0 ? $part : 255 - $part;
            $amount = ceil( $limit * $percent / 100 );

            $new_color .= str_pad( dechex( $part + $amount ), 2, '0', STR_PAD_LEFT );

        }

        return $new_color;

    }

    /**
     * Helper function that expands 3-sybol string into 6-sybol by repeating each
     * symbol twice.
     *
     * @since  5.6.0
     * @param  string $hex Short value.
     * @return string      Expanded value.
     */
    public static function su_expand_short_color( $value ) {

        if ( ! is_string( $value ) || 3 !== strlen( $value ) ) {
            return $value;
        }

        return $value[0] . $value[0] . $value[1] . $value[1] . $value[2] . $value[2];

    }

    /**
     * Custom do_shortcode function for nested shortcodes
     *
     * @since  5.0.4
     * @param string  $content Shortcode content.
     * @param string  $pre     First shortcode letter.
     * @return string          Formatted content.
     */
    public static function su_do_nested_shortcodes_alt( $content, $pre ) {

        if ( strpos( $content, '[_' ) !== false ) {
            $content = preg_replace( '@(\[_*)_(' . $pre . '|/)@', '$1$2', $content );
        }

        return do_shortcode( $content );

    }

    /**
     * Remove underscores from nested shortcodes.
     *
     * @since  5.0.4
     * @param string  $content   String with nested shortcodes.
     * @param string  $shortcode Shortcode tag name (without prefix).
     * @return string            Parsed string.
     */
    public static function su_do_nested_shortcodes( $content, $shortcode ) {

        //if ( get_option( 'su_option_do_nested_shortcodes_alt' ) ) {
        //    return self::su_do_nested_shortcodes_alt( $content, substr( $shortcode, 0, 1 ) );
        //}

        $prefix = 'su_';

        if ( strpos( $content, '[_' . $prefix . $shortcode ) !== false ) {

            $content = str_replace(
                array( '[_' . $prefix . $shortcode, '[_/' . $prefix . $shortcode ),
                array( '[' . $prefix . $shortcode, '[/' . $prefix . $shortcode ),
                $content
            );

            return do_shortcode( $content );

        }

        return do_shortcode( wptexturize( $content ) );

    }

    /**
     * Do shortcodes in attributes.
     *
     * Replace braces with square brackets: {shortcode} => [shortcode], applies do_shortcode() filter.
     *
     * @since  5.0.5
     * @param string  $value Attribute value with shortcodes.
     * @return string        Parsed string.
     */
    public static function su_do_attribute( $value ) {

        $value = str_replace( array( '{', '}' ), array( '[', ']' ), $value );
        $value = do_shortcode( $value );

        return $value;

    }

    /**
     * Helper function that adjusts brightness of a given HEX color value.
     *
     * Examples of use:
     * `su_adjust_brightness( '#fc0', 50 )` - increase color brightness by 50%
     * `su_adjust_brightness( 'ffcc00', -50 )` - decrease color brightness by 50%
     *
     * @since  5.2.0
     * @param  string $color A valid HEX color
     * @param  int $percent  The percent to adjust brightness to.
     * @return string        Adjusted HEX color value.
     */
    public static function su_adjust_brightness( $color, $percent ) {

        if (
            ! self::su_is_valid_hex( $color ) ||
            ! is_numeric( $percent )
        ) {
            return $color;
        }

        $percent = max( -100, min( 100, $percent ) );
        $steps   = round( $percent * 2.55 );
        $color   = ltrim( $color, '#' );

        if ( 3 === strlen( $color ) ) {
            $color = self::su_expand_short_color( $color );
        }

        $color_parts = str_split( $color, 2 );
        $new_color   = '#';

        foreach ( $color_parts as $color_part ) {

            $color_part = hexdec( $color_part );
            $color_part = max( 0, min( 255, $color_part + $steps ) );

            $new_color .= str_pad( dechex( $color_part ), 2, '0', STR_PAD_LEFT );

        }

        return $new_color;

    }

    /**
     * Helper function to check validity of a given HEX color.
     *
     * Valid formats are:
     * - #aabbcc
     * - aabbcc
     * - #abc
     * - abc
     *
     * @since  5.2.0
     * @param  string $color HEX color to check validity of.
     * @return bool          True if a given color mathes accepted pattern, False otherwise.
     */
    public static function su_is_valid_hex( $color ) {
        return preg_match( '/^#([a-f0-9]{3}){1,2}\b$/i', $color ) === 1;
    }
}