<?php
/**
 * EC_Util
 *
 * Clase con funciones de utilidad
 *
 * @since 7.0.0
 */
class EC_Util{

    /**
     * Retorna el URI de la petición actual
     *
     * @return string
     */
    public static function get_request_uri(){
        //$uri = '';
        /*if (wp_doing_ajax()) {
            if(isset($GLOBALS['enc_config'], $GLOBALS['enc_config']['ajax_request_uri'])){
                $uri = $GLOBALS['enc_config']['ajax_request_uri'];
            }
        } else {
            $uri = $_SERVER['REQUEST_URI'];
        }*/
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * Indica si el $section se encuentra en el URI de la petición actual
     *
     * @param $section
     * @return bool
     */
    public static function is_uri_section($section){
        return strpos(self::get_request_uri(), trim($section)) !== false;
    }

    /**
     * Indica si el $section es exactamente igual al URI de la petición actual
     *
     * @param $section
     * @return bool
     */
    public static function is_exact_uri_section($section){
        return self::get_request_uri() == $section;
    }

    /**
     * Indica si algún elemento del array $urls se encuentra en el URI de la petición actual
     *
     * @param $urls
     * @return bool
     */
    public static function exist_uri_in_array($urls) {
        $enc = false; $i=0;
        while(!$enc && $i<count($urls)){
            if(trim($urls[$i]) != ''){
                $enc = self::is_uri_section($urls[$i]);
            }
            $i++;
        }
        return $enc;
    }

    /**
     * Indica si el URI de la petición actual es de una página de la sección de Genfar
     *
     * @return bool
     */
    static public function is_genfar_section(){
        $urls = array(
            '/medicina-2/vademecum/',
            '/medicina/vademecum/genfar/',
            '/medicina/estudios-bioquivalencia/'
        );
        return self::exist_uri_in_array($urls);
    }

    /**
     * Indica si el URI de la petición actual es de una página de clientes
     *
     * @return bool
     */
    public static function is_customer_section(){
        global $wpdb;
        $queryResult = $wpdb->get_results( "SELECT option_value  FROM `ecs_options` WHERE `option_name` LIKE 'options_rutas_de_clientes'");
        $convertResult = explode('<br />', trim(nl2br($queryResult[0]->option_value)));
        return self::exist_uri_in_array($convertResult);
    }

    /**
     * Indica si el URI de la petición actual es de una página donde se deba forzar la imagen destacada
     *
     * @return bool
     */
    public static function is_url_force_featured_image(){
        $lock_urls = array(
            '/medicina/vademecum/genfar/antiinfecciosos-sistemicos/',
            '/medicina/vademecum/genfar/dermatologicos/',
            '/medicina/vademecum/genfar/medicamentos-glucocorticoides/',
            '/medicina/vademecum/genfar/parasitologia/',
            '/medicina/vademecum/genfar/hematopoyetico/',
            '/medicina/vademecum/genfar/cardiovascular/',
            '/medicina/vademecum/genfar/genitourinario/',
            '/medicina/vademecum/genfar/musculo-esqueletico/',
            '/medicina/vademecum/genfar/nervioso-central/',
            '/medicina/vademecum/genfar/respiratorio/',
            '/medicina/vademecum/genfar/tracto-alimentario/'
        );
        return self::exist_uri_in_array($lock_urls);
    }

    /**
     * Indica si el URI de la petición actual es de una página que no deba tener el scroll infinito
     *
     * @return bool
     */
    public static function is_exception_infinite_scroll(){
        $lock_urls = array(
            '/medicina/vademecum/genfar/vademecum-genfar/',
            '/medicina/vademecum/genfar/genfar-rx/',
            '/medicina/vademecum/genfar/genfar-rx1/',
        );
        return self::exist_uri_in_array($lock_urls);
    }

    /**
     * Renderiza un mensaje cuando no se encuentran posts para mostrar
     *
     * @return string
     */
    static public function no_posts() {
        $buffy = '<div class="no-results td-pb-padding-side">';
        $buffy .= '<h2>' . __('No se encontraron posts para mostrar', 'EC') . '</h2>';
        $buffy .= '</div>';
        return $buffy;
    }

    /**
     * Shows a soft error. The site will run as usual if possible. If the user is logged in and has 'switch_themes'
     * privileges this will also output the caller file path
     * @param $file - The file should be __FILE__
     * @param $message
     */
    public static function error($file, $message, $more_data = '') {
        echo '<br><br>Theme Error:<br>';
        echo $message;
        if (is_user_logged_in() and current_user_can('switch_themes')){
            echo '<br>' . $file;
            if (!empty($more_data)) {
                echo '<br><br><pre>';
                echo 'more data:' . PHP_EOL;
                print_r($more_data);
                echo '</pre>';
            }
        };
    }

    /**
     * returns a string containing the numbers of words or chars for the content
     *
     * @param $post_content - the content thats need to be cut
     * @param $limit        - limit to cut
     * @param string $show_shortcodes - if shortcodes
     * @return string
     */
    static function excerpt($post_content, $limit, $show_shortcodes = '') {
        //REMOVE shortscodes and tags
        if ($show_shortcodes == '') {
            // strip_shortcodes(); this remove all shortcodes and we don't use it, is nor ok to remove all shortcodes like dropcaps
            // this remove the caption from images
            $post_content = preg_replace("/\[caption(.*)\[\/caption\]/i", '', $post_content);
            // this remove the shortcodes but leave the text from shortcodes
            $post_content = preg_replace('`\[[^\]]*\]`','',$post_content);
        }
        //wp_strip_all_tags
        //$post_content = stripslashes(wp_filter_nohtml_kses($post_content));
        $post_content = stripslashes(wp_strip_all_tags($post_content));

        // remove the youtube link from excerpt
        //$post_content = preg_replace('~(?:http|https|)(?::\/\/|)(?:www.|)(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/ytscreeningroom\?v=|\/feeds\/api\/videos\/|\/user\S*[^\w\-\s]|\S*[^\w\-\s]))([\w\-]{11})[a-z0-9;:@?&%=+\/\$_.-]*~i', '', $post_content);

        $ret_excerpt = mb_substr($post_content, 0, $limit);
        if (mb_strlen($post_content)>=$limit) {
            $ret_excerpt = $ret_excerpt.'...';
        }


        return $ret_excerpt;
    }

    /**
     * Cuenta el número de párrafos en $content
     *
     * @param $content
     * @return int
     */
    public static function count_paragraphs($content){
        //$pattern = "/<p>.*?<\/p>/gm"; // Global & Multiline
        return count(explode('</p>', $content))-1;//preg_match_all($pattern,$content);
    }

    /**
     * Devuelve la IP del usuario
     *
     * @return mixed
     */
    public static function get_user_ip(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    /**
     * Carga una plantilla
     *
     * @param $folder
     * @param $name
     * @return void
     */
    public static function load_template($folder, $name){
        if ( $overridden_template = locate_template( $name ) ) {
            load_template( $overridden_template );
        } else {
            load_template( $folder . $name );
        }
    }

    /**
     * Remueve los tags <div> y <aside> de $str
     *
     * @param $str
     * @return array|string|string[]|null
     */
    static function remove_div_tag($str){
        return preg_replace('/\<[\/]{0,1}aside[^\>]*\>/i', '', preg_replace('/\<[\/]{0,1}div[^\>]*\>/i', '', $str));
    }

    /**
     * Inserta el código del anuncio en el contenido
     *
     * @param $content
     * @param $ads_data
     * @return string
     */
    static public function insert_ads_in_content($content, $ads_data){
        $closing_p = '</p>';
        $paragraphs = explode( $closing_p, $content );
        $ind = 0;
        $final_paragraphs = array();
        foreach ($paragraphs as $index => $paragraph) {
            if ( trim( $paragraph ) ) {
                $final_paragraphs[$ind] = $paragraph . $closing_p;
                foreach($ads_data as $data){
                    if ( $data['index'] == $ind + 1 ) {
                        $final_paragraphs[$ind] .= $data['ad'];
                    }
                }
                $ind++;
            }
        }

        return implode( '', $final_paragraphs );
    }

    /**
     * Devuelve el nivel de la categoría indica por $category_id
     *
     * @param $category_id
     * @return int
     */
    public static function get_category_level($category_id = 0) {
        // Obtener la categoría actual si no se proporciona un ID
        if (empty($category_id)) {
            $category = get_queried_object();
            if ($category instanceof WP_Term) {
                $category_id = $category->term_id;
            }
        }

        // Verificar si se encontró un ID de categoría válido
        if (!empty($category_id)) {
            $ancestors = get_ancestors($category_id, 'category');
            //echo '<pre>';print_r($ancestors);echo '</pre>';
            // El nivel es igual a la cantidad de ancestros
            $level = count($ancestors);

            // Retornar el nivel de la categoría
            return $level;
        }

        // Si no se encuentra una categoría válida, retornar 0
        return 0;
    }

    /**
     * Devuelve el tipo de página actual (posts|category|home)
     *
     * @return string
     */
    public static function get_page_type(){
        $page_type = 'none';
        if(is_home() || is_front_page()){
            $page_type = 'home';
        } else if(is_category()){
            $page_type = 'category';
        } else if(is_single()){
            $page_type = 'post';
        } else if(is_page()){
            $page_type = 'page';
        } else if(is_search()){
            $page_type = 'search';
        } else if(is_tag()){
            $page_type = 'tag';
        } else if(is_tax()) {
            $page_type = 'tax';
        } else if(is_author()){
            $page_type = 'author';
        } else if(is_404()){
            $page_type = '404';
        }
        return $page_type;
    }
}
