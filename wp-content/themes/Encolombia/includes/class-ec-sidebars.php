<?php
/**
 * EC_Sidebars
 *
 * Clase para gestionar los sidebars del tema
 *
 * @since 7.0.0
 */

class EC_Sidebars{

    private static $instance;

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new EC_Sidebars();
        }

        return self::$instance;
    }

    private $sidebars = array();

    /**
     * EC_Sidebars constructor.
     */
    private function __construct()
    {
        $this->sidebars = array(
            'segundo-widget-area' => array(
                'name' => __( 'SEGUNDO CONTENIDO', 'EC' ),
                'description' => __( 'Segunda área de contenido del sidebar', 'EC' ),
                'head' => false,
                'paragraph' => ''
            ),
            // WIDGETS DE CONTENIDO EN SIDEBAR DERECHA
            'tercer-widget-area' => array(
                'name' => __( 'TERCER CONTENIDO', 'EC' ),
                'description' => __( 'Tercera área de contenido del sidebar', 'EC' ),
                'head' => false,
                'paragraph' => ''
            ),
            // Area 3, located befores the content. Empty by default.
            'after-content-widget-area' => array(
                'name' => __( 'Inferior 336x280', 'EC' ),
                'description' => __( 'After content widget area', 'EC' ),
                'head' => false,
                'paragraph' => '',
                'admanager_head' => 'adm_head_inferior'
            ),

            //HEAD
            'head-home-md'  => array(
                'name' => __( 'Head de la página en el home', 'EC' ),
                'description' => __( 'Coloca los scripts en el head del home.', 'EC' ),
                'head' => true,
                'paragraph' => '',
                'device' => 'MD'
            ),
            'enc_head_desktop' => array(
                'name' => __( 'Head Desktop', 'EC' ),
                'description' => __( 'Head Desktop', 'EC' ),
                'head' => true,
                'paragraph' => '',
                'device' => 'D'
            ),
            'enc_head_mobile' => array(
                'name' => __( 'Head Mobile', 'EC' ),
                'description' => __( 'Head Mobile', 'EC' ),
                'head' => true,
                'paragraph' => '',
                'device' => 'M'
            ),

            //DESKTOP
            'enc_top' => array(
                'name' => __( 'ENC_Top: Superior Horizontal Desktop', 'EC' ),
                'description' => __( 'Superior de los posts sólo para desktop en posts y categorías', 'EC' ),
                'head' => false,
                'paragraph' => '',
                'device' => 'D'
            ),
            'enc_inline1' => array(
                'name' => __( 'Enc_Inline1 - Desktop', 'EC' ),
                'description' => __( 'Se muestra en los posts y en las categorías en desktop', 'EC' ),
                'head' => false,
                'paragraph' => 2,
                'row' => 1,
                'device' => 'D'
            ),
            'enc_inline2' => array(
                'name' => __( 'Enc_Inline2 - Desktop', 'EC' ),
                'description' => __( 'Se muestra en los posts y en las categorías en desktop', 'EC' ),
                'head' => false,
                'paragraph' => 6,
                'row' => 2,
                'device' => 'D'
            ),
            'enc_inline3' => array(
                'name' => __( 'Enc_Inline3 - Desktop', 'EC' ),
                'description' => __( 'Se muestra en los posts y en las categorías en desktop', 'EC' ),
                'head' => false,
                'paragraph' => 9,
                'row' => 4,
                'device' => 'D'
            ),
            'enc_inline4' => array(
                'name' => __( 'Enc_Inline4 - Desktop', 'EC' ),
                'description' => __( 'Se muestra en los posts y en las categorías en desktop', 'EC' ),
                'head' => false,
                'paragraph' => 14,
                'row' => 5,
                'device' => 'D'
            ),
            'enc_inline5' => array(
                'name' => __( 'Enc_Inline5 - Desktop', 'EC' ),
                'description' => __( 'Se muestra en los posts y en las categorías en desktop', 'EC' ),
                'head' => false,
                'paragraph' => 16,
                'row' => 6,
                'device' => 'D'
            ),
            'enc_inline6' => array(
                'name' => __( 'Enc_Inline6 - Desktop', 'EC' ),
                'description' => __( 'Se muestra en los posts y en las categorías en desktop', 'EC' ),
                'head' => false,
                'paragraph' => 20,
                'row' => 7,
                'device' => 'D'
            ),
            'enc_sidebar1' => array(
                'name' => __( 'ENC_Sidebar1 - Desktop', 'EC' ),
                'description' => __( 'Sidebar posición 1 en escritorio', 'EC' ),
                'head' => false,
                'device' => 'D'
            ),
            'enc_sidebar2' => array(
                'name' => __( 'ENC_Sidebar2 - Desktop', 'EC' ),
                'description' => __( 'Sidebar posición 2 en escritorio', 'EC' ),
                'head' => false,
                'device' => 'D'
            ),
            'enc_video' => array(
                'name' => __( 'ENC_Video: despues del parrafo 3 en desktop y móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts después del párrafo 3 en desktop y movil', 'EC' ),
                'head' => false,
                'paragraph' => 3,
                'paragraph_m' => 3,
                'row' => 3,
                'row_m' => 5,
                'device' => 'MD'
            ),

            //MOBILE
            'enc_mob_top' => array(
                'name' => __( 'ENC_Mob_Top: Superior Horizontal Móvil', 'EC' ),
                'description' => __( 'Superior en categorías y posts para móvil', 'EC' ),
                'head' => false,
                'paragraph' => '',
                'device' => 'M'
            ),
            'enc_mob_inline1' => array(
                'name' => __( 'ENC_Mob_Inline1 - Móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts y categorías en móvil', 'EC' ),
                'head' => false,
                'paragraph_m' => 1,
                'row_m' => 1,
                'device' => 'M'
            ),
            'enc_mob_inline2' => array(
                'name' => __( 'ENC_Mob_Inline2 - Móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts y categorías en móvil', 'EC' ),
                'head' => false,
                'paragraph_m' => 2,
                'row_m' => 3,
                'device' => 'M'
            ),
            'enc_mob_inline3' => array(
                'name' => __( 'ENC_Mob_Inline3 - Móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts y categorías en móvil', 'EC' ),
                'head' => false,
                'paragraph_m' => 4,
                'row_m' => 8,
                'device' => 'M'
            ),
            'enc_mob_inline4' => array(
                'name' => __( 'ENC_Mob_Inline4 - Móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts y categorías en móvil', 'EC' ),
                'head' => false,
                'paragraph_m' => 6,
                'row_m' => 10,
                'device' => 'M'
            ),
            'enc_mob_inline5' => array(
                'name' => __( 'ENC_Mob_Inline5 - Móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts y categorías en móvil', 'EC' ),
                'head' => false,
                'paragraph_m' => 7,
                'row_m' => 12,
                'device' => 'M'
            ),
            'enc_mob_inline6' => array(
                'name' => __( 'ENC_Mob_Inline6 - Móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts y categorías en móvil', 'EC' ),
                'head' => false,
                'paragraph_m' => 9,
                'row_m' => 14,
                'device' => 'M'
            ),
            'enc_mob_inline7' => array(
                'name' => __( 'ENC_Mob_Inline7 - Móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts y categorías en móvil', 'EC' ),
                'head' => false,
                'paragraph_m' => 11,
                'row_m' => 16,
                'device' => 'M'
            ),
            'enc_mob_inline8' => array(
                'name' => __( 'ENC_Mob_Inline8 - Móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts y categorías en móvil', 'EC' ),
                'head' => false,
                'paragraph_m' =>13,
                'row_m' => 18,
                'device' => 'M'
            ),

            //BOTONES
            'enc_boton_1' => array(
                'name' => __( 'Botón 1: P10 escritorio y P5 móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts, en el párrafo 10 en escritorio y en el párrafo 5 en móvil.', 'EC' ),
                'head' => false,
                'paragraph' => 10,
                'paragraph_m' => 5,
            ),
            'enc_boton_2' => array(
                'name' => __( 'Botón 2: P15 escritorio y P7 móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts, en el párrafo 15 en escritorio y en el párrafo 10 en móvil.', 'EC' ),
                'head' => false,
                'paragraph' => 15,
                'paragraph_m' => 10,
            ),
            'enc_boton_3' => array(
                'name' => __( 'Botón 3: P15 escritorio y P7 móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts, en el párrafo 18 en escritorio y en el párrafo 15 en móvil.', 'EC' ),
                'head' => false,
                'paragraph' => 18,
                'paragraph_m' => 15,
            ),

            //SPONSORED POSTS
            'adm_head_sponsored_posts' => array(
                'name' => __( 'AdManager head Sponsored Posts', 'EC' ),
                'description' => __( 'AdManager script head Sponsored posts', 'EC' ),
                'head' => true,
                'paragraph' => '',
                'device' => 'MD'
            ),
            'enc_sponsoredpost_top' => array(
                'name' => __( 'ENC_SponsoredPost_Top: Superior Horizontal', 'EC' ),
                'description' => __( 'Superior de los posts', 'EC' ),
                'head' => false,
                'paragraph' => ''
            ),
            'enc_sponsoredpost_inline_pos1' => array(
                'name' => __( 'ENC_SponsoredPost_Inline_Pos1: P3 escritorio y P2 móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts, en el párrafo 3 en escritorio y en el párrafo 2 en móvil.', 'EC' ),
                'head' => false,
                'paragraph' => 2,
                'paragraph_m' => 1,
            ),
            'enc_sponsoredpost_inline_pos2' => array(
                'name' => __( 'ENC_SponsoredPost_Inline_Pos2: P7 escritorio y P3 móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts, en el párrafo 7 en escritorio y en el párrafo 3 en móvil.', 'EC' ),
                'head' => false,
                'paragraph' => 6,
                'paragraph_m' => 2,
            ),
            'enc_sponsoredpost_rr_pos1' => array(
                'name' => __( 'ENC_SponsoredPost_RR_Pos1', 'EC' ),
                'description' => __( 'Sidebar posición 1 en escritorio y en posts párrafo 10 en móvil', 'EC' ),
                'head' => false,
                'paragraph_m' => 9,
            ),
            'enc_sponsoredpost_rr_pos2' => array(
                'name' => __( 'ENC_SponsoredPost_RR_Pos2', 'EC' ),
                'description' => __( 'Sidebar posición 2 en escritorio y en posts párrafo 12 en móvil', 'EC' ),
                'head' => false,
                'paragraph_m' => 11,
            ),
            'enc_sponsoredpost_rr_pos3' => array(
                'name' => __( 'ENC_SponsoredPost_RR_Pos3', 'EC' ),
                'description' => __( 'Sidebar posición 3 en escritorio y en posts párrafo 14 en móvil', 'EC' ),
                'head' => false,
                'paragraph_m' =>13,
            ),
            'enc_sponsoredvideo' => array(
                'name' => __( 'ENC_SponsoredVideo: despues del parrafo 3 en desktop y móvil', 'EC' ),
                'description' => __( 'Se muestra en los posts después del párrafo 3 en desktop y movil', 'EC' ),
                'head' => false,
                'paragraph' => 7,
                'paragraph_m' => 7,
                'row' => 6,
                'row_m' => 6,
            ),

            //SPONSORED CATEGORIAS
            'adm_head_sponsored_category' => array(
                'name' => __( 'AdManager head Sponsored Category', 'EC' ),
                'description' => __( 'AdManager script head sponsored categories', 'EC' ),
                'head' => true,
                'paragraph' => '',
                'device' => 'MD'
            ),
            'enc_sponsoredcategory_top' => array(
                'name' => __( 'ENC_SponsoredCategory_Top: Superior Horizontal 728 x 90', 'EC' ),
                'description' => __( 'Superior de las categorías', 'EC' ),
                'head' => false,
                'paragraph' => ''
            ),
            'enc_sponsoredcategory_inline_pos1' => array(
                'name' => __( 'ENC_SponsoredCategory_Inline_Pos1: Pos x en escritorio y pos x en móvil', 'EC' ),
                'description' => __( 'Se muestra en páginas de categorías despues de la posición x en escritorio y despues de la posición x en movil', 'EC' ),
                'head' => false,
                'row' => 1,
                'row_m' => 2,
            ),
            'enc_sponsoredcategory_inline_pos2' => array(
                'name' => __( 'ENC_SponsoredCategory_Inline_Pos2: Pos x en escritorio y pos x en móvil', 'EC' ),
                'description' => __( 'Se muestra en páginas de categorías despues de la posición x en escritorio y despues de la posición x en movil', 'EC' ),
                'head' => false,
                'row' => 2,
                'row_m' => 4,
            ),
            'enc_sponsoredcategory_rr_pos1' => array(
                'name' => __( 'ENC_SponsoredCategory_RR_Pos1', 'EC' ),
                'description' => __( 'Sidebar posición 1 en escritorio y en posts párrafo 10 en móvil', 'EC' ),
                'head' => false,
                'row' => 4,
                'row_m' => 6,
            ),
            'enc_sponsoredcategory_rr_pos2' => array(
                'name' => __( 'ENC_SponsoredCategory_RR_Pos2', 'EC' ),
                'description' => __( 'Sidebar posición 2 en escritorio y en posts párrafo 12 en móvil', 'EC' ),
                'head' => false,
                'row' => 6,
                'row_m' => 8,
            ),
            'enc_sponsoredcategory_rr_pos3' => array(
                'name' => __( 'ENC_SponsoredCategory_RR_Pos3', 'EC' ),
                'description' => __( 'Sidebar posición 3 en escritorio y en posts párrafo 14 en móvil', 'EC' ),
                'head' => false,
                'row' => 8,
                'row_m' => 10,
            ),
        );
    }

    public function get_sidebars(){
        return $this->sidebars;
    }

    public function get_sidebar_info($id){
        return isset($this->sidebars[$id]) ? $this->sidebars[$id] : null;
    }

    public function register(){
        foreach ($this->sidebars as $key => $sb){
            $style = '';
            switch($key) {
                case 'enc_inline1':
                case 'enc_inline2':
                case 'enc_inline3':
                case 'enc_inline4':
                case 'enc_post_inline_pos1':
                case 'enc_post_inline_pos2':
                case 'enc_post_inline_pos3':
                case 'enc_post_inline_pos4':
                case 'enc_post_inline_pos5':
		            $style = 'style="min-height: '.(EC()->is_mobile ? '288px' : '90px').';"';
                                      
 		        break;
                case 'enc_video':
                    $style = 'style="min-height: '.(EC()->is_mobile ? '200px' : '460px').';"';
                    break;

            }
            register_sidebar( array(
                'name' => $sb['name'],
                'id' => $key,
                'description' => $sb['description'],
                'before_widget' => $sb['head'] ? '' : '<aside class="widget %2$s ' . $key . '" ' . $style . '>',
                'after_widget' => $sb['head'] ? '' : '</aside>',
                'before_title' => '<div class="block-title"><span>',
                'after_title' => '</span></div>'
            ) );
        }
    }

    public function render_sidebar($id){
        //var_dump(is_active_sidebar( $id ));
        if(is_active_sidebar( $id )):
            dynamic_sidebar( $id );
        endif;
    }

    public function get_sidebar_code($id){
        ob_start();
        $this->render_sidebar($id);
        return ob_get_clean();
    }
}
