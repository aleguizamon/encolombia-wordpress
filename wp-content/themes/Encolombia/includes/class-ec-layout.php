<?php
/**
 * EC_Layout
 *
 * Clase con funciones de renderizado de algunos elementos de la plantilla
 *
 * @since 7.0.0
 */
class EC_Layout  {
    private static $instance;

    /**
     * EC_Layout constructor.
     */
    private function __construct()
    {

    }

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new EC_Layout();
        }
        return self::$instance;
    }

    public function render_site_logo(){
        $output = '';
        if(EC()->is_mobile){
            $enc_customLogo = EC_LOGO_URL_M;
            $enc_customLogoR = EC_LOGO_RETINA_URL_M;
        } else {
            $enc_customLogo = EC_LOGO_URL;
            $enc_customLogoR = EC_RETINA_LOGO_URL;
        }
        $enc_use_h1_logo = false;
        /*if (EC()->page_type === 'home') {
            $enc_use_h1_logo = true;
        }*/
        if($enc_use_h1_logo === true) {
            $output.= '<h1 class="enc-logo">';
        }
        $output .= '<div class="enc-header-logo" itemscope="itemscope" itemtype="https://schema.org/Organization">';
        $output.= '<a class="enc-main-logo" itemprop="url" href="' . esc_url(home_url( '/' )) . '">';
        $output.= '<img src="'.esc_attr($enc_customLogo).'" width="200" height="42" srcset="'.esc_attr($enc_customLogo) . ', '.esc_attr($enc_customLogoR) . ' 2x" alt="ENCOLOMBIA" title="ENCOLOMBIA.COM"/>';
        $output.= '</a>';
        $output.= '<meta itemprop="name" content="'.get_bloginfo( 'name', 'display' ) . '">';

        if($enc_use_h1_logo === true) {
            $output.= '</h1>';
        }
        $output .= '</div>';
        return $output;
    }

    /**
     * Renderiza la paginación de posts o categorías
     *
     * @param $current_category_name
     * @return void
     */
    public function render_pagination($current_category_name = '') {
        global $wp_query;

        $pagenavi_options = $this->pagenavi_init();
        //$request = $wp_query->request;
        //$posts_per_page = intval(get_query_var('posts_per_page'));
        $paged = intval(get_query_var('paged'));
        //$numposts = $wp_query->found_posts;
        $max_page = $wp_query->max_num_pages;
        /*if(!is_admin() and is_category()) {
	        $posts_shown_in_loop = enc_params::$posts_per_page;
          $numposts = $wp_query->found_posts - $posts_shown_in_loop; // fix the pagination, we have x less posts because the rest are in the top posts loop
          $max_page = ceil($numposts / $posts_per_page);
        }*/
        if(empty($paged) || $paged == 0) {
            $paged = 1;
        }

        $pages_to_show = intval($pagenavi_options['num_pages']);
        $larger_page_to_show = intval($pagenavi_options['num_larger_page_numbers']);
        $larger_page_multiple = intval($pagenavi_options['larger_page_numbers_multiple']);
        $pages_to_show_minus_1 = $pages_to_show - 1;
        $half_page_start = floor($pages_to_show_minus_1/2);
        $half_page_end = ceil($pages_to_show_minus_1/2);
        $start_page = $paged - $half_page_start;
        if($start_page <= 0) {
            $start_page = 1;
        }
        $end_page = $paged + $half_page_end;
        if(($end_page - $start_page) != $pages_to_show_minus_1) {
            $end_page = $start_page + $pages_to_show_minus_1;
        }
        if($end_page > $max_page) {
            $start_page = $max_page - $pages_to_show_minus_1;
            $end_page = $max_page;
        }
        if($start_page <= 0) {
            $start_page = 1;
        }
        $larger_per_page = $larger_page_to_show*$larger_page_multiple;
        $larger_start_page_start = ($this->enc_round_number($start_page, 10) + $larger_page_multiple) - $larger_per_page;
        $larger_start_page_end = $this->enc_round_number($start_page, 10) + $larger_page_multiple;
        $larger_end_page_start = $this->enc_round_number($end_page, 10) + $larger_page_multiple;
        $larger_end_page_end = $this->enc_round_number($end_page, 10) + ($larger_per_page);
        if($larger_start_page_end - $larger_page_multiple == $start_page) {
            $larger_start_page_start = $larger_start_page_start - $larger_page_multiple;
            $larger_start_page_end = $larger_start_page_end - $larger_page_multiple;
        }
        if($larger_start_page_start <= 0) {
            $larger_start_page_start = $larger_page_multiple;
        }
        if($larger_start_page_end > $max_page) {
            $larger_start_page_end = $max_page;
        }
        if($larger_end_page_end > $max_page) {
            $larger_end_page_end = $max_page;
        }

        if($max_page > 1 || intval($pagenavi_options['always_show']) == 1) {
            echo '<div class="enc-title-pagination">VER MÁS: '.$current_category_name.'</div>';
            $pages_text = str_replace("%CURRENT_PAGE%", number_format_i18n($paged), $pagenavi_options['pages_text']);
            $pages_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pages_text);
            echo '<div class="enc-page-nav enc-padding-side">';
            echo'<div class="page-buttons">';
            previous_posts_link($pagenavi_options['prev_text']);
            if ($start_page >= 2 && $pages_to_show < $max_page) {
                $first_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['first_text']);
                echo '<a href="'.esc_url(get_pagenum_link()).'" class="first" title="'.$first_page_text.'">'.$first_page_text.'</a>';
                if(!empty($pagenavi_options['dotleft_text']) && ($start_page > 2)) {
                    echo '<span class="extend">'.$pagenavi_options['dotleft_text'].'</span>';
                }
            }
            for($i = $start_page; $i  <= $end_page; $i++) {
                if($i == $paged) {
                    $current_page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['current_text']);
                    echo '<span class="current">'.$current_page_text.'</span>';
                } else {
                    $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
                    echo '<a href="'.esc_url(get_pagenum_link($i)).'" class="page" title="'.$page_text.'">'.$page_text.'</a>';
                }
            }
            if ($end_page < $max_page) {
                if(!empty($pagenavi_options['dotright_text']) && ($end_page + 1 < $max_page)) {
                    echo '<span class="extend">'.$pagenavi_options['dotright_text'].'</span>';
                }
                $last_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['last_text']);
                echo '<a href="'.esc_url(get_pagenum_link($max_page)).'" class="last" title="'.$last_page_text.'">'.$last_page_text.'</a>';
            }
            next_posts_link($pagenavi_options['next_text'], $max_page);
            echo '</div>';
            if(!empty($pages_text)) {
                echo '<span class="pages">'.$pages_text.'</span>';
            }
            echo '<div class="clearfix"></div>';
            echo '</div>';
        }
    }

    /**
     * Inicializa las opciones de paginación
     *
     * @return array
     */
    private function pagenavi_init() {
        $pagenavi_options = array();
        $pagenavi_options['pages_text'] = __('Página %CURRENT_PAGE% de %TOTAL_PAGES%');
        $pagenavi_options['current_text'] = '%PAGE_NUMBER%';
        $pagenavi_options['page_text'] = '%PAGE_NUMBER%';
        $pagenavi_options['first_text'] = __('1');
        $pagenavi_options['last_text'] = __('%TOTAL_PAGES%');
        if (is_rtl()) {
            $pagenavi_options['next_text'] = '<i class="enc-icon-menu-right"></i>';
            $pagenavi_options['prev_text'] = '<i class="enc-icon-menu-left"></i>';
        } else {
            $pagenavi_options['next_text'] = '<img src="'.get_template_directory_uri().'/assets/images/arrow-right.png" width="24px" height="24px" alt="Página siguiente" />';//'<i class="enc-icon-menu-right"></i>';
            $pagenavi_options['prev_text'] = '<img src="'.get_template_directory_uri().'/assets/images/arrow-left.png" width="24px" height="24px" alt="Página anterior" />';//'<i class="enc-icon-menu-left"></i>';
        }
        $pagenavi_options['dotright_text'] = __('...');
        $pagenavi_options['dotleft_text'] = __('...');
        $pagenavi_options['num_pages'] = 3;
        $pagenavi_options['always_show'] = 1;
        $pagenavi_options['num_larger_page_numbers'] = 3;
        $pagenavi_options['larger_page_numbers_multiple'] = 1000;
        return $pagenavi_options;
    }

    /**
     * Redondea un número al más cercano
     *
     * @param $num
     * @param $tonearest
     * @return float|int
     */
    private function enc_round_number($num, $tonearest) {
        return floor($num/$tonearest)*$tonearest;
    }

    public function site_networks($class = 'home-social-container', $footer = false){
        $output = '<div class="'.$class.'">
                    <span class="enc-social-icon-wrap"> <a target="_blank" rel="noreferrer" href="'.EC_SOCIAL_FACEBOOK_LINK.'" title="Facebook"> <i class="enc-icon enc-icon-facebook"></i> </a> </span>
                    <span class="enc-social-icon-wrap"> <a target="_blank" rel="noreferrer" href="'.EC_SOCIAL_PINTEREST_LINK.'" title="Pinterest"> <i class="enc-icon enc-icon-pinterest"></i> </a> </span>';
        if($footer){
            $output .= '<span class="enc-social-icon-wrap"> <a target="_blank" rel="noreferrer" href="'.EC_SOCIAL_YOUTUBE_LINK.'" title="Youtube"> <i class="enc-icon enc-icon-youtube"></i> </a> </span>';
        }
        $output .= '</div>';
        return $output;

    }

}
