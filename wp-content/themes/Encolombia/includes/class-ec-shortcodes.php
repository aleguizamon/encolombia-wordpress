<?php
/**
 * EC_Shortcodes
 *
 * Clase para registrar todos los shortcodes del tema
 *
 * @since 7.0.0
 */
class EC_Shortcodes{
    private static $instance;

    /**
     * EC_Shortcodes constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new EC_Shortcodes();
        }

        return self::$instance;
    }

    public function configure(){
        add_filter( 'widget_text', 'shortcode_unautop');
        add_filter( 'widget_text', 'do_shortcode');

        add_shortcode( 'enc_block_big_grid_1', array(new EC_Block_Big_Grid(), 'render') );
        add_shortcode( 'category-sections', array(new EC_Block_Category_Sections(), 'render') );
        add_shortcode( 'enc_block_11', array(new EC_Block_11(), 'render') );
        add_shortcode( 'enc_block_7', array(new EC_Block_7(), 'render') );

        add_shortcode( 'enc_su_button', array(new EC_SU_Button(), 'render') );
        add_shortcode( 'enc_su_spacer', array(new EC_SU_Spacer(), 'render') );
        add_shortcode( 'enc_su_row', array(new EC_SU_Row(), 'render') );
        add_shortcode( 'enc_su_column', array(new EC_SU_Column(), 'render') );
        add_shortcode( 'enc_su_spoiler', array(new EC_SU_Spoiler(), 'render') );
        add_shortcode( 'enc_su_note', array(new EC_SU_Note(), 'render') );
        add_shortcode( 'enc_vc_button', array(new EC_Wpbakery(), 'render_button') );
        add_shortcode( 'enc_vc_wp_search', array(new EC_Wpbakery(), 'render_search') );

        add_shortcode( 'enc_is_mobile', array(new EC_SC_Is_Mobile(), 'render') );
        add_shortcode( 'enc_is_desktop', array(new EC_SC_Is_Desktop(), 'render') );
        add_shortcode( 'enc_is_url', array(new EC_SC_Is_Url(), 'render') );
        add_shortcode( 'enc_is_customer_section', array(new EC_SC_Is_Url(), 'render_not_is_customer_section') );
        add_shortcode( 'can_rham', array(new EC_SC_Can_Render_Admanager(), 'render') );
        //add_shortcode( 'enc_site_networks', array(enc_social::get_instance(), 'site_networks') );
    }
}
