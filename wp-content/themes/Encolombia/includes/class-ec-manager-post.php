<?php

class EC_Manager_Post {

    use EC_Base_Post;

    /**
     * Objeto de WP_Term de la categoría actual del post.
     * La categoría actual es la que sale en la URL.
     *
     * @var WP_Term
     */
    public $current_category = null;

    /**
     * Indica si el post pertenece a una categoría de publicación.
     *
     * @var bool
     */
    public $is_publication = false;

    /**
     * Indica si el contenido actual del post contiene shortcodes de shortcode ultimate
     *
     * @var bool
     */
    public $is_shortcode_ultimate = false;

    /**
     * EC_Manager_Post constructor.
     */
    public function __construct(WP_Post $post){
        $this->init_post($post);
        $this->is_publication = $this->get_is_publication();
        $this->current_category = $this->get_current_category();
        $this->is_shortcode_ultimate = EC_Util_Legacy::is_shortcode_ultimate($this->post);
    }

    private function get_is_publication() {
        $is_publication = '0';
        if ($this->categories && isset($this->categories[0]) && !is_wp_error($this->categories[0])) {
            $is_publication = get_option('enc_is_pub-' . $this->categories[0]->cat_ID, '0');
        }
        return $is_publication == '1';
    }

    /**
     * Como es un post debo obtener la categoría del post.
     * El post puede tener asociadas varias categorias.
     * La categoria a escoger es la que tenga el slug en la url
     *
     * @return WP_Term|null
     */
    private function get_current_category() {
        $_cat = null;
        if ($this->categories) {
            foreach ($this->categories as $cat) {
                if(EC_Util::is_uri_section($cat->slug)){
                    $_cat = $cat;
                    break;
                }
            }
        }
        return $_cat;
    }

    /**
     * get the category spot of the single post / single post type
     * @return string - the html of the spot
     */
    function get_category_html()
    {
        $terms_ui_array  = array();
        if (!empty($this->categories)) {
            foreach ( $this->categories as $category ) {
                //show the category, only if we didn't already showed the parent
                $terms_ui_array[ $category->name ]  = array(
                    'link'         => get_category_link( $category->cat_ID ),
                    'hide_on_post' => 'show'
                );
            }
        }

        $buffy = '';
        $buffy .= '<ul class="enc-category">';

        foreach ( $terms_ui_array as $term_name => $term_params ) {
            $buffy .= '<li class="entry-category"><a href="' . $term_params['link'] . '">' . $term_name . '</a></li>';
        }
        $buffy .= '</ul>';

        return $buffy;
    }

    /**
     * Gets the article title on single pages and on modules that use this class as a base (module15 on Newsmag for example).
     * @param string $cut_at - not used, it's added to maintain strict standards
     * @return string
     */
    function get_title($cut_at = '')
    {
        $buffy = '';
        if (!empty($this->title)) {
            $buffy .= '<h1 class="entry-title">';
            $buffy .= $this->title;
            $buffy .= '</h1>';
        }
        return $buffy;
    }

    /**
     * the content of a single post or single post type
     * @return mixed|string|void
     */
    public function get_content()
    {
        global $page, $numpages, $multipage;
        $content = get_the_content(__('Ver más', 'EC'));

        if ($multipage == 1 && $numpages == $page && strpos($content, '</div></div></div></div>') !== false) {
            $content = str_replace('</div></div></div></div>', '', $content);
        }

        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);

        return $content;
    }

    public function list_other_posts_in_category()
    {
        $related_posts = array();

        // Obtenemos la categoría del post de entrada
        if ($this->categories) {
            $category_ids = array();
            foreach ($this->categories as $category) {
                $category_ids[] = $category->term_id;
            }

            // Consultamos los posts que pertenecen a la categoría
            $args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'cat' => $category_ids[0],//implode(',', $category_ids),
                'posts_per_page' => -1,
                'order' => 'DESC',
                'orderby' => 'date'
            );

            $query = new WP_Query($args);

            // Creamos el array con la información de cada post
            if ($query->have_posts()) {
                $ind = 1;
                while ($query->have_posts()) {
                    $query->the_post();
                    $related_posts[] = array(
                        'ID' => get_the_ID(),
                        'url' => get_the_permalink(),
                        'label' => get_the_title(),
                        'active' => get_the_ID() == $this->post->ID,
                        'position' => $ind
                    );
                    $ind++;
                }
            }

            wp_reset_postdata();
        }

        return $related_posts;
    }

    public function render_other_posts_in_category()
    {
        $cat_name = isset($this->categories[0]) ? $this->categories[0]->name : '';
        $html = sprintf('<div class="nav-title">%s</div>', $cat_name);
        $html .= '<ol>';
        foreach ($this->list_other_posts_in_category() as $item) {
            $html .= sprintf('<li class="%s" data-position="%d" data-id="%d"><a href="%s">%s</a></li>', $item['active'] ? 'item active' : 'item', $item['position'], $item['ID'], $item['url'], $item['label']);
        }
        $html .= '</ol>';
        return $html;
    }

    public function render_structured_data()
    {
        $image_data = '';
        if ($this->post_thumb_id) {
            $image_data = wp_get_attachment_image_src($this->post_thumb_id);
        }

        $output = '
            <span class="page-meta" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                <meta itemprop="name" content="Encolombia">
            </span>
            <meta itemprop="datePublished" content="' . $this->post->post_date . '">
            <meta itemprop="dateModified" content="' . $this->post->post_modified . '">
            <meta itemscope="" itemprop="mainEntityOfPage" itemtype="https://schema.org/WebPage" itemid="' . $this->permalink . '">
            <span class="page-meta" itemprop="publisher" itemscope="" itemtype="https://schema.org/Organization">
                <span class="page-meta" itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
                    <meta itemprop="url" content="https://encolombia.com/wp-content/uploads/2017/06/logo-encolombia-nuevo.png">
                </span>
                <meta itemprop="name" content="encolombia.com">
            </span>';
        $output .= '<meta itemprop="headline " content="' . $this->post->post_title . '">';
        if (!empty($image_data)) {
            $output .= '<span class="page-meta" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
                <meta itemprop="url" content="' . $image_data[0] . '">
                <meta itemprop="width" content="696">
                <meta itemprop="height" content="411">
            </span>';
        }

        return $output;
    }

    public function render_more_cat_btn_from_html($content)
    {
        $doc = new DOMDocument();
        $doc->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
        $xpath = new DOMXPath($doc);
        $categoryLink = $xpath->query("//ul[@class='enc-category']/li[1]/a")->item(0);
        $html = '';

        if ($categoryLink) {
            $categoryUrl = $categoryLink->getAttribute('href');
            $categoryName = $categoryLink->textContent;
            $html = '<div class="btn-more-cat"><a href="' . $categoryUrl . '">Más de "' . $categoryName . '" Aquí</a></div>';
        }

        return $html;
    }

    function related_posts($force_sidebar_position = '')
    {
        if ($this->post->post_type != 'post') {
            return '';
        }

        $td_related_ajax_filter_type = 'cur_post_same_categories';

        // the number of rows to show. this number will be multiplied with the hardcoded limit
        $tds_similar_articles_rows = 2;

        $td_related_limit = 3 * $tds_similar_articles_rows;
        $td_related_class = '';
        $td_column_number = 3;

        $td_block_args = array(
            'limit' => $td_related_limit,
            'ajax_pagination' => 'next_prev',
            'live_filter' => $td_related_ajax_filter_type,  //live atts - this is the default setting for this block
            'td_ajax_filter_type' => 'td_custom_related', //this filter type can overwrite the live filter @see
            'class' => $td_related_class,
            'td_column_number' => $td_column_number,
            'category__in' => wp_get_post_categories($this->post->ID),
            'post__not_in' => $this->post->ID
        );

        return EC_Block_Related_Posts::get_instance()->render($td_block_args, 3);

    }

    public function related_publications()
    {
        return (new EC_Block_Related_Categories($this->current_category))->render();
    }
}