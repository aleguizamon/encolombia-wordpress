<?php

class EC_Manager_Category {

    use EC_Base_Category;

    const PUBS_BY_ROW = 6;

    const PUBS_BY_ROW_M = 2;

    const ID_FARMACOVIG = 2548;

    const ID_SALUD_ESTETICA = 8;

    const ID_ESTETICA = 83;

    const ID_GUIA_NUTRICION = 1171;

    const ID_MEDICINA_ALTERNATIVA = 442;

    const CATS_BY_ROW = 6;

    const CATS_BY_ROW_M = 2;

    /**
     * Indica si la categoria es de publicación.
     *
     * @var bool
     */
    public $is_publication = false;

    public $paged;

    public $data;

    public $type_pub;

    public $ind_pos_ads = 0;
    public $current_row = 0;
    public $ITEMS_BY_ROW = 4;
    public $ITEMS_BY_ROW_M = 1;

    public $ads_units_category = array(
        'enc_inline1',
        'enc_inline2',
        'enc_inline3',
        'enc_inline4',
        'enc_inline5',
        'enc_inline6',
    );
    public $ads_units_category_mob = array(
        'enc_mob_inline1',
        'enc_mob_inline2',
        'enc_mob_inline3',
        'enc_mob_inline4',
        'enc_mob_inline5',
        'enc_mob_inline6',
        'enc_mob_inline7',
        'enc_mob_inline8',
    );
    public $ads_units_category_sponsored = array(
        'enc_sponsoredcategory_inline_pos1',
        'enc_sponsoredcategory_inline_pos2',
        'enc_sponsoredcategory_rr_pos1',
        'enc_sponsoredcategory_rr_pos2',
        'enc_sponsoredcategory_rr_pos3',
    );

    public function __construct(WP_Term $category)
    {
        $this->initialize($category);
        $this->is_publication = $this->is_publication();
        $this->paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

        if($this->is_publication) {
            $this->require_level = true;
            $this->data = $this->get_data_publications();
        } else {
            $this->require_level = false;
            $this->data = $this->get_data_subcats();
        }


    }

    private function is_publication() {
        $is_publication = '0';
        if($this->category) {
            $is_publication = get_option('enc_is_pub-' . $this->category->term_id, '0');
        }
        return $is_publication == '1';
    }

    public function render_childs() {
        if($this->is_publication) {
            $this->render_list_publications();
        } else {
            $this->render_list_subcats();
        }
    }

    private function render_list_publications() {
        if($this->data){
            if($this->type_pub == 1 || $this->type_pub == 2){
                echo '<div class="enc-list-publications"><div class="enc-row">';
                $ind = 0;
                $temp_count = 0;
                $max_nbr_ads_in_section = ceil(count($this->data) / self::PUBS_BY_ROW) - 1;
                foreach ($this->data as $childcat):
                    echo '
                    <div class="col-lg-2 col-md-3 col-sm-4">
                        <div class="pub-item">
                            <div class="pub-thumb"><img alt="'.$this->generate_title_attribute($childcat->cat_name).'" width="210px" height="auto"  src="'.($this->get_image_url($childcat->term_id, '180x225')).'" /> </div>
                            <div class="pub-content">
                            <h2 class="pub-title"><a alt="'.$this->generate_title_attribute($childcat->cat_name).'" href="'.get_category_link($childcat->cat_ID).'"> '. $childcat->cat_name .'</a></h2>
                        </div>
                        </div>                        
                    </div>
                    ';

                    if( (!EC()->is_mobile && ($ind+1) % self::PUBS_BY_ROW == 0) || (EC()->is_mobile && ($ind+1) % self::PUBS_BY_ROW_M == 0) ){
                        if( (!EC()->is_mobile && $this->ind_pos_ads < $max_nbr_ads_in_section) || EC()->is_mobile) {
                            $this->current_row++;
                            $temp_count = 0;
                        }
                    } else {
                        $temp_count++;
                    }

                    $this->render_sidebars();

                    $ind++;
                endforeach;

                if($temp_count > 0){
                    if( (!EC()->is_mobile && $this->ind_pos_ads < $max_nbr_ads_in_section) || EC()->is_mobile) {
                        $this->current_row++;
                        $this->render_sidebars();
                    }
                }

                echo '</div></div>';
                wp_reset_query();
                wp_reset_postdata();
            }
        }
    }

    private function render_list_subcats() {
        if($this->data){
            echo '<div class="enc-subcategory-list enc-row">';
            $ind = 0;
            $temp_count = 0;
            $max_nbr_ads_in_section = ceil(count($this->data) / self::CATS_BY_ROW) - 1;
            foreach ($this->data as $cat):
                $base_url = get_category_link( $cat );

                if($this->category->term_id != self::ID_FARMACOVIG){
                    echo '
                    <div class="col-lg-2 col-md-3 col-sm-4">
                        <div class="item-cat style2">
                            <div class="item-image">
                                <a class="item-image" href="'.$base_url.'" title="'.$cat->cat_name.'">
                                    <img src="'.$this->get_image_url($cat->term_id, EC()->is_mobile ? '150x150' : '150x150').'" alt="'.$cat->cat_name.'"  title="'.$cat->cat_name.'"  />
                                </a>
                            </div>
                            <div class="item-content">
                                <h3 class="item-title">
                                    <a href="'.$base_url.'" title="'.$cat->cat_name.'"   >'.$this->get_title($cat->cat_name, 40).'</a>
                                </h3>
                            </div>
                        
                        </div>
                    </div>
                    ';

                    if( (!EC()->is_mobile && ($ind+1) % self::CATS_BY_ROW == 0) || (EC()->is_mobile && ($ind+1) % self::CATS_BY_ROW_M == 0) ){
                        if( (!EC()->is_mobile && $this->ind_pos_ads < $max_nbr_ads_in_section) || EC()->is_mobile) {
                            $this->current_row++;
                            $temp_count = 0;
                        }

                    } else {
                        $temp_count++;
                    }

                    $this->render_sidebars();
                } else {
                    echo '<div class="catlist-horiz">';
                    echo '<a class="item-image" href="'.$base_url.'" title="'.the_title_attribute().'">';
                    echo '<img src="'.$this->get_image_url($cat->term_id, '224x146').'" alt="'.$cat->category_description.'"  title="'.$cat->cat_name.'"  />';
                    echo '</a>';
                    echo '<h3 class="item-title"><a href="'.$base_url.'">'.$cat->cat_name.'</a></h3>';
                    echo '</div>';
                }

                $ind++;
            endforeach;

            if($temp_count > 0){
                if( (!EC()->is_mobile && $this->ind_pos_ads < $max_nbr_ads_in_section) || EC()->is_mobile) {
                    $this->current_row++;
                    $this->render_sidebars();
                }
            }

            echo '</div>';
        }
    }

    public function render_category_post($ind, WP_Post $post){
        $module = new EC_Module_4($post);
        echo $module->render();

        if( (!EC()->is_mobile && ($ind+1) % $this->ITEMS_BY_ROW == 0) || (EC()->is_mobile && ($ind+1) % $this->ITEMS_BY_ROW_M == 0) ){
            $this->current_row++;
        }

        $sidebar_info = null;
        if(EC()->is_customer_section) {
            if(isset($this->ads_units_category_sponsored[$this->ind_pos_ads])){
                $sidebar_info = EC()->sidebars->get_sidebar_info($this->ads_units_category_sponsored[$this->ind_pos_ads]);
            }
            if($sidebar_info){
                if(!EC()->is_mobile && isset($sidebar_info['row']) && is_numeric($sidebar_info['row']) && $this->current_row == $sidebar_info['row'] ){
                    EC()->sidebars->render_sidebar($this->ads_units_category_sponsored[$this->ind_pos_ads]);
                    $this->ind_pos_ads++;
                } else if(EC()->is_mobile && isset($sidebar_info['row_m']) && is_numeric($sidebar_info['row_m']) && $this->current_row == $sidebar_info['row_m']){
                    EC()->sidebars->render_sidebar($this->ads_units_category_sponsored[$this->ind_pos_ads]);
                    $this->ind_pos_ads++;
                }
            }
        } else {
            $cat_ad_units = EC()->is_mobile ? $this->ads_units_category_mob : $this->ads_units_category;
            if(isset($cat_ad_units[$this->ind_pos_ads])){
                $sidebar_info = EC()->sidebars->get_sidebar_info($cat_ad_units[$this->ind_pos_ads]);
            }
            if($sidebar_info){
                if(!EC()->is_mobile && /*strpos(enc_params::$ads_units_category[$ind_pos_ads], 'enc_category_rr_pos') === false &&*/ isset($sidebar_info['row']) && is_numeric($sidebar_info['row']) && $this->current_row == $sidebar_info['row'] ){
                    EC()->sidebars->render_sidebar($cat_ad_units[$this->ind_pos_ads]);
                    $this->ind_pos_ads++;
                } else if(EC()->is_mobile && isset($sidebar_info['row_m']) && is_numeric($sidebar_info['row_m']) && $this->current_row == $sidebar_info['row_m']){
                    EC()->sidebars->render_sidebar($cat_ad_units[$this->ind_pos_ads]);
                    $this->ind_pos_ads++;
                }
            }
        }
    }

    public function get_data_publications()
    {
        if((int)$this->category_level == 1){
            $args = array(
                'type'                     => 'post',
                'child_of'                 => '' ,
                'parent'                   => $this->category->term_id,
                'orderby'                  => 'date',
                'order'                    => 'DESC',
                'hide_empty'               => 0,
                'hierarchical'             => 0,
                'exclude'                  => '',
                'include'                  => '',
                'number'                   => '',
                'taxonomy'                 => 'category',
                'pad_counts'               => false,
                'post_status' => 'publish',
            );
            $this->type_pub = 1;
        } else {
            $args = array(
                'type'                     => 'post',
                'child_of'                 => $this->category->term_id,
                'parent'                   => '',
                'orderby'                  => 'date',
                'order'                    => 'DESC',
                'hide_empty'               => 0,
                'hierarchical'             => 1,
                'exclude'                  => '',
                'include'                  => '',
                'number'                   => '',
                'taxonomy'                 => 'category',
                'pad_counts'               => false,
                'post_status' => 'publish',
            );
            $this->type_pub = 2;
        }
        $data = get_categories( $args );

        if(empty($data) /*&& (int)$this->category_level != 1*/){
            $args = array (
                'post_type'=>'post',
                'category' => $this->category,
                'paged' => $this->paged,
                'orderby' => 'date',
                'order' => 'DESC',
                'ignore_sticky_posts'=> 1,
                'posts_per_page' => 1,
                'taxonomy' => 'category',
                'post_status' => 'publish',
                'category__in' => array($this->category->term_id)
            );
            // The Query
            $this->type_pub = 3;
            $data = new WP_Query($args);
        }
        return $data;
    }

    public function get_data_subcats()
    {
        $data = array();
        if (!$this->paged || $this->paged < 2){
            if($this->is_salud_estetica()){
                $args = array(
                    'child_of'                 => '',
                    'parent'                   => self::ID_SALUD_ESTETICA,
                    'orderby'                  => 'name',
                    'order'                    => 'ASC',
                    'hide_empty'               => 1,
                    'hierarchical'             => 1,
                    'exclude'                  => '',
                    'include'                  => '',
                    'number'                   => '',
                    'taxonomy'                 => 'category',
                    'pad_counts'               => false
                );
                $group1 = get_categories( $args );
                $args['parent'] = self::ID_ESTETICA;
                $group2 = get_categories( $args );
                $group3 = array();
                $group4 = array();
                foreach ($group1 as $g){
                    if($g->cat_ID == self::ID_GUIA_NUTRICION || $g->cat_ID == self::ID_MEDICINA_ALTERNATIVA){
                        $group3[] = $g;
                    } else {
                        $group4[] = $g;
                    }
                }
                $data = array_merge($group4, $group2, $group3);
            } else {
                $args = array(
                    'child_of'                 => '',
                    'parent'                   => $this->category->term_id,
                    'orderby'                  => 'name',
                    'order'                    => 'ASC',
                    'hide_empty'               => 1,
                    'hierarchical'             => 1,
                    'exclude'                  => '',
                    'include'                  => '',
                    'number'                   => '',
                    'taxonomy'                 => 'category',
                    'pad_counts'               => false
                );
                $data = get_categories( $args );
            }
        }
        return $data;
    }

    public function is_salud_estetica(){
        return $this->category->term_id == self::ID_SALUD_ESTETICA;
    }

    public function render_sidebars(){
        $cat_ad_units = EC()->is_mobile ? $this->ads_units_category_mob : $this->ads_units_category;
        $sidebar_info = null;
        if(isset($cat_ad_units[$this->ind_pos_ads])){
            $sidebar_info = EC()->sidebars->get_sidebar_info($cat_ad_units[$this->ind_pos_ads]);
        }
        if($sidebar_info){
            if(!EC()->is_mobile && strpos($cat_ad_units[$this->ind_pos_ads], 'enc_category_rr_pos') === false && isset($sidebar_info['row']) && is_numeric($sidebar_info['row']) && $this->current_row == $sidebar_info['row'] ){
                EC()->sidebars->render_sidebar($cat_ad_units[$this->ind_pos_ads]);
                $this->ind_pos_ads++;
            } else if(EC()->is_mobile && isset($sidebar_info['row_m']) && is_numeric($sidebar_info['row_m']) && $this->current_row == $sidebar_info['row_m']){
                EC()->sidebars->render_sidebar($cat_ad_units[$this->ind_pos_ads]);
                $this->ind_pos_ads++;
            }
        }
    }

    public function redirect_if_last() {
        if($this->type_pub == 3 && $this->data->have_posts()){
            $this->data->the_post();
            $post_url = get_permalink();
            wp_redirect($post_url);
            exit;
        }
    }

}