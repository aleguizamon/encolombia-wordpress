<?php

class EC_Base_Block {

    private $atts = array();
    protected $td_query;

    protected function get_att($att_name) {
        if (!empty($this->atts[$att_name])) {
            return $this->atts[$att_name];
        }

        return '';
    }

    /**
     * Used by blocks that need auto generated titles
     * @return string
     */
    function get_block_title() {
        $custom_title = $this->get_att('custom_title');
        $custom_url = $this->get_att('custom_url');

        if (empty($custom_title)) {
            $custom_title = 'Block title';
        }

        // there is a custom title
        $buffy = '';
        $buffy .= '<h3 class="enc-block-title-1">';
        if (!empty($custom_url)) {
            $buffy .= '<a href="' . esc_url($custom_url) . '" >' . esc_html($custom_title) . '</a>';
        } else {
            $buffy .= '<span >' . esc_html($custom_title) . '</span>';
        }
        $buffy .= '</h3>';
        return $buffy;
    }

    /**
     * the base render function. This is called by all the child classes of this class
     * @param $atts
     * @param $content
     * @return string ''
     */
    function render($atts, $column_number) {
        // WARNING! all the atts MUST BE DEFINED HERE !!! It's easier to maintain and we always have a list of them all
        $this->atts = shortcode_atts ( // add defaults (if an att is not in this list, it will be removed!)
            array(
                'limit' => 5,  // @todo trebuie refactoriata partea cu limita, in paginatie e hardcodat tot 5 si deja este setat in constructor aici
                'orderby' => '',
                'post_ids' => '', // post id's filter (separated by commas)
                'tag_slug' => '', // tag slug filter (separated by commas)
                'autors_id' => '', // filter by authors ID ?
                'installed_post_types' => '', // filter by custom post types
                'category_id' => '',
                'category_ids' => '',
                'custom_title' => '',       // used in td_block_template_1.php
                'custom_url' => '',         // used in td_block_template_1.php
                'show_child_cat' => '',
                'sub_cat_ajax' => '',
                'ajax_pagination' => '',
                'header_color' => '',       // used in td_block_template_1.php + here for js -> loading color
                'header_text_color' => '',  // used in td_block_template_1.php

                'ajax_pagination_infinite_stop' => '',
                'td_column_number' => $column_number, // if no column number passed, get from VC

                // ajax preloader
                'td_ajax_preloading' => '',


                // drop down list + other live filters?
                'td_ajax_filter_type' => '',
                'td_ajax_filter_ids' => '',
                'td_filter_default_txt' => __('Todo', 'EC'),

                // classes?  @see get_block_classes
                'color_preset' => '',
                'border_top' => '',
                'class' => '',
                'el_class' => '',
                'offset' => '', // the offset

                'css' => '', //custom css - used by VC

                'tdc_css' => '', //custom css - used by TagDiv Composer
                'tdc_css_class' => '', // unique css class - used by TagDiv Composer to add inline css ('class' could not be used because it's not unique)
                'tdc_css_class_style' => '', // unique css class - used by get_css to add inline css on td-element-style ('class' could not be used because it's not unique)

                // live filters
                // $atts['live_filter'] is set by the 'user'. cur_post_same_tags | cur_post_same_author | cur_post_same_categories
                'live_filter' => '',

                // the two live filters are set by @see td_block::add_live_filter_atts
                'live_filter_cur_post_id' => '',      /** @see td_block::add_live_filter_atts */
                'live_filter_cur_post_author' => '',  /** @see td_block::add_live_filter_atts */

                'block_template_id' => '', // used to load a different block template on this specific block. By default the block will load the global block template from the panel
                'category__in'=> '',
                'post__not_in' => '',
            ),
            $atts
        );

        $this->td_query = &EC_Data_Source::get_wp_query($this->atts);
        return '';
    }


}
