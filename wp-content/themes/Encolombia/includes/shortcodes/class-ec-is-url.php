<?php

class EC_SC_Is_Url {

    public function render($atts, $content = null){
        if(isset($atts['url']) && $atts['url'] == '/' && EC()->is_home()){
            return do_shortcode( $content );
        } else {
            return null;
        }
    }

    public function render_not_is_customer_section($atts, $content = null){
        if( !EC()->is_customer_section() ){
            return do_shortcode( $content );
        } else {
            return '';
        }
    }

}