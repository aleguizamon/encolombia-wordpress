<?php

class EC_SC_Is_Desktop {

    public function render($atts, $content = null){
        if( !EC()->is_mobile ){
            return do_shortcode( $content );
        } else {
            return null;
        }
    }

}