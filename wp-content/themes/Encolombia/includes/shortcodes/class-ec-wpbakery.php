<?php

class EC_Wpbakery
{

    function render_button($atts)
    {
        //title="Productos Orgánicos" target="_self" color="#ffffff;" href="/vida-estilo/alimentacion/alimentosorganicos/productos-organicos
        $color = $size = $icon = $target = $href = $el_class = $title = '';
        $output = '';
        extract($atts);

        $a_class = '';

        if ('' !== $el_class) {
            $tmp_class = explode(' ', strtolower($el_class));
            $tmp_class = str_replace('.', '', $tmp_class);
            /*if ( in_array( 'prettyphoto', $tmp_class ) ) {
                wp_enqueue_script( 'prettyphoto' );
                wp_enqueue_style( 'prettyphoto' );
                $a_class .= ' prettyphoto';
                $el_class = str_ireplace( 'prettyphoto', '', $el_class );
            }*/
            if (in_array('pull-right', $tmp_class) && '' !== $href) {
                $a_class .= ' pull-right';
                $el_class = str_ireplace('pull-right', '', $el_class);
            }
            if (in_array('pull-left', $tmp_class) && '' !== $href) {
                $a_class .= ' pull-left';
                $el_class = str_ireplace('pull-left', '', $el_class);
            }
        }

        if ('same' === $target || '_self' === $target) {
            $target = '';
        }
        $target = ('' !== $target) ? ' target="' . esc_attr($target) . '"' : '';

        $color = ('' !== $color) ? ' wpb_' . $color : '';
        $size = ('' !== $size && 'wpb_regularsize' !== $size) ? ' wpb_' . $size : ' ' . $size;
        $icon = ('' !== $icon && 'none' !== $icon) ? ' ' . $icon : '';
        $i_icon = ('' !== $icon) ? ' <i class="icon"> </i>' : '';
        $el_class = $this->getExtraClass($el_class);

        $css_class = 'wpb_button'; //apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_button ' . $color . $size . $icon . $el_class, $this->settings['base'], $atts );

        if ('' !== $href) {
            $output .= '<span class="' . esc_attr($css_class) . '">' . $title . $i_icon . '</span>';
            $output = '<a class="wpb_button_a' . esc_attr($a_class) . '" title="' . esc_attr($title) . '" href="' . esc_attr($href) . '"' . $target . '>' . $output . '</a>';
        } else {
            $output .= '<button class="' . esc_attr($css_class) . '">' . $title . $i_icon . '</button>';

        }
        return $output;
    }

    public function render_search($atts)
    {
        $title = $el_class = $el_id = '';
        $output = '';
        // $atts = vc_map_get_attributes($this->getShortcode(), $atts);
        if (is_array($atts)) extract($atts);

        $el_class = $this->getExtraClass($el_class);
        $wrapper_attributes = array();
        if (!empty($el_id)) {
            $wrapper_attributes[] = 'id="' . esc_attr($el_id) . '"';
        }
        $output = '<div ' . implode(' ', $wrapper_attributes) . ' class="vc_wp_search wpb_content_element' . esc_attr($el_class) . '">';
        $type = 'WP_Widget_Search';
        $args = array();
        global $wp_widget_factory;

        if (is_object($wp_widget_factory) && isset($wp_widget_factory->widgets, $wp_widget_factory->widgets[$type])) {
            ob_start();
            the_widget($type, $atts, $args);
            $output .= ob_get_clean();

            $output .= '</div>';

            return $output;
        } else {
            return 'Widget ' . esc_attr($type) . 'Not found in : vc_wp_search';
        }
    }

    /**
     * @param $el_class
     *
     * @return string
     */
    public function getExtraClass($el_class)
    {
        $output = '';
        if ('' !== $el_class) {
            $output = ' ' . str_replace('.', '', $el_class);
        }

        return $output;
    }


}