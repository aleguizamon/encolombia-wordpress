<?php

class EC_SU_Spoiler {

    public function render($atts, $content){
        $atts           = shortcode_atts(
            array(
                'title'         => __( 'Spoiler title', 'shortcodes-ultimate' ),
                'open'          => 'no',
                'style'         => 'default',
                'icon'          => 'plus',
                'anchor'        => '',
                'scroll_offset' => 0,
                'class'         => '',
            ),
            $atts,
            'spoiler'
        );
        $atts['style']  = str_replace( array( '1', '2' ), array( 'default', 'fancy' ), $atts['style'] );
        $atts['anchor'] = ( $atts['anchor'] ) ? ' data-anchor="' . str_replace( array( ' ', '#' ), '', sanitize_text_field( $atts['anchor'] ) ) . '"' : '';
        if ( 'yes' !== $atts['open'] ) {
            $atts['class'] .= ' su-spoiler-closed';
        }
        /*su_query_asset( 'css', 'su-icons' );
        su_query_asset( 'css', 'su-shortcodes' );
        su_query_asset( 'js', 'jquery' );
        su_query_asset( 'js', 'su-shortcodes' );*/
        //do_action( 'su/shortcode/spoiler', $atts );
        $id = uniqid();
        $output = '<div class="su-spoiler su-spoiler-style-' . $atts['style'] . ' su-spoiler-icon-' . $atts['icon'] . $this->su_get_css_class( $atts ) . '"' . $atts['anchor'] . ' data-scroll-offset="' . intval( $atts['scroll_offset'] ) . '"><div id="su-spoiler-title-'.$id.'" class="su-spoiler-title" tabindex="0" role="button"><span class="su-spoiler-icon" style="display: none"></span>' . EC_Util_Legacy::su_do_attribute( $atts['title'] ) . '</div><div class="su-spoiler-content su-u-clearfix su-u-trim">' . EC_Util_Legacy::su_do_nested_shortcodes( $content, 'spoiler' ) . '</div></div>';
        $output .= '<script>
            var _counter_'.$id.' = 1;                        
            document.getElementById("su-spoiler-title-'.$id.'").addEventListener("click", function (ev) {
                console.log(ev);
                if(_counter_'.$id.'){
                    ev.target.parentElement.classList.remove("su-spoiler-closed");
                    _counter_'.$id.' = 0;
                }                                
                else{
                    ev.target.parentElement.classList.add("su-spoiler-closed");
                    _counter_'.$id.' = 1;
                }       
            });
        
        </script>';
        /*$output .= '<script>
            var _counter_'.$id.' = 1;
            var intervalSpoiler_'.$id.' = setInterval(function(){
                if(window.jQuery){
                    jQuery(document).ready(function(){
                        jQuery("body").on("click", ".su-spoiler-title", function(e){
                            if(_counter_'.$id.'){
                                jQuery(e.currentTarget).closest(".su-spoiler").removeClass("su-spoiler-closed");
                                _counter_'.$id.' = 0;
                            }                                
                            else{
                                jQuery(e.currentTarget).closest(".su-spoiler").addClass("su-spoiler-closed");
                                _counter_'.$id.' = 1;
                            }                                    
                        });
                    });
                    clearInterval(intervalSpoiler_'.$id.');
                }
            }, 500); 

</script>';*/
        return $output;
    }

    public function su_get_css_class( $atts ) {
        return $atts['class'] ? ' ' . trim( $atts['class'] ) : '';
    }
}