<?php

class EC_SU_Row {

    public function render($atts, $content){
        $atts = shortcode_atts( array( 'class' => '' ), $atts );

        return '<div class="su-row' . $this->su_get_css_class( $atts ) . '">' . EC_Util_Legacy::su_do_nested_shortcodes( $content, 'row' ) . '</div>';
    }

    public function su_get_css_class( $atts ) {
        return $atts['class'] ? ' ' . trim( $atts['class'] ) : '';
    }




}