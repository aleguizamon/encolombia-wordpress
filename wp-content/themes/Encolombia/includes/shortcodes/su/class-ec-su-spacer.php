<?php

class EC_SU_Spacer {

    public function render($atts, $content){
        $atts = shortcode_atts( array(
            'size'  => '20',
            'class' => ''
        ), $atts, 'spacer' );
        //su_query_asset( 'css', 'su-shortcodes' );
        return '<div class="su-spacer' . $this->su_get_css_class( $atts ) . '" style="height:' . (string) $atts['size'] . 'px"></div>';
    }

    public function su_get_css_class( $atts ) {
        return $atts['class'] ? ' ' . trim( $atts['class'] ) : '';
    }




}