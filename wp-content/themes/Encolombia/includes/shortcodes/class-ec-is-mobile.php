<?php

class EC_SC_Is_Mobile {

    public function render($atts, $content = null){
        if( EC()->is_mobile ){
            return do_shortcode( $content );
        } else {
            return null;
        }
    }

}