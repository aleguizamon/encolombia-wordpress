<?php

class EC_Performance{

    private static $instance;

    /**
     * EC_Performance constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new EC_Performance();
        }

        return self::$instance;
    }

    //TODO: no se incorporo dns_prefetch

    public function configure(){
        add_action( 'wp_enqueue_scripts', array($this, 'unregister_assets'), 1001);
        add_action( 'init', array($this, 'init_optimization'), 9999 );
        add_action( 'template_redirect',  array($this, 'disable_tags_page'));
        //add_action('wp_default_scripts', array($this, 'remove_jquery_migrate'));
        add_action('wp_head', array($this, 'render_preconnect'), 8);
        $this->remove_junk_from_head();
        $this->remove_emojis();
    }

    public function unregister_assets() {

        //quita el css de gutemberg
        //wp_deregister_style('wp-block-library');

        //desabilitar el sticky del plugin otw sidebar widget manager
        wp_deregister_style('otw_grid_manager');
        wp_deregister_style('otw_sbm.css');
        wp_deregister_style('otw_shortocde');
        wp_deregister_script('otw_jquery_sticky.js');
        wp_deregister_script('otw_sticky_sbm.js');
        wp_deregister_script('jquery_sticky.js');

    }

    /**
     * Remove junk from head
     */
    public function remove_junk_from_head(){

        add_filter('the_generator', function(){ return ''; });
        remove_action('wp_head', 'wp_generator');

        remove_action('wp_head', 'rsd_link'); // remove really simple discovery (RSD) link
        remove_action('wp_head', 'wlwmanifest_link'); // remove wlwmanifest.xml (needed to support windows live writer)

        remove_action('wp_head', 'feed_links', 2); // remove rss feed links (if you don't use rss)
        remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links

        remove_action('wp_head', 'index_rel_link'); // remove link to index page

        remove_action('wp_head', 'start_post_rel_link', 10, 0); // remove random post link
        remove_action('wp_head', 'parent_post_rel_link', 10, 0); // remove parent post link
        remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // remove the next and previous post links
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

        remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 ); // remove shortlink
    }

    public function init_optimization() {
        // Remove the REST API endpoint.
        remove_action( 'rest_api_init', 'wp_oembed_register_route' );
        // Turn off oEmbed auto discovery.
        add_filter( 'embed_oembed_discover', '__return_false' );
        // Don't filter oEmbed results.
        remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
        // Remove oEmbed discovery links.
        remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
        // Remove oEmbed-specific JavaScript from the front-end and back-end.
        remove_action( 'wp_head', 'wp_oembed_add_host_js' );
        add_filter( 'tiny_mce_plugins', array($this, 'disable_embeds_tiny_mce_plugin') );
        // Remove all embeds rewrite rules.
        add_filter( 'rewrite_rules_array', array($this, 'disable_embeds_rewrites') );
        // Remove filter of the oEmbed result before any HTTP requests are made.
        remove_filter( 'pre_oembed_result', 'wp_filter_pre_oembed_result', 10 );
        //Quita las etiquetas de los posts
        unregister_taxonomy_for_object_type( 'post_tag', 'post' );
    }

    public function remove_emojis(){
        // REMOVE WP EMOJI
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');

        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
        remove_action( 'admin_print_styles', 'print_emoji_styles' );
    }

    /**
     * Disable tags page
     */
    public function disable_tags_page(){
        if(is_tag()){
            wp_redirect( home_url(), 301 );
        }
    }

    public function disable_embeds_tiny_mce_plugin($plugins) {
        return array_diff($plugins, array('wpembed'));
    }

    public function disable_embeds_rewrites($rules) {
        foreach($rules as $rule => $rewrite) {
            if(false !== strpos($rewrite, 'embed=true')) {
                unset($rules[$rule]);
            }
        }
        return $rules;
    }


    function remove_jquery_migrate($scripts)
    {
        if (!is_admin() && isset($scripts->registered['jquery'])) {
            $script = $scripts->registered['jquery'];

            if ($script->deps) { // Check whether the script has any dependencies
                $script->deps = array_diff($script->deps, array(
                    'jquery-migrate'
                ));
            }
        }
    }

    public function render_preconnect(){
        $preconnects = array(
            'https://www.googletagservices.com',
            'https://securepubads.g.doubleclick.net',
            'https://live.demand.supply',
        );

        $output = '';
        foreach($preconnects as $preconnect){
            $output .= '<link rel="preconnect" href="' . $preconnect . '">';
        }
        echo $output;
    }

}
