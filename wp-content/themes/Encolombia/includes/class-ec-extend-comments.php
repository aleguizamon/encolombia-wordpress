<?php

class EC_Extend_Comments{
    private static $instance;

    /**
     * EC_Extend_Comments constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new EC_Extend_Comments();
        }

        return self::$instance;
    }

    public function configure(){
        add_action( 'wp_head', array($this, 'ec_force_comment'));
        add_filter( 'comment_form_default_fields', array($this, 'custom_fields'));
        add_action( 'comment_post', array($this, 'save_comment_meta_data' ));
        add_action( 'add_meta_boxes_comment', array($this, 'extend_comment_add_meta_box' ));
        add_action( 'edit_comment', array($this, 'extend_comment_edit_metafields' ));
    }

    // añadir campos en los comentarios
    public function custom_fields($fields) {

        $commenter = wp_get_current_commenter();
        $req = get_option( 'require_name_email' );
        $aria_req = ( $req ? " aria-required='true'" : '' );

        $fields[ 'author' ] = '<p class="comment-form-author">'.
            //'<label for="author">' . __( 'Nombre', 'EC' ) . 	( $req ? '<span class="required">&nbsp;(*)</span></label>' : '' ).
            '<input placeholder="'.__( 'Nombre', 'EC' ) . 	( $req ? '&nbsp;(*)' : '' ).'" id="author" name="author" type="text" value="'. esc_attr( $commenter['comment_author'] ) .
            '" size="30" tabindex="1"' . $aria_req . ' /></p>';

        $fields[ 'email' ] = '<p class="comment-form-email">'.
            //'<label for="email">' . __( 'Correo electrónico', 'EC' ) . 	( $req ? '<span class="required">&nbsp;(*)</span></label>' : '' ).
            '<input placeholder="' . __( 'Correo electrónico', 'EC' ) . 	( $req ? '&nbsp;(*)' : '' ).'" id="email" name="email" type="text" value="'. esc_attr( $commenter['comment_author_email'] ) .
            '" size="30"  tabindex="2"' . $aria_req . ' /></p>';

        /*$fields[ 'url' ] = '<p class="comment-form-url">'.
            '<label for="url">' . __( 'Sitio web' ) . '</label>'.
            '<input id="url" name="url" type="text" value="'. esc_attr( $commenter['comment_author_url'] ) .
            '" size="30"  tabindex="3" /></p>';*/

        $fields[ 'phone' ] = '<p class="comment-form-phone">'.
            //'<label for="phone">' . __( 'Teléfono', 'EC' ) . '</label>'.
            '<input placeholder="' . __( 'Teléfono', 'EC' ) . '" id="phone" name="phone" type="text" size="30"  tabindex="4" /></p>';

        return $fields;
    }

    // Save the comment meta data along with comment
    public function save_comment_meta_data( $comment_id ) {
        if ( ( isset( $_POST['phone'] ) ) && ( $_POST['phone'] != '') )
            $phone = $_POST['phone'];
        add_comment_meta( $comment_id, 'phone', $phone );
    }

    //Add an edit option in comment edit screen
    public function extend_comment_add_meta_box() {
        add_meta_box( 'phone', __( 'Campos Extras', 'EC' ), 'extend_comment_meta_box', 'comment', 'normal', 'high' );
    }

    function extend_comment_meta_box ( $comment ) {
        $phone = get_comment_meta( $comment->comment_ID, 'phone', true );
        wp_nonce_field( 'extend_comment_update', 'extend_comment_update', false );
        ?>

        <p>
            <label for="phone"><?php _e( 'Teléfono' ); ?></label>
            <input type="text" name="phone" value="<?php echo esc_attr( $phone ); ?>" class="widefat" />
        </p>
        <?php
    }

    // Update comment meta data from comment edit screen
    public function extend_comment_edit_metafields( $comment_id ) {
        if( ! isset( $_POST['extend_comment_update'] ) || ! wp_verify_nonce( $_POST['extend_comment_update'], 'extend_comment_update' ) ) return;

        if ( ( isset( $_POST['phone'] ) ) && ( $_POST['phone'] != '') ):
            $phone = wp_filter_nohtml_kses($_POST['phone']);
            update_comment_meta( $comment_id, 'phone', $phone );
        else :
            delete_comment_meta( $comment_id, 'phone');
        endif;
    }

    // obliga poner comentarios en las paginas de categorias
    public function ec_force_comment( ) {
        global $withcomments;
        if(is_category())
            $withcomments = true; //force to show the comment on category page
    }
}