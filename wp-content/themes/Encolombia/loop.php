<?php

if (have_posts()) {
    $ind = 0;
    $cats = array();

    if(!EC()->is_search()){
        echo '<div class="enc-row loop-nuevas-pub">';
    }
    while ( have_posts() ) : the_post();

        global $post;
        if(EC()->is_search()){
          $module = new EC_Module_Search($post);
        }else{
          $module = new EC_Module_4($post);
        }
        echo $module->render();
        $ind++;
    endwhile; //end loop
    if(!EC()->is_search()){
        echo '</div>';
    }
    
} else {
    echo EC_Util::no_posts();
}
