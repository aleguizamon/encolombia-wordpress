<?php

get_header();

?>
<div class="enc-main-content-wrap enc-container-wrap">
    <div class="enc-container">

        <div class="enc-404-title">
            <?php echo __('Ooops... Error 404', 'EC'); ?>
        </div>

        <div class="enc-404-sub-title">
            <?php echo __('Lo sentimos, pero la página que estás buscando no existe.', 'EC'); ?>
        </div>

        <div class="enc-404-sub-sub-title">
            <a href="<?php echo esc_url(home_url( '/' )); ?>"><?php echo __('IR A INICIO', 'EC'); ?></a>
        </div>


        <div class="ec-title-home">
            <h4 class="enc-block-title"><span><?php echo __('NUESTROS ÚLTIMOS POSTS', 'EC')?></span></h4>
        </div>
        <br>
        <div class="enc-row">
        <?php
        $args = array(
            'post_type'=> 'post',
            'showposts' => 6
        );
        query_posts($args);

        while ( have_posts() ) : the_post();
            global $post;
            $module = new EC_Module_4($post);
            echo $module->render(3);
        endwhile; //end loop

        //locate_template('loop.php', true);
        wp_reset_query();

        ?>
        </div>

    </div> <!-- /.td-container -->
</div> <!-- /.td-main-content-wrap -->

<?php
get_footer();
?>