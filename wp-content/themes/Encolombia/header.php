<!doctype html >
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <title><?php wp_title('|', true, 'right'); ?></title>
    <link rel="preload" as="font" href="<?php echo EC()->site_url; ?>/wp-content/themes/Encolombia/assets/fonts/IBMPlexSans-Regular.woff2" crossorigin>
    <link rel="preload" as="font" href="<?php echo EC()->site_url; ?>/wp-content/themes/Encolombia/assets/fonts/IBMPlexSans-Light.woff2" crossorigin>
    <link rel="preload" as="font" href="<?php echo EC()->site_url; ?>/wp-content/themes/Encolombia/assets/fonts/IBMPlexSans-Bold.woff2" crossorigin>
    <link rel="preload" as="font" href="<?php echo EC()->site_url; ?>/wp-content/themes/Encolombia/assets/fonts/IBMPlexSans-Medium.woff2" crossorigin>
    <meta charset="<?php bloginfo( 'charset' );?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <meta name="p:domain_verify" content="902173efe2183982042a885afa69d635"/>
    <meta name="referrer" content="no-referrer-when-downgrade" />
    <link rel="icon" type="image/png" href="<?php echo EC_FAV_ICON; ?>">
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo EC_ICON_IOS_76; ?>">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo EC_ICON_IOS_120; ?>">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo EC_ICON_IOS_152; ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo EC_ICON_IOS_114; ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo EC_ICON_IOS_144; ?>">
    <?php
    wp_head();
    ?>
</head>
<?php flush(); ?>
<body <?php body_class() ?> itemscope="itemscope" itemtype="https://schema.org/WebPage">
<!-- Actualizando... -->
<div id="yVAlUnEIyyGYoEl4EVnJk895-clQJwSvOsPZLrdB1v5X3ql4KheV"></div>
<?php if(EC()->is_post()): ?>
<div class="enc-progress-container"><div class="enc-progress-bar" id="encpbar"></div></div>
<?php endif; ?>
<div id="td-outer-wrap" class="td-theme-wrap">
    <header id="header" >
        <div class="enc-container main-header" id="main-header">
            <div class="enc-row">
                <div class="col-md-12">
                    <div class="top-header-inner">
                        <div class="site-logo" style="width:202px; height: 42px;">
                            <?php echo EC()->layout->render_site_logo(); ?>
                        </div>

                        <div class="enc-header-right">
                            <div class="enc-header-menu-wrap">
                                <nav id="enc-header-menu" role="navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
                                    <?php
                                    wp_nav_menu(array(
                                        'container' => '',
                                        'theme_location' => 'header-menu',
                                        'menu' => 'DesktopMenu',
                                        'menu_class'=> 'menu-desktop-single',
                                        'fallback_cb' => '_enc_wp_no_menu',
                                        'link_after' => '',
                                        'walker'  => new EC_Walker_Nav_Menu()
                                    ));

                                    //if no menu
                                    function _enc_wp_no_menu() {
                                        //this is the default menu
                                        echo '<ul class="">';
                                        echo '<li class="menu-item-first"><a href="' . esc_url(home_url( '/' )) . 'wp-admin/nav-menus.php">Click here - to use the wp menu builder</a></li>';
                                        echo '</ul>';
                                    }
                                    ?>
                                </nav>
                            </div>
                        </div>

                        <div class="header-icon-area">
                            <div class="search-icon">
                                <a href="#enc-header-search" title="Buscar en encolombia.com">
                                    <svg width="20" height="20" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M17.1249 16.2411L12.4049 11.5211C13.5391 10.1595 14.1047 8.41291 13.9841 6.64483C13.8634 4.87675 13.0657 3.22326 11.7569 2.02834C10.4482 0.833415 8.7291 0.189061 6.95736 0.229318C5.18562 0.269575 3.49761 0.991344 2.24448 2.24448C0.991344 3.49761 0.269575 5.18562 0.229318 6.95736C0.189061 8.7291 0.833415 10.4482 2.02834 11.7569C3.22326 13.0657 4.87675 13.8634 6.64483 13.9841C8.41291 14.1047 10.1595 13.5391 11.5211 12.4049L16.2411 17.1249L17.1249 16.2411ZM1.49989 7.12489C1.49989 6.01237 1.82979 4.92483 2.44787 3.99981C3.06596 3.07478 3.94446 2.35381 4.97229 1.92807C6.00013 1.50232 7.13113 1.39093 8.22227 1.60797C9.31342 1.82501 10.3157 2.36074 11.1024 3.14741C11.889 3.93408 12.4248 4.93636 12.6418 6.02751C12.8588 7.11865 12.7475 8.24965 12.3217 9.27748C11.896 10.3053 11.175 11.1838 10.25 11.8019C9.32495 12.42 8.23741 12.7499 7.12489 12.7499C5.63355 12.7482 4.20377 12.1551 3.14924 11.1005C2.09471 10.046 1.50154 8.61622 1.49989 7.12489Z" fill="currentColor"></path>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="mobile-header-bar">
        <div class="mobile-header-bar-inner">
            <span class="mobile-toggle-btn">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </span>

            <div class="mobile-header-logo">
                <?php echo EC()->layout->render_site_logo(); ?>
            </div>

            <div class="info">
                <div class="search-icon">
                    <a href="#enc-header-search" title="Buscar en encolombia.com">
                        <svg width="20" height="20" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M17.1249 16.2411L12.4049 11.5211C13.5391 10.1595 14.1047 8.41291 13.9841 6.64483C13.8634 4.87675 13.0657 3.22326 11.7569 2.02834C10.4482 0.833415 8.7291 0.189061 6.95736 0.229318C5.18562 0.269575 3.49761 0.991344 2.24448 2.24448C0.991344 3.49761 0.269575 5.18562 0.229318 6.95736C0.189061 8.7291 0.833415 10.4482 2.02834 11.7569C3.22326 13.0657 4.87675 13.8634 6.64483 13.9841C8.41291 14.1047 10.1595 13.5391 11.5211 12.4049L16.2411 17.1249L17.1249 16.2411ZM1.49989 7.12489C1.49989 6.01237 1.82979 4.92483 2.44787 3.99981C3.06596 3.07478 3.94446 2.35381 4.97229 1.92807C6.00013 1.50232 7.13113 1.39093 8.22227 1.60797C9.31342 1.82501 10.3157 2.36074 11.1024 3.14741C11.889 3.93408 12.4248 4.93636 12.6418 6.02751C12.8588 7.11865 12.7475 8.24965 12.3217 9.27748C11.896 10.3053 11.175 11.1838 10.25 11.8019C9.32495 12.42 8.23741 12.7499 7.12489 12.7499C5.63355 12.7482 4.20377 12.1551 3.14924 11.1005C2.09471 10.046 1.50154 8.61622 1.49989 7.12489Z" fill="currentColor"></path>
                        </svg>
                    </a>
                </div>

            </div>
        </div>
        <div class="mobile-menu">
            <?php
            wp_nav_menu(array(
                'theme_location' => 'header-menu',
                'menu' => 'MobileSingleMenu',
                'menu_class'=> '',
                'fallback_cb' => '_enc_wp_no_menu_mov',
                'link_after' => '',
                'walker'  => new EC_Walker_Nav_Menu()
            ));
            function _enc_wp_no_menu_mov() {
                //this is the default menu
                echo '<ul class="">';
                echo '<li class="menu-item-first"><a href="' . esc_url(home_url( '/' )) . 'wp-admin/nav-menus.php">Click here - to use the wp menu builder</a></li>';
                echo '</ul>';
            }
            ?>
        </div>
    </div>

    <div id="enc-header-search" class="header-search">
        <button type="button" class="close">×</button>
        <form role="search" method="get" class="enc-search-form" action="<?php echo esc_url(home_url( '/' )); ?>">
            <input type="text" value="<?php echo get_search_query(); ?>" name="s" autocomplete="off" placeholder="Ingrese su búsqueda........" />
            <button type="submit" class="search-btn">
                <svg width="20" height="20" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M17.1249 16.2411L12.4049 11.5211C13.5391 10.1595 14.1047 8.41291 13.9841 6.64483C13.8634 4.87675 13.0657 3.22326 11.7569 2.02834C10.4482 0.833415 8.7291 0.189061 6.95736 0.229318C5.18562 0.269575 3.49761 0.991344 2.24448 2.24448C0.991344 3.49761 0.269575 5.18562 0.229318 6.95736C0.189061 8.7291 0.833415 10.4482 2.02834 11.7569C3.22326 13.0657 4.87675 13.8634 6.64483 13.9841C8.41291 14.1047 10.1595 13.5391 11.5211 12.4049L16.2411 17.1249L17.1249 16.2411ZM1.49989 7.12489C1.49989 6.01237 1.82979 4.92483 2.44787 3.99981C3.06596 3.07478 3.94446 2.35381 4.97229 1.92807C6.00013 1.50232 7.13113 1.39093 8.22227 1.60797C9.31342 1.82501 10.3157 2.36074 11.1024 3.14741C11.889 3.93408 12.4248 4.93636 12.6418 6.02751C12.8588 7.11865 12.7475 8.24965 12.3217 9.27748C11.896 10.3053 11.175 11.1838 10.25 11.8019C9.32495 12.42 8.23741 12.7499 7.12489 12.7499C5.63355 12.7482 4.20377 12.1551 3.14924 11.1005C2.09471 10.046 1.50154 8.61622 1.49989 7.12489Z" fill="currentColor"></path>
                </svg>
            </button>
        </form>
    </div>

    <?php if(!EC()->is_home()): ?>
    <nav aria-label="Breadcrumb" class="breadcrumb">
        <div class="enc-container">
            <?php if ( function_exists( 'yoast_breadcrumb' ) ) { yoast_breadcrumb(); } ?>
        </div>
        <button class="breadcrumb-toggle-button mobile-only" title="Ver todo"><svg height="25" viewBox="0 0 45 50" width="25" xmlns="http://www.w3.org/2000/svg"><path d="M14.83 16.42l9.17 9.17 9.17-9.17 2.83 2.83-12 12-12-12z"/><path d="M0-.75h48v48h-48z" fill="none"/></svg></button>
    </nav>
    <?php endif; ?>
<?php //flush(); ?>
