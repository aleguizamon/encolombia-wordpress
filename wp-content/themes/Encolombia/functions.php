<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

if ( ! defined( 'EC_DIR' ) ) {
    define( 'EC_DIR', get_template_directory() );
}

// Include the main WooCommerce class.
if ( !class_exists( 'Encolombia', false ) ) {
    include_once EC_DIR . '/includes/class-encolombia.php';
}

/**
 * Returns the main instance of Encolombia class.
 *
 * @return Encolombia
 */
function EC() {
    return Encolombia::instance();
}
EC()->configure();