<?php
/**
 * Template Name: Página sin sidebar
 */

get_header();
?>
    <div class="enc-main-content-wrap ">
        <div class="enc-container">
            <div class="enc-row">
                <div class="col-md-12 enc-main-content" role="main">
                    <?php
                    if (have_posts()) {
                        while ( have_posts() ) : the_post();
                            ?>
                            <div class="enc-page-header">
                                <h1 class="entry-title enc-page-title">
                                    <span><?php the_title() ?></span>
                                </h1>
                            </div>
                            <div class="enc-page-content">
                                <?php the_content(); ?>
                            </div>
                        <?php   endwhile;//end loop

                    }
                    ?>
                    <?php if ( is_active_sidebar( 'after-content-widget-area')  ): ?>
                        <div class="after-content-widget-area">
                            <?php EC()->sidebars->render_sidebar('after-content-widget-area'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div> <!-- /.td-pb-row -->
        </div> <!-- /.td-container -->
    </div> <!-- /.td-main-content-wrap -->

<?php
get_footer();