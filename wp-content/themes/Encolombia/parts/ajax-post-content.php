<?php

$mod_single = new EC_Module_Single($next_post);
$html_category = $mod_single->get_category();
?>
<div class="post-inner async" id="post-inner-<?php echo $mod_single->post->ID; ?>" itemscope itemtype="https://schema.org/Article">
    <header>
        <div class="post-category">
            <?php echo $html_category; ?>
        </div>
        <?php echo $mod_single->get_title();?>
        <?php
        $is_pub = $mod_single->is_publication();
        if( (!$is_pub && !EC_Util::is_uri_section('/medicina/estudios-bioquivalencia/')) || ($is_pub && EC_Util::is_url_force_featured_image()) ){
            if(EC()->is_mobile){
                echo $mod_single->get_image('enc_330x205');
            } else {
                echo $mod_single->get_image('enc_696x0');
            }
        }
        ?>
    </header>
    <div class="post-content">
        <?php echo $next_content; ?>
        <?php echo $mod_single->render_structured_data(); ?>
        <?php echo $mod_single->render_more_cat_btn_from_html($html_category); ?>
        <?php comments_template('', true); ?>
    </div>
</div>