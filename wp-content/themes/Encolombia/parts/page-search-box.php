<?php

/*  ----------------------------------------------------------------------------
    This is the search box used at the top of the search results
    It's used by /search.php


 */

/**
 * @note:
 * we use esc_url(home_url( '/' )) instead of the WordPress @see get_search_link function because that's what the internal
 * WordPress widget it's using and it was creating duplicate links like: yoursite.com/search/search_query and yoursite.com?s=search_query
 */
?>

<h1 class="entry-title enc-page-title">
    <span class="enc-search-query"><?php echo get_search_query(); ?></span> - <span> <?php  echo __('Resultados de la búsqueda', 'EC');?></span>
</h1>

<div class="search-page-search-wrap">
    <form method="get" class="enc-search-form-widget" action="<?php echo esc_url(home_url( '/' )); ?>">
        <div role="search">
            <input class="enc-widget-search-input" type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" /><input class="wpb_button wpb_btn-inverse btn" type="submit" id="searchsubmit" value="Buscar" />
        </div>
    </form>
    <div class="enc_search_subtitle">
        Si no está satisfecho con los resultados, realice otra búsqueda
    </div>
</div>
