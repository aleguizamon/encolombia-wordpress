<?php if(!EC()->is_genfar_section && !EC()->is_home()): ?>
    <div class="container-adverts-top" style="min-height: <?php echo !EC()->is_mobile ? '277px' : '127px'; ?>;">
        <div class="enc-container " style="padding: 0px;">
            <?php
            $sidebar_name = '';
            if(EC()->is_post() && EC()->is_customer_section){
                $sidebar_name = 'enc_sponsoredpost_top';
            } else if(EC()->is_category() && EC()->is_customer_section){
                $sidebar_name = 'enc_sponsoredcategory_top';
            } else if(EC()->is_post() || EC()->is_page() || EC()->is_category()) {
                $sidebar_name = EC()->is_mobile ? 'enc_mob_top' : 'enc_top';
            }
            if(!empty($sidebar_name)){
                EC()->sidebars->render_sidebar($sidebar_name);
            }
            ?>
        </div>
    </div>
<?php endif; ?>