<?php
/*  ----------------------------------------------------------------------------
    the blog index template
 */

get_header();
?>
    <!--<div class="cat-header-bg"></div>-->
    <div class="enc-main-content-wrap enc-container-wrap">
        <div class="author-section dark">
            <div class="enc-container">
                <div class="enc-row">
                    <div class="col-lg-12">
                        <?php echo (new EC_Module_Author())->render(); ?>
                    </div>
                </div>
            </div>
        </div>

        <?php get_template_part( 'parts/advert-top', '' ); ?>

        <div class="author-posts-section ">
            <div class="enc-container">
                <div class="enc-row">
                    <div class="col-lg-12">
                        <h3 class="enc-block-title-1">
                            Publicaciones
                            <span class="titledot"></span>
                            <span class="titleline"></span>
                        </h3>

                        <?php
                        if (have_posts()) {
                            $ind = 0;
                            $cats = array();
                            echo '<div class="enc-row posts-container">';
                            while ( have_posts() ) : the_post();
                                global $post;
                                $module = new EC_Module_4($post);
                                echo $module->render();
                                $ind++;
                            endwhile;
                            echo '</div>';

                            EC()->layout->render_pagination();

                        } else {
                            echo EC_Util::no_posts();
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php

get_footer();
