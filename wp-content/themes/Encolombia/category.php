<?php
/*  ----------------------------------------------------------------------------
    the blog index template
 */

get_header();

$mc = EC()->manager_category;
?>
    <!--<div class="cat-header-bg"></div>-->
    <div class="enc-main-content-wrap enc-container-wrap">
        <div class="subcategory-section dark">
            <div class="enc-container">
                <div class="enc-row">
                    <div class="col-lg-12">
                        <h1 class="enc-block-title-2">
                            <?php echo $mc->category->name; ?>
                        </h1>
                        <?php $mc->render_childs(); ?>
                    </div>
                </div>
            </div>
        </div>

        <?php get_template_part( 'parts/advert-top', '' ); ?>

        <div class="category-posts-section <?php echo $mc->is_publication ? 'publications' : 'no-publications'; ?>">
            <div class="enc-container">
                <div class="enc-row">
                    <div class="col-lg-12">
                        <?php if(!$mc->is_publication): ?>
                            <h3 class="enc-block-title-1">
                                Nuevas Publicaciones
                                <span class="titledot"></span>
                                <span class="titleline"></span>
                            </h3>

                            <?php
                            if (have_posts()) {
                                $ind = 0;
                                echo '<div class="enc-row posts-container">';
                                while ( have_posts() ) : the_post();
                                    global $post;
                                    $mc->render_category_post($ind, $post);
                                    $ind++;
                                endwhile;
                                echo '</div>';
                            } else {
                                echo EC_Util::no_posts();
                            }

                            if(EC()->is_customer_section):
                                EC()->sidebars->render_sidebar('enc_sponsoredvideo');
                            endif;
                            ?>
                        <?php else: ?>
                            <?php
                            if(EC()->is_customer_section):
                                EC()->sidebars->render_sidebar('enc_sponsoredvideo');
                            endif;
                            ?>
                            <?php EC()->sidebars->render_sidebar('after-content-widget-area'); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php

get_footer();
