<?php
$sb1 = null;
$sb2 = null;
?>
<?php
    if(!EC()->is_mobile){//escritorio
        if((EC()->is_post() || EC()->is_page()) && EC()->is_customer_section){
            $sb1 = EC()->sidebars->get_sidebar_code('enc_sponsoredpost_rr_pos1');
        } else if(EC()->is_category() && EC()->is_customer_section) {
            $sb1 = EC()->sidebars->get_sidebar_code('enc_sponsoredcategory_rr_pos1');
        } else if( (EC()->is_post() || EC()->is_page()) && !EC()->is_customer_section){
            $sb1 = EC()->sidebars->get_sidebar_code('enc_sidebar1');
        } else if(EC()->is_category() && !EC()->is_customer_section) {
            $sb1 = EC()->sidebars->get_sidebar_code('enc_sidebar1');
        }
        if(!empty($sb1)) {
            echo $sb1;
        }

        EC()->sidebars->render_sidebar('segundo-widget-area');
        if((EC()->is_post() || EC()->is_page()) && EC()->is_customer_section){
            $sb2 = EC()->sidebars->get_sidebar_code('enc_sponsoredpost_rr_pos2');
        } else if(EC()->is_category() && EC()->is_customer_section) {
            $sb2 = EC()->sidebars->get_sidebar_code('enc_sponsoredcategory_rr_pos2');
        } else if( (EC()->is_post() || EC()->is_page()) && !EC()->is_customer_section){
            $sb2 = EC()->sidebars->get_sidebar_code('enc_sidebar2');
        } else if(EC()->is_category() && !EC()->is_customer_section) {
            $sb2 = EC()->sidebars->get_sidebar_code('enc_sidebar2');
        }
        if(!empty($sb2)) {
            echo '<div class="sb-sticky-ad">' . $sb2 . '</div>';
        }
    }
?>

