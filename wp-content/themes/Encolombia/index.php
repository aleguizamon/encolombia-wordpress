<?php
/*  ----------------------------------------------------------------------------
    the blog index template
 */

get_header();

$template_id = 'home';
$td_sidebar_position = 'td-sidebar-right';
?>

    <div class="enc-main-content-wrap enc-container-wrap">

        <div class="enc-container enc-blog-index <?php echo $td_sidebar_position; ?>">
            <div class="enc-row">
                <?php
                switch ($td_sidebar_position) {
                    case 'td-sidebar-right':
                        ?>
                        <div class="col-md-8 enc-main-content">
                            <div class="enc-ss-main-content">
                                <?php
                                //locate_template('loop.php', true);
                                //echo td_page_generator::get_pagination();
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4 enc-main-sidebar">
                            <?php get_sidebar(); ?>
                        </div>
                        <?php
                        break;
                    default:
                        ?>
                        <div class="col-md-12 enc-main-content">
                            <div class="enc-ss-main-content">
                                <?php
                                //locate_template('loop.php', true);
                                //echo td_page_generator::get_pagination();
                                ?>
                            </div>
                        </div>
                        <?php
                        break;
                }
                ?>
            </div> <!-- /.td-pb-row -->
        </div> <!-- /.td-container -->
    </div> <!-- /.td-main-content-wrap -->

<?php
get_footer();