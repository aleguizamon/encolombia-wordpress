<?php
/**
 * Template Name: Blank Page
 */
get_header();

if (have_posts()) { ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="enc-main-content-wrap enc-main-page-wrap">
            <div class="enc-container">
                <?php the_content(); ?>
            </div>
        </div> <!-- /.td-main-content-wrap -->
    <?php endwhile; ?>
<?php
}
get_footer();