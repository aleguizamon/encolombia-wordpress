<?php
get_header();


/**
 * The single post loop Default template
 **/

?>
<main class="enc-main-content-wrap enc-container-wrap">
    <?php
    if (have_posts()) {
        the_post();
        //global $post;
        $mp = EC()->manager_post;
        $base_author = new EC_Base_Author(EC()->manager_post->post);
        //$mod_single = new EC_Module_Single($post);
        //$title = $post->post_title;
        //$permalink = get_the_permalink($post->ID);
        //$thumbnail = get_the_post_thumbnail_url($post);
        //$cats = $mod_single->get_categories();
        //$current_cat = is_array($cats) && count($cats) > 0 ? $cats[0] : null;
        //$html_category = $mod_single->get_category();
        //$is_pub = EC()->is_publication; //$mod_single->is_publication();
        //$has_img = EC()->manager_post->thumbnail_url !== false; //has_post_thumbnail($mod_single->post->ID);
        $is_infinite_scroll = !EC_Util::is_exception_infinite_scroll();
        $is_mobile = EC()->is_mobile;
        //$is_genfar_section = EC()->is_genfar_section;
    ?>
        <div id="contentHolder">
            <div id="post-<?php echo $mp->post->ID;?>" class="<?php echo join(' ', get_post_class());?>" itemscope itemtype="https://schema.org/Article">
                <header>
                    <div class="entry-thumbnail-area <?php echo $mp->has_featured_image() ? '' : 'no-img' ?>">
                        <?php

                        if( $mp->has_featured_image() ){
                            if($is_mobile){
                                echo $mp->get_image('enc_330x205');
                            } else {
                                echo $mp->get_image('enc_1920x1024');
                            }
                        }
                        ?>

                    </div>
                    <div class="meta-fixed enc-container">
                        <div class="enc-row">
                            <div class="col-lg-3 col-md-12">&nbsp;</div>
                            <div class="col-lg-7 col-md-12">
                                <div class="post-category">
                                    <input type="hidden" name="current_cat" value="<?php echo !empty($mp->current_category) ? $mp->current_category->term_id : -1 ?>" />
                                    <?php echo $mp->get_category_html(); ?>
                                </div>
                                <?php echo $mp->get_title();?>
                                <?php if ($base_author->is_author): ?>
                                <ul class="post-meta">
                                    <li class="author-info">
                                        <?php echo $base_author->get_author_icon(); ?> <a href="#autor" rel="author" class="fn"><?php echo $base_author->author_name; ?></a>
                                    </li>
                                </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </header>

                <?php get_template_part( 'parts/advert-top', '' ); ?>

                <div class="post-main-content">
                    <div class="enc-container">
                        <div class="enc-row">
                            <div class="col-lg-2 col-md-3 col-sm-12">
                                <?php if($is_infinite_scroll): ?>
                                    <?php if(!$is_mobile): ?>
                                        <div class="post-nav-left">
                                            <div class="list">
                                                <?php echo $mp->render_other_posts_in_category(); ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div class="<?php echo $is_infinite_scroll ? 'col-lg-7 col-md-9 col-sm-12' : 'col-lg-7 col-md-12 col-sm-12'; ?>">
                                <div class="post-content">
                                    <div class="<?php echo $is_infinite_scroll ? 'ajax-scroll-post' : 'scroll-post'; ?>">
                                        <div class="post-inner" id="post-inner-<?php echo $mp->post->ID;?>">
                                            <?php echo $mp->get_content(); ?>
                                            <?php if($is_mobile): ?>
                                            <div class="post-nav-left">
                                                <div class="list">
                                                    <?php echo $mp->render_other_posts_in_category(); ?>
                                                </div>
                                                <div class="nav-buttons">
                                                    <button id="previous-btn" disabled>&lt; Anterior</button>
                                                    <button id="next-btn">Siguiente &gt;</button>
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                            <?php if ($base_author->is_author): ?>
                                                <?php echo (new EC_Module_Post_Author($mp->post))->render(); ?>
                                            <?php endif; ?>
                                            <?php echo $mp->render_structured_data(); ?>
                                            <?php echo $mp->render_more_cat_btn_from_html($mp->get_category_html()); ?>
                                            <div class="post-share" style="height: 121px;">
                                                <h4>Compartir:</h4>
                                                <?php echo do_shortcode('[elfsight_social_share_buttons id="1"]'); ?>
                                            </div>
                                            <?php comments_template('', true); ?>

                                            <?php if(is_active_sidebar( 'after-content-widget-area')): ?>
                                                <div class="bottom-post-related1">
                                                    <?php EC()->sidebars->render_sidebar('after-content-widget-area'); ?>
                                                </div>
                                            <?php endif; ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="<?php echo $is_infinite_scroll ? 'col-lg-3 col-md-12 col-sm-12' : 'col-lg-3 col-md-12 col-sm-12'; ?>">
                                <?php get_sidebar(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="end-loop-posts"></div>
        <?php if(!$is_mobile && !EC()->is_genfar_section): ?>
        <div class="bottom-post-related2">
            <div class="enc-container">
                <?php
                if($mp->is_publication){
                    echo $mp->related_publications();
                } else {
                    echo $mp->related_posts();
                }
                ?>
            </div>
        </div>
        <?php endif; ?>

    <?php
    } else {
        echo '<div class="enc-row"><div class="col-md-12">';
        //no posts
        echo EC_Util::no_posts();
        echo '</div></div>';
    }
    ?>
</main>
<?php get_footer(); ?>