"use strict";

// Class Definition
var ENCS_CAT = function() {

    var canLoadMore = true;
    var currentPage = 1;
    var loading = false;
    var container = null;
    //var loadMoreBtn = $('.load-more-btn');
    //var postType = loadMoreBtn.data('post-type');

    var init = function () {
        container = jQuery('.posts-container');
    }

    var showLoading = function () {
        jQuery('.posts-container').append('<div class="loading-posts">Cargando...</div>');
    }

    var removeLoading = function (){
        jQuery('.posts-container .loading-posts').remove();
    }

    var handleScroll = function () {
        jQuery(window).scroll(function() {
            //console.log('ll', jQuery(window).scrollTop() + jQuery(window).height(), container.offset().top + container.outerHeight() - 100, (jQuery(window).scrollTop() + jQuery(window).height()) > (container.offset().top + container.outerHeight() - 100));
            if(container.length > 0 && (jQuery(window).scrollTop() + jQuery(window).height() > container.offset().top + container.outerHeight() - 150)) {
                if(!loading && canLoadMore) {
                    loading = true;
                    showLoading();

                    jQuery.ajax({
                        url: enc_data.ajax_url,
                        type: 'POST',
                        data: {
                            action: 'load_more_posts',
                            current_page: currentPage + 1,
                            category: enc_data.cat_slug,
                        },
                        success: function(response) {
                            if(response.success) {
                                container.append(response.data.html);
                                currentPage++;
                                if(response.data.can_load_more) {
                                    loading = false;
                                } else {
                                    canLoadMore = false;
                                }
                                removeLoading();
                            }
                        }
                    });
                }
            }
        });
    }



    return {
        init: function() {
            init();
            handleScroll();
        },

    };

}();




jQuery(document).ready(function() {
    "use strict";

    ENCS_CAT.init();
});