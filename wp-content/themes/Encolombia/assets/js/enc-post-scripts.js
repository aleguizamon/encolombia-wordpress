"use strict";

// Class Definition
var ENCS_POST = function() {

    var searchPostPosition = function () {
        var current = jQuery('.post-nav-left ol li[data-id="'+ enc_data.current_post +'"]').data('position');
        return jQuery.isNumeric(current) ? current : 0;
    }

    var bottomOffset = 3200;
    var max_count = 4;
    var onScrollPagi = true;
    var current_count_post = null;
    //var initial_count_post = null;
    var current_cat = null;
    var scroll_load_count = 0;

    var init = function () {
        current_cat = jQuery('input[name="current_cat"]').val();
        current_count_post = searchPostPosition();
        max_count = calculateMaxCount();
        //initial_count_post = current_count_post;
        current_count_post++;
    }

    var calculateMaxCount = function () {
        return jQuery('body.is_pub').length > 0 ? max_count = 1000 : max_count = 4;
    }

    var showLoading = function () {
        //jQuery('.ajax-scroll-post').append('<div class="loading-post">Cargando...</div>');
    }

    var removeLoading = function (){
        jQuery('.ajax-scroll-post .loading-post').remove();
    }

    var showNextBtn = function () {
        if(onScrollPagi){
            jQuery('.ajax-scroll-post').append('<div class="next-post"><a href="#" title="Siguiente post">Ver Siguiente Post</a></div>');
        }
    }

    var removeNextBtn = function () {
        jQuery('.ajax-scroll-post .next-post').remove();
    }

    var changeUrl = function (url) {
        window.history.pushState({}, '', url);
    }

    var activeSideMenuItem = function (id) {
        jQuery('.post-nav-left ol li.active').removeClass('active');
        jQuery('.post-nav-left ol li[data-id="'+id+'"]').addClass('active');
    }

    var adUnits = [
        ['ENC_Post_Inline_Pos1', [[728, 90], [300, 250]] ],
        ['ENC_Post_Inline_Pos2', [[728, 90], [300, 250]] ],
        ['ENC_Post_Inline_Pos3', [[728, 90], [300, 250]] ],
        ['ENC_Post_Inline_Pos4', [[728, 90], [300, 250]] ],
        ['ENC_Post_Inline_Pos5', [[728, 90], [300, 250]] ],
        ['ENC_Post_RR_Pos1', [[300, 250], [300, 600]] ],
        ['ENC_Post_RR_Pos2', [[300, 250], [300, 600]] ],
        ['ENC_Post_RR_Pos3', [[300, 250], [300, 600]] ],
    ];

    var searchAdUnitSizes = function (valor) {
        for (var i = 0; i < adUnits.length; i++) {
            if (adUnits[i][0] === valor) {
                return adUnits[i][1];
            }
        }
        return null; // Devuelve null si no se encuentra el elemento
    }

    var generateRandomId = function () {
        var characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        var length = 10;
        var randomId = '';

        for (var i = 0; i < length; i++) {
            var randomIndex = Math.floor(Math.random() * characters.length);
            randomId += characters.charAt(randomIndex);
        }

        return randomId;
    }

    var replaceAdsContent = function (content) {
        var ids = {};
        var parser = new DOMParser();
        var doc = parser.parseFromString(content, 'text/html');

        var asides = doc.getElementsByTagName('aside');
        for (var i = 0; i < asides.length; i++) {
            var aside = asides[i];
            var classList = aside.classList;

            for (var j = 0; j < adUnits.length; j++) {
                var adUnit = adUnits[j][0].toLowerCase();
                if (classList.contains(adUnit)) {
                    aside.innerHTML = ''; // Elimina el contenido de la etiqueta aside

                    var nuevoSlotId = "div-gpt-ad-" + generateRandomId();
                    var nuevoDiv = document.createElement("div");
                    nuevoDiv.id = nuevoSlotId;
                    //nuevoDiv.style.minWidth = "320px";
                    //nuevoDiv.style.minHeight = "50px";
                    aside.appendChild(nuevoDiv);
                    ids[adUnits[j][0]] = nuevoSlotId;
                    break;
                }
            }
        }
        return [doc.documentElement.innerHTML, ids];
    }

    var loadNewAds = function (ids) {
        for (var prop in ids) {
            var sizes = searchAdUnitSizes(prop);
            if (ids.hasOwnProperty(prop) && sizes) {
                googletag.cmd.push(function() {
                    console.log('aaa');
                    var slot = googletag.defineSlot('/1020006/' + prop, sizes, ids[prop]).addService(googletag.pubads());
                    googletag.display(ids[prop]);
                    googletag.pubads().refresh([slot]);

                    setInterval(function() {
                        googletag.cmd.push(function() {
                            googletag.pubads().refresh([slot]);
                        });
                    }, 30000); // 60 segundos
                });
            }
        }
    }

    var getNextPost = function (callback) {
        showLoading();
        jQuery.ajax({
            url: enc_data.ajax_url,
            data: {
                action: 'getNextPost',
                position: current_count_post,
                cat_id: current_cat
            },
            type: 'POST',
            dataType: 'json',
            beforeSend: function() {
                onScrollPagi = false;
            },
            success: function(resp) {
                if (resp.success) {
                    removeLoading();
                    if(resp.data.current_id == 0){
                        onScrollPagi = false;
                    } else {
                        if(enc_data.is_sponsored == 'Y') {
                            var contentData = replaceAdsContent(resp.data.next_content);
                            var content = contentData[0];
                            jQuery('.ajax-scroll-post').append(content);
                            loadNewAds(contentData[1]);
                        } else {
                            jQuery('.ajax-scroll-post').append(resp.data.next_content);
                        }

                        console.log('changeUrl1');
                        changeUrl(resp.data.next_url);

                        // Cambiar el título de la página
                        document.title = resp.data.next_title;

                        activeSideMenuItem(resp.data.current_id);
                        current_count_post++;
                        onScrollPagi = true;

                        if(callback && typeof callback === 'function'){
                            callback();
                        }
                        /*var contentData = replaceAdsContent(resp.data.next_content);
                        var content = contentData[0];
                        jQuery('#div-gpt-ad-9101225-2').html('');
                        jQuery('.ajax-scroll-post').append(content);
                        loadNewAds(contentData[1]);
                        console.log('changeUrl9');
                        changeUrl(resp.data.next_url);

                        // Cambiar el título de la página
                        document.title = resp.data.next_title;

                        activeSideMenuItem(resp.data.current_id);
                        current_count_post++;
                        onScrollPagi = true;

                        if(callback && typeof callback === 'function'){
                            callback();
                        }*/
                    }

                }
            }
        })
    }

    var checkPostIsLoaded = function (id) {
        if(jQuery('#post-inner-'+id).length > 0){
            return true;
        }
        return false;
    }

    var handleNextPostBtn = function () {
        jQuery('body').on('click', '.next-post a', function(event) {
            event.preventDefault();
            removeNextBtn();
            getNextPost();
        })
    }


    var handleShowCommentsBtn = function() {
        if (jQuery("#title-form-comments a").length > 0) {
            jQuery('body').on('click', '#title-form-comments a', function(event) {
                event.preventDefault();
                jQuery('#container-respond').toggleClass('open');
            })
        }
        if (jQuery(".comments-list .comment-title a").length > 0) {
            jQuery('body').on('click', '.comments-list .comment-title a', function(event) {
                event.preventDefault();
                jQuery('.comments-list .comments-container').toggleClass('open');
                if(jQuery('.comments-list .comments-container').hasClass('open')){
                    jQuery('.comments-list .comment-title a span').text('OCULTAR ');
                } else {
                    jQuery('.comments-list .comment-title a span').text('VER ');
                }
            })
        }
    }

    var handleProgressBar = function() {
        jQuery(window).on('scroll', scrollFunction);
        function scrollFunction() {
            var target = jQuery('#contentHolder');
            if (target.length > 0) {
                var contentHeight = target.outerHeight();
                var documentScrollTop = jQuery(document).scrollTop();
                var targetScrollTop = target.offset().top;
                var scrolled = documentScrollTop - targetScrollTop;
                if (0 <= scrolled) {
                    var scrolledPercentage = (scrolled / contentHeight) * 100;
                    if (scrolledPercentage >= 0 && scrolledPercentage <= 100) {
                        scrolledPercentage = scrolledPercentage >= 90 ? 100 : scrolledPercentage;
                        jQuery("#encpbar").css({
                            width: scrolledPercentage + "%"
                        })
                    }
                } else {
                    jQuery("#encpbar").css({
                        width: "0%"
                    })
                }
            }
        }
    }

    var handleStickyHeader = function() {
        jQuery(window).on('scroll', function() {
            if (jQuery(this).scrollTop() > 100) {
                jQuery("body").addClass("not-top");
                jQuery("body").removeClass("top")
            } else {
                jQuery("body").addClass("top");
                jQuery("body").removeClass("not-top")
            }
        });
    }

    var handleScrollPost = function() {
        if (jQuery(".ajax-scroll-post").length > 0) {

            //var current_post = enc_data.current_post;
            jQuery(window).scroll(function() {
                if (!onScrollPagi)
                    return;
                if (scroll_load_count >= max_count) {
                    showNextBtn();
                    onScrollPagi = false;
                    return
                }
                if (jQuery(document).scrollTop() > (jQuery(document).height() - bottomOffset) && onScrollPagi) {
                    //let cat_ids = jQuery('input#neeon-cat-ids').val();
                    scroll_load_count++;
                    getNextPost();
                }
            })
        }
    }

    var handleSideMenu = function() {
        if (jQuery(".post-nav-left ol li").length > 0) {
            jQuery('body').on('click', '.post-nav-left ol li a', function(event) {
                event.preventDefault();
                var id = jQuery(this).parent('li').data('id');
                var url = jQuery(this).attr('href');
                if(checkPostIsLoaded(id)){
                    jQuery('html, body').animate({
                        scrollTop: jQuery('#post-inner-'+id).offset().top - 80
                    }, 1000);
                    changeUrl(url);
                    activeSideMenuItem(id);
                } else {
                    jQuery('html, body').animate({
                        scrollTop: jQuery('.end-loop-posts').offset().top - 100
                    }, 1000);
                    current_count_post = jQuery(this).parent('li').data('position');
                    getNextPost(function () {
                        jQuery('html, body').animate({
                            scrollTop: jQuery('#post-inner-'+id).offset().top - 80
                        }, 1000);
                        changeUrl(url);
                        activeSideMenuItem(id);
                    });
                }

            });
        }

    }

    var handleListPostsNavButtons = function() {
        if (jQuery(".post-nav-left .nav-buttons").length == 0) {
            return;
        }

        var itemsPerPage = 5; // Número de elementos a mostrar por página
        var $listContainer = jQuery('.post-nav-left ol');
        var $previousBtn = jQuery('#previous-btn');
        var $nextBtn = jQuery('#next-btn');
        var currentIndex = 0;

        // Cargar los primeros elementos al cargar la página
        loadItems(currentIndex);

        // Cargar los elementos anteriores al hacer clic en el botón "Anterior"
        $previousBtn.on('click', function() {
            if (currentIndex > 0) {
                currentIndex -= itemsPerPage;
                loadItems(currentIndex);
                updateButtons();
            }
        });

        // Cargar los elementos siguientes al hacer clic en el botón "Siguiente"
        $nextBtn.on('click', function() {
            currentIndex += itemsPerPage;
            loadItems(currentIndex);
            updateButtons();
        });

        // Cargar los elementos mediante AJAX
        function loadItems(startIndex) {
            jQuery('.post-nav-left ol li.item').css('display', 'none');
            jQuery('.post-nav-left ol li.item').slice(startIndex, startIndex + itemsPerPage).css('display', 'block');
            // Simular una llamada AJAX para obtener los datos
            //var items = getItemsFromServer(startIndex, itemsPerPage);

            // Limpiar la lista actual
            //$listContainer.empty();

            // Agregar los elementos a la lista
            /*$.each(items, function(index, item) {
                var listItem = '<li>' + item + '</li>';
                $listContainer.append(listItem);
            });*/
        }

        // Obtener los elementos de la lista desde el servidor (simulado)
        function getItemsFromServer(startIndex, count) {
            var items = [];
            var $listItems = $('.item');

            for (var i = startIndex; i < startIndex + count; i++) {
                var $item = $listItems.eq(i);
                if ($item.length) {
                    items.push($item.text());
                }
            }

            return items;
        }

        // Actualizar el estado de los botones de navegación
        function updateButtons() {
            var $listItems = jQuery('.post-nav-left ol li.item');
            var totalItems = $listItems.length;

            $previousBtn.prop('disabled', currentIndex === 0);
            $nextBtn.prop('disabled', currentIndex + itemsPerPage >= totalItems);
        }
    }

    return {
        init: function() {
            init();
            handleShowCommentsBtn();
            handleProgressBar();
            handleStickyHeader();
            handleScrollPost();
            handleNextPostBtn();
            //handleSideMenu();
            handleListPostsNavButtons();
        },

    };

}();




jQuery(document).ready(function() {
    "use strict";

    ENCS_POST.init();
});
