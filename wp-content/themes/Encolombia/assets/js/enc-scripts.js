"use strict";

// Class Definition
var ENCS = function() {

    var _fixedHeader = function(){
        window.onscroll = function() {myFunctionScroll()};
        var sticky = 100;
        var stickym = 70;

        function myFunctionScroll() {
            if (jQuery('#header').css('display') == 'block') {
                //console.log('www', jQuery(window).scrollTop(), sticky);
                if (jQuery(window).scrollTop() > sticky) {
                    jQuery('#header').addClass("header-sticky");
                    jQuery('nav.breadcrumb').addClass("breadcrumb-sticky");
                } else {
                    jQuery('#header').removeClass("header-sticky");
                    jQuery('nav.breadcrumb').removeClass("breadcrumb-sticky");
                }
            } else if (jQuery('.mobile-header-bar').css('display') == 'block') {
                //console.log('mmm', jQuery(window).scrollTop(), stickym);
                if (jQuery(window).scrollTop() > stickym) {
                    jQuery('.mobile-header-bar').addClass("header-sticky");
                    jQuery('nav.breadcrumb').addClass("breadcrumb-sticky-m");
                } else {
                    jQuery('.mobile-header-bar').removeClass("header-sticky");
                    jQuery('nav.breadcrumb').removeClass("breadcrumb-sticky-m");
                }
            }
        }
    }

    var _handleTabsIndice = function (){
        var tabsIndice = document.querySelectorAll('.enc-tabs-indice');
        if(tabsIndice.length > 0){
            var tabs = document.querySelectorAll('.enc-tab-item');
            for(var i=0; i<tabs.length; i++){
                tabs[i].addEventListener("click", function (ev) {
                    for(var j=0; j<tabs.length; j++){
                        tabs[j].classList.remove('tab-active');
                    }
                    ev.target.classList.add('tab-active');
                    var content = document.querySelectorAll('.enc-tabs-content-item');
                    for(var j=0; j<content.length; j++){
                        content[j].classList.remove('tab-active');
                    }
                    document.getElementById('tab-' + ev.target.dataset.selector).classList.add('tab-active');
                });
            }
        }
    }


    var _handleSearchBtn = function() {
        jQuery('a[href="#enc-header-search"]').on("click", function(event) {
            event.preventDefault();
            jQuery("#enc-header-search").addClass("open");
            jQuery('#enc-header-search > form > input[type="search"]').focus()
        });
        jQuery("#enc-header-search, #enc-header-search button.close").on("click keyup", function(event) {
            if (event.target === this || event.target.className === "close" || event.keyCode === 27) {
                jQuery(this).removeClass("open")
            }
        });
    }

    var _handleMovilBtn = function (){
        jQuery('.mobile-header-bar .mobile-toggle-btn').on('click', function(e) {
            e.preventDefault();
            if (jQuery('.mobile-menu').is(":visible")) {
                jQuery('.mobile-menu').slideUp();
                jQuery('body').removeClass('slidemenuon')
            } else {
                jQuery('.mobile-menu').slideDown();
                jQuery('body').addClass('slidemenuon')
            }
        });

    }

    var _handleMenuMovil = function (){
        jQuery('#menu-mobilesinglemenu li.menu-item-has-children > a').on('click', function(e) {
            e.preventDefault();
            var _self = jQuery(this)
                , sub_menu = _self.parent().find('>.sub-menu');
            if (_self.hasClass('open')) {
                sub_menu.slideUp();
                _self.removeClass('open')
            } else {
                sub_menu.slideDown();
                _self.addClass('open')
            }
        });

    }

    var _handleTabs = function (){
        var tabs = document.querySelectorAll('.vc_tta-tab-link');
        for(var i=0; i<tabs.length; i++){
            tabs[i].addEventListener("click", function (ev) {
                var panels = document.querySelectorAll('.vc_tta-panel.show-element');
                for(var i=0;i<panels.length;i++) panels[i].classList.remove('show-element');
                var tabs = document.querySelectorAll('li.vc_tta-tab.vc_active');
                for(var i=0;i<tabs.length;i++) tabs[i].classList.remove('vc_active');
                if(ev.target.nodeName == 'SPAN'){
                    document.getElementById(ev.target.parentElement.dataset.tab.substr(1)).classList.add('show-element');
                    ev.target.parentElement.parentElement.classList.add('vc_active');
                } else if(ev.target.nodeName == 'A'){
                    document.getElementById(ev.target.dataset.tab.substr(1)).classList.add('show-element');
                    ev.target.parentElement.classList.add('vc_active');
                }
            });
        }
    }

    var _handleLinkExpandContent = function (){
        var links = document.querySelectorAll('.link-expand-content');
        for(var i=0; i<links.length; i++){
            links[i].addEventListener("click", function (ev) {
                if(ev.target.nodeName == 'SPAN'){
                    document.getElementById(ev.target.parentElement.dataset.container).classList.toggle('hide-element');
                } else if(ev.target.nodeName == 'A'){
                    document.getElementById(ev.target.dataset.container).classList.toggle('hide-element');
                }
            });
        }
    }

    var toggleBreadcrumbHeight = function() {
        var breadcrumbContainer = document.querySelector('.breadcrumb');
        var toggleButton = document.querySelector('.breadcrumb-toggle-button');
        
        if (breadcrumbContainer) {
            // Actualizar el estado del botón al cargar la página
            window.addEventListener('load', function() {
                console.log(breadcrumbContainer.scrollHeight);
                if (breadcrumbContainer.scrollHeight > 44) {
                    breadcrumbContainer.style.maxHeight = '35px';
                    toggleButton.style.display = 'block';
                }else{
                    breadcrumbContainer.style.maxHeight = breadcrumbContainer.scrollHeight + 'px';
                    toggleButton.style.display = 'none';
                }
            });

            toggleButton.addEventListener("click", function() {
                if (breadcrumbContainer.style.maxHeight === '35px') {
                    breadcrumbContainer.style.maxHeight = breadcrumbContainer.scrollHeight + 'px';
                    toggleButton.innerHTML = '<svg height="25" viewBox="0 0 45 50" width="25" xmlns="http://www.w3.org/2000/svg"><path d="M14.83 30.83l9.17-9.17 9.17 9.17 2.83-2.83-12-12-12 12z"/><path d="M0 0h48v48h-48z" fill="none"/></svg>';
                } else {
                    breadcrumbContainer.style.maxHeight = '35px';
                    toggleButton.innerHTML = '<svg height="25" viewBox="0 0 45 50" width="25" xmlns="http://www.w3.org/2000/svg"><path d="M14.83 16.42l9.17 9.17 9.17-9.17 2.83 2.83-12 12-12-12z"/><path d="M0-.75h48v48h-48z" fill="none"/></svg>';
                }
            });
        }   
        
    };


    return {
        // public functions
        init: function() {
            _fixedHeader();
            //_handleStickyHeader();
            _handleSearchBtn();
            _handleMovilBtn();
            _handleMenuMovil();
            _handleTabs();
            _handleTabsIndice();
            _handleLinkExpandContent();
            toggleBreadcrumbHeight();
        },

    };

}();

jQuery(document).ready(function($) {
    ENCS.init();
});