<?php

if (have_posts()) {
    $ind = 0;
    global $exist_subcats;
    $cats = array();

    if(!is_search()){
        echo '<div class="enc-row loop-nuevas-pub">';
    }
    while ( have_posts() ) : the_post();

        global $post;
        if(is_search()){
          $module = new enc_module_search($post);
        }else{
          $module = new enc_module_4($post);
        }
        echo $module->render();
        $ind++;
    endwhile; //end loop
    if(!is_search()){
        echo '</div>';
    }
    
} else {
    echo enc_util::no_posts();
}
