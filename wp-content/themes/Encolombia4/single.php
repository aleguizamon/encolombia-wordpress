<?php
get_header();

if(enc_util::is_posts_v3()){
    locate_template('single-v3.php', true);
} else {
    locate_template('single-v2.php', true);
}

get_footer();
?>