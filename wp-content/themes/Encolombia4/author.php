<?php
/*  ----------------------------------------------------------------------------
    the blog index template
 */

get_header();

$ind_pos_ads = 0;
$current_row = 0;
$ITEMS_BY_ROW = 4;
$ITEMS_BY_ROW_M = 1;

global $is_publication, $enc_block_subcat_pub;

$enc_block = $enc_block_subcat_pub;
/*$is_publication = enc_base_category::is_publication(); //is_category ($publications);
if($is_publication){
    $enc_block = enc_block_list_publications::get_instance();
} else{
    $enc_block = enc_block_list_subcategories::get_instance();
}*/

//$enc_social = enc_social::get_instance();

?>
    <!--<div class="cat-header-bg"></div>-->
    <div class="enc-main-content-wrap enc-container-wrap">
        <div class="author-section dark">
            <div class="enc-container">
                <div class="enc-row">
                    <div class="col-lg-12">
                        <?php echo (new enc_module_author())->render(); ?>
                    </div>
                </div>
            </div>
        </div>

        <?php get_template_part( 'parts/advert-top', '' ); ?>

        <div class="author-posts-section ">
            <div class="enc-container">
                <div class="enc-row">
                    <div class="col-lg-12">
                        <h3 class="enc-block-title-1">
                            Publicaciones
                            <span class="titledot"></span>
                            <span class="titleline"></span>
                        </h3>

                        <?php
                        if (have_posts()) {
                            $ind = 0;
                            //global $exist_subcats;
                            $cats = array();
                            echo '<div class="enc-row posts-container">';
                            while ( have_posts() ) : the_post();
                                global $post;
                                $module = new enc_module_4($post);
                                echo $module->render();

                                /*if( (!wp_is_mobile() && ($ind+1) % $ITEMS_BY_ROW == 0) || (wp_is_mobile() && ($ind+1) % $ITEMS_BY_ROW_M == 0) ){
                                    $current_row++;
                                }

                                $sidebar_info = null;
                                if(isset(enc_params::$ads_units_category[$ind_pos_ads])){
                                    $sidebar_info = enc_sidebars::get_instance()->get_sidebar_info(enc_params::$ads_units_category[$ind_pos_ads]);
                                }
                                if($sidebar_info){
                                    if(!wp_is_mobile() && strpos(enc_params::$ads_units_category[$ind_pos_ads], 'enc_category_rr_pos') === false && isset($sidebar_info['row']) && is_numeric($sidebar_info['row']) && $current_row == $sidebar_info['row'] ){
                                        enc_sidebars::get_instance()->render_sidebar(enc_params::$ads_units_category[$ind_pos_ads]);
                                        $ind_pos_ads++;
                                    } else if(wp_is_mobile() && isset($sidebar_info['row_m']) && is_numeric($sidebar_info['row_m']) && $current_row == $sidebar_info['row_m']){
                                        enc_sidebars::get_instance()->render_sidebar(enc_params::$ads_units_category[$ind_pos_ads]);
                                        $ind_pos_ads++;
                                    }
                                }*/

                                $ind++;
                            endwhile;
                            echo '</div>';

                            enc_util::get_pagination();

                        } else {
                            echo enc_util::no_posts();
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php

get_footer();
