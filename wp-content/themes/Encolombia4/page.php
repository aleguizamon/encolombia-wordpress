<?php
/*  ----------------------------------------------------------------------------
    the default page template
 */

get_header();

$use_page_builder = enc_util::is_page_builder_content();

if ( function_exists( 'wpcf7_enqueue_scripts' ) && is_page([6124, 16838, 10171]) ) {
    wpcf7_enqueue_scripts();
}
if ( function_exists( 'wpcf7_enqueue_styles' ) && is_page([6124, 16838, 10171]) ) {
    wpcf7_enqueue_styles();
}

if ($use_page_builder) {

    if (have_posts()) { ?>
        <?php while ( have_posts() ) : the_post(); ?>

           <div class="enc-main-content-wrap enc-main-page-wrap">
                <div class="<?php if (!enc_util::tdc_is_installed()) { echo 'enc-container '; } ?>">
                    <?php the_content(); ?>
                </div>
                <?php
                if(enc_params::$enable_or_disable_page_comments == 'show_commentsx') {
                    ?>
                    <div class="enc-container">
                        <?php comments_template('', true); ?>
                    </div>
                    <?php
                }
                ?>
            </div> <!-- /.td-main-content-wrap -->


        <?php endwhile; ?>
    <?php }
} else {
    ?>

    <div class="enc-main-content-wrap ">
        <div class="enc-container">

            <div class="enc-row">
                <div class="col-md-8 enc-main-content" role="main">
                        <?php
                        if (have_posts()) {
                            while ( have_posts() ) : the_post();
                                ?>
                                <div class="enc-page-header">
                                    <h1 class="entry-title enc-page-title">
                                        <span><?php the_title() ?></span>
                                    </h1>
                                </div>
                                <div class="enc-page-content">
                                    <?php the_content(); ?>
                                </div>
                            <?php   endwhile;//end loop

                        }
                        ?>

                        <?php if ( (!is_home()) || (!is_front_page()) /*&& (is_active_sidebar( 'after-content-widget-area') || is_active_sidebar( 'after-content-widget-area-right'))*/ ) : ?>
                            <div class="enc-row">
                                <div class="col-md-6">
                                    <?php echo "<div id='after-content-widget-area' style='margin-bottom: 30px;'></div>"; ?>
                                    <?php //if(is_active_sidebar( 'after-content-widget-area')){ dynamic_sidebar( 'after-content-widget-area' ); }else{echo "&nbsp;";} ?>
                                </div>
                                <div class="col-md-6">
                                    <?php
                                    if(is_active_sidebar( 'after-content-widget-area-right')){
                                        dynamic_sidebar( 'after-content-widget-area-right' );
                                    }else{
                                        echo "&nbsp;";
                                    } ?>
                                </div>
                            </div>
                        <?php endif; ?>


                    <?php
                    if(enc_params::$enable_or_disable_page_comments == 'show_commentsx') {
                        comments_template('', true);
                    }?>
                </div>
                <div class="col-md-4 enc-main-sidebar" role="complementary">
                    <?php get_sidebar(); ?>
                </div>
            </div> <!-- /.td-pb-row -->
        </div> <!-- /.td-container -->
    </div> <!-- /.td-main-content-wrap -->

    <?php
}




get_footer();