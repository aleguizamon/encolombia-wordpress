var ENCS;

(function ($) { "use strict";
    $(document).ready(function () {
        ENCS = {
            fixed_header: function(){
                if($(window).width()<=768){
                    var ht = $('.enc-header-menu-wrap').height();
                    var st = $(window).scrollTop();
                    if(st>ht){
                        $('body').addClass('sticky-header');
                    }else{
                        $('body').removeClass('sticky-header');
                    }
                }else{
                    $('body').removeClass('sticky-header');
                }
            },
            handleTabs1: function (){
                if($('.enc-tabs-indice').length > 0){
                    $('.enc-tabs-indice .enc-tabs-header .enc-tab-item').on('click', function (e){
                        e.preventDefault();
                        $('.enc-tabs-indice .enc-tabs-header .enc-tab-item').removeClass('tab-active');
                        $('#tabselector-' + $(e.currentTarget).data('selector')).addClass('tab-active');

                        $('.enc-tabs-indice .enc-tabs-content .enc-tabs-content-item').removeClass('tab-active');
                        $('#tab-' + $(e.currentTarget).data('selector')).addClass('tab-active');
                        console.log('clic nuevo ', $(e.currentTarget).data('selector'));
                    });
                }
            }
        /*POST_SLUG: '',
        PAGE_TYPE: '',
        CATEGORY_ID: 0,
        POST_ID: 0,
        registerGlobalVars: function () {
            ENCS.POST_SLUG = ajax_enc_data.enc_slug;
            ENCS.PAGE_TYPE = ajax_enc_data.enc_page_type;
            ENCS.CATEGORY_ID = ajax_enc_data.enc_category_id;
            ENCS.POST_ID = ajax_enc_data.enc_post_id;
        },*/
        }

        jQuery(window).on('scroll',function(){
            //Fixed Header
            ENCS.fixed_header();
            ENCS.handleTabs1();
        });
        //ENCS.registerGlobalVars();

    });
}(jQuery));