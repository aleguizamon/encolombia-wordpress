<?php
/**
 * Template Name: Blank Page
 */
get_header();


if (have_posts()) { ?>
    <?php while ( have_posts() ) : the_post(); ?>

        <div class="enc-main-content-wrap enc-main-page-wrap">
            <div class="<?php if (!enc_util::tdc_is_installed()) { echo 'enc-container '; } ?>">
                <?php the_content(); ?>
            </div>
            <?php
            if(enc_params::$enable_or_disable_page_comments == 'show_commentsx') {
                ?>
                <div class="enc-container">
                    <?php comments_template('', true); ?>
                </div>
                <?php
            }
            ?>
        </div> <!-- /.td-main-content-wrap -->


    <?php endwhile; ?>
<?php
}
get_footer();