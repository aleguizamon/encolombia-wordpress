<?php

class enc_is_url {

    public function render($atts, $content = null){
        if(isset($atts['url']) && $atts['url'] == '/' && is_front_page()){
            return do_shortcode( $content );
        } else {
            return null;
        }
    }

    public function render_sortable($atts, $content = null){
        /*if( !enc_util::is_customer_section() ){
            return do_shortcode( $content );
        } else {
            return '';
        }
        return do_shortcode( $content );*/
        return '';
    }

    public function render_not_sortable($atts, $content = null){
        /*if( enc_util::is_customer_section() ){
            return do_shortcode( $content );
        } else {
            return '';
        }*/
        return do_shortcode( $content );
    }

    public function render_not_is_customer_section($atts, $content = null){
        if( !enc_util::is_customer_section() ){
            return do_shortcode( $content );
        } else {
            return '';
        }
    }

    public function render_is_test_page($atts, $content = null){
        if( enc_util::is_single_test2()){
            return do_shortcode( $content );
        } else {
            return '';
        }
    }

    public function render_not_is_test_page($atts, $content = null){
        if( !enc_util::is_single_test2() ){
            return do_shortcode( $content );
        } else {
            return '';
        }
    }

}