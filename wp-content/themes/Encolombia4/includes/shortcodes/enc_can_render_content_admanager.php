<?php

class enc_can_render_content_admanager {

    public function render($atts, $content = null){
        if($this->can_render($atts['id'] ?? '')){
            return do_shortcode( $content );
        } else {
            return null;
        }
    }


    protected function can_render($ad_id){
        $can = false;
        switch ($ad_id){
            case 'enc_post_inline_pos1':
            case 'enc_post_inline_pos2':
            case 'enc_post_inline_pos3':
            case 'enc_post_inline_pos4':
            case 'enc_post_inline_pos5':
            case 'enc_post_rr_pos1':
            case 'enc_post_rr_pos2':
            case 'enc_post_rr_pos3':
                $sidebar_info = enc_sidebars::get_instance()->get_sidebar_info($ad_id);
                if(is_single() && $sidebar_info && (!wp_is_mobile() && isset($sidebar_info['paragraph'])) || ( wp_is_mobile() && isset($sidebar_info['paragraph_m']) ) ){
                    /**
                     * @var $post WP_Post
                     */
                    global $post;
                    $can = $this->can_insert_ad_in_content($post->post_content, wp_is_mobile() ? (int)$sidebar_info['paragraph_m'] : (int)$sidebar_info['paragraph']);
                }
                break;
        }

        return $can;
    }

    protected function can_insert_ad_in_content($content, $paragraph_number){
        $closing_p = '</p>';
        $paragraphs = explode( $closing_p, $content );
        $count = 0;
        foreach ($paragraphs as $paragraph) {
            if ( trim( $paragraph ) ) {
                $count++;
            }
        }
        return $count >= $paragraph_number;
    }


}