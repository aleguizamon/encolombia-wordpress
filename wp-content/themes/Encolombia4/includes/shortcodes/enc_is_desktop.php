<?php

class enc_is_desktop {

    public function render($atts, $content = null){
        if( !wp_is_mobile() ){
            return do_shortcode( $content );
        } else {
            return null;
        }
    }

}