<?php

class enc_can_render_admanager {
    
    const GENFAR = "/medicina/folletos-genfar/";
    
    const SLOTS = array(
	    'enc_top',
        'enc_inline1',
        'enc_inline2',
        'enc_inline3',
        'enc_inline4',
        'enc_inline5',
        'enc_inline6',
        'enc_sidebar1',
        'enc_sidebar2',
        'enc_mob_top',
        'enc_mob_inline1',
        'enc_mob_inline2',
        'enc_mob_inline3',
        'enc_mob_inline4',
        'enc_mob_inline5',
        'enc_mob_inline6',
        'enc_mob_inline7',
        'enc_mob_inline8',
        'enc_sponsoredpost_inline_pos1',
        'enc_sponsoredpost_inline_pos2',
	    'enc_sponsoredpost_rr_pos1',
        'enc_sponsoredpost_rr_pos2',
        'enc_sponsoredpost_rr_pos3',
	    'enc_video',
        'enc_sponsoredvideo',
	    'enc_sponsoredcategory_inline_pos1',
        'enc_sponsoredcategory_inline_pos2',
	    'enc_sponsoredcategory_rr_pos1',
        'enc_sponsoredcategory_rr_pos2',
        'enc_sponsoredcategory_rr_pos3'
    );

    public function render($atts, $content = null){
        $ad_id = $this->detect_ad_id($content);
        
        if($this->can_render($ad_id)){
            return $content;
        } else {
            return null;
        }
    }

    protected function detect_ad_id($content){
        $ad_id = '';
        $content = strtolower($content);
        foreach (self::SLOTS as $SLOT){
            if(strpos($content, $SLOT) !== false){
                $ad_id = $SLOT;
            }
        }

        return $ad_id;
    }

    protected function can_render($ad_id){
        $can = true;
        $is_mobile = wp_is_mobile();
        
        /*if( is_category("folletos-genfar") ){
            return true;
        }
        
        if(!$is_mobile && in_array($ad_id, array(
                'enc_sidebar1',
                'enc_sidebar2',
                'enc_sponsoredpost_rr_pos1',
                'enc_sponsoredpost_rr_pos2',
                'enc_category_rr_pos1',
                'enc_category_rr_pos2',
                'enc_category_rr_pos3',
                'enc_sponsoredcategory_rr_pos1',
                'enc_sponsoredcategory_rr_pos2',
                'enc_sponsoredcategory_rr_pos3'))){
            return true;
        }

        if(strpos($ad_id, 'enc_inline') !== false || strpos($ad_id, 'enc_top') !== false || strpos($ad_id, 'enc_post_') !== false || strpos($ad_id, 'enc_sponsoredpost_') !== false || strpos($ad_id, 'enc_video') !== false || strpos($ad_id, 'enc_sponsoredvideo') !== false){
            $sidebar_info = enc_sidebars::get_instance()->get_sidebar_info($ad_id);
            if(is_single() && $sidebar_info && ( (!$is_mobile && isset($sidebar_info['paragraph'])) || ( $is_mobile && isset($sidebar_info['paragraph_m']) )) ){
                $post_content = apply_filters( 'the_content', get_the_content() );
                $can = $this->can_insert_ad_in_content($post_content, $is_mobile ? (int)$sidebar_info['paragraph_m'] : (int)$sidebar_info['paragraph']);
            }
        } else if(strpos($ad_id, 'enc_category_') !== false || strpos($ad_id, 'enc_sponsoredcategory_') !== false){
            $sidebar_info = enc_sidebars::get_instance()->get_sidebar_info($ad_id);
            if(is_category() && $sidebar_info && ( (!$is_mobile && isset($sidebar_info['row'])) || ( $is_mobile && isset($sidebar_info['row_m']) ) ) ){
               $can = $this->can_insert_ad_in_category_page($is_mobile ? (int)$sidebar_info['row_m'] : (int)$sidebar_info['row']);
            }
        }*/
        return $can;
    }

    protected function can_insert_ad_in_category_page($row){
        $is_mobile = wp_is_mobile();
        $ITEMS_BY_ROW = 2;
        $ITEMS_BY_ROW_M = 1;
        $CATS_BY_ROW = 3;
        $CATS_BY_ROW_M = 1;
        $total_rows = 0;
        $is_publication = enc_base_category::is_publication();
        if($is_publication){
            $enc_block = enc_block_list_publications::get_instance();
        } else{
            $enc_block = enc_block_list_subcategories::get_instance();
        }
        $block_data = $enc_block->_getData();
        $total = is_array($block_data) ? count($block_data) : 0;
        $total_rows = $is_mobile ? (int)($total / $CATS_BY_ROW_M) : (int)($total / $CATS_BY_ROW);
        if(!$is_publication){
            global $wp_query;
            /**
             * @var $wp_query WP_Query
             */
            $total = count($wp_query->get_posts());
            $total_rows += $is_mobile ? (int)($total / $ITEMS_BY_ROW_M) : (int)($total / $ITEMS_BY_ROW);
        }
        return $total_rows >= $row;
    }

    protected function can_insert_ad_in_content($content, $paragraph_number){

        $closing_p = '</p>';
        $paragraphs = explode( $closing_p, $content );
        //if(count($paragraphs)>2)return true;
        $count = 0;
        foreach ($paragraphs as $paragraph) {
            if ( trim( $paragraph ) ) {
                $count++;
            }
        }
        
        return $count >= $paragraph_number;
    }


}
