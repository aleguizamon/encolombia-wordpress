<?php

class enc_su_column {

    public function render($atts, $content){
        $atts = shortcode_atts( array(
            'size'   => '1/2',
            'center' => 'no',
            'last'   => null,
            'class'  => ''
        ), $atts, 'column' );

        if ( $atts['last'] !== null && $atts['last'] == '1' ) {
            $atts['class'] .= ' su-column-last';
        }

        if ( $atts['center'] === 'yes' ) {
            $atts['class'] .= ' su-column-centered';
        }

        //su_query_asset( 'css', 'su-shortcodes' );

        return '<div class="su-column su-column-size-' . str_replace( '/', '-', $atts['size'] ) . $this->su_get_css_class( $atts ) . '"><div class="su-column-inner su-u-clearfix su-u-trim">' . enc_util::su_do_nested_shortcodes( $content, 'column' ) . '</div></div>';
    }

    public function su_get_css_class( $atts ) {
        return $atts['class'] ? ' ' . trim( $atts['class'] ) : '';
    }

}