<?php

class enc_su_note {

    public function render($atts, $content){
        $atts = shortcode_atts(
            array(
                'note_color' => '#FFFF66',
                'text_color' => '#333333',
                'background' => null, // 3.x
                'color'      => null, // 3.x
                'radius'     => '3',
                'class'      => '',
            ),
            $atts,
            'note'
        );

        if ( null !== $atts['color'] ) {
            $atts['note_color'] = $atts['color'];
        }

        if ( null !== $atts['background'] ) {
            $atts['note_color'] = $atts['background'];
        }

        // Prepare border-radius
        $radius = '0' !== $atts['radius']
            ? 'border-radius:' . $atts['radius'] . 'px;-moz-border-radius:' . $atts['radius'] . 'px;-webkit-border-radius:' . $atts['radius'] . 'px;'
            : '';

        //su_query_asset( 'css', 'su-shortcodes' );

        return '<div class="su-note' . $this->su_get_css_class( $atts ) . '" style="border-color:' . enc_util::su_adjust_brightness( $atts['note_color'], -10 ) . ';' . $radius . '"><div class="su-note-inner su-u-clearfix su-u-trim" style="background-color:' . $atts['note_color'] . ';border-color:' . enc_util::su_adjust_brightness( $atts['note_color'], 80 ) . ';color:' . $atts['text_color'] . ';' . $radius . '">' . enc_util::su_do_nested_shortcodes( $content, 'note' ) . '</div></div>';
    }

    public function su_get_css_class( $atts ) {
        return $atts['class'] ? ' ' . trim( $atts['class'] ) : '';
    }




}