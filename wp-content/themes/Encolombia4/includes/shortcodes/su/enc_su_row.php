<?php

class enc_su_row {

    public function render($atts, $content){
        $atts = shortcode_atts( array( 'class' => '' ), $atts );

        return '<div class="su-row' . $this->su_get_css_class( $atts ) . '">' . enc_util::su_do_nested_shortcodes( $content, 'row' ) . '</div>';
    }

    public function su_get_css_class( $atts ) {
        return $atts['class'] ? ' ' . trim( $atts['class'] ) : '';
    }




}