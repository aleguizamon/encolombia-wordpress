<?php

class enc_layout  {
    private static $instance;

    /**
     * enc_config constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_layout();
        }
        return self::$instance;
    }

    public function render_site_logo(){
        $output = '';
        if(wp_is_mobile()){
            $enc_customLogo = enc_params::$logo_url_m;
            $enc_customLogoR = enc_params::$logo_retina_url_m;
        } else {
            $enc_customLogo = enc_params::$logo_url;
            $enc_customLogoR = enc_params::$logo_retina_url;
        }
        $enc_use_h1_logo = false;
        if (is_home()) {
            $enc_use_h1_logo = true;
        }
        if($enc_use_h1_logo === true) {
            $output.= '<h1 class="enc-logo">';
        }
        $output .= '<div class="enc-header-logo" itemscope="itemscope" itemtype="https://schema.org/Organization">';
        $output.= '<a class="enc-main-logo" itemprop="url" href="' . esc_url(home_url( '/' )) . '">';
        $output.= '<img src="'.esc_attr($enc_customLogo).'" width="200" height="42" srcset="'.esc_attr($enc_customLogo) . ', '.esc_attr($enc_customLogoR) . ' 2x" alt="ENCOLOMBIA" title="ENCOLOMBIA.COM"/>';
        $output.= '</a>';
        $output.= '<meta itemprop="name" content="'.get_bloginfo( 'name', 'display' ) . '">';

        if($enc_use_h1_logo === true) {
            $output.= '</h1>';
        }
        $output .= '</div>';
        return $output;
    }

}
