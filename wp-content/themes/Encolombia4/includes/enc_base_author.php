<?php

class enc_base_author {
    public $author_name;
    public $author_posts_url;
    public $author_color;
    public $author_bio;
    public $author_firstname;
    public $author_lastname;

    public $author_url;

    protected $current_author;
    public $description;
    public $googleplus;
    public $twitter;
    public $facebook;
    public $linkedin;
    public $instagram;

    public $is_author;

    function __construct($post = null) {
        if(!empty($post)){
            $this->load_post_author_data($post);
        } else {
            $this->load_current_author_data();
        }
    }

    public function load_current_author_data(){
        $this->current_author = get_queried_object();
        $this->description = get_the_author_meta('description', $this->current_author->ID);
        $this->googleplus  = get_user_meta($this->current_author->ID, 'googleplus', true);
        $this->twitter     = get_user_meta($this->current_author->ID, 'twitter', true);
        $this->facebook    = get_user_meta($this->current_author->ID, 'facebook', true);
        $this->linkedin    = get_user_meta($this->current_author->ID, 'linkedin', true);
        $this->instagram    = get_user_meta($this->current_author->ID, 'instagram', true);
    }

    public function load_post_author_data($post){
        $user_data = get_userdata($post->post_author);
        $this->author_name = get_post_meta($post->ID, 'author-name_value', true);
        if(empty($this->author_name)){
            $this->author_name = get_the_author_meta('display_name', $post->post_author);//get_the_author();
            $this->author_bio = get_the_author_meta('description', $post->post_author);
        } else {
            $this->author_bio = get_post_meta($post->ID, 'author-bio_value', true);
        }
        $this->author_posts_url = get_author_posts_url($post->post_author);
        $this->author_color = $this->get_author_icon($post->post_author);
        $this->author_firstname = get_the_author_meta('first_name', $post->post_author);
        $this->author_lastname = get_the_author_meta('last_name', $post->post_author);
        $this->twitter     = get_user_meta($post->post_author, 'twitter', true);
        $this->facebook    = get_user_meta($post->post_author, 'facebook', true);
        $this->linkedin    = get_user_meta($post->post_author, 'linkedin', true);
        $this->instagram    = get_user_meta($post->post_author, 'instagram', true);
        $this->author_url    = $user_data->user_url;

        $this->is_author = $user_data && in_array('author', $user_data->roles);
    }

    public function get_author_icon($author_id=0){
        if($author_id == 0){
            $firstletter = mb_substr(get_the_author(), 0, 1);
        }else{
            $firstletter = mb_substr(get_the_author_meta('display_name', $author_id), 0, 1);
        }
        if ($firstletter == 'ñ' || $firstletter == 'Ñ' || $firstletter == 'á' || $firstletter == 'Á' || $firstletter == 'é' || $firstletter == 'É' || $firstletter == 'í' || $firstletter == 'Í' || $firstletter == 'ó' || $firstletter == 'Ó' || $firstletter == 'ú' || $firstletter == 'Á'){
            return '<span class="post-author-icon">'.$firstletter.'</span>';
        } else {
            return '<span class="post-author-icon">'.strtoupper($firstletter).'</span>';
        }
    }

}
