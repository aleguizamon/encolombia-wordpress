<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-14
 * Time: 04:47
 */

class enc_util{

    private static $is_page_builder_content;

    static function get_request_uri(){
        $uri = '';
        if (wp_doing_ajax()) {
            if(isset($GLOBALS['enc_config'], $GLOBALS['enc_config']['ajax_request_uri'])){
                $uri = $GLOBALS['enc_config']['ajax_request_uri'];
            }
        } else {
            $uri = $_SERVER['REQUEST_URI'];
        }
        return $uri;
    }

    static function enc_is_mobile() {
        $is_mobile = false;
        if (wp_doing_ajax()) {
            if(isset($GLOBALS['enc_config'], $GLOBALS['enc_config']['ajax_is_mobile'])){
                $is_mobile = $GLOBALS['enc_config']['ajax_is_mobile'];
            }
        } else {
            $is_mobile = wp_is_mobile();
        }
        return $is_mobile;
    }

    /**
     * Just get $is_page_builder_content
     * It doesn't make sense to have a set, so function isn't in 'get' format
     * @return mixed
     */
    static function is_page_builder_content() {

        if (!isset(self::$is_page_builder_content)) {
            global $post;
            self::$is_page_builder_content = self::is_pagebuilder_content($post);
        }
        return self::$is_page_builder_content;
    }

    /**
     * Checks a page content and tries to determin if a page was build with a pagebuilder (tdc or vc)
     * @param $post WP_Post
     * @return bool
     */
    static function is_pagebuilder_content($post) {

        if (empty($post->post_content)) {
            return false;
        }

        /**
         * detect the page builder
         * check for the vc_row, evey pagebuilder page must have vc_row in it
         */
        $matches = array();
        //$preg_match_ret = preg_match('/\[.*vc_row.*\]/s', $post->post_content, $matches);
        $preg_match_ret = preg_match('/.*class="vc_row wpb_row vc_row-fluid".*/s', $post->post_content, $matches);
        if ($preg_match_ret !== 0 && $preg_match_ret !== false ) {
            return true;
        }

        return false;
    }

    /**	 *
     * @return bool returns true if the TagDiv Composer is installed
     */
    static function tdc_is_installed() {
        if (class_exists('tdc_state', false) === true ) {
            return true;
        }
        return false;
    }

    static public function is_genfar_section(){

        $urls = array(
            '/medicina-2/vademecum/',
            '/medicina/vademecum/genfar/',
            '/medicina/estudios-bioquivalencia/'
        );

        $enc = false; $i=0;
        while(!$enc && $i<count($urls)){
            if(trim($urls[$i]) != ''){
                $pos = strpos(self::get_request_uri(), trim($urls[$i]));
                if($pos === false){}
                else{
                    $enc = true;
                }
            }
            $i++;
        }
        return $enc;

    }
    
    public static function is_folletos_genfar_list(){
        $request = self::get_request_uri();
        
        if(strpos($request, "/medicina/folletos-genfar/" ) !== false ){
            return true;
        }else{
            return false;
        }
    }

    static public function no_posts() {
        $buffy = '<div class="no-results td-pb-padding-side">';
        $buffy .= '<h2>' . __('No se encontraron posts para mostrar', enc_params::$translate_domain) . '</h2>';
        $buffy .= '</div>';
        return $buffy;
    }

    /**
     * Shows a soft error. The site will run as usual if possible. If the user is logged in and has 'switch_themes'
     * privileges this will also output the caller file path
     * @param $file - The file should be __FILE__
     * @param $message
     */
    public static function error($file, $message, $more_data = '') {
        echo '<br><br>Theme Error:<br>';
        echo $message;
        if (is_user_logged_in() and current_user_can('switch_themes')){
            echo '<br>' . $file;
            if (!empty($more_data)) {
                echo '<br><br><pre>';
                echo 'more data:' . PHP_EOL;
                print_r($more_data);
                echo '</pre>';
            }
        };
    }

    public static function is_kalooga_test_pages(){
        return false;
        $denied_urls = array(
            '/salud-estetica/nutricion/',
            //'/economia/comercio/como-operar-mercado-forex/'
        );

        $enc = false; $i=0;
        while(!$enc && $i<count($denied_urls)){
            if(trim($denied_urls[$i]) != ''){
                $pos = strpos(self::get_request_uri(), trim($denied_urls[$i]));
                if($pos === false){}
                else{
                    $enc = true;
                }
            }
            $i++;
        }
        //return false;
        return $enc;

    }

    /**
     * returns a string containing the numbers of words or chars for the content
     *
     * @param $post_content - the content thats need to be cut
     * @param $limit        - limit to cut
     * @param string $show_shortcodes - if shortcodes
     * @return string
     */
    static function excerpt($post_content, $limit, $show_shortcodes = '') {
        //REMOVE shortscodes and tags
        if ($show_shortcodes == '') {
            // strip_shortcodes(); this remove all shortcodes and we don't use it, is nor ok to remove all shortcodes like dropcaps
            // this remove the caption from images
            $post_content = preg_replace("/\[caption(.*)\[\/caption\]/i", '', $post_content);
            // this remove the shortcodes but leave the text from shortcodes
            $post_content = preg_replace('`\[[^\]]*\]`','',$post_content);
        }
        //wp_strip_all_tags
        //$post_content = stripslashes(wp_filter_nohtml_kses($post_content));
        $post_content = stripslashes(wp_strip_all_tags($post_content));

        // remove the youtube link from excerpt
        //$post_content = preg_replace('~(?:http|https|)(?::\/\/|)(?:www.|)(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/ytscreeningroom\?v=|\/feeds\/api\/videos\/|\/user\S*[^\w\-\s]|\S*[^\w\-\s]))([\w\-]{11})[a-z0-9;:@?&%=+\/\$_.-]*~i', '', $post_content);

        $ret_excerpt = mb_substr($post_content, 0, $limit);
        if (mb_strlen($post_content)>=$limit) {
            $ret_excerpt = $ret_excerpt.'...';
        }


        return $ret_excerpt;
    }

    public static function is_category_level($depth){
        $current_category = get_query_var('cat');
        $my_category  = get_categories('include='.$current_category);
        $cat_depth=0;

        if ($my_category[0]->category_parent == 0){
            $cat_depth = 0;
        } else {

            while( $my_category[0]->category_parent != 0 ) {
                $my_category = get_categories('include='.$my_category[0]->category_parent);
                $cat_depth++;
            }
        }
        if ($cat_depth == intval($depth)) { return true; }
        return null;
    }

    public static function count_paragraphs($content){
        //$pattern = "/<p>.*?<\/p>/gm"; // Global & Multiline
        return count(explode('</p>', $content))-1;//preg_match_all($pattern,$content);
    }

    public static function get_user_ip(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public static function exist_youtube_video(){
        return true;
        $exist = false;
        if(is_single()){
            global $post;
            $pos = strpos($post->post_content, 'youtube.com/embed');
            if($pos === false){
                if($post->ID == 71582){ $exist = true; }
            }
            else{
                $exist = true;
            }
        }
        if(!$exist){
            $allowed_urls = array(
                '/salud-estetica/estetica/cuidado-de-la-piel/',
                '/vida-estilo/alimentacion/recetas/ensaladas/',
                '/vida-estilo/alimentacion/recetas/postres/',
            );
            foreach($allowed_urls as $url){
                $pos = strpos(self::get_request_uri(), $url);
                if($pos === false){}
                else{
                    $exist = true;
                }
            }
        }
        return $exist;
    }

    static function get_pagination($current_category_name = '') {
        global $wp_query;

        $pagenavi_options = self::pagenavi_init();
        //$request = $wp_query->request;
        //$posts_per_page = intval(get_query_var('posts_per_page'));
        $paged = intval(get_query_var('paged'));
        //$numposts = $wp_query->found_posts;
        $max_page = $wp_query->max_num_pages;
        /*if(!is_admin() and is_category()) {
	        $posts_shown_in_loop = enc_params::$posts_per_page;
          $numposts = $wp_query->found_posts - $posts_shown_in_loop; // fix the pagination, we have x less posts because the rest are in the top posts loop
          $max_page = ceil($numposts / $posts_per_page);
        }*/
        if(empty($paged) || $paged == 0) {
            $paged = 1;
        }

        $pages_to_show = intval($pagenavi_options['num_pages']);
        $larger_page_to_show = intval($pagenavi_options['num_larger_page_numbers']);
        $larger_page_multiple = intval($pagenavi_options['larger_page_numbers_multiple']);
        $pages_to_show_minus_1 = $pages_to_show - 1;
        $half_page_start = floor($pages_to_show_minus_1/2);
        $half_page_end = ceil($pages_to_show_minus_1/2);
        $start_page = $paged - $half_page_start;
        if($start_page <= 0) {
            $start_page = 1;
        }
        $end_page = $paged + $half_page_end;
        if(($end_page - $start_page) != $pages_to_show_minus_1) {
            $end_page = $start_page + $pages_to_show_minus_1;
        }
        if($end_page > $max_page) {
            $start_page = $max_page - $pages_to_show_minus_1;
            $end_page = $max_page;
        }
        if($start_page <= 0) {
            $start_page = 1;
        }
        $larger_per_page = $larger_page_to_show*$larger_page_multiple;
        $larger_start_page_start = (self::enc_round_number($start_page, 10) + $larger_page_multiple) - $larger_per_page;
        $larger_start_page_end = self::enc_round_number($start_page, 10) + $larger_page_multiple;
        $larger_end_page_start = self::enc_round_number($end_page, 10) + $larger_page_multiple;
        $larger_end_page_end = self::enc_round_number($end_page, 10) + ($larger_per_page);
        if($larger_start_page_end - $larger_page_multiple == $start_page) {
            $larger_start_page_start = $larger_start_page_start - $larger_page_multiple;
            $larger_start_page_end = $larger_start_page_end - $larger_page_multiple;
        }
        if($larger_start_page_start <= 0) {
            $larger_start_page_start = $larger_page_multiple;
        }
        if($larger_start_page_end > $max_page) {
            $larger_start_page_end = $max_page;
        }
        if($larger_end_page_end > $max_page) {
            $larger_end_page_end = $max_page;
        }

        if($max_page > 1 || intval($pagenavi_options['always_show']) == 1) {
            echo '<div class="enc-title-pagination">VER MÁS: '.$current_category_name.'</div>';
            $pages_text = str_replace("%CURRENT_PAGE%", number_format_i18n($paged), $pagenavi_options['pages_text']);
            $pages_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pages_text);
            echo '<div class="enc-page-nav enc-padding-side">';
            echo'<div class="page-buttons">';
            previous_posts_link($pagenavi_options['prev_text']);
            if ($start_page >= 2 && $pages_to_show < $max_page) {
                $first_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['first_text']);
                echo '<a href="'.esc_url(get_pagenum_link()).'" class="first" title="'.$first_page_text.'">'.$first_page_text.'</a>';
                if(!empty($pagenavi_options['dotleft_text']) && ($start_page > 2)) {
                    echo '<span class="extend">'.$pagenavi_options['dotleft_text'].'</span>';
                }
            }
            for($i = $start_page; $i  <= $end_page; $i++) {
                if($i == $paged) {
                    $current_page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['current_text']);
                    echo '<span class="current">'.$current_page_text.'</span>';
                } else {
                    $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
                    echo '<a href="'.esc_url(get_pagenum_link($i)).'" class="page" title="'.$page_text.'">'.$page_text.'</a>';
                }
            }
            if ($end_page < $max_page) {
                if(!empty($pagenavi_options['dotright_text']) && ($end_page + 1 < $max_page)) {
                    echo '<span class="extend">'.$pagenavi_options['dotright_text'].'</span>';
                }
                $last_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['last_text']);
                echo '<a href="'.esc_url(get_pagenum_link($max_page)).'" class="last" title="'.$last_page_text.'">'.$last_page_text.'</a>';
            }
            next_posts_link($pagenavi_options['next_text'], $max_page);
            echo '</div>';
            if(!empty($pages_text)) {
                      echo '<span class="pages">'.$pages_text.'</span>';
            }
            echo '<div class="clearfix"></div>';
            echo '</div>';
        }
    }

    static function pagenavi_init() {
        $pagenavi_options = array();
        $pagenavi_options['pages_text'] = __('Página %CURRENT_PAGE% de %TOTAL_PAGES%');
        $pagenavi_options['current_text'] = '%PAGE_NUMBER%';
        $pagenavi_options['page_text'] = '%PAGE_NUMBER%';
        $pagenavi_options['first_text'] = __('1');
        $pagenavi_options['last_text'] = __('%TOTAL_PAGES%');
        if (is_rtl()) {
            $pagenavi_options['next_text'] = '<i class="enc-icon-menu-right"></i>';
            $pagenavi_options['prev_text'] = '<i class="enc-icon-menu-left"></i>';
        } else {
            $pagenavi_options['next_text'] = '<img src="'.get_template_directory_uri().'/assets/images/arrow-right.png" width="24px" height="24px" alt="Página siguiente" />';//'<i class="enc-icon-menu-right"></i>';
            $pagenavi_options['prev_text'] = '<img src="'.get_template_directory_uri().'/assets/images/arrow-left.png" width="24px" height="24px" alt="Página anterior" />';//'<i class="enc-icon-menu-left"></i>';
        }
        $pagenavi_options['dotright_text'] = __('...');
        $pagenavi_options['dotleft_text'] = __('...');
        $pagenavi_options['num_pages'] = 3;
        $pagenavi_options['always_show'] = 1;
        $pagenavi_options['num_larger_page_numbers'] = 3;
        $pagenavi_options['larger_page_numbers_multiple'] = 1000;
        return $pagenavi_options;
    }

    static function enc_round_number($num, $tonearest) {
        return floor($num/$tonearest)*$tonearest;
    }

    public static function exclude_scripts_for_test(){
        $lock = false;
        $lock_urls = array(
            '/medio-ambiente/ramsar/hume-marcolineamientos/hume-marcolineamientos7/',
            '/medio-ambiente/ramsar/hume-marcolineamientos/'
        );
        $i=0;
        while(!$lock && $i<count($lock_urls)){
            if(trim($lock_urls[$i]) != ''){
                $pos = strpos(self::get_request_uri(), trim($lock_urls[$i]));
                if($pos === false){}
                else{
                    $lock = true;
                }
            }
            $i++;
        }
        return $lock;
    }

    public static function is_uri_section($section){
        return strpos(self::get_request_uri(), $section) !== false;
    }

    public static function is_exact_uri_section($section){
        return self::get_request_uri() == $section;
    }

    public static function insert_after_paragraph( $insertion, $paragraph_id, $content ) {
        $closing_p = '</p>';
        $paragraphs = explode( $closing_p, $content );

        foreach ($paragraphs as $index => $paragraph) {
            if ( trim( $paragraph ) ) {
                $paragraphs[$index] .= $closing_p;
            }

            if ( $paragraph_id == $index + 1 ) {
                $paragraphs[$index] .= $insertion;
            }
        }
        return implode( '', $paragraphs );
    }

    public static function is_customer_section(){
        global $wpdb, $is_customer_section;
        if(isset($is_customer_section[self::get_request_uri()])){
            $enc = $is_customer_section[self::get_request_uri()];
        } else {
            $queryResult = $wpdb->get_results( "SELECT option_value  FROM `ecs_options` WHERE `option_name` LIKE 'options_rutas_de_clientes'");
            $convertResult = explode('<br />', trim(nl2br($queryResult[0]->option_value))); //explode("\n",trim(nl2br($queryResult[0]->option_value)));
            //$resultFilter = array_filter($convertResult);
            $enc = false;
            $lock_urls = $convertResult;
            $i=0;
            while(!$enc && $i<count($lock_urls)){
                if(trim($lock_urls[$i]) != ''){
                    $pos = strpos(self::get_request_uri(), trim($lock_urls[$i]));
                    if($pos === false){}
                    else{
                        $enc = true;
                    }
                }
                $i++;
            }
            $is_customer_section[self::get_request_uri()] = $enc;
        }

        return $enc;
    }

    public static function is_folletos_genfar_page(){
        $enc = false;
        $lock_urls = array(
            '/medicina/folletos-genfar/folletos-genfar-valganciclovir/',
            '/medicina/folletos-genfar/folletos-genfar-rosuvastatina/',
            '/medicina/folletos-genfar/folletos-genfar-risperidona/',
            '/medicina/folletos-genfar/folletos-genfar-lacosamida/'
        );
        $i=0;
        while(!$enc && $i<count($lock_urls)){
            if(trim($lock_urls[$i]) != ''){
                $pos = strpos(self::get_request_uri(), trim($lock_urls[$i]));
                if($pos === false){}
                else{
                    $enc = true;
                }
            }
            $i++;
        }
        return $enc;
    }

    public static function is_url_force_featured_image(){
        $enc = false;
        $lock_urls = array(
            '/medicina/vademecum/genfar/antiinfecciosos-sistemicos/',
            '/medicina/vademecum/genfar/dermatologicos/',
            '/medicina/vademecum/genfar/medicamentos-glucocorticoides/',
            '/medicina/vademecum/genfar/parasitologia/',
            '/medicina/vademecum/genfar/hematopoyetico/',
            '/medicina/vademecum/genfar/cardiovascular/',
            '/medicina/vademecum/genfar/genitourinario/',
            '/medicina/vademecum/genfar/musculo-esqueletico/',
            '/medicina/vademecum/genfar/nervioso-central/',
            '/medicina/vademecum/genfar/respiratorio/',
            '/medicina/vademecum/genfar/tracto-alimentario/'
        );
        $i=0;
        while(!$enc && $i<count($lock_urls)){
            if(trim($lock_urls[$i]) != ''){
                $pos = strpos(self::get_request_uri(), trim($lock_urls[$i]));
                if($pos === false){}
                else{
                    $enc = true;
                }
            }
            $i++;
        }
        return $enc;
    }

    public static function load_template($folder, $name){
        if ( $overridden_template = locate_template( $name ) ) {
            load_template( $overridden_template );
        } else {
            load_template( $folder . $name );
        }
    }

    static function remove_div_tag($str){
        return preg_replace('/\<[\/]{0,1}aside[^\>]*\>/i', '', preg_replace('/\<[\/]{0,1}div[^\>]*\>/i', '', $str));
    }

    static function insert_content_after_paragraph( $insertion, $paragraph_id, $content ) {
        $closing_p = '</p>';
        $paragraphs = explode( $closing_p, $content );
        $total = count($paragraphs)-1;

        if(empty(trim($paragraphs[$total]))){
            unset($paragraphs[$total]);
        }

        $ind = 0;
        $final_paragraphs = array();
        foreach ($paragraphs as $index => $paragraph) {
            if ( trim( $paragraph ) ) {
                $final_paragraphs[$ind] = $paragraph . $closing_p;

                if ( $paragraph_id == $ind + 1 ) {
                    $final_paragraphs[$ind] .= $insertion;
                }
                $ind++;
            }
        }

        return implode( '', $final_paragraphs );
    }

    static public function insert_ads_in_content($content, $ads_data){
        $closing_p = '</p>';
        $paragraphs = explode( $closing_p, $content );
        $ind = 0;
        $final_paragraphs = array();
        foreach ($paragraphs as $index => $paragraph) {
            if ( trim( $paragraph ) ) {
                $final_paragraphs[$ind] = $paragraph . $closing_p;
                foreach($ads_data as $data){
                    if ( $data['index'] == $ind + 1 ) {
                        $final_paragraphs[$ind] .= $data['ad'];
                    }
                }
                $ind++;
            }
        }

        return implode( '', $final_paragraphs );
    }


    public static function is_single_test(){
        $enc = false;
        $urls = array(
            '/medicina/guiasmed/benceno/recomendaciones3-2/'
        );
        $i=0;
        while(!$enc && $i<count($urls)){
            if(trim($urls[$i]) != ''){
                $enc = self::is_exact_uri_section(trim($urls[$i]));
            }
            $i++;
        }
        return $enc;
    }

    public static function is_category_test(){
        $enc = false;
        $urls = array(
            '/derecho/'
        );
        $i=0;
        while(!$enc && $i<count($urls)){
            if(trim($urls[$i]) != ''){
                $enc = self::is_exact_uri_section(trim($urls[$i]));
            }
            $i++;
        }
        return $enc;
    }

    public static function is_single_test2(){
        $enc = false;
        $urls = array(
            '/medicina/revistas-medicas/menopausia/vm-263/mujer-perimenopausica/'
        );
        $i=0;
        while(!$enc && $i<count($urls)){
            if(trim($urls[$i]) != ''){
                $enc = self::is_exact_uri_section(trim($urls[$i]));
            }
            $i++;
        }
        return $enc;
    }

    public static function is_single_test3(){
        $enc = false;
        $urls = array(
            '/educacion-cultura/historia-universal/ano-nuevo-chino/'
        );
        $i=0;
        while(!$enc && $i<count($urls)){
            if(trim($urls[$i]) != ''){
                $enc = self::is_exact_uri_section(trim($urls[$i]));
            }
            $i++;
        }
        return $enc;
    }

    public static function is_category_test2(){
        $enc = false;
        $urls = array(
            '/medicina-odontologia/odontologia/'
        );
        $i=0;
        while(!$enc && $i<count($urls)){
            if(trim($urls[$i]) != ''){
                $enc = self::is_exact_uri_section(trim($urls[$i]));
            }
            $i++;
        }
        return $enc;
    }

    public static function is_single_sponsored_test(){
        $enc = false;
        $urls = array(
            '/economia/empresas/transporte-mercancias-emprendimiento/transporte-mercancia-tiendas-alimentos/'
        );
        $i=0;
        while(!$enc && $i<count($urls)){
            if(trim($urls[$i]) != ''){
                $enc = self::is_exact_uri_section(trim($urls[$i]));
            }
            $i++;
        }
        return $enc;
    }

    public static function is_category_sponsored_test(){
        $enc = false;
        $urls = array(
            '/economia/empresas/transporte-mercancias-emprendimiento/'
        );
        $i=0;
        while(!$enc && $i<count($urls)){
            if(trim($urls[$i]) != ''){
                $enc = self::is_exact_uri_section(trim($urls[$i]));
            }
            $i++;
        }
        return $enc;
    }

    public static function is_posts_v3(){ return true;
        $enc = false;
        $lock_urls = array(
            '/vida-estilo/',
        );
        $i=0;
        while(!$enc && $i<count($lock_urls)){
            if(trim($lock_urls[$i]) != ''){
                $pos = strpos(self::get_request_uri(), trim($lock_urls[$i]));
                if($pos === false){}
                else{
                    $enc = true;
                }
            }
            $i++;
        }
        return $enc;
    }

    public static function get_category_level($category_id = 0) {
        // Obtener la categoría actual si no se proporciona un ID
        if (empty($category_id)) {
            $category = get_queried_object();
            if ($category instanceof WP_Term) {
                $category_id = $category->term_id;
            }
        }

        // Verificar si se encontró un ID de categoría válido
        if (!empty($category_id)) {
            $ancestors = get_ancestors($category_id, 'category');
//echo '<pre>';print_r($ancestors);echo '</pre>';
            // El nivel es igual a la cantidad de ancestros
            $level = count($ancestors);

            // Retornar el nivel de la categoría
            return $level;
        }

        // Si no se encuentra una categoría válida, retornar 0
        return 0;
    }

    public static function is_exception_infinite_scroll(){
        $enc = false;
        $lock_urls = array(
            '/medicina/vademecum/genfar/vademecum-genfar/',
            '/medicina/vademecum/genfar/genfar-rx/',
            '/medicina/vademecum/genfar/genfar-rx1/',
        );
        $i=0;
        while(!$enc && $i<count($lock_urls)){
            if(trim($lock_urls[$i]) != ''){
                $pos = strpos(self::get_request_uri(), trim($lock_urls[$i]));
                if($pos === false){}
                else{
                    $enc = true;
                }
            }
            $i++;
        }
        return $enc;
    }













    static function is_shortcode_ultimate() {
        global $post;
        if (empty($post->post_content)) {
            return false;
        }

        /**
         * detect the page builder
         * check for the vc_row, evey pagebuilder page must have vc_row in it
         */
        $matches = array();
        $preg_match_ret = preg_match('/\[.*su_.*\]/s', $post->post_content, $matches);
        if ($preg_match_ret !== 0 && $preg_match_ret !== false ) {
            return true;
        }

        return false;
    }



    /**
     * Helper function that adjusts lightness of a given HEX color value.
     *
     * Examples of use:
     * `su_adjust_lightness( '#fc0', 50 )` - increase color lightness by 50%
     * `su_adjust_lightness( 'ffcc00', -50 )` - decrease color lightness by 50%
     *
     * @since  5.6.0
     * @param  string $color A valid HEX color
     * @param  int $percent  The percent to adjust lightness to.
     * @return string        Adjusted HEX color value.
     */
    public static function su_adjust_lightness( $color, $percent ) {

        if (
            ! self::su_is_valid_hex( $color ) ||
            ! is_numeric( $percent )
        ) {
            return $color;
        }

        $percent   = max( -100, min( 100, $percent ) );
        $color     = ltrim( $color, '#' );
        $new_color = '#';

        if ( 3 === strlen( $color ) ) {
            $color = self::su_expand_short_color( $color );
        }

        $color = array_map( 'hexdec', str_split( $color, 2 ) );

        foreach ( $color as $part ) {

            $limit  = $percent < 0 ? $part : 255 - $part;
            $amount = ceil( $limit * $percent / 100 );

            $new_color .= str_pad( dechex( $part + $amount ), 2, '0', STR_PAD_LEFT );

        }

        return $new_color;

    }

    /**
     * Helper function that expands 3-sybol string into 6-sybol by repeating each
     * symbol twice.
     *
     * @since  5.6.0
     * @param  string $hex Short value.
     * @return string      Expanded value.
     */
    public static function su_expand_short_color( $value ) {

        if ( ! is_string( $value ) || 3 !== strlen( $value ) ) {
            return $value;
        }

        return $value[0] . $value[0] . $value[1] . $value[1] . $value[2] . $value[2];

    }

    /**
     * Custom do_shortcode function for nested shortcodes
     *
     * @since  5.0.4
     * @param string  $content Shortcode content.
     * @param string  $pre     First shortcode letter.
     * @return string          Formatted content.
     */
    public static function su_do_nested_shortcodes_alt( $content, $pre ) {

        if ( strpos( $content, '[_' ) !== false ) {
            $content = preg_replace( '@(\[_*)_(' . $pre . '|/)@', '$1$2', $content );
        }

        return do_shortcode( $content );

    }

    /**
     * Remove underscores from nested shortcodes.
     *
     * @since  5.0.4
     * @param string  $content   String with nested shortcodes.
     * @param string  $shortcode Shortcode tag name (without prefix).
     * @return string            Parsed string.
     */
    public static function su_do_nested_shortcodes( $content, $shortcode ) {

        //if ( get_option( 'su_option_do_nested_shortcodes_alt' ) ) {
        //    return self::su_do_nested_shortcodes_alt( $content, substr( $shortcode, 0, 1 ) );
        //}

        $prefix = 'su_';

        if ( strpos( $content, '[_' . $prefix . $shortcode ) !== false ) {

            $content = str_replace(
                array( '[_' . $prefix . $shortcode, '[_/' . $prefix . $shortcode ),
                array( '[' . $prefix . $shortcode, '[/' . $prefix . $shortcode ),
                $content
            );

            return do_shortcode( $content );

        }

        return do_shortcode( wptexturize( $content ) );

    }

    /**
     * Do shortcodes in attributes.
     *
     * Replace braces with square brackets: {shortcode} => [shortcode], applies do_shortcode() filter.
     *
     * @since  5.0.5
     * @param string  $value Attribute value with shortcodes.
     * @return string        Parsed string.
     */
    public static function su_do_attribute( $value ) {

        $value = str_replace( array( '{', '}' ), array( '[', ']' ), $value );
        $value = do_shortcode( $value );

        return $value;

    }

    /**
     * Helper function that adjusts brightness of a given HEX color value.
     *
     * Examples of use:
     * `su_adjust_brightness( '#fc0', 50 )` - increase color brightness by 50%
     * `su_adjust_brightness( 'ffcc00', -50 )` - decrease color brightness by 50%
     *
     * @since  5.2.0
     * @param  string $color A valid HEX color
     * @param  int $percent  The percent to adjust brightness to.
     * @return string        Adjusted HEX color value.
     */
    public static function su_adjust_brightness( $color, $percent ) {

        if (
            ! self::su_is_valid_hex( $color ) ||
            ! is_numeric( $percent )
        ) {
            return $color;
        }

        $percent = max( -100, min( 100, $percent ) );
        $steps   = round( $percent * 2.55 );
        $color   = ltrim( $color, '#' );

        if ( 3 === strlen( $color ) ) {
            $color = self::su_expand_short_color( $color );
        }

        $color_parts = str_split( $color, 2 );
        $new_color   = '#';

        foreach ( $color_parts as $color_part ) {

            $color_part = hexdec( $color_part );
            $color_part = max( 0, min( 255, $color_part + $steps ) );

            $new_color .= str_pad( dechex( $color_part ), 2, '0', STR_PAD_LEFT );

        }

        return $new_color;

    }

    /**
     * Helper function to check validity of a given HEX color.
     *
     * Valid formats are:
     * - #aabbcc
     * - aabbcc
     * - #abc
     * - abc
     *
     * @since  5.2.0
     * @param  string $color HEX color to check validity of.
     * @return bool          True if a given color mathes accepted pattern, False otherwise.
     */
    public static function su_is_valid_hex( $color ) {
        return preg_match( '/^#([a-f0-9]{3}){1,2}\b$/i', $color ) === 1;
    }


}
