<?php
/*
 * NIVEL 0: NO SE MUESTRA NADA
 * NIVEL 1: MUESTRA LAS OTRAS CATEGORIAS DEL NIVEL 0
 * NIVEL 2: MUESTRA LAS CATEGORIAS DEL NIVEL 1
 * NIVEL 3: MUESTRA LAS CATEGORIAS DEL NIVEL 2
 * POST: MUESTRA LAS CATEGORIAS DEL NIVEL 3
 *
 *
 * */

class enc_block_morethan_sidebar extends enc_base_category{

    private static $instance;

    private $data = null;

    private $page;

    /*
     * 42 Vida y Estilo - Salud y Estética 8
     * 43 Turismo - Medio Ambiente 41
     * 41 Medio Ambiente - Educación 48
     * 48 Educación - 41 Medio Ambiente
     * 40 => 41
     * 6 Economía - 41 Medio Ambiente
     * 5 Derecho - 6 Economía
     * 45 Medicina - 57 Salud
     * 8 Salud y Estética - 42 Vida y Estilo
    */
    private $redirects = array(
        42 => 8,
        43 => 41,
        41 => 48,
        48 => 41,
        40 => 41,
        6  => 41,
        5  => 6,
        45 => 576,
        8  => 42,
    );

    private $exist_more = false;

    /**
     * enc_block_publications_sidebar constructor.
     */
    public function __construct()
    {
        //if($this->is_publication()) return;
        $this->require_level = true;
        $this->require_parent = false;
        parent::__construct();
        $this->get_data();
    }

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_block_morethan_sidebar();
        }
        return self::$instance;
    }

    public function render(&$ind_pos_ads=null, &$current_row=null){
        //if($this->is_publication()) return;
        $buffy = '';
        if($this->data){
            if(!wp_is_mobile()){
                if(is_single()){
                    global $post;
                    $temp_post_id = $post->ID;
                    $is_genfar_section = enc_util::is_genfar_section();
                    $buffy .= '<aside class="widget widget-content-sidebar-related '.($is_genfar_section ? 'genfar-sidebar-list' : '').'">';
                    //$buffy .= '<div class="pub-sidebar2 '.($is_genfar_section ? 'genfar-sidebar-list' : '').'">';
                    //$buffy .= '<div class="enc-span12 sidebar-catinfo">';
                    $buffy .= '<h4 class="sidebar-catinfo-title">+DE: '.$this->get_current_cat()->cat_name.'</h4>';
                    //$buffy .= '</div>';
                    $buffy .= '<div class="sidebar-indice">';
                    $buffy .= '<ol class="indice">';

                    $this->page = 1;
                    $limit = $is_genfar_section ? 10 : 3;
                    $index = 1;
                    while ($this->data->have_posts()):
                        $this->data->the_post();

                        $buffy .= '<li class="item-indice indice_page_' . $this->page. ' '.($post->ID == $temp_post_id ? "current" : '').'" style="display:'.($this->page == 1 ? "block" : "none").';">';
                        $buffy .= '<a href="'.get_the_permalink().'" title="'.the_title_attribute('echo=0').'" >'.get_the_title().'</a>';
                        $buffy .= '</li>';

                        if ($index % $limit == 0) {
                            $this->page++;
                        }
                        $index++;
                    endwhile;

                    $buffy .= '</ol>';
                    if ($this->page > 1):
                        $buffy .= '<div class="sidebar-indice-pagination" >';
                        $buffy .= '<a href="javascript:void(0);" class="sb-btn-page sb-btn-page-prev" ><img src="'.get_template_directory_uri().'/assets/images/arrow-left.png" width="24px" height="24px" alt="Página anterior" /></a>';
                        $buffy .= '<a href="javascript:void(0);" class="sb-btn-page sb-btn-page-next" ><img src="'.get_template_directory_uri().'/assets/images/arrow-right.png" width="24px" height="24px" alt="Página siguiente" /></a>';
                        $buffy .= '</div>';
                    endif;
                    $buffy .= '</div>';
                    //$buffy .= '</div>';
                    $buffy .= '</aside>';
                } else if (is_object($this->parent_cat) && isset($this->parent_cat->term_id)) {
                    $buffy .= '<aside class="widget widget-content-sidebar-related">';
                    //$buffy .= '<div class="pub-sidebar2">';
                    //$buffy .= '<div class="enc-span12 sidebar-catinfo">';
                    $buffy .= '<h4 class="sidebar-catinfo-title">+DE: '.$this->parent_cat->cat_name.'</h4>';
                    //$buffy .= '</div>';
                    $buffy .= '<div class="sidebar-indice">';
                    $buffy .= '<ol class="indice">';

                    $this->page = 1;
                    $limit = 3;
                    $index = 1;
                    foreach ($this->data as $child):
                        $buffy .= '<li class="item-indice indice_page_' . $this->page.' '.($child->term_id == $this->get_current_cat()->term_id ? "current" : "").'" style="display:'.($this->page == 1 ? "block" : "none").';">';
                        $buffy .= '<a href="'.get_category_link($child->term_id).'" title="'.$child->cat_name.'" >';
                        $buffy .= $child->cat_name;
                        $buffy .= '</a>';
                        $buffy .= '</li>';
                        if ($index % $limit == 0) {
                            $this->page++;
                        }
                        $index++;
                    endforeach;
                    $buffy .= '</ol>';
                    if ($this->page > 1):
                        $buffy .= '<div class="sidebar-indice-pagination">';
                        $buffy .= '<a href="javascript:void(0);" class="sb-btn-page sb-btn-page-prev" ><img src="'.get_template_directory_uri().'/assets/images/arrow-left.png" width="24px" height="24px" alt="Página anterior" /></a>';
                        $buffy .= '<a href="javascript:void(0);" class="sb-btn-page sb-btn-page-next" ><img src="'.get_template_directory_uri().'/assets/images/arrow-right.png" width="24px" height="24px" alt="Página siguiente" /></a>';
                        $buffy .= '</div>';
                    endif;
                    $buffy .= '</div>';
                    //$buffy .= '</div>';
                    $buffy .= '</aside>';
                } else {
                    $this->page = 0;
                }

                $buffy .= $this->render_script();

            }

        }
        return $buffy;
    }

    public function get_data(){
        if (is_single()) {
            $this->exist_more = true;
            $args = array(
                'post_type' => 'post',
                'category' => $this->get_current_cat(),
                'paged' => 1,
                'orderby' => 'date',
                'order' => 'DESC',
                'ignore_sticky_posts' => 1,
                'posts_per_page' => -1,
                'taxonomy' => 'category',
                'post_status' => 'publish',
                'category__in' => array($this->get_current_cat()->term_id)
            );
            $this->data = new WP_Query($args);
        } else {
            $this->parent_cat = null;
            if ($this->category_level == 0 && isset($this->redirects[get_query_var('cat')])) {
                $this->parent_cat = get_category($this->redirects[get_query_var('cat')]);
            } else if($this->category_level == 0 && !isset($this->redirects[get_query_var('cat')])) {
                // $this->parent_cat = get_category($this->get_current_cat()->cat_ID);
            } else if ($this->category_level == 1 || $this->category_level == 2 || $this->category_level == 3) {
                $this->parent_cat = get_category($this->get_current_cat()->parent);
            }
            if (is_object($this->parent_cat) && isset($this->parent_cat->term_id)) {
                $args = array(
                    'type' => 'post',
                    'child_of' => '',
                    'parent' => $this->parent_cat->term_id,
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'hide_empty' => 0,
                    'hierarchical' => 1,
                    'exclude' => '',
                    'include' => '',
                    'number' => '',
                    'taxonomy' => 'category',
                    'pad_counts' => false,
                    'post_status' => 'publish',
                );
                $this->data = get_categories($args);
                $this->exist_more = count($this->data) > 0;
            }
        }
    }

    public function render_script(){
        $buffy = '';
        if ($this->page > 1){
            $buffy .= '<script>
            var current_page = 1;
            var max_page = '.$this->page.';  
            var _items;
            for (var i = 1; i <= max_page; i++) {
                _items = document.querySelectorAll(\'.item-indice.indice_page_\' + i);
                for (var j = 0; j < _items.length; j++) {
                    _items[j].style.display = i == current_page ? \'table\' : \'none\';
                }
            }
            document.querySelector(\'.sb-btn-page-prev\').classList.add(\'disabled\');
            document.querySelector(\'.sb-btn-page-prev\').addEventListener(\'click\', function () {
                if (current_page > 1) {
                    current_page--;                    
                    for (var i = 1; i <= max_page; i++) {
                        _items = document.querySelectorAll(\'.item-indice.indice_page_\' + i);
                        for (var j = 0; j < _items.length; j++) {
                            _items[j].style.display = i == current_page ? \'table\' : \'none\';
                        }                        
                    }
                    document.querySelector(\'.sb-btn-page-next\').classList.remove(\'disabled\');
                }
                if (current_page <= 1) {                    
                    document.querySelector(\'.sb-btn-page-prev\').classList.add(\'disabled\');
                    document.querySelector(\'.sb-btn-page-next\').classList.remove(\'disabled\');
                }
            });           
            document.querySelector(\'.sb-btn-page-next\').addEventListener(\'click\', function () {
                if (current_page < max_page) {
                    current_page++;                    
                    for (var i = 1; i <= max_page; i++) {
                        _items = document.querySelectorAll(\'.item-indice.indice_page_\' + i);
                        for (var j = 0; j < _items.length; j++) {
                            _items[j].style.display = i == current_page ? \'table\' : \'none\';
                        }                        
                    }
                    document.querySelector(\'.sb-btn-page-prev\').classList.remove(\'disabled\');
                }
                if (current_page >= max_page) {                    
                    document.querySelector(\'.sb-btn-page-next\').classList.add(\'disabled\');
                    document.querySelector(\'.sb-btn-page-prev\').classList.remove(\'disabled\');
                }
            });          
            </script>';
        }
        return $buffy;
    }

    public function render_script_old(){
        $buffy = '';
        if ($this->page > 1):
            $buffy .= '<script>
            var intervalCat2Sidebar = setInterval(intervalCatSidebar2, 500);
            function intervalCatSidebar2() {
                if (window.jQuery) {
                    var current_page = 1;
                    var max_page = '.$this->page.';
                    //jQuery("[class^=\'item-indice indice_page_\']").css(\'display\', \'none\');
                    //jQuery(".item-indice.indice_page_1").css(\'display\', \'table\');
                    jQuery(\'.sb-btn-page-prev\').addClass(\'disabled\');
                    jQuery(\'body\').on(\'click\', \'.sb-btn-page-prev\', function (e) {
                        if (current_page > 1) {
                            current_page--;
                            jQuery("[class^=\'item-indice indice_page_\']").css(\'display\', \'none\');
                            jQuery(".item-indice.indice_page_" + current_page).css(\'display\', \'table\');
                            jQuery(\'.sb-btn-page-prev\').removeClass(\'disabled\');
                        }
                        if (current_page <= 1) {
                            jQuery(\'.sb-btn-page-prev\').addClass(\'disabled\');
                            jQuery(\'.sb-btn-page-next\').removeClass(\'disabled\');
                        }
                    });
                    jQuery(\'body\').on(\'click\', \'.sb-btn-page-next\', function (e) {
                        if (current_page < max_page) {
                            current_page++;
                            jQuery("[class^=\'item-indice indice_page_\']").css(\'display\', \'none\');
                            jQuery(".item-indice.indice_page_" + current_page).css(\'display\', \'table\');
                            jQuery(\'.sb-btn-page-prev\').removeClass(\'disabled\');
                        }
                        if (current_page >= max_page) {
                            jQuery(\'.sb-btn-page-next\').addClass(\'disabled\');
                            jQuery(\'.sb-btn-page-prev\').removeClass(\'disabled\');
                        }
                    });
                    clearInterval(intervalCat2Sidebar);
                }
            }
            </script>';
        endif;
        return $buffy;
    }
}