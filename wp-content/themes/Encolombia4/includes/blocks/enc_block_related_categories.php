<?php
class enc_block_related_categories extends enc_base_category {

    private static $instance;

    private $data = null;

    /**
     * enc_block_related_categories constructor.
     */
    public function __construct()
    {
        $this->require_level = false;
        $this->require_parent = false;
        parent::__construct();
        $this->get_data();
    }

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_block_related_categories();
        }
        return self::$instance;
    }

    function render(&$ind_pos_ads=null, &$current_row=null) {
        $buffy = '';
        if($this->data){
            $buffy .= '<div class="enc_block_related_pubs">';
            $buffy .= '<h3 class="enc-block-title-1">';
            $buffy .= 'PUBLICACIONES RELACIONADAS';
            $buffy .= '<span class="titledot"></span><span class="titleline"></span>';
            $buffy .= '</h3>';
            $buffy .= '<div class="enc-row">';

            foreach ($this->data as $cat):
                //$category_id = get_cat_ID( 'Category Name' );
                $base_url = get_category_link( $cat );

                $buffy .= '<div class="col-lg-2 col-md-3 col-sm-6">';
                $buffy .= '<div class="pub-item">';
                $buffy .= '<div class="pub-thumb">';
                $buffy .= '<a class="item-image" href="'.$base_url.'" title="'.the_title_attribute(array('echo' => false)).'">';
                $buffy .= '<img src="'.$this->get_image_url($cat->term_id, '180x225').'" alt="'.$cat->cat_name.'"  title="'.$cat->cat_name.'"  />';
                $buffy .= '</a>';
                $buffy .= '</div>';
                $buffy .= '<div class="pub-content">';
                $buffy .= '<h3 class="pub-title"><a href="'.$base_url.'">'.$cat->cat_name.'</a></h3>';
                $buffy .= '</div>';
                $buffy .= '</div>';
                $buffy .= '</div>';
            endforeach;

            $buffy .= '</div>';
            $buffy .= '</div>';
        }
        return $buffy;
    }

    public function get_data()
    {
        $args = array(
            'type' => 'post',
            'child_of' => 0,
            'parent' => $this->get_current_cat()->parent,
            'orderby' => 'name',
            'order' => 'ASC',
            'hide_empty' => 1,
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'number' => 6,
            'taxonomy' => 'category',
            'pad_counts' => false
        );
        $this->data = get_categories($args);
    }

    /*public function list_related_categories($category = null){
        global $wp_query;
        $content = '';
        $cats = null;
        if(empty($category) || !is_object($category)){
            $postcat = get_the_category( $wp_query->post->ID );
            $postcat = isset($postcat[0]) ? $postcat[0] : null;
        }
        else{
            $postcat = $category;
        }

        if(!empty($postcat)) {
            $args = array(
                'type' => 'post',
                'child_of' => 0,
                'parent' => $postcat->parent,
                'orderby' => 'name',
                'order' => 'ASC',
                'hide_empty' => 1,
                'hierarchical' => 1,
                'exclude' => '',
                'include' => '',
                'number' => 4,
                'taxonomy' => 'category',
                'pad_counts' => false
            );
            $cats = get_categories($args);

        }


        if($cats != NULL):
            $content = '<div class="parent-list-categories ec-title-home">
           
            <div class="enc-subcategory-list">
                <div class="list-subcats">
                    <div class="enc-row">';

            $ind = 0;
            foreach ($cats as $cat):
                $category_id = get_cat_ID( 'Category Name' );
                //$category_link = get_category_link( $category_id );
                if($cat != NULL) {
                    $base_url = get_category_link( $cat );
                }
                $content .= '<div class="enc-span3">
                            <div class="item-cat">
                                <a class="item-image" href="'.$base_url.'" title="'.the_title_attribute(array('echo' => false)).'">
                                    <img src="';
                if (function_exists('z_taxonomy_image_url'))
                    $content .= z_taxonomy_image_url($cat->term_id, 'art-gal');

                $content .= '" alt="'.$cat->cat_name.'"  title="'.$cat->cat_name.'"  />
                                </a>
                                <h3 class="item-title"><a href="'.$base_url.'">'.$cat->cat_name.'</a></h3>
                            </div>
                        </div>';
                $ind++;
            endforeach;

            $content .= '</div>
                </div>
            </div>
        </div>';
        endif;
        return $content;
    }*/


}