<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-29
 * Time: 05:13
 */
class enc_block_publications_sidebar extends enc_base_category{

    private static $instance;

    private $data = null;

    private $page;

    /**
     * enc_block_publications_sidebar constructor.
     */
    public function __construct()
    {
        //if(!$this->is_publication()) return;
        $this->require_level = true;
        $this->require_parent = true;
        parent::__construct();
        $this->get_data();
    }

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_block_publications_sidebar();
        }

        return self::$instance;
    }

    public function render(&$ind_pos_ads=null, &$current_row=null){
        //var_dump($this->is_publication());
        //if(!$this->is_publication()) return;
        $buffy = '';
        if($this->data){
            $buffy .= '<div class="pub-sidebar '.(enc_util::is_genfar_section() ? 'genfar-sidebar-list' : '').'">';
            $buffy .= $this->render_head();
            $buffy .= $this->render_body();
            $buffy .= $this->render_script();
            $buffy .= '</div>';
        }
        return $buffy;
    }

    public function get_data(){
        switch ($this->category_level){
            case "P":
                $args = array (
                    'post_type'=>'post',
                    'category' => $this->current_cat[0],
                    'paged' => 1,
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'ignore_sticky_posts'=> 1,
                    'posts_per_page' =>-1,
                    'taxonomy' => 'category',
                    'post_status' => 'publish',
                    'category__in' => array($this->current_cat[0]->term_id)
                );
                $this->data = &enc_data_source::get_wp_query($args);
                break;
            case 2:
            case 3:
                $parentCat = get_category($this->current_cat[0]->parent);
                $args = array(
                    'type'                     => 'post',
                    'child_of'                 => '',
                    'parent'                   => $parentCat->term_id,
                    'orderby'                  => 'date',
                    'order'                    => 'DESC',
                    'hide_empty'               => 0,
                    'hierarchical'             => 1,
                    'exclude'                  => '',
                    'include'                  => '',
                    'number'                   => '',
                    'taxonomy'                 => 'category',
                    'pad_counts'               => false,
                    'post_status' => 'publish',
                );
                $this->data = get_categories( $args );
                break;
        }
    }

    private function render_head(){
        //$buffy = '<div class="enc-span12 sidebar-catinfo">';
        $buffy = '';
        if($this->category_level == 'P'){
            $buffy .= '<div class="pub-sidebar-title">';
            //$buffy .= '<div class="enc-module-thumb">';
            /*if($this->parentCat){
                $buffy .= '<a class="pub-title-thumb" href="'.get_category_link($this->parentCat->term_id).'"  title="">';
                $buffy .= '<img width="'.(enc_util::is_genfar_section() ? '100px' : '70px').'" height="auto" class="entry-thumb td-animation-stack-type0-2" src="'.($this->get_image_url($this->parentCat->term_id)).'" alt="" title="'.$this->parentCat->cat_name.'">';
                $buffy .= '</a>';
            }*/
            //$buffy .= '</div>';
            //$buffy .= '<div class="item-details">';
            
            if(enc_util::is_genfar_section()){
                $buffy .= '<h3 class="pub-title-text">';
                $buffy .= '<a  href="'.get_category_link(is_array($this->current_cat) ? $this->current_cat[0]->term_id : $this->current_cat->term_id).'"  title="'.(is_array($this->current_cat) ? $this->current_cat[0]->cat_name : $this->current_cat->cat_name).'">'.(is_array($this->current_cat) ? $this->current_cat[0]->cat_name : $this->current_cat->cat_name).'</a>';
                $buffy .= '</h3>';
            } else {
                $buffy .= '<h3 class="pub-title-text">';
                $buffy .= '<a  href="'.get_category_link($this->parentCat->term_id).'"  title="'.$this->parentCat->cat_name.'">'.$this->parentCat->cat_name.'</a>';
                $buffy .= '</h3>';
                $buffy .= '<h4 class="pub-title-subtext">';
                $buffy .= '<a  href="'.get_category_link(is_array($this->current_cat) ? $this->current_cat[0]->term_id : $this->current_cat->term_id).'"  title="'.(is_array($this->current_cat) ? $this->current_cat[0]->cat_name : $this->current_cat->cat_name).'">'.(is_array($this->current_cat) ? $this->current_cat[0]->cat_name : $this->current_cat->cat_name).'</a>';
                $buffy .= '</h4>';
            }
            //$buffy .= '</div>';
            $buffy .= '</div>';
        }
        else if($this->category_level == 2){
            //$buffy .= '<div class="enc-span12 sidebar-catinfo">';
            $buffy .= '<h4 class="sidebar-catinfo-title">'.$this->parentCat->cat_name.'</h4>';
            //$buffy .= '</div>';
        }
        else if($this->category_level == 3){
            //$buffy .= '<div class="enc-span12 sidebar-catinfo">';
            $buffy .= '<div class="pub-sidebar-title " '.(enc_util::is_genfar_section() ? 'style="min-height:52px;"' : '').'>';
            //$buffy .= '<div class="enc-module-thumb">';

            //$buffy .= '<a class="pub-title-thumb" href="'.get_category_link($this->parentCat->term_id).'"  title="">';
            //$buffy .= '<img width="'.(enc_util::is_genfar_section() ? '100px' : '70px').'" height="auto" class="entry-thumb " src="'.($this->get_image_url($this->parentCat->term_id)).'" alt="" title="'.$this->parentCat->cat_name.'">';
            //$buffy .= '</a>';

            //$buffy .= '</div>';
            //$buffy .= '<div class="item-details">';
            $buffy .= '<h3 class="pub-title-text">';
            $buffy .= '<a href="'.get_category_link($this->parentCat->term_id).'"  title="'.$this->parentCat->cat_name.'">'.$this->parentCat->cat_name.'</a>';
            $buffy .= '</h3>';
            //$buffy .= '</div>';
            $buffy .= '</div>';
            //$buffy .= '</div>';
        }
        //$buffy .= '</div>';
        return $buffy;
    }

    private function render_body(){
        global $post;
        $temp_post_id = $post->ID;

        $buffy = '<div class="sidebar-indice">';
        $buffy .= '<ol class="indice">';

        $this->page = 1;
        $limit = enc_util::is_genfar_section() ? 10 : 5;
        $index = 1;
        if($this->category_level == 'P'){
            while ( $this->data->have_posts() ): $this->data->the_post();
                $buffy .= '<li class="item-indice indice_page_'.$this->page.' '.($post->ID == $temp_post_id ? 'current' : '').'">';
                $buffy .= '<a href="'.get_the_permalink().'" title="'.the_title_attribute( 'echo=0' ).'" >'.get_the_title().'</a>';
                $buffy .= '</li>';
                if($index%$limit == 0){
                    $this->page++;
                }
                $index++;
            endwhile;
        } else if($this->category_level == 2 || $this->category_level == 3){
            foreach ( $this->data as $child ):
                $buffy .= '<li class="item-indice indice_page_'.$this->page.($child->term_id == $this->current_cat[0]->term_id ? ' current' : '').'">';
                $buffy .= '<a href="'.get_category_link($child->term_id).'" title="'.$child->cat_name.'" >'.$child->cat_name.'</a>';
                $buffy .= '</li>';
                if($index%$limit == 0){
                    $this->page++;
                }
                $index++;
            endforeach;
        }
        $buffy .= '</ol>';
        if($this->page > 1):
            $buffy .= '<div class="sidebar-indice-pagination">';
            $buffy .= '<a href=\'javascript:void(0);\' class=\'sb-btn-page sb-btn-page-prev\'><img src="'.get_template_directory_uri().'/assets/images/arrow-left.png" width="24px" height="24px" alt="Página anterior" /></a>';
            $buffy .= '<a href=\'javascript:void(0);\' class=\'sb-btn-page sb-btn-page-next\'><img src="'.get_template_directory_uri().'/assets/images/arrow-right.png" width="24px" height="24px" alt="Página siguiente" /></a>';
            $buffy .= '</div>';
        endif;
        $buffy .= '</div>';
        return $buffy;
    }

    public function render_script(){
        $buffy = '';
        if ($this->page > 1){
            $buffy .= '<script>
            var current_page = 1;
            var max_page = '.$this->page.';  
            var _items;
            for (var i = 1; i <= max_page; i++) {
                _items = document.querySelectorAll(\'.item-indice.indice_page_\' + i);
                for (var j = 0; j < _items.length; j++) {
                    _items[j].style.display = i == current_page ? \'table\' : \'none\';
                }
            }
            document.querySelector(\'.sb-btn-page-prev\').classList.add(\'disabled\');
            document.querySelector(\'.sb-btn-page-prev\').addEventListener(\'click\', function () {
                if (current_page > 1) {
                    current_page--;                    
                    for (var i = 1; i <= max_page; i++) {
                        _items = document.querySelectorAll(\'.item-indice.indice_page_\' + i);
                        for (var j = 0; j < _items.length; j++) {
                            _items[j].style.display = i == current_page ? \'table\' : \'none\';
                        }                        
                    }
                    document.querySelector(\'.sb-btn-page-next\').classList.remove(\'disabled\');
                }
                if (current_page <= 1) {                    
                    document.querySelector(\'.sb-btn-page-prev\').classList.add(\'disabled\');
                    document.querySelector(\'.sb-btn-page-next\').classList.remove(\'disabled\');
                }
            });           
            document.querySelector(\'.sb-btn-page-next\').addEventListener(\'click\', function () {
                if (current_page < max_page) {
                    current_page++;                    
                    for (var i = 1; i <= max_page; i++) {
                        _items = document.querySelectorAll(\'.item-indice.indice_page_\' + i);
                        for (var j = 0; j < _items.length; j++) {
                            _items[j].style.display = i == current_page ? \'table\' : \'none\';
                        }                        
                    }
                    document.querySelector(\'.sb-btn-page-prev\').classList.remove(\'disabled\');
                }
                if (current_page >= max_page) {                    
                    document.querySelector(\'.sb-btn-page-next\').classList.add(\'disabled\');
                    document.querySelector(\'.sb-btn-page-prev\').classList.remove(\'disabled\');
                }
            });          
            </script>';
        }
        return $buffy;
    }

    public function render_script_old(){
        $buffy = '';
        if($this->page > 1):
            $buffy .= '
            <script>
                var intervalCatSidebar = setInterval(intervalCatSidebar1, 500);
                function intervalCatSidebar1(){
                    if(!!window.jQuery){
                        var current_page = 1;
                        var max_page = '.$this->page.';
                        jQuery("[class^=\'item-indice indice_page_\']").css(\'display\', \'none\');
                        jQuery(".item-indice.indice_page_1").css(\'display\', \'table\');
                        jQuery(\'.sb-btn-page-prev\').addClass(\'disabled\');
                        jQuery(\'body\').on(\'click\', \'.sb-btn-page-prev\', function(e){
                            if(current_page > 1){
                                current_page--;
                                jQuery("[class^=\'item-indice indice_page_\']").css(\'display\', \'none\');
                                jQuery(".item-indice.indice_page_"+current_page).css(\'display\', \'table\');
                                jQuery(\'.sb-btn-page-prev\').removeClass(\'disabled\');
                            }
                            if(current_page <= 1){
                                jQuery(\'.sb-btn-page-prev\').addClass(\'disabled\');
                                jQuery(\'.sb-btn-page-next\').removeClass(\'disabled\');
                            }
                        });
                        jQuery(\'body\').on(\'click\', \'.sb-btn-page-next\', function(e){
                            if(current_page < max_page){
                                current_page++;
                                jQuery("[class^=\'item-indice indice_page_\']").css(\'display\', \'none\');
                                jQuery(".item-indice.indice_page_"+current_page).css(\'display\', \'table\');
                                jQuery(\'.sb-btn-page-prev\').removeClass(\'disabled\');
                            }
                            if(current_page >= max_page){
                                jQuery(\'.sb-btn-page-next\').addClass(\'disabled\');
                                jQuery(\'.sb-btn-page-prev\').removeClass(\'disabled\');
                            }
                        });
                        clearInterval(intervalCatSidebar);
                    }
                }
            </script>';
        endif;
        return $buffy;
    }
}