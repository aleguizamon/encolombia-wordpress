<?php

class enc_block_list_subcategories extends enc_base_category{

    private static $instance;

    private $data = null;

    private $paged;

    private $is_mobile;

    const ID_FARMACOVIG = 2548;

    const ID_SALUD_ESTETICA = 8;

    const ID_ESTETICA = 83;

    const ID_GUIA_NUTRICION = 1171;

    const ID_MEDICINA_ALTERNATIVA = 442;

    const CATS_BY_ROW = 6;

    const CATS_BY_ROW_M = 2;

    /**
     * enc_block_list_subcategories constructor.
     */
    public function __construct()
    {
        $this->paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        parent::__construct();
        $this->get_data();
        $this->update_exist_subcats($this->data);
    }

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_block_list_subcategories();
        }

        return self::$instance;
    }

    public function render(&$ind_pos_ads, &$current_row){
        if($this->data){
            echo '<div class="enc-subcategory-list enc-row">';
            $this->is_mobile = wp_is_mobile();
            $ind = 0;
            $temp_count = 0;
            $max_nbr_ads_in_section = ceil(count($this->data) / self::CATS_BY_ROW) - 1;
            //var_dump($ind_pos_ads);var_dump($max_nbr_ads_in_section);exit;
            foreach ($this->data as $cat):
                $base_url = get_category_link( $cat );

                if($this->current_cat[0]->cat_ID != self::ID_FARMACOVIG){
                    echo '
                    <div class="col-lg-2 col-md-3 col-sm-4">
                        <div class="item-cat style2">
                            <div class="item-image">
                                <a class="item-image" href="'.$base_url.'" title="'.$cat->cat_name.'">
                                    <img src="'.$this->get_image_url($cat->term_id, $this->is_mobile ? '150x150' : '150x150').'" alt="'.$cat->cat_name.'"  title="'.$cat->cat_name.'"  />
                                </a>
                            </div>
                            <div class="item-content">
                                <h3 class="item-title">
                                    <a href="'.$base_url.'" title="'.$cat->cat_name.'"   >'.$this->get_title($cat->cat_name, 40).'</a>
                                </h3>
                            </div>
                        
                        </div>
                    </div>
                    ';

                    if( (!$this->is_mobile && ($ind+1) % self::CATS_BY_ROW == 0) || ($this->is_mobile && ($ind+1) % self::CATS_BY_ROW_M == 0) ){
                        if( (!$this->is_mobile && $ind_pos_ads < $max_nbr_ads_in_section) || $this->is_mobile) {
                            $current_row++;
                            $temp_count = 0;
                        }

                    } else {
                        $temp_count++;
                    }

                    $this->render_sidebars($ind, $current_row, $ind_pos_ads);
                } else {
                    echo '<div class="catlist-horiz">';
                    echo '<a class="item-image" href="'.$base_url.'" title="'.the_title_attribute().'">';
                    echo '<img src="'.$this->get_image_url($cat->term_id, '224x146').'" alt="'.$cat->category_description.'"  title="'.$cat->cat_name.'"  />';
                    echo '</a>';
                    echo '<h3 class="item-title"><a href="'.$base_url.'">'.$cat->cat_name.'</a></h3>';
                    echo '</div>';
                }

                $ind++;
            endforeach;

            if($temp_count > 0){
                if( (!$this->is_mobile && $ind_pos_ads < $max_nbr_ads_in_section) || $this->is_mobile) {
                    $current_row++;
                    $this->render_sidebars($ind, $current_row, $ind_pos_ads);
                }
            }

            echo '</div>';
        }
        //return $buffy;
    }

    public function render_sidebars($ind, $current_row, &$global_index){

         if(enc_util::is_category_sponsored_test()){
            $sidebar_info = null;
            if(isset(enc_params::$ads_units_category_sponsored[$global_index])){
                $sidebar_info = enc_sidebars::get_instance()->get_sidebar_info(enc_params::$ads_units_category_sponsored[$global_index]);
            }
            if($sidebar_info){
                if(!$this->is_mobile && strpos(enc_params::$ads_units_category_sponsored[$global_index], 'enc_sponsoredcategory_rr_pos') === false && isset($sidebar_info['row']) && is_numeric($sidebar_info['row']) && $current_row == $sidebar_info['row'] ){
                    enc_sidebars::get_instance()->render_sidebar(enc_params::$ads_units_category_sponsored[$global_index]);
                    $global_index++;
                } else if($this->is_mobile && isset($sidebar_info['row_m']) && is_numeric($sidebar_info['row_m']) && $current_row == $sidebar_info['row_m']){
                    enc_sidebars::get_instance()->render_sidebar(enc_params::$ads_units_category_sponsored[$global_index]);
                    $global_index++;
                }
            }
        } else {
             $cat_ad_units = $this->is_mobile ? enc_params::$ads_units_category_mob : enc_params::$ads_units_category;
             $sidebar_info = null;
             if(isset($cat_ad_units[$global_index])){
                 $sidebar_info = enc_sidebars::get_instance()->get_sidebar_info($cat_ad_units[$global_index]);
             }
             if($sidebar_info){
                 if(!$this->is_mobile && strpos($cat_ad_units[$global_index], 'enc_category_rr_pos') === false && isset($sidebar_info['row']) && is_numeric($sidebar_info['row']) && $current_row == $sidebar_info['row'] ){
                     enc_sidebars::get_instance()->render_sidebar($cat_ad_units[$global_index]);
                     $global_index++;
                 } else if($this->is_mobile && isset($sidebar_info['row_m']) && is_numeric($sidebar_info['row_m']) && $current_row == $sidebar_info['row_m']){
                     enc_sidebars::get_instance()->render_sidebar($cat_ad_units[$global_index]);
                     $global_index++;
                 }
             }
         }
    }

    public function get_data()
    {//1171 442
        if (!$this->paged || $this->paged < 2){
            if($this->is_salud_estetica()){
                $args = array(
                    'child_of'                 => '',
                    'parent'                   => self::ID_SALUD_ESTETICA,
                    'orderby'                  => 'name',
                    'order'                    => 'ASC',
                    'hide_empty'               => 1,
                    'hierarchical'             => 1,
                    'exclude'                  => '',
                    'include'                  => '',
                    'number'                   => '',
                    'taxonomy'                 => 'category',
                    'pad_counts'               => false
                );
                $group1 = get_categories( $args );
                $args['parent'] = self::ID_ESTETICA;
                $group2 = get_categories( $args );
                $group3 = array();
                $group4 = array();
                foreach ($group1 as $g){
                    if($g->cat_ID == self::ID_GUIA_NUTRICION || $g->cat_ID == self::ID_MEDICINA_ALTERNATIVA){
                        $group3[] = $g;
                    } else {
                        $group4[] = $g;
                    }
                }
                $this->data = array_merge($group4, $group2, $group3);
            } else {
                $current_cat = $this->get_current_cat();
                if(isset($current_cat->cat_ID)){
                    $args = array(
                        'child_of'                 => '',
                        'parent'                   => $this->get_current_cat()->cat_ID,
                        'orderby'                  => 'name',
                        'order'                    => 'ASC',
                        'hide_empty'               => 1,
                        'hierarchical'             => 1,
                        'exclude'                  => '',
                        'include'                  => '',
                        'number'                   => '',
                        'taxonomy'                 => 'category',
                        'pad_counts'               => false
                    );
                    $this->data = get_categories( $args );
                } else {
                    $this->data = array();
                }

            }
        }
    }

    public function is_salud_estetica(){
        $current_cat = $this->get_current_cat();
        return isset($current_cat->cat_ID) && $current_cat->cat_ID == self::ID_SALUD_ESTETICA;
    }

    public function _getData(){
        return $this->data;
    }


}