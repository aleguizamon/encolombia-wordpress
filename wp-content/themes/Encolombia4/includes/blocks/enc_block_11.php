<?php

/**
 * Class enc_block_11
 */
class enc_block_11 extends enc_base_post {



    function render($atts, $td_column_number = ''){
        $td_column_number = 2;

        parent::render($atts, $td_column_number); // sets the live atts, $this->atts, $this->block_uid, $this->td_query (it runs the query)

        $buffy = '<div class="enc_block_11 ec-title-home" >';
        $buffy .= $this->get_block_title(); //get the block title
        $buffy .= '<div class="enc_block_inner">';
        $buffy .= $this->inner($this->td_query->posts); //inner content of the block
        $buffy .= '</div>';
        $buffy .= '</div> <!-- ./block -->';
        return $buffy;
    }

    function inner($posts) {
        $buffy = '';
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $enc_module_10 = new enc_module_10($post);
                $buffy .= $enc_module_10->render();
            }
        }
        return $buffy;
    }
}
