<?php

class enc_block_list_publications extends enc_base_category{

    private static $instance;

    private $data = null;

    private $type = 0;

    private $paged;

    private $is_mobile;

    const CATS_BY_ROW = 6;

    const CATS_BY_ROW_M = 2;

    /**
     * enc_block_list_subcategories constructor.
     */
    public function __construct()
    {
        $this->require_level = true;
        $this->paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        parent::__construct();
        $this->get_data();
    }

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_block_list_publications();
        }

        return self::$instance;
    }

    public function get_data()
    {
        if((int)$this->category_level == 1){
            $args = array(
                'type'                     => 'post',
                'child_of'                 => '' ,
                'parent'                   => $this->get_current_cat()->cat_ID,
                'orderby'                  => 'date',
                'order'                    => 'DESC',
                'hide_empty'               => 0,
                'hierarchical'             => 0,
                'exclude'                  => '',
                'include'                  => '',
                'number'                   => '',
                'taxonomy'                 => 'category',
                'pad_counts'               => false,
                'post_status' => 'publish',
            );
            $this->type = 1;
        } else {
            $args = array(
                'type'                     => 'post',
                'child_of'                 => $this->get_current_cat()->cat_ID,
                'parent'                   => '',
                'orderby'                  => 'date',
                'order'                    => 'DESC',
                'hide_empty'               => 0,
                'hierarchical'             => 1,
                'exclude'                  => '',
                'include'                  => '',
                'number'                   => '',
                'taxonomy'                 => 'category',
                'pad_counts'               => false,
                'post_status' => 'publish',
            );
            $this->type = 2;
        }
        $this->data = get_categories( $args );

        if(empty($this->data) /*&& (int)$this->category_level != 1*/){
            $args = array (
                'post_type'=>'post',
                'category' => $this->get_current_cat(),
                'paged' => $this->paged,
                'orderby' => 'date',
                'order' => 'DESC',
                'ignore_sticky_posts'=> 1,
                'posts_per_page' => 1,
                'taxonomy' => 'category',
                'post_status' => 'publish',
                'category__in' => array($this->get_current_cat()->cat_ID)
            );
            // The Query
            $this->type = 3;
            $this->data = new WP_Query($args);
        }

    }

    public function render(&$ind_pos_ads, &$current_row){
        //$buffy = '';
        if($this->data){
            if($this->type == 1 || $this->type == 2){
                echo '<div class="enc-list-publications"><div class="enc-row">';
                $this->is_mobile = wp_is_mobile();
                $ind = 0;
                $temp_count = 0;
                $max_nbr_ads_in_section = ceil(count($this->data) / self::CATS_BY_ROW) - 1;
                //<a alt="'.the_title_attribute( 'echo=0' ).'" class="pub-thumb" href="'.get_category_link($childcat->term_id).'"><img alt="'.the_title_attribute( 'echo=0' ).'" width="180px" height="auto"  src="'.($this->get_image_url($childcat->term_id, '180x225')).'" /> </a>
                foreach ($this->data as $childcat):
                    echo '
                    <div class="col-lg-2 col-md-3 col-sm-4">
                        <div class="pub-item">
                            <div class="pub-thumb"><img alt="'.the_title_attribute( 'echo=0' ).'" width="210px" height="auto"  src="'.($this->get_image_url($childcat->term_id, '180x225')).'" /> </div>
                            <div class="pub-content">
                            <h2 class="pub-title"><a alt="'.the_title_attribute( 'echo=0' ).'" href="'.get_category_link($childcat->cat_ID).'"> '. $childcat->cat_name .'</a></h2>
                        </div>
                        </div>                        
                    </div>
                    ';

                    if( (!$this->is_mobile && ($ind+1) % self::CATS_BY_ROW == 0) || ($this->is_mobile && ($ind+1) % self::CATS_BY_ROW_M == 0) ){
                        if( (!$this->is_mobile && $ind_pos_ads < $max_nbr_ads_in_section) || $this->is_mobile) {
                            $current_row++;
                            $temp_count = 0;
                        }
                    } else {
                        $temp_count++;
                    }

                    $this->render_sidebars($ind, $current_row, $ind_pos_ads);

                    $ind++;
                endforeach;

                if($temp_count > 0){
                    if( (!$this->is_mobile && $ind_pos_ads < $max_nbr_ads_in_section) || $this->is_mobile) {
                        $current_row++;
                        $this->render_sidebars($ind, $current_row, $ind_pos_ads);
                    }
                }

                echo '</div></div>';
                //$this->render_sidebar_indicefinal();
                wp_reset_query();
                wp_reset_postdata();
            } /*else if($this->type == 3){
                if ( $this->paged < 2 && $this->data->have_posts() ):
                    echo '<ol class="indice">';
                    while ( $this->data->have_posts() ):
                        $this->data->the_post();
                        //$do_not_duplicate = $post->ID;
                        echo '<li class="item-indice">';
                        echo '<a href="'.get_the_permalink().'" title="'.the_title_attribute( 'echo=0' ).'" >'.get_the_title().'</a>';
                        echo '</li>';
                    endwhile;
                    echo '</ol>';
                endif;
                wp_reset_query();
                wp_reset_postdata();
            }*/
        }
        //return $buffy;
    }

    public function render_sidebar_indicefinal(){
        enc_sidebars::get_instance()->render_sidebar('indice-final');
    }
    public function render_sidebars($ind, $current_row, &$global_index){
         if(enc_util::is_category_sponsored_test()){
            $sidebar_info = null;
            if(isset(enc_params::$ads_units_category_sponsored[$global_index])){
                $sidebar_info = enc_sidebars::get_instance()->get_sidebar_info(enc_params::$ads_units_category_sponsored[$global_index]);
            }
            if($sidebar_info){
                if(!$this->is_mobile && strpos(enc_params::$ads_units_category_sponsored[$global_index], 'enc_sponsoredcategory_rr_pos') === false && isset($sidebar_info['row']) && is_numeric($sidebar_info['row']) && $current_row == $sidebar_info['row'] ){
                    enc_sidebars::get_instance()->render_sidebar(enc_params::$ads_units_category_sponsored[$global_index]);
                    $global_index++;
                } else if($this->is_mobile && isset($sidebar_info['row_m']) && is_numeric($sidebar_info['row_m']) && $current_row == $sidebar_info['row_m']){
                    enc_sidebars::get_instance()->render_sidebar(enc_params::$ads_units_category_sponsored[$global_index]);
                    $global_index++;
                }
            }
        } else {
            $cat_ad_units = $this->is_mobile ? enc_params::$ads_units_category_mob : enc_params::$ads_units_category;
            $sidebar_info = null;
            if(isset($cat_ad_units[$global_index])){
                $sidebar_info = enc_sidebars::get_instance()->get_sidebar_info($cat_ad_units[$global_index]);
            }
            if($sidebar_info){
                if(!$this->is_mobile && strpos($cat_ad_units[$global_index], 'enc_category_rr_pos') === false && isset($sidebar_info['row']) && is_numeric($sidebar_info['row']) && $current_row == $sidebar_info['row'] ){
                    enc_sidebars::get_instance()->render_sidebar($cat_ad_units[$global_index]);
                    $global_index++;
                } else if($this->is_mobile && isset($sidebar_info['row_m']) && is_numeric($sidebar_info['row_m']) && $current_row == $sidebar_info['row_m']){
                    enc_sidebars::get_instance()->render_sidebar($cat_ad_units[$global_index]);
                    $global_index++;
                }
            }
        }
    }

    public function _getData(){
        return $this->type == 1 || $this->type == 2 ? $this->data : $this->data->get_posts();
    }

    public function get_var_data(){
        return $this->data;
    }

    public function get_type(){
        return $this->type;
    }


}