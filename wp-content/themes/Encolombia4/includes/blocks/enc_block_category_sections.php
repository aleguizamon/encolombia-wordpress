<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-16
 * Time: 08:43
 */

class enc_block_category_sections{

    public $title = 'SECCIONES';
    public $data = array(
        array(
            'image' => array(
                'url' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-medicina-370x230.jpg',
                'width' => '370',//'354',
                'height' => '230',//'215'
                'url_m' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-medicina-330x205.jpg',
                'width_m' => '330',//'354',
                'height_m' => '205',//'215'
                'id' => 202845,
            ),
            'title' => 'MEDICINA',
            'link' => 'https://encolombia.com/medicina/',
            'subcats' => array(
                /*array(
                    'title' => 'REVISTAS MÉDICAS',
                    'link' => 'https://encolombia.com/medicina/revistas-medicas/'
                ),
                array(
                    'title' => 'GUÍAS PARA MÉDICOS',
                    'link' => 'https://encolombia.com/medicina/guiasmed/'
                ),
                array(
                    'title' => 'LIBRERÍA DIGITAL',
                    'link' => 'https://encolombia.com/medicina/ld-medicina/'
                )*/
            )
        ),
        array(
            'image' => array(
                'url' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-vidaestilo-370x230.jpg',
                'width' => '370',
                'height' => '230',
                'url_m' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-vidaestilo-330x205.jpg',
                'width_m' => '330',
                'height_m' => '205',
                'id' => 202849,
            ),
            'title' => 'VIDA Y ESTILO',
            'link' => 'https://encolombia.com/vida-estilo/',
            'subcats' => array(
                /*array(
                    'title' => 'ALIMENTACIÓN',
                    'link' => 'https://encolombia.com/vida-estilo/alimentacion/'
                ),
                array(
                    'title' => 'NIÑOS',
                    'link' => 'https://encolombia.com/vida-estilo/ninos/'
                ),
                array(
                    'title' => 'HOGAR',
                    'link' => 'https://encolombia.com/vida-estilo/temas-de-hogar/'
                )*/
            )
        ),
        array(
            'image' => array(
                'url' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-derecho-370x230.jpg',
                'width' => '370',
                'height' => '230',
                'url_m' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-derecho-330x205.jpg',
                'width_m' => '330',
                'height_m' => '205',
                'id' => 202842,
            ),
            'title' => 'DERECHO',
            'link' => 'https://encolombia.com/derecho/',
            'subcats' => array(
                /*array(
                    'title' => 'LEYES',
                    'link' => 'https://encolombia.com/derecho/leyes/'
                ),
                array(
                    'title' => 'CÓDIGOS',
                    'link' => 'https://encolombia.com/derecho/codigos/'
                ),
                array(
                    'title' => 'ASESORÍAS Y GUÍAS LEGAL',
                    'link' => 'https://encolombia.com/derecho/guias-legales/'
                )*/
            )
        ),
        array(
            'image' => array(
                'url' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-salud-370x230.jpg',
                'width' => '370',
                'height' => '230',
                'url_m' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-salud-330x205.jpg',
                'width_m' => '330',
                'height_m' => '205',
                'id' => 202846,
            ),
            'title' => 'SALUD',
            'link' => 'https://encolombia.com/salud',
            'subcats' => array(
                /*array(
                    'title' => 'GUÍAS DE SALUD',
                    'link' => 'https://encolombia.com/salud/guias/'
                ),
                array(
                    'title' => 'DIRECTORIOS DE SERVICIOS',
                    'link' => 'https://encolombia.com/salud/dir-salud/'
                ),
                array(
                    'title' => 'NUTRICIÓN SALUDABLE',
                    'link' => 'https://encolombia.com/salud/nutricion-saludable/'
                )*/
            )
        ),
        array(
            'image' => array(
                'url' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-estetica-370x230.jpg',
                'width' => '370',
                'height' => '230',
                'url_m' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-estetica-330x205.jpg',
                'width_m' => '330',
                'height_m' => '205',
                'id' => 202844,
            ),
            'title' => 'ESTÉTICA Y BELLEZA',
            'link' => 'https://encolombia.com/salud-estetica/',
            'subcats' => array(
                /*array(
                    'title' => 'CONSEJOS DE ESTÉTICA',
                    'link' => 'https://encolombia.com/salud-estetica/estetica/consejos/'
                ),
                array(
                    'title' => 'EXPERTOS EN BELLEZA',
                    'link' => 'https://encolombia.com/salud-estetica/estetica/expertos-belleza/'
                ),
                array(
                    'title' => 'RUTINAS DE CUIDADO DIARIO',
                    'link' => 'https://encolombia.com/salud-estetica/estetica/expertos-cuidado-diario/'
                )*/
            )
        ),
        array(
            'image' => array(
                'url' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-economia-370x230.jpg',
                'width' => '370',
                'height' => '230',
                'url_m' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-economia-330x205.jpg',
                'width_m' => '330',
                'height_m' => '205',
                'id' => 202843,
            ),
            'title' => 'ECONOMÍA Y EMPRESA',
            'link' => 'https://encolombia.com/economia/',
            'subcats' => array(
                /*array(
                    'title' => 'NEGOCIOS INTERNACIONALES',
                    'link' => 'https://encolombia.com/economia/internacional/'
                ),
                array(
                    'title' => 'AGROINDUSTRIA',
                    'link' => 'https://encolombia.com/economia/agroindustria/'
                ),
                array(
                    'title' => 'INFORMACIÓN ECONÓMICA',
                    'link' => 'https://encolombia.com/economia/info-economica/'
                )*/
            )
        ),
        array(
            'image' => array(
                'url' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-veterinaria-370x230.jpg',
                'width' => '370',
                'height' => '230',
                'url_m' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-veterinaria-330x205.jpg',
                'width_m' => '330',
                'height_m' => '205',
                'id' => 202848,
            ),
            'title' => 'VETERINARIA',
            'link' => 'https://encolombia.com/veterinaria/',
            'subcats' => array(
                /*array(
                    'title' => 'MASCOTAS',
                    'link' => 'https://encolombia.com/vida-estilo/mascotas/'
                ),
                array(
                    'title' => 'TEMAS DE INTERÉS VETERINARIO',
                    'link' => 'https://encolombia.com/veterinaria/tv-interes/'
                ),
                array(
                    'title' => 'DIRECTORIO DE SERVICIOS',
                    'link' => 'https://encolombia.com/veterinaria/dvc/'
                )*/
            )
        ),
        array(
            'image' => array(
                'url' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-turismo-370x230.jpg',
                'width' => '370',
                'height' => '230',
                'url_m' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-turismo-330x205.jpg',
                'width_m' => '330',
                'height_m' => '205',
                'id' => 202847,
            ),
            'title' => 'TURISMO',
            'link' => 'https://encolombia.com/turismo/',
            'subcats' => array(
                /*array(
                    'title' => 'DESTINOS TURÍSTICOS',
                    'link' => 'https://encolombia.com/turismo/destinos-turisticos/'
                ),
                array(
                    'title' => 'AEROLÍNEAS',
                    'link' => 'https://encolombia.com/turismo/aerolineas/'
                ),
                array(
                    'title' => 'HOTELES',
                    'link' => 'https://encolombia.com/turismo/hoteles-en-el-mundo/'
                )*/
            )
        ),
        array(
            'image' => array(
                'url' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-cultura-370x230.jpg',
                'width' => '370',
                'height' => '230',
                'url_m' => 'https://encolombia.com/wp-content/uploads/2017/12/seccion-cultura-330x205.jpg',
                'width_m' => '330',
                'height_m' => '205',
                'id' => 202841,
            ),
            'title' => 'EDUCACIÓN Y CULTURA',
            'link' => 'https://encolombia.com/educacion-cultura/',
            'subcats' => array(
                /*array(
                    'title' => 'ARTE',
                    'link' => 'https://encolombia.com/educacion-cultura/arte-cultura/'
                ),
                array(
                    'title' => 'GEOGRAFÍA',
                    'link' => 'https://encolombia.com/educacion-cultura/geografia/'
                ),
                array(
                    'title' => 'TEMAS DE INTERÉS EDUCATIVO',
                    'link' => 'https://encolombia.com/educacion-cultura/educacion/'
                )*/
            )
        ),
    );

    public function render($atts){
        $buffy = '<div class="sections-home ec-title-home">';
        if(!wp_is_mobile()){
            $buffy .= '<h4 class="enc-block-title-1" style="text-align: center; display: block;">'.$this->title.'</h4>';
        }
        $buffy .= '<div class="enc-row ec-cards-home">';
        $ind = 1;
        foreach ($this->data as $item):
            $buffy .= '<div class="col-sm-4">';
            $buffy .= '<div class="enc-module-container col-'.$ind.'">';
            $buffy .= '<a class="enc-module-thumb card-home" href="'.$item['link'].'">';
            if(wp_is_mobile()){
                $buffy .= '<img width="'.$item['image']['width_m'].'" height="'.$item['image']['height_m'].'" class="entry-thumb" src="'.$item['image']['url_m'].'" '.enc_thumbs::get_srcset_sizes('enc_330x205', $item['image']['width_m'], $item['image']['url_m'], $item['image']['id']).' alt="'.$item['title'].'" title="'.$item['title'].'">';
            } else {
                $buffy .= '<img width="'.$item['image']['width'].'" height="'.$item['image']['height'].'" class="entry-thumb" src="'.$item['image']['url'].'" '.enc_thumbs::get_srcset_sizes('enc_370x230', $item['image']['width'], $item['image']['url'], $item['image']['id']).' alt="'.$item['title'].'" title="'.$item['title'].'">';
            }
            
            /*if (count($item['subcats']) > 0):
                $buffy .= '<span class="card-mask">';
                foreach ($item['subcats'] as $cat):
                    $buffy .= '<a href="'.$cat['link'].'">'.$cat['title'].'</a>';
                endforeach;
                $buffy .= '</span>';
            endif;*/
            $buffy .= '<h3 class="entry-title enc-module-title" style="margin: 0px;">'.$item['title'].'</h3>';
            $buffy .= '</a>';
            $buffy .= '</div>';
            $buffy .= '</div>';

            if(wp_is_mobile() && is_front_page()){
                if("MEDICINA" == $item['title']){
                    $buffy .= "
                    <div class='col-sm-4'>
                        <div class='enc-module-container enc_home_inline_pos2'>
                            <!-- GPT AdSlot 2 for Ad unit 'ENC_Home_Inline_Pos2' ### Size: [[728,90],[300,250],[336,280],[250,250]] -->
                            <div id='div-gpt-ad-8932392-2'>
                              <script>
                                googletag.cmd.push(function() { googletag.display('div-gpt-ad-8932392-2'); });
                              </script>
                            </div>
                            <!-- End AdSlot 2 -->
                        </div>
                    </div>";
                } else if("SALUD" == $item['title']) {
                    $buffy .= "
                    <div class='col-sm-4'>
                        <div class='enc-module-container enc_home_inline_pos3'>
                            <!-- GPT AdSlot 3 for Ad unit 'ENC_Home_Inline_Pos3' ### Size: [[970,250],[970,90],[728,90],[300,250],[336,280],[250,250]] -->
                            <div id='div-gpt-ad-8932392-3'>
                              <script>
                                googletag.cmd.push(function() { googletag.display('div-gpt-ad-8932392-3'); });
                              </script>
                            </div>
                            <!-- End AdSlot 3 -->
                        </div>
                    </div>";
                } else if("ECONOMÍA Y EMPRESA" == $item['title']) {
                    $buffy .= "
                    <div class='col-sm-4'>
                        <div class='enc-module-container enc_home_rr_pos1'>
                            <!-- GPT AdSlot 4 for Ad unit 'ENC_Home_RR_Pos1' ### Size: [[300,250]] -->
                            <div id='div-gpt-ad-8932392-4'>
                              <script>
                                googletag.cmd.push(function() { googletag.display('div-gpt-ad-8932392-4'); });
                              </script>
                            </div>
                            <!-- End AdSlot 4 -->
                        </div>
                    </div>";
                } else if("EDUCACIÓN Y CULTURA" == $item['title']) {
                    $buffy .= "
                    <div class='col-sm-4'>
                        <div class='enc-module-container enc_home_rr_pos2'>
                            <!-- GPT AdSlot 5 for Ad unit 'ENC_Home_RR_Pos2' ### Size: [[300,250]] -->
                            <div id='div-gpt-ad-8932392-5'>
                              <script>
                                googletag.cmd.push(function() { googletag.display('div-gpt-ad-8932392-5'); });
                              </script>
                            </div>
                            <!-- End AdSlot 5 -->
                        </div>
                    </div>";
                }
            }


            $ind++;
        endforeach;
        $buffy .= '</div>';
        $buffy .= '</div>';

        return $buffy;
    }
}