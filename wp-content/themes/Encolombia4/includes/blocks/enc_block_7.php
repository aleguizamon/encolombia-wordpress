<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-16
 * Time: 12:51
 */

class enc_block_7 extends enc_base_post {

    function render($atts, $td_column_number = ''){
        if(!$this->can_show()) return '';
        
        $td_column_number = 2;
        parent::render($atts, $td_column_number); // sets the live atts, $this->atts, $this->block_uid, $this->td_query (it runs the query)

        $buffy = '<div class="enc_block_7 enc-column-' . $td_column_number . '" >';
        $buffy .= $this->get_block_title(); //get the block title
        $buffy .= '<div class="enc_block_inner">';
        $buffy .= $this->inner($this->td_query->posts); //inner content of the block
        $buffy .= '</div>';
        $buffy .= '</div> <!-- ./block -->';
        return $buffy;
    }

    function inner($posts, $td_column_number = '') {
        $buffy = '';
        if (!empty($posts)) {
            $ind = 1;
            foreach ($posts as $post) {
                $enc_module_6 = new enc_module_6($post, $ind);
                $buffy .= $enc_module_6->render($post);
                $ind++;
            }
        }
        return $buffy;
    }

    private function can_show(){
        return !is_page(70975);
    }

}