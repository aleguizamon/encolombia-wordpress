<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-14
 * Time: 03:57
 */

define("ENC_THEME_VERSION", "2.0");

class enc_config{

    private static $instance;

    /**
     * enc_config constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_config();
        }

        return self::$instance;
    }

    public function configure(){
        enc_params::$template_directory_uri = get_template_directory_uri();
        add_action( 'init', array($this, 'init_config'));
        add_action( 'template_redirect',  array($this, 'mark_is_pub'));
        add_theme_support( 'post-thumbnails' );
        add_action('pre_get_posts', array($this, 'set_category_posts_order'));
        add_action( 'wp_enqueue_scripts', array($this, 'theme_enqueue_styles'), 1001);
        if ( enc_util::is_genfar_section()) {
            add_filter( 'body_class', function( $classes ) {return array_merge( $classes, array( 'page_genfar' ) );} );
        }
        //add_filter('next_posts_link_attributes', array($this, 'next_pagination_class'));
        //add_filter('previous_posts_link_attributes', array($this, 'prev_pagination_class'));
    }

    public function set_category_posts_order($query) {
        //if($query->query['post_type'] == 'post'){ echo "<pre>"; print_r($query); exit;}
        if ($query->is_category() && $query->is_main_query()) {
            $query->set('orderby', 'date'); // ordena por título de manera ascendente
            $query->set('order', 'DESC');
        }
    }

    public function next_pagination_class($attr){
        $attr .= ' class="page-arrow-next"';
        return $attr;
    }

    public function prev_pagination_class($attr){
        $attr .= ' class="page-arrow-prev"';
        return $attr;
    }


    function theme_enqueue_styles() {
        global $is_publication;
        $is_front_page = is_front_page();
        wp_register_script('enc-scripts', get_template_directory_uri() . '/assets/js/enc-scripts.js', array('jquery'));
        wp_enqueue_script('enc-scripts');
        wp_dequeue_script('otw_grid_manager');

        /*$ads_vars = $this->get_ads_vars();
        wp_localize_script( 'enc-scripts', 'ajax_enc_data', array(
            'enc_slug' => $ads_vars['enc_slug'],
            'enc_page_type' => $ads_vars['enc_page_type'],
            'enc_category_id' => $ads_vars['enc_category_id'],
            'enc_post_id' => $ads_vars['enc_post_id'],
        ));*/
        wp_enqueue_style('enc-fonts', get_template_directory_uri() . '/assets/css/fonts.css', '', ENC_THEME_VERSION, 'all' );
        wp_enqueue_style('enc-layout', get_template_directory_uri() . '/assets/css/layout.css', '', ENC_THEME_VERSION, 'all' );
        wp_enqueue_style('enc-general', get_template_directory_uri() . '/assets/css/general.css', '', ENC_THEME_VERSION, 'all' );

        if(!$is_front_page){
            wp_enqueue_style('enc-breadcrumb', get_template_directory_uri() . '/assets/css/breadcrumbs.css', '', ENC_THEME_VERSION, 'all' );
        }

        if($is_front_page){
            if(!wp_is_mobile()){
                wp_enqueue_style('enc-big-grid', get_template_directory_uri() . '/assets/css/old/big-grid.css', '', ENC_THEME_VERSION, 'all' );
            }
            wp_enqueue_style('enc-module10', get_template_directory_uri() . '/assets/css/old/module10.css', '', ENC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-secciones-home', get_template_directory_uri() . '/assets/css/old/secciones-home.css', '', ENC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-block7', get_template_directory_uri() . '/assets/css/old/block7.css', '', ENC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-module6', get_template_directory_uri() . '/assets/css/old/module6.css', '', ENC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-search-home', get_template_directory_uri() . '/assets/css/old/search-sidebar-home.css', '', ENC_THEME_VERSION, 'all' );
        } else if(is_404()){
            wp_enqueue_style('enc-404', get_template_directory_uri() . '/assets/css/404-page.css', '', ENC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-module4', get_template_directory_uri() . '/assets/css/module4.css', '', ENC_THEME_VERSION, 'all' );
        } else if(is_search()){
            wp_enqueue_style('enc-search', get_template_directory_uri() . '/assets/css/old/search-page.css', '', ENC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-module4', get_template_directory_uri() . '/assets/css/module4.css', '', ENC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-pagination', get_template_directory_uri() . '/assets/css/old/pagination.css', '', ENC_THEME_VERSION, 'all' );
            if(!wp_is_mobile()){
                wp_enqueue_style('enc-block7', get_template_directory_uri() . '/assets/css/old/block7.css', '', ENC_THEME_VERSION, 'all' );
                wp_enqueue_style('enc-module6', get_template_directory_uri() . '/assets/css/old/module6.css', '', ENC_THEME_VERSION, 'all' );
            }
        } else if(is_single()){
            //$cats = get_the_category( get_the_ID() );
            wp_register_script('enc-posts-scripts', get_template_directory_uri() . '/assets/js/enc-post-scripts.js', array('jquery'));
            wp_enqueue_script('enc-posts-scripts');
            wp_localize_script( 'enc-posts-scripts', 'enc_data', array(
                'ajax_url' => admin_url( 'admin-ajax.php' ),
                'post_scroll_limit' => 100,
                'current_post' => get_the_ID(),
                'is_sponsored' => enc_util::is_customer_section() ? 'Y' : 'N',
                //'current_cat' => $cats[0]->term_id
            ));

            wp_enqueue_style('enc-post-detail-v3', get_template_directory_uri() . '/assets/css/post-detail-v3.css', '', ENC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-module-1', get_template_directory_uri() . '/assets/css/module1.css', '', ENC_THEME_VERSION, 'all' );

            if(!wp_is_mobile()) {
                wp_enqueue_style('enc-block7', get_template_directory_uri() . '/assets/css/old/block7.css', '', ENC_THEME_VERSION, 'all');
                wp_enqueue_style('enc-module6', get_template_directory_uri() . '/assets/css/old/module6.css', '', ENC_THEME_VERSION, 'all');
            }
            /*wp_enqueue_style('enc-more-than', get_template_directory_uri() . '/assets/css/more-than.css', '', ENC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-social', get_template_directory_uri() . '/assets/css/social-networks.css', '', ENC_THEME_VERSION, 'all' );
            if(!wp_is_mobile()){
                wp_enqueue_style('enc-block7', get_template_directory_uri() . '/assets/css/block7.css', '', ENC_THEME_VERSION, 'all' );
                wp_enqueue_style('enc-module6', get_template_directory_uri() . '/assets/css/module6.css', '', ENC_THEME_VERSION, 'all' );
                wp_enqueue_style('enc-related-posts', get_template_directory_uri() . '/assets/css/related-posts.css', '', ENC_THEME_VERSION, 'all' );
            }*/
            wp_enqueue_style('enc-comments', get_template_directory_uri() . '/assets/css/comments.css', '', ENC_THEME_VERSION, 'all' );
            if(enc_util::is_page_builder_content()){
                wp_enqueue_style('enc-visual-composer', get_template_directory_uri() . '/assets/css/visual-composer.css', '', ENC_THEME_VERSION, 'all' );
            }
            if(enc_util::is_shortcode_ultimate()){
                wp_enqueue_style('enc-su', get_template_directory_uri() . '/assets/css/shortcode-ultimate.css', '', ENC_THEME_VERSION, 'all' );
            }
            if ( enc_util::is_genfar_section()) {
                wp_enqueue_style('enc-genfar', get_template_directory_uri() . '/assets/css/genfar-pages.css', '', ENC_THEME_VERSION, 'all' );
                wp_enqueue_style('enc-genfar-tabs', get_template_directory_uri() . '/assets/css/tabs-genfar.css', '', ENC_THEME_VERSION, 'all' );
            }
            if ($is_publication) {
                wp_enqueue_style('enc-list-pub', get_template_directory_uri() . '/assets/css/publications-list.css', '', ENC_THEME_VERSION, 'all' );
            }
        } else if(is_category()){
            wp_register_script('enc-cats-scripts', get_template_directory_uri() . '/assets/js/enc-cats-scripts.js', array('jquery'));
            wp_enqueue_script('enc-cats-scripts');
            wp_localize_script( 'enc-cats-scripts', 'enc_data', array(
                'ajax_url' => admin_url( 'admin-ajax.php' ),
                'cat_slug' => get_query_var('category_name'),
            ));
            wp_enqueue_style('enc-category', get_template_directory_uri() . '/assets/css/category.css', '', ENC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-module4', get_template_directory_uri() . '/assets/css/module4.css', '', ENC_THEME_VERSION, 'all' );
            //wp_enqueue_style('enc-breadcrumb', get_template_directory_uri() . '/assets/css/breadcrumbs.css', '', ENC_THEME_VERSION, 'all' );
            if($is_publication){
                wp_enqueue_style('enc-list-pub', get_template_directory_uri() . '/assets/css/publications-list.css', '', ENC_THEME_VERSION, 'all' );
            } else {
                wp_enqueue_style('enc-list-subcats', get_template_directory_uri() . '/assets/css/subcategories-list.css', '', ENC_THEME_VERSION, 'all' );
            }
            //wp_enqueue_style('enc-more-than', get_template_directory_uri() . '/assets/css/more-than.css', '', ENC_THEME_VERSION, 'all' );
            //wp_enqueue_style('enc-new-pub', get_template_directory_uri() . '/assets/css/new-publications.css', '', ENC_THEME_VERSION, 'all' );
            //wp_enqueue_style('enc-pagination', get_template_directory_uri() . '/assets/css/pagination.css', '', ENC_THEME_VERSION, 'all' );
            //wp_enqueue_style('enc-social', get_template_directory_uri() . '/assets/css/social-networks.css', '', ENC_THEME_VERSION, 'all' );
            //wp_enqueue_style('enc-comments', get_template_directory_uri() . '/assets/css/comments.css', '', ENC_THEME_VERSION, 'all' );
            //if(!wp_is_mobile()){
            //    wp_enqueue_style('enc-block7', get_template_directory_uri() . '/assets/css/block7.css', '', ENC_THEME_VERSION, 'all' );
            //    wp_enqueue_style('enc-module6', get_template_directory_uri() . '/assets/css/module6.css', '', ENC_THEME_VERSION, 'all' );
            //}
        } else if(is_author()){
            wp_enqueue_style('enc-pagination', get_template_directory_uri() . '/assets/css/old/pagination.css', '', ENC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-author', get_template_directory_uri() . '/assets/css/author.css', '', ENC_THEME_VERSION, 'all' );
            wp_enqueue_style('enc-module4', get_template_directory_uri() . '/assets/css/module4.css', '', ENC_THEME_VERSION, 'all' );
        } else if ( enc_util::is_genfar_section()) {
            //wp_enqueue_style('enc-genfar', get_template_directory_uri() . '/assets/css/genfar-pages.css', '', ENC_THEME_VERSION, 'all' );
        } else if(is_page()){
            //wp_enqueue_style('enc-social', get_template_directory_uri() . '/assets/css/social-networks.css', '', ENC_THEME_VERSION, 'all' );
            if(!wp_is_mobile()){
                wp_enqueue_style('enc-block7', get_template_directory_uri() . '/assets/css/old/block7.css', '', ENC_THEME_VERSION, 'all' );
                wp_enqueue_style('enc-module6', get_template_directory_uri() . '/assets/css/old/module6.css', '', ENC_THEME_VERSION, 'all' );
            }
        }

    }

    public function mark_is_pub(){
        if ( is_single() ) {
            global $post, $is_publication;
            $is_publication = false;
            $mod_single = new enc_module_single($post);
            if(!empty($mod_single) && $mod_single->is_publication()){
                $is_publication = true;
                add_filter( 'body_class', function( $classes ) {return array_merge( $classes, array( 'is_pub' ) );} );
            }
        } else if (is_category()) {
            global $is_publication, $enc_block_subcat_pub;
            $is_publication = enc_base_category::is_publication();
//var_dump($is_publication);exit;
            if($is_publication){
                $enc_block_subcat_pub = enc_block_list_publications::get_instance();

                if ($enc_block_subcat_pub->get_type() == 3 && $enc_block_subcat_pub->get_var_data()->have_posts()) {
                    // Obtener el primer post de la consulta
                    $enc_block_subcat_pub->get_var_data()->the_post();

                    // Obtener la URL del post
                    $post_url = get_permalink();

                    // Redireccionar a la URL del primer post
                    wp_redirect($post_url);
                    exit;
                }

            } else{
                $enc_block_subcat_pub = enc_block_list_subcategories::get_instance();
            }
        }
    }

    public function init_config(){
        if (!wp_doing_ajax()) {
            $GLOBALS['enc_config'] = array(
                'ajax_request_uri' => $_SERVER['REQUEST_URI'],
                'ajax_is_mobile' => wp_is_mobile(),
            );
        }

        /** Registro los tipo de menu del tema */
        $this->register_nav_menus();

        /** Si esta en modo DEV cargo las imágenes de encolombia.com */
        if (ENC_DEV_MODE) {
            add_filter( 'wp_get_attachment_image_src', array($this, 'replace_image_url_for_dev') );
        }
    }

    public function register_nav_menus(){
        register_nav_menus(
            array(
                'top-menu' => 'Top Header Menu',
                'header-menu' => 'Header Menu (main)',
                'footer-menu' => 'Footer Menu'
            )
        );
    }

    public function replace_image_url_for_dev($image){
        $image[0] = str_replace(ENC_DEV_ROUTE, enc_params::$prod_route, $image[0]);
        return $image;
    }

    public function get_ads_vars(){
        $data = array(
            'enc_slug' => '',
            'enc_page_type' => '',
            'enc_category_id' => '',
            'enc_category_name' => '',
            'enc_category_level' => '',
            'enc_post_id' => ''
        );
        $data['enc_slug'] = $_SERVER['REQUEST_URI'];
        $data['enc_page_type'] = is_front_page() ? 'home' : (is_single() ? 'post' : (is_category() ? 'category' : 'none'));
        $data['enc_category_id'] = 0;
        /*if (is_single() || is_page()):
            global $post;
            $data['enc_post_id'] = $post->ID;
            $_categories = get_the_category($post->ID);
            if (!empty($_categories) && isset($_categories[0], $_categories[0]->term_id)):
                $data['enc_category_id'] = $_categories[0]->term_id;
            endif;
        else:
            $data['enc_post_id'] = 0;
        endif;*/
        if (is_category()):
            $data['enc_category_id'] = get_query_var('cat');
        endif;

        // Obtener la categoría actual
        $category = get_queried_object();
        if ($category instanceof WP_Term) {
            $data['enc_category_name'] = $category->name;
            $data['enc_category_id'] = (string)$category->term_id;
        } else if($category instanceof WP_Post){
            $data['enc_post_id'] = (string)$category->ID;
            $categories = get_the_category();
            if ($categories) {
                $enc = false; $ind = 0;
                while (!$enc && $ind < count($categories)) {
                    $enc = strpos($data['enc_slug'], $categories[$ind]->slug) !== false;
                    if (!$enc) {
                        $ind++;
                    }
                }
                $data['enc_category_name'] = $categories[!$enc ? 0 : $ind]->name;
                $data['enc_category_id'] = $categories[!$enc ? 0 : $ind]->term_id;
            }
        }

        // Obtener el nivel de la categoría actual
        if(!empty($data['enc_category_id'])){
            $data['enc_category_level'] = enc_util::get_category_level($data['enc_category_id']);
        }
//echo "<pre>";print_r($data);echo "</pre>";exit;
        return $data;
    }
}