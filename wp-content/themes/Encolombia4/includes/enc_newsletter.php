<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-15
 * Time: 10:10
 */

class enc_newsletter{
    private static $instance;

    /**
     * enc_config constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_newsletter();
        }

        return self::$instance;
    }

    public function configure(){
        add_action('wpcf7_before_send_mail', array($this, 'ec_newsletter_before_send'));
        //add_action( 'wp_enqueue_scripts', array($this, 'ec_newslette_theme_enqueue_styles'), 1001);
    }



    function ec_newsletter_before_send($cf7){
        global $wpdb;
        $results = $wpdb->get_results( 'SELECT * FROM '.$wpdb->prefix.'cf7dbplugin_submits WHERE form_name = "Newsletter" AND field_name = "email" AND field_value = "'.$_REQUEST['email'].'" order by submit_time DESC', OBJECT );

        if(count($results) > 0) {
            $wpdb->update(
                $wpdb->prefix.'cf7dbplugin_submits',
                array('field_value' => $_SERVER['HTTP_REFERER']),
                array('submit_time' => $results[0]->submit_time, 'field_name' => 'url'),
                array('%s'),
                array('%f', '%s')
            );
        }
        if(count($results) == 1){
            $wpdb->update(
                $wpdb->prefix.'cf7dbplugin_submits',
                array( 'field_value' => '0' ),
                array( 'submit_time' => $results[0]->submit_time, 'field_name' => 'confirm' ),
                array( '%s' ),
                array( '%f', '%s' )
            );
        }

        if(count($results) > 1){
            add_filter( 'wpcf7_skip_mail', function($skip, $cf7){ return true; }, 10, 2 );
            $record = $wpdb->get_results( 'SELECT * FROM '.$wpdb->prefix.'cf7dbplugin_submits WHERE submit_time = '.$results[1]->submit_time.' AND field_name = "confirm"', OBJECT );
            if($record && $record[0]){
                $wpdb->update(
                    $wpdb->prefix.'cf7dbplugin_submits',
                    array( 'field_value' => $record[0]->field_value ),
                    array( 'submit_time' => $results[0]->submit_time, 'field_name' => 'confirm' ),
                    array( '%s' ),
                    array( '%f', '%s' )
                );
            }

            for($i=1;$i<count($results);$i++){
                $wpdb->delete( $wpdb->prefix.'cf7dbplugin_submits', array( 'submit_time' => $results[$i]->submit_time ), array( '%f' ) );
            }

        }

        add_filter( 'wpcf7_mail_components', array($this, 'ec_filter_send_email_cf7_components'), 10, 2);

    }


    public function ec_filter_send_email_cf7_components($components, $currentCf){
        global $wpdb;
        $results = $wpdb->get_results( 'SELECT * FROM '.$wpdb->prefix.'cf7dbplugin_submits WHERE form_name = "Newsletter" AND field_name = "email" AND field_value = "'.$_REQUEST['email'].'" order by submit_time DESC', OBJECT );

        if($results && isset($results[0])){
            $components['body'] = str_replace('@CONFIRM_LINK@', get_site_url().'/confirmar-suscripcion?t='.base64_encode($results[0]->submit_time), $components['body']);
        }
        return $components;
    }

    public function ec_newslette_theme_enqueue_styles(){
      wp_enqueue_style('jquery-multiselect',  get_template_directory_uri() ."/assets/css/jquery.multiselect.css", array(), null, 'all' );
      wp_register_script( 'jquery.multiselect',  get_template_directory_uri() ."/assets/js/jquery.multiselect.js", array('jquery'), null, true);
      wp_enqueue_script( 'jquery.multiselect');
    }
}
