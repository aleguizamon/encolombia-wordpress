<?php
/**
 * User: Andresen Miguel
 * Date: 2019-09-18
 */

class enc_social  {
    private static $instance;

    /**
     * enc_config constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_social ();
        }
        return self::$instance;
    }

    public function social_share_buttons($permalink, $thumbnail, $title){
        if(wp_is_mobile()){ return ''; }
        // return '<div class="enc-post-sharing enc-post-sharing-bottom enc-with-like">
        //         <ul class="social-share">
        //             <li style="margin-top: -3px;">
        //                 <a data-pin-do="buttonPin" data-pin-count="beside" data-pin-lang="en" href="https://www.pinterest.com/pin/create/button/?url='.urlencode($permalink).'&media='.urlencode($thumbnail).'&description='.urlencode($title).'"></a>
        //             </li>
        //             <li style="margin-top: -3px;">
        //                 <div class="fb-share-button" data-href="'.$permalink.'" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u='.urlencode($permalink).'&src=sdkpreparse" class="fb-xfbml-parse-ignore" rel="noreferrer">Share</a></div>
        //             </li>
        //             <li style="margin-top: -3px;">
        //                 <div class="fb-like" data-href="'.$permalink.'" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
        //             </li>
        //             <li>
        //                 <a href="https://twitter.com/intent/tweet?ref_src=twsrc%5Etfw" class="twitter-share-button" data-text="'.$title.'" data-related="" data-show-count="true" data-lang="en">Tweet</a>

        //             </li>
        //             <li>
        //                 <div class="g-ytsubscribe" data-channelid="UCI0bCA9an0iUY5dsScfGv1g" data-layout="default" data-count="default"></div>
        //                 <style >#___ytsubscribe_0{vertical-align: bottom !important;}</style>
        //             </li>
        //         </ul>
        //     </div>';
    }

    public function site_networks($class = 'home-social-container', $footer = false){
        //<span class="enc-social-icon-wrap"> <a target="_blank" href="'.enc_params::$social_twitter_link.'" title="Twitter"> <i class="enc-icon enc-icon-twitter"></i> </a> </span>
        //
        $output = '<div class="'.$class.'">
                    <span class="enc-social-icon-wrap"> <a target="_blank" rel="noreferrer" href="'.enc_params::$social_facebook_link.'" title="Facebook"> <i class="enc-icon enc-icon-facebook"></i> </a> </span>
                    <span class="enc-social-icon-wrap"> <a target="_blank" rel="noreferrer" href="'.enc_params::$social_pinterest_link.'" title="Pinterest"> <i class="enc-icon enc-icon-pinterest"></i> </a> </span>';
        if($footer){
            $output .= '<span class="enc-social-icon-wrap"> <a target="_blank" rel="noreferrer" href="'.enc_params::$social_youtube_link.'" title="Youtube"> <i class="enc-icon enc-icon-youtube"></i> </a> </span>';
        }
        $output .= '</div>';
        return $output;

    }

}
