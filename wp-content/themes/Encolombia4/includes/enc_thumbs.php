<?php
class enc_thumbs{
    private static $instance;

    private $thumbs = array(
        '100x70' => array(
            'retina' => true,
            'used_on' => array('old\enc_module_6')
        ),
        '218x150' => array(
            'retina' => true,
            'used_on' => array('old\enc_module_10', 'ENC_MODULE_RELATED_POST'),
            'tablet' => '150x103',
            'mobile' => '100x70'
        ),
        '290x198' => array( //antes: 265x198 -regenerado 290x217
            'retina' => true,
            'used_on' => array('ENC_MODULE_MX_4')
        ),
        '370x218' => array( //ANTES: 336x198
            'retina' => true,
            'used_on' => array('ENC_MODULE_4')
        ),
        '582x399' => array(//antes: 532x399 -regenerado 437
            'retina' => true,
            'used_on' => array('ENC_MODULE_MX_1')
        ),
        '696x0' => array(
            'retina' => true,
            'used_on' => array('LOOP_SINGLE')
        ),
        '237x141' => array(//ANTES: 224x146
            'retina' => true,
            'used_on' => array('enc_block_list_subcategories')
        ),
        '180x225' => array(
            'retina' => true,
            'used_on' => array('enc_block_list_publications')
        ),
        '330x205' => array(
            'retina' => true,
            'used_on' => array('SECCIONES DEL HOME MOBILE')
        ),
        '370x230' => array(//antes: 354x215
            'retina' => true,
            'used_on' => array('SECCIONES DEL HOME')
        ),
        '337x204' => array(//antes: 354x215
            'retina' => true,
            'used_on' => array('ARTICULOS EN CATS')
        ),
        '210x262' => array(//antes: 354x215
            'retina' => true,
            'used_on' => array('PUBLICACIONES: SUBCATEGORIAS')
        ),
        '1920x1024' => array(//antes: 354x215
            'retina' => true,
            'used_on' => array('POST FEATURED IMAGE')
        )
        /*,
        '370x550' => array(//antes: 354x215
            'retina' => true,
            'used_on' => array('ENC_MODULE_4')
        )*/
    );

    /**
     * enc_thumbs constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_thumbs();
        }

        return self::$instance;
    }

    public function install(){
        //add_filter ('jpeg_quality', function(){ return 80; });

        $sizes = $this->generate_sizes_to_install();

        foreach ($sizes as $size){
            enc_api_thumb::add($this->generate_size_name($size),
                array(
                    'name' => $this->generate_size_name($size),
                    'width' => $size['width'],
                    'height' => $size['height'],
                    'crop' => array('center', 'top'),
                    //'post_format_icon_size' => 'small',
                    //'used_on' => $size['used_on'],
                    //'no_image_path' => get_template_directory_uri() ."/assets/images/no-thumb/",
                )
            );
        }
        //echo "<pre>"; print_r(array_keys(enc_api_thumb::get_all()));
        enc_api_thumb::save();
    }

    private function generate_sizes_to_install(){
        $sizes = array();
        foreach ($this->thumbs as $k => $v){
            $dimmensions = $this->get_dimmensions($k);
            $sizes[] = array_merge($dimmensions, array('used_on' => $v['used_on']));
            if($v['retina']){
                $sizes[] = array(
                    'width' => $dimmensions['width']*2,
                    'height' => $dimmensions['height']*2,
                    'used_on' => $v['used_on']
                );
            }
            if(isset($v['tablet'])){
                $dimmensions = $this->get_dimmensions($v['tablet']);
                $sizes[] = array_merge($dimmensions, array('used_on' => $v['used_on']));
            }
            if(isset($v['mobile'])){
                $dimmensions = $this->get_dimmensions($v['mobile']);
                $sizes[] = array_merge($dimmensions, array('used_on' => $v['used_on']));
            }
        }
        return $sizes;
    }

    private function generate_size_name($size){
        return sprintf('enc_%dx%d', $size['width'], $size['height']);
    }

    private function get_dimmensions($size){
        $parts = explode('x', $size);
        return array('width' => $parts[0], 'height' => $parts[1]);
    }

    /**
     * Returns the srcset and sizes parameters or an empty string
     * @param $thumb_type - thumbnail name/type (ex. td_356x220)
     * @param $thumb_width - thumbnail width
     * @param $thumb_url - thumbnail url
     * @return string
     */
    static function get_srcset_sizes($thumb_type, $thumb_width, $thumb_url, $thumb_id = null) {
        $return_buffer = '';
        /*if (!empty($thumb_width) && in_array($thumb_type, array('enc_582x399', 'enc_290x198'))) {
            $base_image = self::get_base_image($thumb_url, $thumb_type);
            $thumb_type_parts = explode('_', $thumb_type);
            $thumb_type_measure = explode('x', $thumb_type_parts[1]);
            $image_default = $thumb_url;
            $image_retina = sprintf("%s-%dx%d.%s", $base_image[0], (int)$thumb_type_measure[0] * 2, (int)$thumb_type_measure[1] * 2, $base_image[1]);

            $return_buffer = ' srcset="' . esc_url($image_default) . ', ' . esc_url($image_retina) . ' 2x"';
            }*/


            //backwards compatibility - check if wp_get_attachment_image_srcset is defined, it was introduced only in WP 4.4
            if (function_exists('wp_get_attachment_image_srcset') && !empty($thumb_id)) {
                //retina srcset and sizes
                if (!empty($thumb_width)) {
                    $thumb_w = ' ' . $thumb_width . 'w';
                    $retina_thumb_width = $thumb_width * 2;
                    $retina_thumb_w = ' ' . $retina_thumb_width . 'w';
                    //retrieve retina thumb url

                    $base_image = self::get_base_image($thumb_url, $thumb_type);
                    $thumb_type_parts = explode('_', $thumb_type);
                    $thumb_type_measure = explode('x', $thumb_type_parts[1]);
                    $image_retina = sprintf("%s%dx%d", 'enc_', (int)$thumb_type_measure[0] * 2, (int)$thumb_type_measure[1] * 2);


                    //$retina_url =  wp_get_attachment_image_src($thumb_id, $thumb_type . '_retina');
                    $retina_url =  wp_get_attachment_image_src($thumb_id, $image_retina);
                    //if(is_user_logged_in()){var_dump($image_retina); var_dump($retina_url);}
                    //srcset and sizes
                    if ($retina_url !== false) {
                        //$return_buffer .= ' srcset="' . esc_url( $thumb_url ) . $thumb_w . ', ' . esc_url( $retina_url[0] ) . $retina_thumb_w . '" sizes="(-webkit-min-device-pixel-ratio: 2) ' . $retina_thumb_width . 'px, (min-resolution: 192dpi) ' . $retina_thumb_width . 'px, ' . $thumb_width . 'px"';
                        $return_buffer .= ' srcset="' . esc_url( $thumb_url ) . ', ' . esc_url( $retina_url[0] ) . ' 2x"';
                    }

                    //responsive srcset and sizes
                }
                else {
                    $thumb_srcset = wp_get_attachment_image_srcset($thumb_id, $thumb_type);
                    $thumb_sizes = wp_get_attachment_image_sizes($thumb_id, $thumb_type);
                    if ($thumb_srcset !== false && $thumb_sizes !== false) {
                        $return_buffer .=  ' srcset="' . $thumb_srcset . '" sizes="' . $thumb_sizes . '"';
                    }
                }
            }

        //if (ENC_DEV_MODE) {
        $return_buffer = str_replace(ENC_DEV_ROUTE, enc_params::$prod_route, $return_buffer);
        //}

        return $return_buffer;
    }

    static function get_base_image($url, $thumb_type){
        $data = array();
        $thumb_type_parts = explode('_', $thumb_type);
        $data[1] = self::get_extension($url);
        if(isset($thumb_type_parts[1]) && strpos($url, $thumb_type_parts[1]) !== false){
            $data[0] = substr($url, 0, strpos($url, '-' . $thumb_type_parts[1]));
        } else {
            $data[0] = empty($data[1]) ? $url : substr($url, 0, strpos($url, '.' . $data[1]));
        }

        return $data;
    }

    static function get_extension($url){
        $parts = explode('.', $url);
        return count($parts) > 1 ? $parts[count($parts)-1] : '';
    }

    static function get_part_without_ext($url){

    }

}
