<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-15
 * Time: 09:11
 */
require_once('enc_cf_category.php');

class enc_admin_config{

    private static $instance;

    /**
     * enc_config constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_admin_config();
        }

        return self::$instance;
    }

    public function configure(){
        add_action( 'admin_head', array($this, 'sort_sidebars_by_name'));
        add_filter("tiny_mce_before_init", array($this, "ec_tiny_mce_before_init"), 10, 2);

        enc_cf_category::get_instance()->configure();
    }


    public function sort_sidebars_by_name() {
        global $wp_registered_sidebars;
        $temp = array();
        foreach($wp_registered_sidebars as $key => $value){
            $temp[$key] =  trim(strtolower($value['name']));
        }
        asort($temp);
        $final = array();
        foreach($temp as $key1 => $value1){

            foreach($wp_registered_sidebars as $key2 => $value2){
                if($key1 == $key2){
                    $final[$key2] = $value2;
                    continue;
                }
            }
        }
        $wp_registered_sidebars = $final;
    }



    public function ec_tiny_mce_before_init($mceInit, $editor_id)
    {
        $mceInit['extended_valid_elements'] = 'a[href|id|onclick|rev|rel|name|target|class]';
        return $mceInit;
    }
}