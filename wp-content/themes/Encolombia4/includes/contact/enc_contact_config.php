<?php
class enc_contact_config{
    private static $instance;

    private static $USED_IN_PAGES = array('/publique-sus-articulos/', '/contacts/', '/quienes-somos/');

    /**
     * enc_contact_config constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_contact_config();
        }

        return self::$instance;
    }

    public function configure() {
        if($this->can_install()){
            $this->includes();
            add_action( 'wp_enqueue_scripts', array($this, 'contact_enqueue_scripts'), 1001);
            add_shortcode( 'enc-contact-form', array($this, 'render_contact_form') );
        }
    }

    private function includes(){
        //require_once(get_template_directory() . '/includes/contact/templates/contact-template.php');
        //require_once(get_template_directory() . '/includes/shortcodes/enc_block_publications_sidebar.php');

    }

    public function contact_enqueue_scripts() {
        wp_enqueue_style('enc-contact', get_template_directory_uri() . '/includes/contact/assets/enc-contact.js', array('jquery'), '1.0', 'all' );
    }

    private function can_install(){
        $can = false;
        foreach(self::$USED_IN_PAGES as $item){
            if(enc_util::is_uri_section($item)){
                $can = true;
            }
        }
        return $can;
    }

    public function render_contact_form($atts){
        $buffy = '';
        $template = '';
        if(isset($atts['type'])){
            $template = $atts['type'] == 'contacto' ? 'contact-template.php' : ($atts['type'] == 'publicar' ? 'publication-template.php' : '');
        }
        if(!empty($template)){
            ob_start();
            enc_util::load_template(dirname( __FILE__ ) . '/templates/', $template);
            $buffy = ob_get_clean();
        }
        return $buffy;
    }




}