<?php
class enc_scheme_factory{

    public function get_scheme(){
        if(enc_scheme_recipe::is_recipe()){
            return enc_scheme_recipe::get_instance();
        }

        return null;
    }

}