<?php
 
/**
 * Class enc_block - base class for blocks
 */


class enc_base_post {

    private $atts = array();
    protected $td_query;


    protected function get_att($att_name) {
        if (!empty($this->atts[$att_name])) {
            return $this->atts[$att_name];
        }

        return '';
    }

    /**
     * Used by blocks that need auto generated titles
     * @return string
     */
    function get_block_title() {
        $custom_title = $this->get_att('custom_title');
        $custom_url = $this->get_att('custom_url');

        if (empty($custom_title)) {
            $custom_title = 'Block title';
        }

        // there is a custom title
        $buffy = '';
        $buffy .= '<h3 class="enc-block-title-1">';
        if (!empty($custom_url)) {
            $buffy .= '<a href="' . esc_url($custom_url) . '" >' . esc_html($custom_title) . '</a>';
        } else {
            $buffy .= '<span >' . esc_html($custom_title) . '</span>';
        }
        $buffy .= '</h3>';
        return $buffy;
    }

    /**
     * the base render function. This is called by all the child classes of this class
     * @param $atts
     * @param $content
     * @return string ''
     */
    function render($atts, $column_number) {
        // build the $this->atts

        //$atts = $this->add_live_filter_atts($atts); // add live filter atts


        // WARNING! all the atts MUST BE DEFINED HERE !!! It's easier to maintain and we always have a list of them all
        $this->atts = shortcode_atts ( // add defaults (if an att is not in this list, it will be removed!)
            array(
                'limit' => 5,  // @todo trebuie refactoriata partea cu limita, in paginatie e hardcodat tot 5 si deja este setat in constructor aici
                'orderby' => '',
                'post_ids' => '', // post id's filter (separated by commas)
                'tag_slug' => '', // tag slug filter (separated by commas)
                'autors_id' => '', // filter by authors ID ?
                'installed_post_types' => '', // filter by custom post types
                'category_id' => '',
                'category_ids' => '',
                'custom_title' => '',       // used in td_block_template_1.php
                'custom_url' => '',         // used in td_block_template_1.php
                'show_child_cat' => '',
                'sub_cat_ajax' => '',
                'ajax_pagination' => '',
                'header_color' => '',       // used in td_block_template_1.php + here for js -> loading color
                'header_text_color' => '',  // used in td_block_template_1.php

                'ajax_pagination_infinite_stop' => '',
                'td_column_number' => $column_number, // if no column number passed, get from VC

                // ajax preloader
                'td_ajax_preloading' => '',


                // drop down list + other live filters?
                'td_ajax_filter_type' => '',
                'td_ajax_filter_ids' => '',
                'td_filter_default_txt' => __('Todo', enc_params::$translate_domain),

                // classes?  @see get_block_classes
                'color_preset' => '',
                'border_top' => '',
                'class' => '',
                'el_class' => '',
                'offset' => '', // the offset

                'css' => '', //custom css - used by VC

                'tdc_css' => '', //custom css - used by TagDiv Composer
                'tdc_css_class' => '', // unique css class - used by TagDiv Composer to add inline css ('class' could not be used because it's not unique)
                'tdc_css_class_style' => '', // unique css class - used by get_css to add inline css on td-element-style ('class' could not be used because it's not unique)

                // live filters
                // $atts['live_filter'] is set by the 'user'. cur_post_same_tags | cur_post_same_author | cur_post_same_categories
                'live_filter' => '',

                // the two live filters are set by @see td_block::add_live_filter_atts
                'live_filter_cur_post_id' => '',      /** @see td_block::add_live_filter_atts */
                'live_filter_cur_post_author' => '',  /** @see td_block::add_live_filter_atts */

                'block_template_id' => '', // used to load a different block template on this specific block. By default the block will load the global block template from the panel
                'category__in'=> '',
                'post__not_in' => '',
            ),
            $atts
        );


        /*// @todo vezi daca e necesara chestia asta! si daca merge cum trebuie
        if (!empty($this->atts['custom_title'])) {
            $this->atts['custom_title'] = htmlspecialchars($this->atts['custom_title'], ENT_QUOTES );
        }
        if (!empty($this->atts['custom_url'])) {
            $this->atts['custom_url'] = htmlspecialchars($this->atts['custom_url'], ENT_QUOTES );
        }*/


        //update unique id on each render
        //$this->block_uid = enc_global::td_generate_unique_id();

        /** add the unique class to the block. The _rand class is used by the blocks js. @see tdBlocks.js  */
        //$unique_block_class = $this->block_uid . '_rand';
        //$this->add_class($unique_block_class);

        // Set the 'tdc_css_class' parameter
        //$this->atts['tdc_css_class'] = $unique_block_class;

        /** The _rand_style class is used by td-element-style to add style */
        //$unique_block_class_style = $this->block_uid . '_rand_style';
        //$this->atts['tdc_css_class_style'] = $unique_block_class_style;

        /*$td_pull_down_items = array();

        // do the query and make the AJAX filter only on loop blocks
        if ($this->is_loop_block() === true) {

            //by ref do the query
            $this->td_query = &td_data_source::get_wp_query($this->atts);

            // get the pull down items
            $td_pull_down_items = $this->block_loop_get_pull_down_items();
        }*/

        $this->td_query = &enc_data_source::get_wp_query($this->atts);





/*
        if (td_util::is_mobile_theme()) {

            // The mobile theme uses only 'td_block_template_1' (in api this is the only registered block template)
            $td_block_template_id = 'td_block_template_1';

        } else {
            $td_block_template_id = $this->atts['block_template_id'];
            if ( empty( $td_block_template_id ) ) {
                $td_block_template_id = td_options::get( 'tds_global_block_template', 'td_block_template_1' );
            }


            $demo_id = td_util::get_loaded_demo_id();
            if ($demo_id !== false) {
                $custom_block_template_path = td_global::$demo_list[$demo_id]['folder']  . $td_block_template_id . '.php';
                if (file_exists($custom_block_template_path)) {
                    require_once $custom_block_template_path;
                }
            }
        }


        $this->td_block_template_data = array(
            'atts' => $atts,
            'block_uid' => $this->block_uid,
            'unique_block_class' => $unique_block_class,
            'td_pull_down_items' => $td_pull_down_items,
        );
        $this->td_block_template_instance = new $td_block_template_id($this->td_block_template_data);
*/





        return '';
    }


}
