<?php

class enc_yoast_breadcrumb_extend{

    private static $instance;

    /**
     * enc_yoast_breadcrumb_extend constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_yoast_breadcrumb_extend();
        }

        return self::$instance;
    }


    public function configure(){
        //add_filter( 'wpseo_breadcrumb_links', array($this, 'remove_current_post_title_from_breadcrumb') );
        add_filter( 'wpseo_breadcrumb_output_wrapper', function() { return 'ol'; } );
        //add_filter( 'wpseo_breadcrumb_output_class', function ($class) { return 'breadcrumb__list'; } );
        add_filter( 'wpseo_breadcrumb_single_link_wrapper', function ($wrapper){ return 'li'; });
        add_filter( 'wpseo_breadcrumb_separator', function($separator){ return ''; } );
        add_filter( 'wpseo_breadcrumb_output', array($this, 'custom_yoast_breadcrumb_output'), 999, 2 );
    }

    function custom_yoast_breadcrumb_output( $output, $presentation ) {
        //$separator = '»';
        //$output = str_replace( '/a></li', sprintf('/a><span class="separator">%s</span></li', $separator), $output );
        $output = str_replace( '<ol>', '<ol class="breadcrumb__list" itemscope itemtype="https://schema.org/BreadcrumbList">', $output );
        $output = str_replace( '<li>', '<li class="breadcrumb__item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">', $output );
        $output = preg_replace( '#<a href="(.*?)">(.*?)</a>#', '<a href="$1" itemprop="item"><span itemprop="name">$2</span></a>', $output );
        $output = $this->add_position_meta_to_list($output);

        return $output;
    }

    protected function add_position_meta_to_list($list_html) {
        // Crear una instancia de DOMDocument para manipular el HTML
        $dom = new DOMDocument();
        $html = mb_convert_encoding($list_html, 'HTML-ENTITIES', "UTF-8");
        $dom->loadHTML($html);
        //$dom->encoding = 'UTF-8';

        // Obtener todos los elementos <li>
        $li_items = $dom->getElementsByTagName('li');
        $count = 1;

        // Iterar sobre los elementos <li>
        foreach ($li_items as $li) {
            // Crear un nuevo elemento meta con el itemprop de posición y el valor actual de $count
            $meta = $dom->createElement('meta');
            $meta->setAttribute('itemprop', 'position');
            $meta->setAttribute('content', $count);

            // Agregar el nuevo elemento meta al elemento <li>
            $li->appendChild($meta);

            // Incrementar el contador
            $count++;
        }

        // Obtener el HTML actualizado de la lista y devolverlo
        $list_html = $dom->saveHTML($dom->getElementsByTagName('ol')->item(0));
        return $list_html;
    }

    public function remove_current_post_title_from_breadcrumb( $links ) {
        if ( count( $links ) > 1 ) {
            array_pop( $links );
        }
        return $links;
    }
}