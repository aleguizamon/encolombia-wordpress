<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-10-09
 * Time: 14:04
 */

abstract class enc_base_category{

    protected $current_cat;

    protected $parent_cat = null;

    protected $category_level;

    protected $require_parent = false;

    protected $require_level = false;



    /**
     * enc_base_category constructor.
     */
    public function __construct()
    {
        $this->initialize();
    }

    public abstract function render(&$ind_pos_ads, &$current_row);
    public abstract function get_data();

    private function initialize(){
        $this->current_cat = is_single() ? get_the_category() : array(get_category(get_query_var('cat')));
        if($this->require_parent && isset($this->current_cat[0], $this->current_cat[0]->parent)){
            $this->parentCat = get_category($this->current_cat[0]->parent);
        }
        if($this->require_level){
            //echo "ANTES:"; var_dump($this->category_level);
            $this->category_level = is_single() ? 'P' : $this->get_category_level();
        }
    }

    public function update_exist_subcats($data){
        global $exist_subcats;
        $exist_subcats = !empty($data);
    }


    public static function is_publication(){
        $current_cat = is_single() ? get_the_category() : array(get_category(get_query_var('cat')));
        $is_publication = '0';
        if($current_cat && isset($current_cat[0]) && !is_wp_error($current_cat[0])){
            $is_publication = get_option('enc_is_pub-' . $current_cat[0]->cat_ID, '0');
            //var_dump($current_cat[0]->cat_ID);
            //echo "<pre>";print_r(ot_get_option( 'cat_publi', '' ));
        }
        return $is_publication == '1';
        /*global $publications;
        if ($this->current_cat && isset($this->current_cat[0]) && is_wp_error($this->current_cat[0])){
            $this->current_cat = null;
        }
        //echo "WW";var_dump($this->current_cat);
        return $term_id ? in_array($term_id, $publications) :
            (!empty($this->current_cat) ? (is_array($this->current_cat) && in_array($this->current_cat[0]->cat_ID, $publications)) : false);*/

    }

    public function get_category_level(){
        $current_category = get_query_var('cat');
        $parent_separator = ' --- ';
        $parents = get_category_parents($current_category,false, $parent_separator);
        if(is_wp_error($parents)){
            $parents = '';
        }
        return count(explode($parent_separator, trim($parents)))-1;

        /*$my_category  = get_categories('include='.$current_category);
        $cat_depth=0;

        if ($my_category[0]->category_parent == 0){
            $cat_depth = 0;
        } else {
            while( $my_category[0]->category_parent != 0 ) {
                $my_category = get_categories('include='.$my_category[0]->category_parent);
                $cat_depth++;
            }
        }
        echo 'NIVEL: '.$cat_depth.'<br>';
        echo 'OTRONIVEL: '.$lev.'<br>';
        return $cat_depth;*/
    }

    function get_image_url($term_id = NULL, $size = 'full', $return_placeholder = FALSE) {
        if (!$term_id) {
            if (is_category())
                $term_id = get_query_var('cat');
            elseif (is_tag())
                $term_id = get_query_var('tag_id');
            elseif (is_tax()) {
                $current_term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
                $term_id = $current_term->term_id;
            }
        }

        $taxonomy_image_url = get_option('z_taxonomy_image'.$term_id);

        if(!empty($taxonomy_image_url)){
            if(!empty($size)){
                $parts = explode('.', $taxonomy_image_url);
                $taxonomy_image_url = str_replace('.'.$parts[count($parts)-1], '-' . $size . '.' . $parts[count($parts)-1], $taxonomy_image_url);
            }
            //if(ENC_DEV_MODE)
                $taxonomy_image_url = str_replace(ENC_DEV_ROUTE, enc_params::$prod_route, $taxonomy_image_url);
        }

        if ($return_placeholder)
            return ($taxonomy_image_url != '') ? $taxonomy_image_url : Z_IMAGE_PLACEHOLDER;
        else
            return $taxonomy_image_url;
    }

    public function get_title($title = '', $size = 40){
        $title = empty($title) ? $this->current_cat[0]->cat_name : $title;
        return strlen($title) > $size ? substr($title, 0, $size).'...' : $title;
    }

    /**
     * @return mixed
     */
    public function get_current_cat()
    {
        return $this->current_cat[0];
    }

    /**
     * @return null
     */
    public function getParentCat()
    {
        return $this->parent_cat;
    }


}