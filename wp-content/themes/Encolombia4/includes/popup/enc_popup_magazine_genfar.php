<?php
class enc_popup_magazine_genfar extends enc_popup{

    private static $pages = array(238059, 238051, 238032, 238061);
    private static $instance;

    /**
     * enc_popup_magazine_genfar constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_popup_magazine_genfar();
        }
        return self::$instance;
    }



    protected function can_show()
    {
        return is_page(self::$pages) || enc_util::is_folletos_genfar_page();
    }

    public function render()
    {
        include_once get_template_directory() . '/includes/popup/templates/enc_popup_mg_template.php';
        exit;
    }

    public static function can_show_popup(){
        return is_page(self::$pages) || enc_util::is_folletos_genfar_page();
    }
}