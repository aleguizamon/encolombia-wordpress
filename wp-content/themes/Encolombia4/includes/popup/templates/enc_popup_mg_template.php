<div class="zoom-anim-dialog mfp-hide" id="genfar-magazine-modal" >
    <div class="popup-wrapper">
        <div class="popup-inner">
            <h3 class="popup-title">MATERIAL DISEÑADO CON FINES EDUCATIVOS Y DIRIGIDOS AL CUERPO MEDICO.</h3>
            <p class="popup-content">
                Información prescriptiva completa a disposición del médico, en la Dirección Medica de Sanofi Aventis de Colombia S.A.
            </p>
            <p class="popup-content">
                Transversal 23 N 97-73, Ed.City Business, Pisos 8 y 9, Bogotá D.C. Tel: (1) 6214400 – Fax: (1) 7444237
            </p>
            <a href="javascript:;" id="genfar-accept-terms" class="popup-primary-btn">&lt;&lt;Certifico que soy Médico o Profesional de la Salud&gt;&gt;</a>
        </div>
    </div>
</div>
<a href="#genfar-magazine-modal" class="popup-with-zoom-anim" id="link-open-gm-modal" style="display: none;">Open Modal</a>
<script>
    var interval_genfar = setInterval(function() {
        if (!!window.jQuery) {
            jQuery(document).ready(function() {
                jQuery('#link-open-gm-modal').magnificPopup({
                    type: 'inline',
                    fixedContentPos: false,
                    fixedBgPos: true,
                    overflowY: 'auto',
                    closeBtnInside: true,
                    preloader: false,
                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in'
                });
                jQuery('#genfar-accept-terms').on('click', function (e){
                    e.preventDefault();
                    Cookies.set('enc-genfar-terms', 'Y');
                    jQuery('#link-open-gm-modal').magnificPopup('close');
                });

                var genfar_terms = Cookies.get('enc-genfar-terms') === undefined ? '' : Cookies.get('enc-genfar-terms');
                if(genfar_terms !== 'Y'){
                    jQuery('#link-open-gm-modal').click();
                }
            });
            clearInterval(interval_genfar);
        }
    }, 500);
</script>
<?php
