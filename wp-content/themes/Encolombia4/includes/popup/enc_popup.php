<?php
abstract class enc_popup{

    protected abstract function can_show();
    protected abstract function render();

    public function install()
    {
        add_action( 'wp', array($this, 'configure'), 99);
    }

    public function configure(){
        if($this->can_show()){
            add_action( 'wp_enqueue_scripts', array($this, 'enqueue_scripts'), 99);
            add_action( 'wp_footer', array($this, 'render'), 100 );
        }
    }

    public function enqueue_scripts() {
        wp_enqueue_style('enc-popup', get_template_directory_uri() . '/assets/css/magnific-popup.css', array(), '1.0', 'all' );
        wp_enqueue_style('enc-popup-custom', get_template_directory_uri() . '/assets/css/popup.css', array(), '1.0', 'all' );
        wp_enqueue_script('enc-cookie', get_template_directory_uri() . '/assets/js/js.cookie.js', array('jquery'), '1.0', 'all' );
        wp_enqueue_script('enc-popup', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', array('jquery'), '1.0', 'all' );
    }



    /*public function configure() {
        if($this->can_install()){
            $this->includes();
            add_action( 'wp_enqueue_scripts', array($this, 'contact_enqueue_scripts'), 1001);
            add_shortcode( 'enc-contact-form', array($this, 'render_contact_form') );
        }
    }

    private function includes(){
        //require_once(get_template_directory() . '/includes/contact/templates/contact-template.php');
        //require_once(get_template_directory() . '/includes/shortcodes/enc_block_publications_sidebar.php');

    }



    private function can_install(){
        $can = false;
        foreach(self::$USED_IN_PAGES as $item){
            if(enc_util::is_uri_section($item)){
                $can = true;
            }
        }
        return $can;
    }

    public function render_contact_form($atts){
        $buffy = '';
        $template = '';
        if(isset($atts['type'])){
            $template = $atts['type'] == 'contacto' ? 'contact-template.php' : ($atts['type'] == 'publicar' ? 'publication-template.php' : '');
        }
        if(!empty($template)){
            ob_start();
            enc_util::load_template(dirname( __FILE__ ) . '/templates/', $template);
            $buffy = ob_get_clean();
        }
        return $buffy;
    }
*/



}