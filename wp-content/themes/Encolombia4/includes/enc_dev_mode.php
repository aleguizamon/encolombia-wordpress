<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-28
 * Time: 18:43
 */

if (!defined('ENC_DEV_MODE')) {
    define("ENC_DEV_MODE", true);
}

if (!defined('ENC_DEV_ROUTE')) {
    define("ENC_DEV_ROUTE", 'https://encolombia.local');
}
