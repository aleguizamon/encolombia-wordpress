<?php
class enc_disable_author_pages{
    private static $instance;

    /**
     * enc_disable_author_pages constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_disable_author_pages();
        }

        return self::$instance;
    }

    public function configure(){
        add_action( 'template_redirect',    array( $this, 'enc_disable_author_page' ) );
        add_filter( 'author_link',          array( $this, 'enc_disable_author_link') );
    }

    /**
     * Overwrite the author url with an empty string
     *
     * @param string $content url to author page
     * @return string
     */
     public function enc_disable_author_link( $content ) {
        return "";
    }

    /**
     * Redirect the user
     *
     * This function is registerd to the template_redirect hook and  checks
     * to redirect the user to the selected page (or to the homepage)
     *
     */
    public function enc_disable_author_page() {
        if ( is_404() && ! ( get_query_var( 'author' ) || get_query_var( 'author_name' ) ) ) {
            return;
        }

        if ( is_author() ) {
            wp_redirect( get_home_url(), 301 );
            exit;
        }
    }


}