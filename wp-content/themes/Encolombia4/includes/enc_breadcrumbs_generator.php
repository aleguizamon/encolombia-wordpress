<?php


class enc_breadcrumbs_generator {

  public static function get_category_breadcrumbs($primary_category_obj) {
      //breadcrumbs array
      $breadcrumbs_array = self::category_breadcrumbs_array($primary_category_obj);

      //breadcrumbs html
      return self::get_breadcrumbs($breadcrumbs_array);
  }

  public static function category_breadcrumbs_array($primary_category_obj) {

      $category_1_name = '';
      $category_1_url = '';
      $category_2_name = '';
      $category_2_url = '';
      $breadcrumbs_array = array();

      if (!empty($primary_category_obj)) {
          if (!empty($primary_category_obj->name)) {
              $category_1_name = $primary_category_obj->name;
              $category_1_url = get_category_link($primary_category_obj->term_id);
          } else {
              $category_1_name = '';
          }

          if (!empty($primary_category_obj->cat_ID)) {
              $category_1_url = get_category_link($primary_category_obj->cat_ID);
          }

          if (!empty($primary_category_obj->parent) and $primary_category_obj->parent != 0) {
              $parent_category_obj = get_category($primary_category_obj->parent);
              if (!empty($parent_category_obj)) {
                  $category_2_name = $parent_category_obj->name;
                  $category_2_url = get_category_link($parent_category_obj->cat_ID);
              }
          }
      }
      if (!empty($category_1_name)) {
          //parent category
          if (!empty($category_2_name)) {
              $breadcrumbs_array [] = array (
                  'title_attribute' => __('Ver publicaciones', enc_params::$translate_domain) . ' ' . htmlspecialchars($category_2_name),
                  'url' => $category_2_url,
                  'display_name' => $category_2_name
              );
          }
          //child category
          $breadcrumbs_array [] = array (
              'title_attribute' => '',
              'url' => $category_1_url,
              'display_name' => $category_1_name
          );
          //pagination
          $td_paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
          if ($td_paged > 1) {
              $breadcrumbs_array [] = array (
                  'title_attribute' => '',
                  'url' => '',
                  'display_name' =>  __('Pagina', enc_params::$translate_domain) . ' ' . $td_paged
              );
          }
      }

      return $breadcrumbs_array;
  }

  static function get_breadcrumbs($breadcrumbs_array) {
      global $post;
      if (empty($breadcrumbs_array)) {
          return '';
      }
      // add home breadcrumb if the theme is configured to show it
      array_unshift($breadcrumbs_array, array(
          'title_attribute' => '',
          'url' => esc_url(home_url( '/' )),
          'display_name' => __('Inicio', enc_params::$translate_domain)
      ));

      $buffy = '';
      $buffy .= '<div class="entry-crumbs">';

      $array_elements = count($breadcrumbs_array);

      foreach ($breadcrumbs_array as $key => $breadcrumb) {

          if ($key == $array_elements - 1) {
              //last element should not display the url
              $breadcrumb['url'] = '';
          }
          if (empty($breadcrumb['url'])) {
              if ($key != 0) { //add separator only after first
                  $buffy .= ' <i class="enc-icon-right enc-bread-sep enc-bred-no-url-last">></i> ';
              }
              //no link - breadcrumb
              $buffy .=  '<span class="enc-bred-no-url-last">' . esc_html( $breadcrumb['display_name'] ) . '</span>';

          } else {
              if ($key != 0) { //add separator only after first
                  $buffy .= ' <i class="enc-icon-right td-bread-sep">></i> ';
              }
              //normal links
              $buffy .= '<span><a title="' . esc_attr( $breadcrumb['title_attribute'] ) . '" class="entry-crumb" href="' . esc_url( $breadcrumb['url'] ) . '">' . esc_html( $breadcrumb['display_name'] ) . '</a></span>';
          }
      }
      $buffy .= '</div>';
      return $buffy;
  }

  static function get_single_breadcrumbs($post_title) {
      //breadcrumbs array
      $breadcrumbs_array = self::single_breadcrumbs_array($post_title);

      //breadcrumbs html
      return self::get_breadcrumbs($breadcrumbs_array);
  }

  private static function single_breadcrumbs_array($post_title) {
      global $post;
      //wordpress default post types
      $wp_post_types = array('post', 'page', 'attachment', 'revision', 'nav_menu_item', 'custom_css', 'customize_changeset');

      if (isset($post) && !in_array($post->post_type, $wp_post_types)) {
          //we have a custom post type
          // return self::custom_post_type_breadcrumbs_array();
          return '';
      }

      $category_1_name = '';
      $category_1_url = '';
      $category_2_name = '';
      $category_2_url = '';

      $primary_category_id =  get_queried_object_id();
      $primary_category_obj = get_the_category($primary_category_id);

      if (!empty($primary_category_obj)) {
          if (!empty($primary_category_obj[0]->name)) {
              $category_1_name = $primary_category_obj[0]->name;
          } else {
              $category_1_name = '';
          }

          if (!empty($primary_category_obj[0]->cat_ID)) {
              $category_1_url = get_category_link($primary_category_obj[0]->cat_ID);
          }

          if (!empty($primary_category_obj[0]->parent) and $primary_category_obj[0]->parent != 0) {
              $parent_category_obj = get_category($primary_category_obj[0]->parent);
              if (!empty($parent_category_obj)) {
                  $category_2_name = $parent_category_obj->name;
                  $category_2_url = get_category_link($parent_category_obj->cat_ID);
              }
          }
      }

      $breadcrumbs_array = array();
      if (!empty($category_1_name)) {
          if (!empty($category_2_name)) {
              $breadcrumbs_array [] = array (
                  'title_attribute' => __('Ver publicaciones') . ' ' . htmlspecialchars($category_2_name),
                  'url' => $category_2_url,
                  'display_name' => $category_2_name
              );
          }

          //child category
          $breadcrumbs_array [] = array (
              'title_attribute' => __('Ver publicaciones') . ' ' . htmlspecialchars($category_1_name),
              'url' => $category_1_url,
              'display_name' => $category_1_name
          );
      }

      return $breadcrumbs_array;
  }

  private static function td_get_canonical_url($post = null) {
      if (function_exists('wp_get_canonical_url')) {
          return wp_get_canonical_url($post);
      }
      $post = get_post( $post );
      if ( ! $post ) {
          return false;
      }
      if ( 'publish' !== $post->post_status ) {
          return false;
      }
      $canonical_url = get_permalink( $post );
      // If a canonical is being generated for the current page, make sure it has pagination if needed.
      if ( $post->ID === get_queried_object_id() ) {
          $page = get_query_var( 'page', 0 );
          if ( $page >= 2 ) {
              if ( '' == get_option( 'permalink_structure' ) ) {
                  $canonical_url = add_query_arg( 'page', $page, $canonical_url );
              } else {
                  $canonical_url = trailingslashit( $canonical_url ) . user_trailingslashit( $page, 'single_paged' );
              }
          }
          $cpage = get_query_var( 'cpage', 0 );
          if ( $cpage ) {
              $canonical_url = get_comments_pagenum_link( $cpage );
          }
      }

      return $canonical_url;
  }

  static function get_search_breadcrumbs() {
      //breadcrumbs array
      $breadcrumbs_array = self::search_breadcrumbs_array();

      //breadcrumbs html
      return self::get_breadcrumbs($breadcrumbs_array);
  }

  private static function search_breadcrumbs_array() {
      $breadcrumbs_array [] = array (
          'title_attribute' => '',
          'url' => '',
          'display_name' =>  __('Busqueda')
      );
      return $breadcrumbs_array;
  }

}
