<?php

class enc_module_author extends enc_base_author {

    function __construct() {
        parent::__construct();
    }

    function render() {
        ob_start();
        ?>

        <div class="post-author author-box" itemprop="author" itemscope itemtype="http://schema.org/Person">
            <div class="author-img">
                <?php echo $this->get_author_icon($this->current_author->ID); ?>
                <h1 class="author-title" itemprop="name"><?php echo $this->current_author->display_name; ?></h1>
            </div>
            <div class="author-info">
                <div itemprop="description" class="post-author-description"><?php echo $this->description; ?></div>
                <ul class="author-social">
                    <?php if($this->current_author->user_url != '') { ?>
                        <li><a target="_blank" itemprop="url" title="Página Web" href="<?php echo $this->current_author->user_url; ?>"><i class="author-social-icon icon-web"></i></a></li>
                    <?php } ?>
                    <?php if($this->facebook != '') { ?>
                        <li><a target="_blank" title="Facebook" href="<?php echo $this->facebook; ?>"><i class="author-social-icon icon-facebook"></i></a></li>
                    <?php } ?>
                    <?php if($this->instagram != '') { ?>
                        <li><a target="_blank" title="Instagram" href="<?php echo $this->instagram; ?>"><i class="author-social-icon icon-instagram"></i></a></li>
                    <?php } ?>
                    <?php if($this->linkedin != '') { ?>
                        <li><a target="_blank" title="Linkedin" href="<?php echo $this->linkedin; ?>"><i class="author-social-icon icon-linkedin"></i></a></li>
                    <?php } ?>
                    <?php if($this->twitter != '') { ?>
                        <li><a target="_blank" title="Twitter" href="http://www.twitter.com/<?php echo $this->twitter; ?>"><i class="author-social-icon icon-twitter"></i></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }


}
