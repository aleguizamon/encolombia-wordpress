<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-14
 * Time: 12:27
 */

class enc_module_4 extends enc_module {
    function __construct($post) {
        //run the parrent constructor
        parent::__construct($post);
    }
    
    
    function render($cols = 2) {
        $class = false;
        $thumbsize = 'enc_337x204';
        if (enc_util::is_folletos_genfar_list()){
            //$thumbsize = 'enc_370x550';
            $class = true;
        }

        ob_start();
        echo '<div class="col-lg-3 col-md-4 col-sm-6">';
        echo '<div class="enc_module_4 '.($class == true ? 'genfar' : ($class == false ? '' : '')).' enc_module_wrap">'
        ?> 
        
            <?php
                echo $this->get_image($thumbsize);
                echo $this->get_title();
            ?>
            <div class="enc-excerpt">
                <?php echo $this->get_excerpt(130); ?>
            </div>
        </div>
        <?php
        echo '</div>';
        return ob_get_clean();
    }
}
