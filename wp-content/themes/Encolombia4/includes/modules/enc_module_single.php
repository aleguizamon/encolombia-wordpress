<?php

/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-14
 * Time: 18:58
 */
class enc_module_single extends enc_module
{

    public $is_single;

    /**
     * enc_module_single constructor.
     */
    public function __construct($post)
    {
        //run the parent constructor
        parent::__construct($post);

        $this->is_single = is_single();
    }


    /**
     * Gets the article title on single pages and on modules that use this class as a base (module15 on Newsmag for example).
     * @param string $cut_at - not used, it's added to maintain strict standards
     * @return string
     */
    function get_title($cut_at = '')
    {
        $buffy = '';
        if (!empty($this->title)) {
            $buffy .= '<h1 class="entry-title">';
            if ($this->is_single === true) {
                $buffy .= $this->title;
            } else {
                $buffy .= '<a href="' . $this->href . '"  title="' . $this->title_attribute . '">';
                $buffy .= $this->title;
                $buffy .= '</a>';
            }
            $buffy .= '</h1>';
        }

        return $buffy;
    }

    /**
     * the content of a single post or single post type
     * @return mixed|string|void
     */
    public function get_content()
    {
        global $page, $numpages, $multipage;
        $content = get_the_content(__('Ver más', enc_params::$translate_domain));

        if ($multipage == 1 && $numpages == $page && strpos($content, '</div></div></div></div>') !== false) {
            $content = str_replace('</div></div></div></div>', '', $content);
        }

        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);

        return $content;
    }

    public function is_publication()
    {
        /*global $publications;
        $lVal = false;
        $current_cat = get_the_category($this->post->ID);
        foreach ($current_cat as $key => $value) {
          if(in_array($value->cat_ID,$publications)){
            $lVal= true;
          }
        }
        return $lVal;*/
        $current_cat = $this->get_categories();
        //var_dump($current_cat);
        $is_publication = '0';
        if ($current_cat && isset($current_cat[0]) && !is_wp_error($current_cat[0])) {
            $is_publication = get_option('enc_is_pub-' . $current_cat[0]->cat_ID, '0');
        }
        return $is_publication == '1';
    }

    /**
     * gets the related posts ONLY on single posts. Does not run on custom post types because we don't know what taxonomy to choose and the
     * blocks do not support custom taxonomies as of 15 july 2015
     * @param string $force_sidebar_position - allows us to overwrite the sidebar position. Useful on templates where the related
     *                              articles appear outside of the loop - sidebar grid (like on the top)
     * @return string
     */
    function related_posts($force_sidebar_position = '')
    {
        global $post;
        if ($post->post_type != 'post') {
            return '';
        }

        $td_related_ajax_filter_type = 'cur_post_same_categories';

        // the number of rows to show. this number will be multiplied with the hardcoded limit
        $tds_similar_articles_rows = 2;

        $td_related_limit = 3 * $tds_similar_articles_rows;
        $td_related_class = '';
        $td_column_number = 3;

        $td_block_args = array(
            'limit' => $td_related_limit,
            'ajax_pagination' => 'next_prev',
            'live_filter' => $td_related_ajax_filter_type,  //live atts - this is the default setting for this block
            'td_ajax_filter_type' => 'td_custom_related', //this filter type can overwrite the live filter @see
            'class' => $td_related_class,
            'td_column_number' => $td_column_number,
            'category__in' => wp_get_post_categories($post->ID),
            'post__not_in' => $post->ID
        );

        return enc_block_related_posts::get_instance()->render($td_block_args, 3);

    }

    public function related_publications()
    {
        return enc_block_related_categories::get_instance()->render();
    }

    function get_post_pagination()
    {
        if (!$this->is_single) {
            return '';
        }
        return wp_link_pages(array(
            'before' => '<div class="page-nav page-nav-post td-pb-padding-side">',
            'after' => '</div>',
            'link_before' => '<div>',
            'link_after' => '</div>',
            'echo' => false,
            'nextpagelink' => '<i class="td-icon-menu-right"></i>',
            'previouspagelink' => '<i class="td-icon-menu-left"></i>'
        ));
    }

    public function render_structured_data()
    {
        $image_data = '';
        if ($this->post_thumb_id) {
            $image_data = wp_get_attachment_image_src($this->post_thumb_id);
        }

        $output = '
            <span class="page-meta" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                <meta itemprop="name" content="Encolombia">
            </span>
            <meta itemprop="datePublished" content="' . $this->post->post_date . '">
            <meta itemprop="dateModified" content="' . $this->post->post_modified . '">
            <meta itemscope="" itemprop="mainEntityOfPage" itemtype="https://schema.org/WebPage" itemid="' . $this->href . '">
            <span class="page-meta" itemprop="publisher" itemscope="" itemtype="https://schema.org/Organization">
                <span class="page-meta" itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
                    <meta itemprop="url" content="https://encolombia.com/wp-content/uploads/2017/06/logo-encolombia-nuevo.png">
                </span>
                <meta itemprop="name" content="encolombia.com">
            </span>';
        $output .= '<meta itemprop="headline " content="' . $this->post->post_title . '">';
        if (!empty($image_data)) {
            $output .= '<span class="page-meta" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
                <meta itemprop="url" content="' . $image_data[0] . '">
                <meta itemprop="width" content="696">
                <meta itemprop="height" content="411">
            </span>';
        }

        return $output;
    }

    public function list_other_posts_in_category()
    {
        $related_posts = array();

        // Obtenemos la categoría del post de entrada
        $categories = $this->get_categories();
        if ($categories) {
            $category_ids = array();
            foreach ($categories as $category) {
                $category_ids[] = $category->term_id;
            }

            // Consultamos los posts que pertenecen a la categoría
            $args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'cat' => $category_ids[0],//implode(',', $category_ids),
                'posts_per_page' => -1,
                'order' => 'DESC',
                'orderby' => 'date'
            );

            $query = new WP_Query($args);

            // Creamos el array con la información de cada post
            if ($query->have_posts()) {
                $ind = 1;
                while ($query->have_posts()) {
                    $query->the_post();
                    $related_posts[] = array(
                        'ID' => get_the_ID(),
                        'url' => get_the_permalink(),
                        'label' => get_the_title(),
                        'active' => get_the_ID() == $this->post->ID,
                        'position' => $ind
                    );
                    $ind++;
                }
            }

            wp_reset_postdata();
        }

        /*usort($related_posts, function($a, $b) {
            return $b['active'] > $a['active'];
        });*/

        return $related_posts;
    }

    public function render_other_posts_in_category()
    {
        $cats = $this->get_categories();
        $cat_name = isset($cats[0]) ? $cats[0]->name : '';
        $html = sprintf('<div class="nav-title">%s</div>', $cat_name);
        $html .= '<ol>';
        foreach ($this->list_other_posts_in_category() as $item) {
            $html .= sprintf('<li class="%s" data-position="%d" data-id="%d"><a href="%s">%s</a></li>', $item['active'] ? 'item active' : 'item', $item['position'], $item['ID'], $item['url'], $item['label']);
        }
        $html .= '</ol>';
        return $html;
    }

    public function render_more_cat_btn_from_html($content)
    {
        $doc = new DOMDocument();
        $doc->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
        $xpath = new DOMXPath($doc);
        $categoryLink = $xpath->query("//ul[@class='enc-category']/li[1]/a")->item(0);
        $html = '';

        if ($categoryLink) {
            $categoryUrl = $categoryLink->getAttribute('href');
            $categoryName = $categoryLink->textContent;
            $html = '<div class="btn-more-cat"><a href="' . $categoryUrl . '">Más de "' . $categoryName . '" Aquí</a></div>';
        }

        return $html;
    }
}
