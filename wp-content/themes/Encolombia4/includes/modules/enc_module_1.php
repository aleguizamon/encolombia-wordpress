<?php

class enc_module_1 extends enc_module {
    function __construct($post) {
        //run the parrent constructor
        parent::__construct($post);
    }
    
    
    function render() {
        $thumbsize = 'enc_370x218';

        ob_start();
        echo '<div class="enc_module_1">';
        echo $this->get_image($thumbsize);
        echo '<div class="entry-content">';
        echo $this->get_title();
        echo '</div>';
        echo '</div>';
        return ob_get_clean();
    }
}
