<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-14
 * Time: 12:27
 */
class enc_module_search extends enc_module
{
    function __construct($post)
    {
        //run the parrent constructor
        parent::__construct($post);
    }

    function render()
    {
        ob_start();
        ?>
        <div class="enc_module_4 enc_module_wrap">
            <?php echo $this->get_title(); ?>
            <small class="enc-link-post"><a
                    href="<?php echo get_permalink(); ?>"><?php echo get_permalink(); ?></a></small>
            <div class="enc-excerpt">
                <?php echo $this->get_excerpt(enc_params::$excerpt_length * 2); ?>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }
}
