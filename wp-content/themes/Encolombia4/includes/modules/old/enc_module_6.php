<?php
class enc_module_6 extends enc_module
{
    private $position;

    function __construct($post, $position = null)
    {
        $this->position = $position;
        //run the parrent constructor
        parent::__construct($post);
    }

    function render()
    {
        ob_start();
        ?>

        <div class="enc_module_6">
            <?php
            if (wp_is_mobile()) {
                if (!is_null($this->position)) echo '<div class="item-position">' . $this->position . '</div>';
            } else {
                echo $this->get_image('enc_100x70');
            }
            ?>
            <?php ?>
            <?php echo $this->get_title(); ?>
        </div>

        <?php return ob_get_clean();
    }
}