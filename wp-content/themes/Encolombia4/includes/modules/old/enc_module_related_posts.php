<?php
class enc_module_related_posts extends enc_module
{

    function __construct($post)
    {
        //run the parrent constructor
        parent::__construct($post);
    }

    function render()
    {
        ob_start();
        ?>

        <div class="enc_module_related_posts enc_module_wrap">
            <?php echo $this->get_image('enc_218x150'); ?>
            <?php echo $this->get_title(); ?>
        </div>
        <?php return ob_get_clean();
    }
}
