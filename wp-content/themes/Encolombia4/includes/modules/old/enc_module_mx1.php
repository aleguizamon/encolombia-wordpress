<?php
class enc_module_mx1 extends enc_module
{

    public function __construct($post)
    {
        //run the parrent constructor
        parent::__construct($post);
    }

    public function render($post_count)
    {
        ob_start();
        ?>
        <div class="enc_module_mx5">
            <?php
            echo $this->get_image('enc_582x399');
            echo $this->get_title();
            ?>
        </div>

        <?php return ob_get_clean();
    }
}

?>
