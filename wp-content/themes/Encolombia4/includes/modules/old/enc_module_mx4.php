<?php
class enc_module_mx4 extends enc_module
{

    public function __construct($post)
    {
        //run the parrent constructor
        parent::__construct($post);
    }

    public function render($post_count)
    {
        ob_start();
        ?>
        <div class="enc_module_mx6 enc-big-grid-post-<?php echo $post_count ?> enc-big-grid-post">
            <?php
            echo $this->get_image('enc_290x198');
            echo $this->get_title();
            ?>
        </div>

        <?php return ob_get_clean();
    }
}

?>
