<?php
class enc_module_10 extends enc_module
{

    function __construct($post)
    {
        //run the parrent constructor
        parent::__construct($post);
    }

    function render()
    {
        ob_start();
        ?>
        <div class="enc_module_10 enc_module_wrap">
            <?php echo $this->get_image('enc_218x150'); ?>
            <div class="item-details">
                <?php echo $this->get_title(); ?>
                <div class="enc-excerpt">
                    <?php echo $this->get_excerpt(wp_is_mobile() ? 60 : 200); ?>
                </div>
            </div>
        </div>
        <?php return ob_get_clean();
    }

}