<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-14
 * Time: 12:38
 */


abstract class enc_module
{
    public $post;
    public $title_attribute;
    public $title;
    public $href;
    public $categories = null;

    protected $post_thumb_id = NULL;

    /**
     * @param $post WP_Post
     * @throws ErrorException
     */
    function __construct($post) {
        if (gettype($post) != 'object' or get_class($post) != 'WP_Post') {
            enc_util::error(__FILE__, 'enc_module: ' . get_Class($this) . '($post): $post is not WP_Post');
        }

        $this->post = $post;

        // by default the WordPress title is not escaped on twenty fifteen
        $this->title = get_the_title($post->ID);
        $this->title_attribute = esc_attr(strip_tags($this->title));
        $this->href = esc_url(get_permalink($post->ID));

        if (has_post_thumbnail($this->post->ID)) {
            $tmp_get_post_thumbnail_id = get_post_thumbnail_id($this->post->ID);
            if (!empty($tmp_get_post_thumbnail_id)) {
                // if we have a wrong id, leave the post_thumb_id NULL
                $this->post_thumb_id = $tmp_get_post_thumbnail_id;
            }
        }
    }

    /**
     * This function returns the title with the appropriate markup.
     * @return string
     */

    function get_title() {
        $buffy = '';
        $buffy .= '<h4 class="entry-title enc-module-title">';
        $buffy .='<a href="' . $this->href . '"  title="' . $this->title_attribute . '">';
        $buffy .= $this->title;
        $buffy .='</a>';
        $buffy .= '</h4>';
        return $buffy;
    }

    /**
     * This method is used by modules to get content that has to be excerpted (cut)
     * IT RETURNS THE EXCERPT FROM THE POST IF IT'S ENTERED IN THE EXCERPT CUSTOM POST FIELD BY THE USER
     * @param string $cut_at - if provided the method will just cat at that point
     * @return string
     */
    function get_excerpt($cut_at = '') {
        if ($this->post->post_excerpt != '') {
            return $this->post->post_excerpt;
        }

        $buffy = '';
        if ($cut_at != '') {
            // simple, $cut_at and return
            $buffy .= enc_util::excerpt($this->post->post_content, $cut_at);
        } else {
            $buffy .= $this->post->post_content;
        }
        return $buffy;
    }

    /**
     * get image - v 3.0  23 ian 2015
     * @param $thumbType
     * @return string
     */
    function get_image($thumbType) {
        $buffy = '';
        $srcset_sizes = '';
        if (!is_null($this->post_thumb_id)) {
            if (!is_null($this->post_thumb_id)) {
                $td_temp_image_url = wp_get_attachment_image_src($this->post_thumb_id, $thumbType);

                $attachment_alt = get_post_meta($this->post_thumb_id, '_wp_attachment_image_alt', true );
                if(empty($attachment_alt)) {
                    $attachment_alt = $this->title;
                }
                $attachment_alt = 'alt="' . esc_attr(strip_tags($attachment_alt)) . '"';
                $attachment_title = ' title="' . esc_attr(strip_tags($this->title)) . '"';

                if (empty($td_temp_image_url[0])) {
                    $td_temp_image_url[0] = '';
                }

                if (empty($td_temp_image_url[1])) {
                    $td_temp_image_url[1] = '';
                }

                if (empty($td_temp_image_url[2])) {
                    $td_temp_image_url[2] = '';
                }

                $srcset_sizes = enc_thumbs::get_srcset_sizes($thumbType, $td_temp_image_url[1], $td_temp_image_url[0], $this->post_thumb_id);
                //if(is_user_logged_in()){ var_dump($srcset_sizes);  var_dump($thumbType); var_dump($td_temp_image_url[1]); var_dump($td_temp_image_url[0]);}
            } else {
                $td_temp_image_url = array();
                if (strpos($thumbType, 'td_') === 0) {
                    $parts = explode('td_', $thumbType);
                    $parts = explode('x', $parts[1]);
                    $td_temp_image_url[1] = $parts[0];
                    $td_temp_image_url[2] = (int)$parts[1] == 0 ? 'auto' : $parts[1];
                    $td_temp_image_url[0] = enc_params::$template_directory_uri . '/assets/images/no-thumb/' . $thumbType . '.png';
                } else {
                    $td_temp_image_url[1] = '';
                    $td_temp_image_url[2] = '';
                    $td_temp_image_url[0] = enc_params::$template_directory_uri . '/assets/images/no-thumb/thumbnail.png';
                }
                $attachment_alt = 'alt=""';
                $attachment_title = '';
            }


            if(isset($td_temp_image_url[0]) && !empty($td_temp_image_url[0])){
                //$buffy .= '<div class="enc-module-thumb enc-post-featured-image" '. (!is_front_page() && is_single() ? ' style="margin-bottom:21px;'. (wp_is_mobile() ? 'height: 205px;' : '') .'"' : '') .'>';
                $buffy .= '<div class="enc-module-thumb enc-post-featured-image" >';
                if (current_user_can('edit_posts')) {
                    $buffy .= '<a class="enc-admin-edit" href="' . get_edit_post_link($this->post->ID) . '">edit</a>';
                }

                $buffy .= '<a href="' . $this->href . '"  title="' . $this->title_attribute . '">';
                $buffy .= '<img width="' . $td_temp_image_url[1] . '" height="' . $td_temp_image_url[2] . '" class="entry-thumb" src="' . $td_temp_image_url[0] . '"' . $srcset_sizes . ' ' . $attachment_alt . $attachment_title . '/>';
                $buffy .= '</a>';
                $buffy .= '</div>'; //end wrapper

                //if (ENC_DEV_MODE) {
                    $buffy = str_replace(ENC_DEV_ROUTE, enc_params::$prod_route, $buffy);
                //}
            }

            return $buffy;
        }
    }

    /**
     * @return null
     */
    public function get_categories()
    {
        return !empty($this->categories) ? $this->categories : get_the_category( $this->post->ID );
    }

    /**
     * get the category spot of the single post / single post type
     * @return string - the html of the spot
     */
    function get_category()
    {
        $terms_ui_array  = array();
        $categories = $this->get_categories();
        if (!empty($categories)) {
            foreach ( $categories as $category ) {
                //get the parent of this cat
                /*$td_parent_cat_obj = get_category( $category->category_parent );

                //if we have a parent and the default category display is disabled show it first
                if ( ! empty( $td_parent_cat_obj->name ) ) {
                    $terms_ui_array[ $td_parent_cat_obj->name ] = array(
                        'link'         => get_category_link( $td_parent_cat_obj->cat_ID ),
                        'hide_on_post' => 'show'
                    );
                }*/

                //show the category, only if we didn't already showed the parent
                $terms_ui_array[ $category->name ]  = array(
                    'link'         => get_category_link( $category->cat_ID ),
                    'hide_on_post' => 'show'
                );
            }
        }

        $buffy = '';
        $buffy .= '<ul class="enc-category">';

        foreach ( $terms_ui_array as $term_name => $term_params ) {
            $buffy .= '<li class="entry-category"><a href="' . $term_params['link'] . '">' . $term_name . '</a></li>';
        }
        $buffy .= '</ul>';

        return $buffy;
    }


}