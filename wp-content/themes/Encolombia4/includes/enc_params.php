<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-12
 * Time: 19:00
 */

class enc_params{
    //public static $dev_mode = true;
    //public static $dev_route = 'http://local.dev.encolombia.com';
    public static $prod_route = 'https://encolombia.com/';
    public static $foundation_year = '1998';
    public static $site_name = 'encolombia.com';
    public static $logo_url = 'https://encolombia.com/wp-content/uploads/2021/02/logo-encolombia-4.png';
    public static $logo_retina_url = 'https://encolombia.com/wp-content/uploads/2021/02/logo-encolombia-4-retina.png';
    public static $logo_url_m = 'https://encolombia.com/wp-content/uploads/2021/03/logo-encolombia-4m.png';
    public static $logo_retina_url_m = 'https://encolombia.com/wp-content/uploads/2021/03/logo-encolombia-4m-retina.png';
    public static $logo_url_footer = 'https://encolombia.com/wp-content/uploads/2017/06/logo-encolombia-nuevo.png';
    public static $logo_retina_url_footer = 'https://encolombia.com/wp-content/uploads/2017/06/logo-encolombia-nuevo-retina.png';
    public static $translate_domain = 'ENCOL_THEME';
    public static $fav_icon = 'https://encolombia.com/wp-content/uploads/2017/06/logo-encolombia-nuevo-favicon.png';
    public static $icon_ios_76 = 'https://encolombia.com/wp-content/uploads/2018/08/logo-encolombia-ios-76.png';
    public static $icon_ios_120 = 'https://encolombia.com/wp-content/uploads/2018/08/logo-encolombia-ios-120.png';
    public static $icon_ios_152 = 'https://encolombia.com/wp-content/uploads/2018/08/logo-encolombia-ios-152.png';
    public static $icon_ios_114 = 'https://encolombia.com/wp-content/uploads/2018/08/logo-encolombia-ios-114.png';
    public static $icon_ios_144 = 'https://encolombia.com/wp-content/uploads/2018/08/logo-encolombia-ios-144.png';
    public static $enable_or_disable_page_comments = 'hide_comments';//'show_comments';
    public static $social_facebook_link = 'https://www.facebook.com/people/encolombiacom/100060538192215/';
    public static $social_pinterest_link = 'https://co.pinterest.com/encolombiacom/boards/';
    public static $social_twitter_link = 'https://twitter.com/encolombia1';
    public static $social_youtube_link = 'https://www.youtube.com/encolombia1';
    public static $template_directory_uri = '';
    public static $excerpt_length = 90;
    public static $posts_per_page = 10;
    public static $enc_avatar = 'https://encolombia.com/wp-content/uploads/2017/06/logo-encolombia-nuevo-favicon.png';
    public static $ads_units_category = array(
        'enc_inline1',
        'enc_inline2',
        'enc_inline3',
        'enc_inline4',
        'enc_inline5',
        'enc_inline6',
    );
    public static $ads_units_category_mob = array(
        'enc_mob_inline1',
        'enc_mob_inline2',
        'enc_mob_inline3',
        'enc_mob_inline4',
        'enc_mob_inline5',
        'enc_mob_inline6',
        'enc_mob_inline7',
        'enc_mob_inline8',
    );
    public static $ads_units_category_sponsored = array(
        'enc_sponsoredcategory_inline_pos1',
        'enc_sponsoredcategory_inline_pos2',
        'enc_sponsoredcategory_rr_pos1',
        'enc_sponsoredcategory_rr_pos2',
        'enc_sponsoredcategory_rr_pos3',
    );
}
