<?php
class enc_options_page{

    private static $instance;

    /**
     * enc_popup_magazine_genfar constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_options_page();
        }
        return self::$instance;
    }

    public function install(){
        
        if( function_exists('acf_add_options_page') ) {
	
            acf_add_options_page(array(
                'page_title' => __('Opciones Encolombia', enc_params::$translate_domain),
                'menu_title' => __('Opciones Encolombia', enc_params::$translate_domain),
                'menu_slug' => 'opciones-encolombia',
            ));
	
        }
        
    }
}
