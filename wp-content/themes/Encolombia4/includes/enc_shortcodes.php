<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-16
 * Time: 08:40
 */

class enc_shortcodes{
    private static $instance;

    /**
     * enc_config constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_shortcodes();
        }

        return self::$instance;
    }

    public function install(){
        $this->includes();

        add_filter( 'widget_text', 'shortcode_unautop');
        add_filter( 'widget_text', 'do_shortcode');

        $category_sections = new enc_block_category_sections();
        add_shortcode( 'category-sections', array($category_sections, 'render') );

        $enc_block_11 = new enc_block_11();
        add_shortcode( 'enc_block_11', array($enc_block_11, 'render') );

        $enc_big_grid_1 = new enc_block_big_grid_1();
        add_shortcode( 'enc_block_big_grid_1', array($enc_big_grid_1, 'render') );

        $enc_block_7 = new enc_block_7();
        add_shortcode( 'enc_block_7', array($enc_block_7, 'render') );

        add_shortcode( 'enc_site_networks', array(enc_social::get_instance(), 'site_networks') );

        add_shortcode( 'enc_vc_button', array(new enc_wpbakery(), 'render_button') );
        add_shortcode( 'enc_vc_wp_search', array(new enc_wpbakery(), 'render_search') );

        add_shortcode( 'enc_su_button', array(new enc_su_button(), 'render') );
        add_shortcode( 'enc_su_spacer', array(new enc_su_spacer(), 'render') );
        add_shortcode( 'enc_su_row', array(new enc_su_row(), 'render') );
        add_shortcode( 'enc_su_column', array(new enc_su_column(), 'render') );
        add_shortcode( 'enc_su_spoiler', array(new enc_su_spoiler(), 'render') );
        add_shortcode( 'enc_su_note', array(new enc_su_note(), 'render') );

        add_shortcode( 'enc_is_mobile', array(new enc_is_mobile(), 'render') );
        add_shortcode( 'enc_is_desktop', array(new enc_is_desktop(), 'render') );
        add_shortcode( 'enc_is_url', array(new enc_is_url(), 'render') );
        add_shortcode( 'enc_is_test_sortable', array(new enc_is_url(), 'render_sortable') );
        add_shortcode( 'enc_not_test_sortable', array(new enc_is_url(), 'render_not_sortable') );
        add_shortcode( 'enc_is_customer_section', array(new enc_is_url(), 'render_not_is_customer_section') );
        add_shortcode( 'render_not_is_test_page', array(new enc_is_url(), 'render_not_is_test_page') );
        add_shortcode( 'render_is_test_page', array(new enc_is_url(), 'render_is_test_page') );
        add_shortcode( 'can_rham', array(new enc_can_render_admanager(), 'render') );
    }

    private function includes(){
        /*
        require_once(get_template_directory() . '/includes/shortcodes/enc_block_publications_sidebar.php');




        */
        require_once(get_template_directory() . '/includes/blocks/enc_block_7.php');
        require_once(get_template_directory() . '/includes/blocks/enc_block_category_sections.php');
        require_once(get_template_directory() . '/includes/blocks/enc_block_11.php');
        require_once(get_template_directory() . '/includes/blocks/enc_block_big_grid_1.php');
        require_once(get_template_directory() . '/includes/shortcodes/enc_wpbakery.php');
        require_once(get_template_directory() . '/includes/shortcodes/su/enc_su_button.php');
        require_once(get_template_directory() . '/includes/shortcodes/su/enc_su_spacer.php');
        require_once(get_template_directory() . '/includes/shortcodes/su/enc_su_row.php');
        require_once(get_template_directory() . '/includes/shortcodes/su/enc_su_column.php');
        require_once(get_template_directory() . '/includes/shortcodes/su/enc_su_spoiler.php');
        require_once(get_template_directory() . '/includes/shortcodes/su/enc_su_note.php');
        require_once(get_template_directory() . '/includes/shortcodes/enc_is_mobile.php');
        require_once(get_template_directory() . '/includes/shortcodes/enc_is_desktop.php');
        require_once(get_template_directory() . '/includes/shortcodes/enc_is_url.php');
        require_once(get_template_directory() . '/includes/shortcodes/enc_can_render_admanager.php');
    }
}
