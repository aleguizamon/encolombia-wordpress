<?php
class enc_ajax{

    private static $instance;

    /**
     * enc_ajax constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_ajax();
        }

        return self::$instance;
    }

    public function configure(){
        add_action('wp_ajax_getNextPost', array( $this,'ajax_get_next_post') );
        add_action('wp_ajax_nopriv_getNextPost', array( $this,'ajax_get_next_post') );
        add_action('wp_ajax_load_more_posts', array( $this,'load_more_posts'));
        add_action('wp_ajax_nopriv_load_more_posts', array( $this,'load_more_posts'));
    }

    public function ajax_get_next_post(){
    /* 
    $response = array(
            'data' => array(
                'next_content' => '',
                'next_url' => '',
                'next_title' => '',
                'current_id' => 0
            ),
            'success' => true
        );
        $position = isset($_POST['position']) ? $_POST['position'] : 1;
        $cat_id = isset($_POST['cat_id']) ? $_POST['cat_id'] : 0;


        //$current_post = isset($_POST['current_post']) ? $_POST['current_post'] : 0;
        //$current_post_obj = get_post($current_post);

        // Get the next post from the same category, ordered by date
        $related_posts = get_posts(array(
            'numberposts'      => $position,
            'category' => $cat_id,
            'orderby' => 'date',
            'order' => 'DESC',
            //'exclude' => $current_post
        ));

        if ($related_posts && isset($related_posts[$position-1])) {
            $next_post = $related_posts[$position-1];
            $next_url = get_permalink($next_post->ID);
            //$next_content = get_post_field('post_content', $next_post->ID, 'display');
            $next_content = get_the_content(__('Ver más', enc_params::$translate_domain), false, $next_post->ID);
            $next_content = apply_filters('the_content', $next_content);
            $next_content = str_replace(']]>', ']]&gt;', $next_content);

            global $withcomments, $post;
            $withcomments = true;
            $post = $next_post;

            ob_start();
            include get_template_directory() . '/parts/ajax-post-content.php';

            // Obtén el título meta configurado en Yoast SEO
            $yoast_title = get_post_meta($next_post->ID, '_yoast_wpseo_title', true);
            // Procesa el título meta con las variables de Yoast SEO
            $processed_title = wpseo_replace_vars($yoast_title, get_post($next_post->ID));

            $response['data']['next_content'] = ob_get_clean();
            $response['data']['next_url'] = $next_url;
            $response['data']['current_id'] = $next_post->ID;
            $response['data']['next_title'] = $processed_title;
        }

        wp_send_json($response, 200);
	*/
    }

    public function load_more_posts() {
        $current_page = $_POST['current_page'];
        $category = $_POST['category'];

        $args = array(
            'post_type' => 'post',
            'paged' => $current_page,
            'posts_per_page' => 32,
            'post_status' => 'publish',
            'orderby' => 'date',
            'order' => 'DESC',
            'category_name' => $category,
        );

        $query = new WP_Query($args);

        if($query->have_posts()) {
            global $post;
            $html = '';

            while($query->have_posts()) {
                $query->the_post();

                $module = new enc_module_4($post);
                $html .= $module->render();
            }

            wp_reset_postdata();

            $response = array(
                'success' => true,
                'data' => array(
                    'html' => $html,
                    'can_load_more' => $query->max_num_pages > $current_page
                )
            );
        } else {
            $response = array(
                'success' => false
            );
        }

        wp_send_json($response);
    }



}
