<?php

//removing the comments sidewide
if (post_type_supports(get_post_type(), 'comments')) {

    if (post_password_required()) {
        return;
    }
    ?>

    <div class="comments" id="comments">
        <?php
        $commenter = wp_get_current_commenter();
        $req = get_option( 'require_name_email' );
        $aria_req = ( $req ? " aria-required='true'" : '' );

        $user = wp_get_current_user();
        $user_identity = $user->exists() ? $user->display_name : '';

        $fields = array(
            'author' =>
                '<div class="comment-form-input-wrap td-form-author">
			            <input class="" id="author" name="author" placeholder="' . __('Nombre:', enc_params::$translate_domain) . ( $req ? '*' : '' ) . '" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" ' . $aria_req . ' />
			            <div class="td-warning-author">' . __('Por favor ingrese su nombre aquí', enc_params::$translate_domain) . '</div>
			         </div>',

            'email'  =>
                '<div class="comment-form-input-wrap td-form-email">
			            <input class="" id="email" name="email" placeholder="' . __('Email:', enc_params::$translate_domain) . ( $req ? '*' : '' ) . '" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" ' . $aria_req . ' />
			            <div class="td-warning-email-error">' . __('¡Has ingresado una dirección de correo electrónico incorrecta!', enc_params::$translate_domain) . '</div>
			            <div class="td-warning-email">' . __('Por favor ingrese su dirección de correo electrónico aquí', enc_params::$translate_domain) . '</div>
			         </div>',

            'url' =>
                '<div class="comment-form-input-wrap td-form-url">
			            <input class="" id="url" name="url" placeholder="' . __('Website:', enc_params::$translate_domain) . '" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" />
                     </div>',
        );

        $defaults = array('fields' => apply_filters('comment_form_default_fields', $fields));
        $defaults['comment_field'] =
            '<div class="clearfix"></div>
				<div class="comment-form-input-wrap td-form-comment">
					<textarea placeholder="' . __('Comentario:', enc_params::$translate_domain) . '" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
					<div class="td-warning-comment">' . __('Por favor ingrese su comentario!', enc_params::$translate_domain) . '</div>
				</div>
		        ';

        $defaults['comment_notes_before'] = '';
        $defaults['comment_notes_after'] = '';
        $defaults['title_reply'] = __('DEJA UNA RESPUESTA', enc_params::$translate_domain);
        $defaults['label_submit'] = __('Publicar comentario', enc_params::$translate_domain);
        $defaults['cancel_reply_link'] = __('Cancelar respuesta', enc_params::$translate_domain);

        $post_id = get_the_ID();
        $url = wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) );

        $defaults['must_log_in'] = '<p class="must-log-in td-login-comment"><a class="td-login-modal-js" data-effect="mpf-td-login-effect" href="' . $url .'">' . __('Inicie sesión para dejar un comentario', enc_params::$translate_domain) . ' </a></p>';

        $defaults['logged_in_as'] = '<p class="logged-in-as">' . sprintf(
            /* 1: edit user link, 2: accessibility text, 3: user name, 4: logout URL */
                '<a href="%1$s" aria-label="%2$s">' . __('Logueado como', enc_params::$translate_domain) . ' %3$s</a>. <a href="%4$s">' . __('¿Cerrar sesión?', enc_params::$translate_domain) . '</a>',
                get_edit_user_link(),
                /* %s: user name */
                esc_attr( sprintf( __( 'Conectado como %s. Edite su perfil.' , enc_params::$translate_domain), $user_identity ) ),
                $user_identity,
                wp_logout_url( apply_filters( 'the_permalink', get_permalink( get_the_ID() ) ) )
            ) . '</p>';
        echo '<h2 id="title-form-comments"><a href="javascript:void();">'.__( 'CLIC AQUÍ Y DÉJANOS TU COMENTARIO' , enc_params::$translate_domain).'</a></h2>';
        echo '<div id="container-respond" >';
        comment_form($defaults);
        echo '</div>';

        ?>

        <?php
        if (have_comments()) {

            // on Newspaper the css class 'td-pb-padding-side' is not applied
            $td_css_cls_pb_padding_side = '';
            $td_css_cls_block_title = '';
            $global_block_template_id = 'td_block_template_1';
            $td_css_cls_block_title = 'td-block-title';

            if ($global_block_template_id === 'td_block_template_1') {
                $td_css_cls_block_title = 'block-title';
            }

            $num_comments = get_comments_number(); // get_comments_number returns only a numeric value
            if ($num_comments > 1) {
                $td_comments_no_text = $num_comments . ' ' . __('COMENTARIOS', enc_params::$translate_domain);
            } else {
                $td_comments_no_text = __('1 COMENTARIO', enc_params::$translate_domain);
            }
            ?>
            <div class="comments-list">
                <h4 class="comment-title <?php echo $td_css_cls_block_title?>">
                    <a href="javascript:void();" title="Ver/ocultar comentarios" ><span>VER </span><?php echo $td_comments_no_text?></a>
                </h4>
                <div class="comments-container">
                    <ol class="comment-list <?php echo $td_css_cls_pb_padding_side ?> " id="container-comment-list">
                        <?php wp_list_comments(array('callback' => 'td_comment')); ?>
                    </ol>
                    <div class="comment-pagination">
                        <?php previous_comments_link(); ?>
                        <?php next_comments_link(); ?>
                    </div>
                </div>
            </div>

        <?php }

        if (!comments_open() and (get_comments_number() > 0)) { ?>
            <p class="td-pb-padding-side"><?php __( 'Los comentarios están cerrados.', enc_params::$translate_domain ); ?></p>
        <?php }
        ?>
    </div> <!-- /.content -->

    <?php
}


//end removing the comments sidewide
/**
 * Custom callback for outputting comments
 *
 * @return void
 * @author tagdiv
 */
function td_comment( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;

    $td_isPingTrackbackClass = '';

    if($comment->comment_type == 'pingback') {
        $td_isPingTrackbackClass = 'pingback';
    }

    if($comment->comment_type == 'trackback') {
        $td_isPingTrackbackClass = 'trackback';
    }

    if (!empty($comment->comment_author_email)) {
        $td_comment_auth_email = $comment->comment_author_email;
    } else {
        $td_comment_auth_email = '';
    }

    $is_reply = $comment->user_id > 0 && $comment->comment_parent > 0 && $depth > 1;
    $td_article_date_unix = @strtotime("{$comment->comment_date_gmt} GMT");
    //print_r($td_article_date_unix);


    ?>
<li class="comment <?php echo $td_isPingTrackbackClass ?>" id="comment-<?php comment_ID() ?>">
    <article>
        <footer>
            <?php
            //echo get_template_directory_uri() . "/images/avatar.jpg";
            //echo get_avatar($td_comment_auth_email, 50, get_template_directory_uri() . "/images/avatar.jpg");
            if($is_reply){
                echo '<img src="'.enc_params::$enc_avatar.'" width="50" height="50" alt="Avatar" class="avatar photo avatar-default">';
            } else {
                echo get_avatar($td_comment_auth_email, 50);
            }
            ?>
            <cite><?php if($is_reply) echo "Encolombia"; else comment_author_link() ?></cite>

            <a class="comment-link" href="#comment-<?php comment_ID() ?>">
                <time pubdate="<?php echo $td_article_date_unix ?>"><?php comment_date() ?> at <?php comment_time() ?></time>
            </a>
        </footer>

        <div class="comment-content">
            <?php if ($comment->comment_approved == '0') { ?>
                <em><?php echo __('Tu comentario está esperando ser moderado', enc_params::$translate_domain); ?></em>
            <?php }
            comment_text(); ?>
        </div>

        <?php if(1==0): ?>
        <div class="comment-meta" id="comment-<?php comment_ID() ?>">
            <?php comment_reply_link(array_merge( $args, array(
                'depth' => $depth,
                'max_depth' => $args['max_depth'],
                'reply_text' => __('Respuesta', enc_params::$translate_domain),
                'login_text' =>  __('Inicia sesión para dejar un comentario', enc_params::$translate_domain)
            )))
            ?>
        </div>
        <?php endif; ?>
    </article>
    <?php

}
?>