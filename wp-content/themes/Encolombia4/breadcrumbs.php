<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 19/11/17
 * Time: 9:28 AM
 */

$bread = null;
if (is_category()) {
    $current_category_id = get_query_var('cat');
    $current_category_obj = get_category($current_category_id);
    $bread = enc_breadcrumbs_generator::get_category_breadcrumbs($current_category_obj);
}
else if(is_single()){
    global $post;
    $bread = enc_breadcrumbs_generator::get_single_breadcrumbs($post->post_title);
}
else if(is_search()){
    $bread = enc_breadcrumbs_generator::get_search_breadcrumbs();
}
if(!empty($bread)){
?>
<div class="enc-container">
    <?php echo $bread; ?>
</div>
<?php
}

?>
