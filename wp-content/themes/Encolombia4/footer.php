            <footer class="enc-footer">
                <div class="enc-footer-wrapper ">
                    <div class="enc-container ">
                        <div class="enc-row">
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="footer-logo-wrap">
                                    <a href="<?php echo esc_url(home_url( '/' )); ?>">
                                        <img src="<?php echo enc_params::$logo_url_footer; ?>" srcset="<?php echo enc_params::$logo_url_footer; ?>, <?php echo esc_attr(enc_params::$logo_retina_url_footer); ?> 2x" alt="ENCOLOMBIA" title="ENCOLOMBIA.COM" />
                                    </a>
                                </div>
                                <?php echo enc_social::get_instance()->site_networks('footer-social-container', true); ?>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <h3 class="footer-title"><?php echo __('Contáctanos', enc_params::$translate_domain); ?></h3>
                                <ul class="ec-menu-footer">
                                    <li><a href="https://encolombia.com/contacts/">Pauta con nosotros</a></li>
                                    <li><a href="https://encolombia.com/publique-sus-articulos/">Publicación de Artículos</a></li>
                                    <li><a href="https://encolombia.com/quienes-somos/">Quienes Somos</a></li>
                                </ul>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-cat-menu-1">
                                <h3 class="footer-title"><?php echo __("Categorías", enc_params::$translate_domain); ?></h3>
                                <?php
                                wp_nav_menu(array(
                                    'menu' => 'footer-categorias-1',
                                    'menu_class'=> 'ec-menu-footer',
                                    //'fallback_cb' => 'enc_wp_footer_menu'
                                ));
                                ?>

                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-cat-menu-2">
                                <?php
                                wp_nav_menu(array(
                                    'menu' => 'footer-categorias-2',
                                    'menu_class'=> 'ec-menu-footer',
                                    //'fallback_cb' => 'enc_wp_footer_menu'
                                ));
                                ?>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-cat-menu-3">
                                <?php
                                wp_nav_menu(array(
                                    'menu' => 'footer-categorias-3',
                                    'menu_class'=> 'ec-menu-footer',
                                    //'fallback_cb' => 'enc_wp_footer_menu'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="enc-sub-footer-container">
                    <div class="enc-container">
                        <div class="enc-row">
                            <div class="col-md-6 col-sm-6">
                                <div class="enc-sub-footer-copy">
                                    &copy; <?php echo enc_params::$foundation_year; ?> - <?php echo date('Y'); ?> <?php echo enc_params::$site_name; ?>. Todos los derechos reservados.
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'footer-menu',
                                    'menu_class'=> 'enc-subfooter-menu',
                                    'fallback_cb' => 'enc_wp_footer_menu'
                                ));

                                //if no menu
                                function enc_wp_footer_menu() {
                                    //do nothing?
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php wp_footer(); ?>
            </footer>
        </div><!--close td-outer-wrap-->
    </body>
</html>