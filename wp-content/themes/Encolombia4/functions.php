<?php
@ini_set( 'upload_max_size' , '1024M' );
@ini_set( 'post_max_size', '1024M');
@ini_set( 'max_execution_time', '2000' );

// cargar parámetros del tema
require_once ('includes/enc_dev_mode.php');
require_once('includes/enc_params.php');
require_once('includes/enc_breadcrumbs_generator.php');
require_once('includes/enc_yoast_breadcrumb_extend.php');
require_once('includes/enc_thumb_registry.php');
require_once('includes/enc_config.php');
require_once('includes/enc_thumbs.php');
require_once('includes/enc_util.php');
require_once('includes/enc_layout.php');
require_once('includes/enc_base_post.php');
require_once('includes/enc_base_author.php');
require_once('includes/enc_base_category.php');
require_once('includes/enc_external_scripts.php');
require_once('includes/enc_sidebars.php');
require_once('includes/enc_social.php');
require_once('includes/enc_performance.php');
require_once('includes/modules/enc_module.php');
require_once('includes/modules/enc_module_single.php');
require_once('includes/modules/enc_module_1.php');
require_once('includes/modules/enc_module_4.php');
require_once('includes/modules/enc_module_post_author.php');
require_once('includes/modules/enc_module_author.php');

require_once('includes/modules/old/enc_module_search.php');
require_once('includes/modules/old/enc_module_mx1.php');
require_once('includes/modules/old/enc_module_mx4.php');
require_once('includes/modules/old/enc_module_10.php');
require_once('includes/modules/old/enc_module_6.php');
require_once('includes/enc_admin_config.php');
require_once('includes/enc_extend_comments.php');
require_once('includes/enc_newsletter.php');
require_once('includes/enc_menu.php');
require_once('includes/enc_shortcodes.php');
require_once('includes/enc_data_source.php');
//require_once('includes/modules/enc_module_related_posts.php');
require_once('includes/blocks/enc_block_list_publications.php');
require_once('includes/blocks/enc_block_morethan_sidebar.php');
require_once('includes/blocks/enc_block_list_subcategories.php');
require_once('includes/blocks/enc_block_related_categories.php');
require_once('includes/blocks/enc_block_related_posts.php');
require_once('includes/enc_disable_author_pages.php');
require_once('includes/contact/enc_contact_config.php');
require_once('tests/enc_test_replace_advanced_ads.php');
require_once('includes/popup/enc_popup.php');
require_once('includes/popup/enc_popup_magazine_genfar.php');
require_once('includes/schemes/enc_scheme_interface.php');
require_once('includes/schemes/enc_scheme_factory.php');
require_once('includes/schemes/enc_scheme_recipe.php');
require_once('includes/enc_options_page.php');
require_once('includes/enc_ajax.php');


/** Configuraciones del front */
enc_config::get_instance()->configure();

/** Instalar miniaturas */
enc_thumbs::get_instance()->install();

/** Configure disable author pages */
//enc_disable_author_pages::get_instance()->configure();

/** Cargar scripts externos */
enc_external_scripts::get_instance()->render();

/** Registro todos los sidebars del tema */
enc_sidebars::get_instance()->register();

/** Optimizar wordpress */
enc_performance::get_instance()->optimize_wp();

/** Extender funcionalidad de los comentarios */
enc_extend_comments::get_instance()->extend();

/** Configurar el newsletter */
//enc_newsletter::get_instance()->configure();

/** Instala shortcodes */
enc_shortcodes::get_instance()->install();

/** Configuraciones del back */
enc_admin_config::get_instance()->configure();

/** Configuracion de formularios de contacto */
enc_contact_config::get_instance()->configure();

/** OPTIONS PAGE */
enc_options_page::get_instance()->install();

/** Personalizar el breadcrumb del Yoast */
enc_yoast_breadcrumb_extend::get_instance()->configure();

/** Llamadas ajax */
enc_ajax::get_instance()->configure();

/** SCHEMES */
$scheme = (new enc_scheme_factory())->get_scheme();
if($scheme){
    $scheme->install();
}
