<?php
class enc_test_replace_advanced_ads{
    private static $instance;

    private $data_head;

    /**
     * enc_config constructor.
     */
    private function __construct()
    {}

    static function get_instance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new enc_test_replace_advanced_ads();
        }

        return self::$instance;
    }

    public function run(){
        if($this->can_run_test()){
            $this->data_head = $this->get_head_ad_vars();
            add_action( 'dynamic_sidebar_before', array($this, 'render_tests_sidebars') );
            add_action('wp_head', array($this, 'render_ads_in_head'), 1);
            add_action( 'wp_footer', array($this, 'render_ads_in_footer') );
            add_filter( 'the_content', array($this, 'render_ads_in_content') );
        }

    }

    public function render_ads_in_head(){
        //echo $this->ad_lateral_2_head();
        //echo $this->ad_lateral_4_head();
        //adplus
        //echo $this->ad_in_article_2();
    }

    public function render_ads_in_footer(){
       //echo $this->ad_sulvo_sticky();

    }

    public function render_ads_in_content($content){
        if ( is_single() ) {
            //$content = $this->insert_ads_after_paragraph($this->ad_in_article_1(), 2, $content);
            //$content = $this->insert_ads_after_paragraph($this->ad_in_article_2(), 3, $content);
            //$content = $this->insert_ads_after_paragraph($this->ad_in_article_3(), 8, $content);
            //$content = $this->insert_ads_after_paragraph($this->ad_in_article_4(), 11, $content);
            //$content = $this->insert_ads_after_paragraph($this->ad_in_article_5(), 13, $content);
        }
        return $content;
    }

    public function render_tests_sidebars($index){
        switch ($index){
            case 'skyscraper-widget-area':
                //echo $this->ad_superior();
                break;
            case 'add-widget-area1':
                //echo $this->ad_lateral_1();
                break;
            case 'add-widget-area2':
                //echo $this->ad_lateral_2_body();
                break;
            case 'add-widget-area3':
                //echo $this->ad_lateral_3();
                //echo $this->ad_lateral_4_body();
                break;
            case 'contenido-final-posts':
                //echo $this->ad_contenido_final_post();
                break;
            case 'after-content-widget-area':
                //echo $this->ad_after_content_widget_area();
                break;
        }
    }

    public function can_run_test(){
        $enc = false;
        $lock_urls = array(
            '/educacion-cultura/educacion/temas-de-interes-educativo/crianza-positiva/'
        );
        foreach($lock_urls as $url){
            if(enc_util::is_exact_uri_section($url)){
                $enc = true;
            }
        }
        return !is_plugin_active('advanced-ads/advanced-ads.php') && $enc;
    }

    public function ad_sulvo_sticky(){
        if(wp_is_mobile()){
            return '<div data-ad="encolombia.com_sticky_mobile_bottom" data-devices="m:1,t:0,d:0" class="demand-supply"></div>';
        } else {
            return '<div data-ad="encolombia.com_728x90_sticky_display_bottom_sticky_desktop" data-devices="m:0,t:0,d:1" class="demand-supply" data-position="center" data-offset="-170px"></div>';
        }
    }

    public function ad_superior(){
        if(wp_is_mobile()){
            return '<div data-ad="encolombia.com_320x100_superiorovil" data-devices="m:1,t:1,d:0" class="demand-supply"></div>';
        } else {
            return '<div data-ad="encolombia.com_responsive_h_970x90_superior" data-devices="m:0,t:0,d:1" class="demand-supply"></div>';
        }
    }

    public function ad_lateral_1(){
        if(wp_is_mobile()){
            return '<div data-ad="encolombia.com_responsive_h_top_rightsidebar_mobile_300x250" data-devices="m:1,t:1,d:0" class="demand-supply"></div>';
        } else {
            return '<div data-ad="encolombia.com_responsive_hr_top_rightsidebar_mobile_desktop_300x250" data-devices="m:1,t:1,d:1" class="demand-supply"></div>';
        }
    }

    public function ad_lateral_2_head(){
        if(wp_is_mobile()){
            return '<script>
                        var googletag = googletag || {};
                        googletag.cmd = googletag.cmd || [];
                    </script>
                    <script>
                        googletag.cmd.push(function() {
                        googletag.defineSlot("/1020006/ENC_mobile_rectangle_BTF_6", [[250, 250], [320, 50], [300, 250], [320, 100], [1, 1]], "div-gpt-ad-1576515711602-0").addService(googletag.pubads());
                        googletag.pubads().setTargeting("type", ["'.$this->data_head['page_type'].'"]);
                        googletag.pubads().setTargeting("slug", ["'.$this->data_head['slug'].'"]);
                        googletag.pubads().setTargeting("articleID", ["'.$this->data_head['post_id'].'"]);
                        googletag.pubads().setTargeting("categoryID", ["'.$this->data_head['category_id'].'"]);
                        googletag.pubads().enableSingleRequest();
                        googletag.enableServices();
                    });
                    </script>';
        } else {
            return '<script>
                      var googletag = googletag || {};
                      googletag.cmd = googletag.cmd || [];
                    </script>
                    
                    <script>
                      googletag.cmd.push(function() {
                        googletag.defineSlot("/1020006/ENC_desktop_rightsidebar_BTF", [[300, 600], [1, 1], [120, 600], [300, 250], [160, 600]], "div-gpt-ad-1576512276966-0").addService(googletag.pubads());
                        googletag.pubads().setTargeting("type", ["'.$this->data_head['page_type'].'"]);
                        googletag.pubads().setTargeting("slug", ["'.$this->data_head['slug'].'"]);
                        googletag.pubads().setTargeting("articleID", ["'.$this->data_head['post_id'].'"]);
                        googletag.pubads().setTargeting("categoryID", ["'.$this->data_head['category_id'].'"]);
                        googletag.pubads().enableSingleRequest();
                        googletag.enableServices();
                      });
                    </script>
';
        }
    }

    public function ad_lateral_2_body(){
        if(wp_is_mobile()){
            return '<!-- /1020006/ENC_mobile_rectangle_BTF_6 -->
                    <div id="div-gpt-ad-1576515711602-0">
                      <script>
                        googletag.cmd.push(function() { googletag.display("div-gpt-ad-1576515711602-0"); });
                      </script>
                    </div>';
        } else {
            return '<!-- /1020006/ENC_desktop_rightsidebar_BTF -->
                    <div id="div-gpt-ad-1576512276966-0">
                      <script>
                        googletag.cmd.push(function() { googletag.display("div-gpt-ad-1576512276966-0"); });
                      </script>
                    </div>';
        }
    }

    public function ad_lateral_3(){
        if(wp_is_mobile()){
            return '<div data-ad="encolombia.com_responsive_r_300x250_mobile_righsidebar_2" data-devices="m:1,t:1,d:0" class="demand-supply"></div>';
        } else {
            return '<div data-ad="encolombia.com_responsive_vr_300x600_desktop_rightsidebar_2" data-devices="m:0,t:0,d:1" class="demand-supply"></div>';
        }
    }

    public function ad_lateral_4_head(){
        if(wp_is_mobile()){
            return '<script>
                      var googletag = googletag || {};
                      googletag.cmd = googletag.cmd || [];
                    </script>
                    
                    <script>
                      googletag.cmd.push(function() {
                        googletag.defineSlot("/1020006/ENC_mobile_rectangle_BTF_7", [[250, 250], [300, 250], [320, 100], [320, 50]], "div-gpt-ad-1541540020275-0").addService(googletag.pubads());
                        googletag.pubads().setTargeting("type", ["'.$this->data_head['page_type'].'"]);
                        googletag.pubads().setTargeting("slug", ["'.$this->data_head['slug'].'"]);
                        googletag.pubads().setTargeting("articleID", ["'.$this->data_head['post_id'].'"]);
                        googletag.pubads().setTargeting("categoryID", ["'.$this->data_head['category_id'].'"]);
                        googletag.pubads().enableSingleRequest();
                        googletag.enableServices();
                      });
                    </script>';
        } else {
            return '<script>
                      var googletag = googletag || {};
                      googletag.cmd = googletag.cmd || [];
                    </script>                    
                    <script>
                      googletag.cmd.push(function() {
                        googletag.defineSlot("/1020006/ENC_desktop_rightsidebar_BTF_2", [[120, 600], [300, 600], [160, 600], [300, 250]], "div-gpt-ad-1541539955883-0").addService(googletag.pubads());
                        googletag.pubads().setTargeting("type", ["'.$this->data_head['page_type'].'"]);
                        googletag.pubads().setTargeting("slug", ["'.$this->data_head['slug'].'"]);
                        googletag.pubads().setTargeting("articleID", ["'.$this->data_head['post_id'].'"]);
                        googletag.pubads().setTargeting("categoryID", ["'.$this->data_head['category_id'].'"]);
                        googletag.pubads().enableSingleRequest();
                        googletag.enableServices();
                      });
                    </script>';
        }
    }

    public function ad_lateral_4_body(){
        if(wp_is_mobile()){
            return '<!-- /1020006/ENC_mobile_rectangle_BTF_7 -->
                    <div id="div-gpt-ad-1541540020275-0">
                    <script>
                    googletag.cmd.push(function() { googletag.display("div-gpt-ad-1541540020275-0"); });
                    </script>
                    </div>';
        } else {
            return '<!-- /1020006/ENC_desktop_rightsidebar_BTF_2 -->
                    <div id="div-gpt-ad-1541539955883-0">
                    <script>
                    googletag.cmd.push(function() { googletag.display("div-gpt-ad-1541539955883-0"); });
                    </script>
                    </div>';
        }
    }

    public function ad_contenido_final_post(){
        return '<div id="SC_TBlock_507678" class="SC_TBlock">loading...</div>
                <script type="text/javascript">
                    (sc_adv_out = window.sc_adv_out || []).push({
                        id : "507678",
                        domain : "n.domnovrek.com"
                    });
                </script>
                <script type="text/javascript" src="//st-n.domnovrek.com/js/a.js"></script>';
    }

    public function ad_after_content_widget_area(){
        if(wp_is_mobile()){
            return '<div data-ad="encolombia.com_responsive_r_mobile_bottom_page_300x250" data-devices="m:1,t:1,d:0" class="demand-supply"></div>';
        } else {
            return '<div data-ad="encolombia.com_responsive_r_desktop_bottom_page_336x280" data-devices="m:0,t:0,d:1" class="demand-supply"></div>';
        }
    }

    public function ad_in_article_1(){
        if(wp_is_mobile()){
            return '<div data-ad="encolombia.com_responsive_hr_mobile_top_page_300x250" data-devices="m:1,t:1,d:0" class="demand-supply"></div>';
        } else {
            return '<div data-ad="encolombia.com_728x90_superior728" data-devices="m:0,t:1,d:1" class="demand-supply"></div>';
        }
    }

    public function ad_in_article_2(){
        return '<script async src="https://cdn.ad.plus/player/adplus.js"></script>
                <script>(function(){var i=\'yVAlUnEIyyGYoEl4EVnJk895-clQJwSvOsPZLrdB1v5X3ql4KheV\';document.write(\'<div id="\'+i+\'"></div>\');(playerPro=window.playerPro||[]).push(i);})();</script>';
    }

    public function ad_in_article_3(){
        if(wp_is_mobile()){
            return '<div data-ad="encolombia.com_responsive_vr_300x250_mobile_in_article_2" data-devices="m:1,t:1,d:0" class="demand-supply"></div>';
        } else {
            return '<div data-ad="encolombia.com_responsive_hr_728x90_in_article_2" data-devices="m:0,t:0,d:1" class="demand-supply"></div>';
        }
    }

    public function ad_in_article_4(){
        if(wp_is_mobile()){
            return '<div data-ad="encolombia.com_responsive_vr_movile_in_article3" data-devices="m:1,t:1,d:1" class="demand-supply"></div>';
        } else {
            return '<div data-ad="encolombia.com_728x90_inarticle3" data-devices="m:0,t:1,d:1" class="demand-supply"></div>';
        }
    }

    public function ad_in_article_5(){
        return '<script defer id="videoo-library" data-id="ac726df7c1c5558b7be3d141eb3a652838e6e3ed3364725a5014ddca248eac39" src="https://static.videoo.tv/ac726df7c1c5558b7be3d141eb3a652838e6e3ed3364725a5014ddca248eac39.js"></script>';
    }





    public function get_head_ad_vars(){
        $slug = $_SERVER['REQUEST_URI'];
        $page_type = is_single() ? 'post' : (is_category() ? 'category' : 'none');
        if(is_single() || is_page()):
            global $post;
            $post_id = $post->ID;
        else:
            $post_id = 0;
        endif;
        if(is_category()):
            $category_id = get_query_var('cat');
        else:
            $category_id = 0;
        endif;
        return array('page_type' => $page_type, 'slug' => $slug, 'post_id' => $post_id, 'category_id' => $category_id);
    }

    public function insert_ads_after_paragraph( $insertion, $paragraph_id, $content ) {
        $closing_p = '</p>';
        $paragraphs = explode( $closing_p, $content );

        foreach ($paragraphs as $index => $paragraph) {
            if ( trim( $paragraph ) ) {
                $paragraphs[$index] .= $closing_p;
            }

            if ( $paragraph_id == $index + 1 ) {
                $paragraphs[$index] .= $insertion;
            }
        }
        return implode( '', $paragraphs );
    }
}