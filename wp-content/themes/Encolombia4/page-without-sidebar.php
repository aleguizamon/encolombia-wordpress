<?php
/**
 * Template Name: Página sin sidebar
 */

get_header();
?>
    <div class="enc-main-content-wrap ">
        <div class="enc-container">
            <div class="enc-row">
                <div class="col-md-12 enc-main-content" role="main">
                        <?php
                        if (have_posts()) {
                            while ( have_posts() ) : the_post();
                                ?>
                                <div class="enc-page-header">
                                    <h1 class="entry-title enc-page-title">
                                        <span><?php the_title() ?></span>
                                    </h1>
                                </div>
                                <div class="enc-page-content">
                                    <?php the_content(); ?>
                                </div>
                            <?php   endwhile;//end loop

                        }
                        ?>

                    <?php
                    if(enc_params::$enable_or_disable_page_comments == 'show_commentsx') {
                        comments_template('', true);
                    }?>

                    <div style="margin-top: 1rem">
                    <?php
                    global $post;
                    $enc_social = enc_social::get_instance();
                    $title = $post->post_title;
                    $permalink = get_the_permalink($post->ID);
                    $thumbnail = get_the_post_thumbnail_url($post);
                    echo $enc_social->social_share_buttons($permalink, $thumbnail, $title);
                    ?>
                    </div>

                    <?php if ( is_active_sidebar( 'after-content-widget-area')  ): ?>
                        <div class="after-content-widget-area">
                            <?php enc_sidebars::get_instance()->render_sidebar('after-content-widget-area'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div> <!-- /.td-pb-row -->
        </div> <!-- /.td-container -->
    </div> <!-- /.td-main-content-wrap -->

<?php
get_footer();