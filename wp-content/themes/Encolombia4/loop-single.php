<?php
/**
 * The single post loop Default template
 **/


if (have_posts()) {
    the_post();
    global $post;
    $enc_social = enc_social::get_instance();

    $mod_single = new enc_module_single($post);
    $title = $post->post_title;
    $permalink = get_the_permalink($post->ID);
    $thumbnail = get_the_post_thumbnail_url($post);
    ?>
    
   <article id="post-<?php echo $mod_single->post->ID;?>" class="<?php echo join(' ', get_post_class());?>" itemscope itemtype="https://schema.org/Article">
        <?php
            if(enc_util::is_genfar_section()):
                echo $mod_single->get_category();
            endif;
        ?>

        <header class="enc-post-title">
            <?php echo $mod_single->get_title();?>
        </header>

        <div class="enc-post-content">
        <?php
        if(!enc_util::is_folletos_genfar_page()){
            $is_pub = $mod_single->is_publication();
            if( (!$is_pub && !enc_util::is_uri_section('/medicina/estudios-bioquivalencia/')) || ($is_pub && enc_util::is_url_force_featured_image()) ){
                if(wp_is_mobile()){
                    echo $mod_single->get_image('enc_330x205');
                } else {
                    echo $mod_single->get_image('enc_696x0');
                }
            }
        }
        ?>

        <?php
            $_content = $mod_single->get_content();
            $count_paragraphs = enc_util::count_paragraphs($_content);
            echo $_content;
        ?>

        </div>

       <?php //if ( is_active_sidebar( 'video-final-texto') && $count_paragraphs < 13 && !enc_util::is_customer_section() && !enc_util::is_uri_section('/vida-estilo/alimentacion/')): ?>
            <?php //dynamic_sidebar( 'video-final-texto' ); ?>
        <?php //endif; ?>

       <?php
       if(!wp_is_mobile()){
           enc_sidebars::get_instance()->render_sidebar('after-content-widget-area');

           locate_template('parts/post-pagination.php', true);
       } else {
           locate_template('parts/post-pagination.php', true);

           enc_sidebars::get_instance()->render_sidebar('after-content-widget-area');

           //enc_sidebars::get_instance()->render_sidebar('segundo-widget-area');

       }
       ?>

        <footer>
            <?php echo $enc_social->social_share_buttons($permalink, $thumbnail, $title); ?>
            <?php echo $mod_single->render_structured_data(); ?>
        </footer>

    </article> <!-- /.post -->

    <?php
    if(!wp_is_mobile() && !enc_util::is_genfar_section($mod_single->post)){
        if($mod_single->is_publication()){
            echo $mod_single->related_publications();
        } else {
            echo $mod_single->related_posts();
        }
    }



    ?>

    <?php if ( is_active_sidebar( 'after-related-posts') && !wp_is_mobile() ) : ?>
        <?php dynamic_sidebar( 'after-related-posts' ); ?>
    <?php endif; ?>

    <?php
        if(!wp_is_mobile()){
            //echo do_shortcode('[contact-form-7 id="198517" title="Newsletter"]');
        }
    ?>


    <?php
} else {
    //no posts
    echo enc_util::no_posts();
}
