<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 2019-09-14
 * Time: 16:06
 */
//global $publications;
//locate_template('parts/publications-sidebar.php', true);
//if(is_category($publications)){
$is_publication = enc_base_category::is_publication();
$is_mobile = wp_is_mobile();
$sb1 = null;
$sb2 = null;
//dynamic_sidebar('Newspaper default');
?>
<?php

    if(!$is_mobile){//escritorio
        if((is_single() || is_page()) && enc_util::is_customer_section()){
            $sb1 = enc_sidebars::get_instance()->get_sidebar_code('enc_sponsoredpost_rr_pos1');
        } else if(is_category() && enc_util::is_customer_section()) {
            $sb1 = enc_sidebars::get_instance()->get_sidebar_code('enc_sponsoredcategory_rr_pos1');
        } else if( (is_single() || is_page()) && !enc_util::is_customer_section()){
            $sb1 = enc_sidebars::get_instance()->get_sidebar_code('enc_sidebar1');
        } else if(is_category() && !enc_util::is_customer_section()) {
            $sb1 = enc_sidebars::get_instance()->get_sidebar_code('enc_sidebar1');
        }
        if(!empty($sb1)) {
            echo $sb1;
        }

        enc_sidebars::get_instance()->render_sidebar('segundo-widget-area');
        if((is_single() || is_page()) && enc_util::is_customer_section()){
            $sb2 = enc_sidebars::get_instance()->get_sidebar_code('enc_sponsoredpost_rr_pos2');
        } else if(is_category() && enc_util::is_customer_section()) {
            $sb2 = enc_sidebars::get_instance()->get_sidebar_code('enc_sponsoredcategory_rr_pos2');
        } else if( (is_single() || is_page()) && !enc_util::is_customer_section()){
            $sb2 = enc_sidebars::get_instance()->get_sidebar_code('enc_sidebar2');
        } else if(is_category() && !enc_util::is_customer_section()) {
            $sb2 = enc_sidebars::get_instance()->get_sidebar_code('enc_sidebar2');
        }
        if(!empty($sb2)) {
            echo '<div class="sb-sticky-ad">' . $sb2 . '</div>';
        }
    }

    if( 1 == 0) {
        if($is_publication){
            if(!wp_is_mobile()){
                echo enc_block_publications_sidebar::get_instance()->render();
            }
        } else if(!$is_publication){
            if(!wp_is_mobile()){
                echo enc_block_morethan_sidebar::get_instance()->render();
            }
        }

        if(!wp_is_mobile() && !enc_util::is_exact_uri_section('/medicina-2/vademecum/')){
            //sidebar2
            if(is_single() && enc_util::is_customer_section()){
                enc_sidebars::get_instance()->render_sidebar('enc_sponsoredpost_rr_pos2');
            } else if(is_category() && enc_util::is_customer_section()){
                enc_sidebars::get_instance()->render_sidebar('enc_sponsoredcategory_rr_pos2');
            } else if((is_single() || is_page()) && !enc_util::is_customer_section()){
                enc_sidebars::get_instance()->render_sidebar('enc_post_rr_pos2');
            } else if(is_category() && !enc_util::is_customer_section()){
                enc_sidebars::get_instance()->render_sidebar('enc_category_rr_pos2');
            }

            enc_sidebars::get_instance()->render_sidebar('segundo-widget-area');

            //sidebar3
            if(is_single() && enc_util::is_customer_section()){
                enc_sidebars::get_instance()->render_sidebar('enc_sponsoredpost_rr_pos3');
            } else if (is_category() && enc_util::is_customer_section()){
                enc_sidebars::get_instance()->render_sidebar('enc_sponsoredcategory_rr_pos3');
            } else if((is_single() || is_page()) && !enc_util::is_customer_section()){
                enc_sidebars::get_instance()->render_sidebar('enc_post_rr_pos3');
            } else if (is_category() && !enc_util::is_customer_section()){
                enc_sidebars::get_instance()->render_sidebar('enc_category_rr_pos3');
            }

        }

        enc_sidebars::get_instance()->render_sidebar('tercer-widget-area');
    }

?>

