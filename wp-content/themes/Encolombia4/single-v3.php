<?php
/**
 * The single post loop Default template
 **/

?>
<main class="enc-main-content-wrap enc-container-wrap">

    <?php
    if (have_posts()) {
        the_post();
        global $post;
        $base_author = new enc_base_author($post);
        $enc_social = enc_social::get_instance();

        $mod_single = new enc_module_single($post);
        $title = $post->post_title;
        $permalink = get_the_permalink($post->ID);
        $thumbnail = get_the_post_thumbnail_url($post);
        $cats = $mod_single->get_categories();
        $current_cat = is_array($cats) && count($cats) > 0 ? $cats[0] : null;
        $html_category = $mod_single->get_category();
        $is_pub = $mod_single->is_publication();
        //var_dump($is_pub);exit;
        //$has_img = (!$is_pub && !enc_util::is_uri_section('/medicina/estudios-bioquivalencia/')) || ($is_pub && enc_util::is_url_force_featured_image());
        $has_img = has_post_thumbnail($mod_single->post->ID);

        $is_infinite_scroll = !enc_util::is_exception_infinite_scroll();
        $is_mobile = wp_is_mobile();
        $is_genfar_section = enc_util::is_genfar_section();
    ?>
        <div id="contentHolder">
            <div id="post-<?php echo $mod_single->post->ID;?>" class="<?php echo join(' ', get_post_class());?>" itemscope itemtype="https://schema.org/Article">
                <header>
                    <div class="entry-thumbnail-area <?php echo $has_img ? '' : 'no-img' ?>">
                        <?php

                        if( $has_img ){
                            if($is_mobile){
                                echo $mod_single->get_image('enc_330x205');
                            } else {
                                echo $mod_single->get_image('enc_1920x1024');
                            }
                        }
                        ?>

                    </div>
                    <div class="meta-fixed enc-container">
                        <div class="enc-row">
                            <div class="col-lg-3 col-md-12">&nbsp;</div>
                            <div class="col-lg-7 col-md-12">
                                <div class="post-category">
                                    <input type="hidden" name="current_cat" value="<?php echo !empty($current_cat) ? $current_cat->cat_ID : -1 ?>" />
                                    <?php echo $html_category; ?>
                                </div>
                                <?php echo $mod_single->get_title();?>
                                <?php if ($base_author->is_author): ?>
                                <ul class="post-meta">
                                    <li class="author-info">
                                        <?php echo $base_author->get_author_icon(); ?> <a href="#autor" rel="author" class="fn"><?php echo $base_author->author_name; ?></a>
                                    </li>
                                </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </header>

                <?php get_template_part( 'parts/advert-top', '' ); ?>

                <div class="post-main-content">
                    <div class="enc-container">
                        <div class="enc-row">
                            <div class="col-lg-2 col-md-3 col-sm-12">
                                <?php if($is_infinite_scroll): ?>
                                    <?php if(!$is_mobile): ?>
                                        <div class="post-nav-left">
                                            <div class="list">
                                                <?php echo $mod_single->render_other_posts_in_category(); ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div class="<?php echo $is_infinite_scroll ? 'col-lg-7 col-md-9 col-sm-12' : 'col-lg-7 col-md-12 col-sm-12'; ?>">
                                <div class="post-content">
                                    <div class="<?php echo $is_infinite_scroll ? 'ajax-scroll-post' : 'scroll-post'; ?>">
                                        <div class="post-inner" id="post-inner-<?php echo $mod_single->post->ID;?>">
                                            <?php echo $mod_single->get_content(); ?>
                                            <?php if($is_mobile): ?>
                                            <div class="post-nav-left">
                                                <div class="list">
                                                    <?php echo $mod_single->render_other_posts_in_category(); ?>
                                                </div>
                                                <div class="nav-buttons">
                                                    <button id="previous-btn" disabled>&lt; Anterior</button>
                                                    <button id="next-btn">Siguiente &gt;</button>
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                            <?php if ($base_author->is_author): ?>
                                                <?php echo (new enc_module_post_author($post))->render(); ?>
                                            <?php endif; ?>
                                            <?php echo $mod_single->render_structured_data(); ?>
                                            <?php echo $mod_single->render_more_cat_btn_from_html($html_category); ?>
                                            <div class="post-share" style="height: 121px;">
                                                <h4>Compartir:</h4>
                                                <?php echo do_shortcode('[elfsight_social_share_buttons id="1"]'); ?>
                                                <?php //echo $enc_social->social_share_buttons($permalink, $thumbnail, $title); ?>
                                            </div>
                                            <?php comments_template('', true); ?>

                                            <?php if(is_active_sidebar( 'after-content-widget-area')): ?>
                                                <div class="bottom-post-related1">
                                                    <?php enc_sidebars::get_instance()->render_sidebar('after-content-widget-area'); ?>
                                                </div>
                                            <?php endif; ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="<?php echo $is_infinite_scroll ? 'col-lg-3 col-md-12 col-sm-12' : 'col-lg-3 col-md-12 col-sm-12'; ?>">
                                <?php get_sidebar(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="end-loop-posts"></div>
        <?php if(!$is_mobile && !$is_genfar_section): ?>
        <div class="bottom-post-related2">
            <div class="enc-container">
                <?php
                if(!$is_mobile && !$is_genfar_section){
                    if($is_pub){
                        echo $mod_single->related_publications();
                    } else {
                        echo $mod_single->related_posts();
                    }
                }
                ?>
            </div>
        </div>
        <?php endif; ?>

    <?php
    } else {
        echo '<div class="enc-row"><div class="col-md-12">';
        //no posts
        echo enc_util::no_posts();
        echo '</div></div>';
    }
    ?>
</main>