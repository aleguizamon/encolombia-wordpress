<div class=" enc-main-content-wrap enc-container-wrap">

    <div class="enc-container">
        <div class="enc-row">
            <?php if(!enc_util::is_folletos_genfar_page()): ?>
                <div class="col-md-8 enc-main-content" role="main">
                    <?php
                        locate_template('loop-single.php', true);
                        if(!wp_is_mobile()){
                            comments_template('', true);
                        }
                    ?>
                </div>
                <div class="col-md-4 enc-main-sidebar" role="complementary">
                    <?php get_sidebar(); ?>
                    <?php if ( is_active_sidebar( 'after-related-posts') && wp_is_mobile() ) : ?>
                        <?php dynamic_sidebar( 'after-related-posts' ); ?>
                    <?php endif; ?>
                    <?php
                        if(wp_is_mobile()){
                            //echo do_shortcode('[contact-form-7 id="198517" title="Newsletter"]');
                            comments_template('', true);
                        }
                    ?>
                </div>
            <?php else: ?>
                <div class="col-md-12 enc-main-content" role="main">
                    <?php
                    locate_template('loop-single.php', true);
                    if(!wp_is_mobile()){
                        comments_template('', true);
                    }
                    ?>
                </div>
            <?php endif; ?>
        </div> <!-- /.td-pb-row -->
    </div> <!-- /.td-container -->
</div> <!-- /.td-main-content-wrap -->