<?php if(!enc_util::is_genfar_section() && !is_front_page()): ?>
    <div class="container-adverts-top" style="min-height: <?php echo !wp_is_mobile() ? '277px' : '127px'; ?>;">
        <div class="enc-container " style="padding: 0px;">
            <?php
            $sidebar_name = '';
            if(is_single() && enc_util::is_customer_section()){
                $sidebar_name = 'enc_sponsoredpost_top';
            } else if(is_category() && enc_util::is_customer_section()){
                $sidebar_name = 'enc_sponsoredcategory_top';
            } else if(is_single() || is_page() || is_category()) {
                $sidebar_name = wp_is_mobile() ? 'enc_mob_top' : 'enc_top';
            }
            if(!empty($sidebar_name)){
                enc_sidebars::get_instance()->render_sidebar($sidebar_name);
            }
            ?>
        </div>
    </div>
<?php endif; ?>