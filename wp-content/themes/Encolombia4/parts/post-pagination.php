<?php
/**
 * Created by PhpStorm.
 * User: aleguizamon
 * Date: 27/11/17
 * Time: 12:40 AM
 */

    if(is_single()) {
        global $post, $page, $numpages, $multipage;
        $prev_post = '';
        $next_post = '';
        if($multipage == 1 && $numpages > 1 && ($page < $numpages || $page > 1)){
            $currentLink = get_permalink($post->ID);
            if($page < $numpages){
                //el siguiente es paginacion del post
                $next_post = $currentLink . (substr($currentLink, -1)=='/' ? ($page+1) : '/' . ($page+1));
            }
            if($page > 1){
                //el anterior es paginacion del post
                $prev_post = $currentLink . (substr($currentLink, -1)=='/' ? ($page-1) : '/' . ($page-1));
            }
        }
        if(empty($prev_post) || empty($next_post)) {
            //paginacion entre posts
            $temp_post = $post;
            $childcat = get_the_category($post->ID);
            $childcat = $childcat[0];
            $parentCat = get_category($childcat->parent);
            $args = array(
                'post_type' => 'post',
                'category' => $childcat,
                'paged' => 1,
                'orderby' => 'date',
                'order' => 'DESC',
                'ignore_sticky_posts' => 1,
                'posts_per_page' => -1,
                'taxonomy' => 'category',
                'post_status' => 'publish',
                'category__in' => array($childcat->term_id)
            );
            $query_index = new WP_Query($args);
            $index_current = 0;
            $index_item = 0;
            $array_posts = array();
            while ( $query_index->have_posts() ): $query_index->the_post();
                if($post->ID == $temp_post->ID){
                    $index_current = $index_item;
                }
                $array_posts[] = $post;
                $index_item++;
            endwhile;
            $cat_current_post = get_the_category($temp_post->ID);
            //echo "<pre>"; print_r($query_index->generate_postdata($array_posts[$index_current-1]));
            if(empty($prev_post)) $prev_post = $index_current == 0 ? get_category_link($cat_current_post[0]) : get_the_permalink($array_posts[$index_current-1]);
            if(empty($next_post)) $next_post = $index_current == count($array_posts)-1 ? null : get_the_permalink($array_posts[$index_current+1]);
        }
?>

    <div class="enc-smart-list-pagination pub-pagination">
        <?php if(!empty($prev_post)): ?>
            <a class="enc-smart-list-button enc-smart-back" rel="back" href="<?php echo $prev_post; ?>">Anterior</a>
        <?php else: ?>
            <span class="enc-smart-list-button enc-smart-back enc-smart-disable">Anterior</span>
        <?php endif; ?>
        <?php if(!empty($next_post)): ?>
            <a class="enc-smart-list-button enc-smart-next" rel="next" href="<?php echo $next_post; ?>" >Siguiente</a>
        <?php else: ?>
            <span class="enc-smart-list-button enc-smart-next enc-smart-disable">Siguiente</span>
        <?php endif; ?>
    </div>



<?php
    }
//endif;