<?php
/* Template Name: Confirmar Suscripcion */

get_header();

$error = '';
if(!isset($_GET['t'])){
    $error = 'Disculpe, no se ha podido confirmar la suscripción. Token inválido.';
}
else{
    global $wpdb;
    $token = base64_decode($_GET['t']);

    $result = $wpdb->get_results( 'SELECT * FROM '.$wpdb->prefix.'cf7dbplugin_submits WHERE submit_time = '.$token.' AND field_name = "confirm"', OBJECT );
    if($result && count($result) > 0){

        $result2 = $wpdb->get_results( 'SELECT * FROM '.$wpdb->prefix.'cf7dbplugin_submits WHERE submit_time = '.$token.' AND field_name = "email"', OBJECT );

        if($result[0]->field_value == 1){
            $error = 'El correo electrónico: '.$result2[0]->field_value.', ya se encuentra confirmado';
        }
        else{
            $wpdb->update(
                'wp_cf7dbplugin_submits',
                array( 'field_value' => '1' ),
                array( 'submit_time' => $result[0]->submit_time, 'field_name' => 'confirm' ),
                array( '%s' ),
                array( '%f', '%s' )
            );
        }
    }
    else{
        $error = 'Disculpe, no se ha podido confirmar la suscripción. Token inválido.';
    }
}
?>
<div class="enc-container">
    <section class="confirm-subscription">
        <?php if(empty($error)): ?>
            <h1>La dirección de correo se ha confirmado</h1>
            <div class="cs-message">Su dirección de correo electrónico <a href="mailto:<?php echo $result2[0]->field_value; ?>"><?php echo $result2[0]->field_value; ?></a> ha sido confirmada</div>
            <div class="cs-info">Esperamos seguir en contacto. Gracias por suscribirse.</div>
        <?php else: ?>
            <h1>Ha ocurrido un error</h1>
            <div class="cs-message"><?php echo $error; ?></div>
        <?php endif; ?>

    </section>
</div>

<?php
get_footer();