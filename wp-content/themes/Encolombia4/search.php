<?php
/*  ----------------------------------------------------------------------------
    the search template
 */


get_header();

?>
<div class="enc-main-content-wrap enc-container-wrap">

    <div class="enc-container">

        <div class="enc-row">
            <div class="col-md-8 enc-main-content">
                <div class="enc-page-header">
                    <?php locate_template('parts/page-search-box.php', true); ?>
                </div>
                <?php locate_template('loop.php', true);?>

                <?php enc_util::get_pagination() ?>
            </div>
            <div class="col-md-4 enc-main-sidebar">
                <?php get_sidebar(); ?>
            </div>
        </div> <!-- /.td-pb-row -->
    </div> <!-- /.td-container -->
</div> <!-- /.td-main-content-wrap -->


<?php
get_footer();
?>
